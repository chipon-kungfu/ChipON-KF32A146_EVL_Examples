/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : EPWM.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file gives examples of the use of EPWM.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/*******************************************************************************
**                               Include Files                                **
*******************************************************************************/
#include "system_init.h"
#include "EPWM.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                                 Macro Defines                              **
*******************************************************************************/

/*******************************************************************************
**                              Private variables                             **
*******************************************************************************/

/*******************************************************************************
**                               Public variables                             **
*******************************************************************************/

/*******************************************************************************
**                              Public Functions                              **
*******************************************************************************/

/**
 * @brief: Initialise the EPWM pins.
 *          EPWMA -- PF12
 *          EPWMB -- PF10
 *          EPWM-TZ1 -- PG10
 *
 * @param in: None
 * @param out: None
 * @retval: None
 */
void EPWM_GpioInit()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_Struct_Init(&GPIO_InitStructure);
    GPIO_InitStructure.m_Pin      = EPWM_GPIO_PIN_MASK_A;
    GPIO_InitStructure.m_Speed    = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Mode     = GPIO_MODE_RMP;
    GPIO_InitStructure.m_PullUp   = GPIO_NOPULL;
    GPIO_InitStructure.m_PullDown = GPIO_NOPULL;
    GPIO_Configuration(EPWM_GPIO_PORT_A, &GPIO_InitStructure);

    GPIO_InitStructure.m_Pin = EPWM_GPIO_PIN_MASK_B;
    GPIO_Configuration(EPWM_GPIO_PORT_B, &GPIO_InitStructure);

    GPIO_InitStructure.m_PullDown = GPIO_PULLDOWN;
    GPIO_InitStructure.m_Pin      = EPWM_GPIO_PIN_MASK_TZ1;
    GPIO_Configuration(EPWM_GPIO_PORT_TZ1, &GPIO_InitStructure);

    /* Remap pins */
    GPIO_Pin_RMP_Config(EPWM_GPIO_PORT_A, EPWM_GPIO_PIN_NUM_A, EPWM_GPIO_REMAP_A);
    GPIO_Pin_RMP_Config(EPWM_GPIO_PORT_B, EPWM_GPIO_PIN_NUM_B, EPWM_GPIO_REMAP_B);
    GPIO_Pin_RMP_Config(EPWM_GPIO_PORT_TZ1, EPWM_GPIO_PIN_NUM_TZ1, EPWM_GPIO_REMAP_TZ1);
}

/**
 * @brief: Configure the EPWMx module to output PWM.
 *
 * @param in: EPWMx: The pointer to EPWMx register structure. The value can be
 *            as follow:
 *                  EPWM11_SFR、EPWM12_SFR、EPWM13_SFR、EPWM16_SFR
 * @param in: period: Period of PWM.
 *                  The MAX. value is 0xFFFFFFFF.
 * @param in: dutyCycle_A: The value of duty cycle register.
 *                  The MAX. value is 0xFFFFFFFF.
 * @param in: dutyCycle_B: The value of duty cycle register.
 *                  The MAX. value is 0xFFFFFFFF.
 * @param out: None
 * @retval: None
 */
void EPWM_PwmGeneratorConfig_IndependentOutpt(
  EPWM_SFRmap *EPWMx, uint32_t period, uint32_t dutyCycle_A, uint32_t dutyCycle_B)
{
    /* Reset EPWMx and enable the peripheral clock */
    EPWM_Reset(EPWMx);

    /* =========== Configure the TB(time base) mode =========== */
    /* Set EPWM mode to timer mode */
    EPWM_Work_Mode_Config(EPWMx, EPWM_TIMER_MODE);
    /* Slect clock source */
    EPWM_Work_Clock_Select(EPWMx, EPWM_CLK_HFCLK);
    /* Set prescaler */
    EPWM_TxPRSC_Value_Config(EPWMx, 15U);
    /* Set counter to zero */
    EPWM_TxCNT_Value_Config(EPWMx, 0U);
#ifdef EPWM_EDGE_ALIGNMENT
    /* Edge alignment */
    /* Set EPWM counting mode to count up  */
    EPWM_Counter_Mode_Select(EPWMx, EPWM_COUNT_UP_OF);
#else
    /* Center alignment */
    /* Set EPWM counting mode to count up and down  */
    EPWM_Counter_Mode_Select(EPWMx, EPWM_COUNT_UP_DOWM_OUF);
#endif
    /* Disable phase control */
    EPWM_Phase_Register_Loading_Enable(EPWMx, FALSE);
    /* Update control */
    EPWM_Global_Loading_Enable(EPWMx, FALSE);
    /* Enable update */
    EPWM_Updata_Event_Enable(EPWMx, TRUE);
    /* Periodic update */
    EPWM_Updata_Event_Config(EPWMx, FALSE);

    /* ======== Configure the AQ(action qualifier) mode ======== */
    /* Set period */
    EPWM_TxPPX_Value_Config(EPWMx, period);
    /* configure the duty cycle */
    EPWM_Duty_Cycle_RegisterABCD_Config(EPWMx, EPWM_REGISTER_A, dutyCycle_A);
    EPWM_Duty_Cycle_RegisterABCD_Config(EPWMx, EPWM_REGISTER_B, dutyCycle_B);
    /* Disable continuous forced output of EPWMx */
    EPWM_Continuous_Mandatory_Output_Config(EPWMx, EPWM_REGISTER_A, EPWM_OUT_PROHIBIT);
    EPWM_Continuous_Mandatory_Output_Config(EPWMx, EPWM_REGISTER_B, EPWM_OUT_PROHIBIT);

/* Output control */
#ifdef EPWM_EDGE_ALIGNMENT
    /* EPWMxA output control */
    /* Set EPWM effective level to low level */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CE0_EPWMAB_OUT, EPWM_OUT_LOW);
    /* Set EPWM effective level to high level when TxCNT is equal to EPWMx_RA */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERA_DU_EPWMAB_OUT, EPWM_OUT_HIGH);
    /* Ignore EPWMxA output level when TxCNT is equal to EPWMxRB */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERB_DU_EPWMAB_OUT, EPWM_OUT_NONE);
    /* Ignore EPWMxA output level when TxCNT is equal to PPX */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CEPPX_EPWMAB_OUT, EPWM_OUT_NONE);

    /* EPWMxB output control */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_B, EPWM_CE0_EPWMAB_OUT, EPWM_OUT_HIGH);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_B, EPWM_CERB_DU_EPWMAB_OUT, EPWM_OUT_LOW);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_B, EPWM_CERA_DU_EPWMAB_OUT, EPWM_OUT_NONE);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_B, EPWM_CEPPX_EPWMAB_OUT, EPWM_OUT_NONE);
#else
    /* Center alignment */
    /* Set EPWM effective level to high level when TxCNT is equal to EPWMx_RA
     * and count up */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERA_DU_EPWMAB_OUT, EPWM_OUT_HIGH);
    /* Set EPWM effective level to low level when TxCNT is equal to EPWMx_RA
     * and count down */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERA_DD_EPWMAB_OUT, EPWM_OUT_LOW);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERB_DU_EPWMAB_OUT, EPWM_OUT_NONE);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERB_DD_EPWMAB_OUT, EPWM_OUT_NONE);

    /* EPWMxB output control */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_B, EPWM_CERB_DU_EPWMAB_OUT, EPWM_OUT_LOW);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_B, EPWM_CERB_DD_EPWMAB_OUT, EPWM_OUT_HIGH);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_B, EPWM_CERA_DU_EPWMAB_OUT, EPWM_OUT_NONE);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_B, EPWM_CERA_DD_EPWMAB_OUT, EPWM_OUT_NONE);
#endif

    /* =========== Configure the DB(dead band) mode =========== */
    /* Set EPWMxA and EPWMxB output bypass */
    EPWM_Dead_Zone_Output_Select(EPWMx, EPWM_UP_BYPASS_DOWN_BYPASS);

    /* ========= Configure the QC(pulse chopper) mode ========= */
    /* Non-enable pulse chopper */
    EPWM_Chopping_Pulse_Mode_Enable(EPWMx, FALSE);

    /* =========== Configure the TZ(trip zone) mode =========== */
    /* Set EPWMxA output to low level when auto shutdown */
    EPWM_Pin_EPWMA_ShouDown_Status_Config(EPWMx, EPWM_DRIVER_PIN_EPWMAB_LOW);
    /* Set EPWMxB output to high level when auto shutdown */
    EPWM_Pin_EPWMB_ShouDown_Status_Config(EPWMx, EPWM_DRIVER_PIN_EPWMAB_HIGH);
    /* Select auto shutdown trigger source as high level on the TZ1 pin */
    EPWM_Auto_ShowDown_Source_Select(EPWMx, EPWM_AUTO_SHOWDOWN_SOURCE_TZ1_HIGH);
    /* Automatic recovery when shut down trigger source disappear */
    EPWM_Auto_Reset_Enable(EPWMx, EPWM_PXASE_AUTO_CLEAR_RESET);

    /* ====================== Enable EPWMx ===================== */
    EPWM_Enable(EPWMx, TRUE);
}

/**
 * @brief: Configure the EPWMx module to output PWM.
 *
 * @param in: EPWMx: The pointer to EPWMx register structure. The value can be
 *            as follow:
 *                  EPWM11_SFR、EPWM12_SFR、EPWM13_SFR、EPWM16_SFR
 * @param in: period: Period of PWM.
 *                  The MAX. value is 0xFFFFFFFF.
 * @param in: dutyCycle: The value of duty cycle register.
 *                  The MAX. value is 0xFFFFFFFF.
 * @param in: deadTime: Dead band delay time.
 * @param out: None
 * @retval: None
 */
void EPWM_PwmGeneratorConfig_ComplementaryOutpt(
  EPWM_SFRmap *EPWMx, uint32_t period, uint32_t dutyCycle, uint32_t deadTime)
{
    /* Reset EPWMx and enable the peripheral clock */
    EPWM_Reset(EPWMx);

    /* =========== Configure the TB(time base) mode =========== */
    /* Set EPWM mode to timer mode */
    EPWM_Work_Mode_Config(EPWMx, EPWM_TIMER_MODE);
    /* Slect clock source */
    EPWM_Work_Clock_Select(EPWMx, EPWM_CLK_HFCLK);
    /* Set prescaler */
    EPWM_TxPRSC_Value_Config(EPWMx, 15U);
    /* Set counter to zero */
    EPWM_TxCNT_Value_Config(EPWMx, 0x0U);
#ifdef EPWM_EDGE_ALIGNMENT
    /* Edge alignment */
    /* Set EPWM counting mode to count up  */
    EPWM_Counter_Mode_Select(EPWMx, EPWM_COUNT_UP_OF);
#else
    /* Center alignment */
    /* Set EPWM counting mode to count up and down  */
    EPWM_Counter_Mode_Select(EPWMx, EPWM_COUNT_UP_DOWM_OUF);
#endif
    /* Non-enabling phase control */
    EPWM_Phase_Register_Loading_Enable(EPWMx, FALSE);
    /* Update control */
    EPWM_Global_Loading_Enable(EPWMx, FALSE);
    /* Enable update */
    EPWM_Updata_Event_Enable(EPWMx, TRUE);
    /* Periodic update */
    EPWM_Updata_Event_Config(EPWMx, FALSE);

    /* ======== Configure the AQ(action qualifier) mode ======== */
    /* Set period */
    EPWM_TxPPX_Value_Config(EPWMx, period);
    /* configure the duty cycle */
    EPWM_Duty_Cycle_RegisterABCD_Config(EPWMx, EPWM_REGISTER_A, dutyCycle);

    /* Disable continuous forced output of EPWMx */
    EPWM_Continuous_Mandatory_Output_Config(EPWMx, EPWM_REGISTER_A, EPWM_OUT_PROHIBIT);

    /* Output control */
#ifdef EPWM_EDGE_ALIGNMENT
    /* EPWMxA output control */
    /* Set EPWM effective level to low level */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CE0_EPWMAB_OUT, EPWM_OUT_LOW);
    /* Set EPWM effective level to high level when TxCNT is equal to EPWMx_RA */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERA_DU_EPWMAB_OUT, EPWM_OUT_HIGH);
    /* Ignore EPWMxA output level when TxCNT is equal to EPWMxRB */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERB_DU_EPWMAB_OUT, EPWM_OUT_NONE);
    /* Ignore EPWMxA output level when TxCNT is equal to PPX */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CEPPX_EPWMAB_OUT, EPWM_OUT_NONE);
#else
    /* Center alignment */
    /* Set EPWM effective level to high level when TxCNT is equal to EPWMx_RA
     * and count up */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERA_DU_EPWMAB_OUT, EPWM_OUT_HIGH);
    /* Set EPWM effective level to low level when TxCNT is equal to EPWMx_RA
     * and count down */
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERA_DD_EPWMAB_OUT, EPWM_OUT_LOW);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERB_DU_EPWMAB_OUT, EPWM_OUT_NONE);
    EPWM_EPWMxAB_OUTPUT_Select(EPWMx, EPWM_REGISTER_A, EPWM_CERB_DD_EPWMAB_OUT, EPWM_OUT_NONE);
#endif

    /* =========== Configure the DB(dead band) mode =========== */
    /* Configure the input source of DB mode */
    EPWM_Dead_Zone_Input_Select(EPWMx, EPWM_EPWMA_UP_EPWMA_DOWM);
    /* Configure the output polarity */
    EPWM_Dead_Zone_Output_Polarity_Select(EPWMx, EPWM_EPWMA_NTOGGLE_EPWMB_TOGGLE);
    /* Set the output source to the deadband signal */
    EPWM_Dead_Zone_Output_Select(EPWMx, EPWM_UP_ENABLE_DOWN_ENABLE);
    /* Set deadband delay time */
    EPWM_Dead_Zone_Time_Config(EPWMx, EPWM_UP_EDGE_DEAD_ZONE, deadTime);
    EPWM_Dead_Zone_Time_Config(EPWMx, EPWM_DOWM_EDGE_DEAD_ZONE, deadTime);

    /* ========= Configure the QC(pulse chopper) mode ========= */
    /* Non-enable pulse chopper */
    EPWM_Chopping_Pulse_Mode_Enable(EPWMx, FALSE);

    /* ======== Configure the TZ(trip zone) mode ======== */
    /* Set EPWMxA output to low level when auto shutdown */
    EPWM_Pin_EPWMA_ShouDown_Status_Config(EPWMx, EPWM_DRIVER_PIN_EPWMAB_LOW);
    /* Set EPWMxB output to high level when auto shutdown */
    EPWM_Pin_EPWMB_ShouDown_Status_Config(EPWMx, EPWM_DRIVER_PIN_EPWMAB_HIGH);
    /* Select auto shutdown trigger source as high level on the TZ1 pin */
    EPWM_Auto_ShowDown_Source_Select(EPWMx, EPWM_AUTO_SHOWDOWN_SOURCE_TZ1_HIGH);
    /* Automatic recovery when shut down trigger source disappear */
    EPWM_Auto_Reset_Enable(EPWMx, EPWM_PXASE_AUTO_CLEAR_RESET);

    /* ==================== Enable EPWMx =================== */
    EPWM_Enable(EPWMx, TRUE);
}

/**
 * @brief: Update dead band delay time.
 *
 * @param in: EPWMx: The pointer to EPWMx register structure. The value can be
 *            as follow:
 *                  EPWM11_SFR、EPWM12_SFR、EPWM13_SFR、EPWM16_SFR
 * @param in: deadTime: Dead band delay time.
 * @param in: updateMode: Time of register update. The value can be
 *            as follow:
 *                  EPWM_UPDATE_NOW -- update immediately
 *                  EPWM_UPDATE_PERIOD -- periodic update
 * @param out: None
 * @retval: None
 */
void EPWM_UpdateDeadTime(EPWM_SFRmap *EPWMx, uint32_t deadTime, uint32_t updateMode)
{
    /* Update control */
    EPWM_Global_Loading_Enable(EPWMx, FALSE);
    /* Non-enable update */
    EPWM_Updata_Event_Enable(EPWMx, FALSE);
    /* Update deadband delay time */
    EPWM_Dead_Zone_Time_Config(EPWMx, EPWM_UP_EDGE_DEAD_ZONE, deadTime);
    EPWM_Dead_Zone_Time_Config(EPWMx, EPWM_DOWM_EDGE_DEAD_ZONE, deadTime);
    if (EPWM_UPDATE_NOW == updateMode)
    {
        /* Update immediately */
        EPWM_Updata_Event_Config(EPWMx, TRUE);
    }
    else
    {
        /* Periodic update */
        EPWM_Updata_Event_Config(EPWMx, FALSE);
    }
    /* Enable update */
    EPWM_Updata_Event_Enable(EPWMx, TRUE);
}

/**
 * @brief: Configure synchronization signal and phase shift size
 *
 * @param in: EPWMx: The pointer to EPWMx register structure. The value can be
 *            as follow:
 *                  EPWM11_SFR、EPWM12_SFR、EPWM13_SFR、EPWM16_SFR
 * @param in: phaseShiftValue: Phase shift size of EPWMx.
 * @param out: None
 * @retval: None
 */
void EPWM_SyncAndPhaseShift_Config(EPWM_SFRmap *EPWMx, uint32_t phaseShiftValue)
{
    /* =========== Configure the TB(time base) mode =========== */
    /* Enable phase shift */
    EPWM_Phase_Register_Loading_Enable(EPWMx, TRUE);
    EPWM_Phase_Direction_Config(EPWMx, EPWM_SYNC_TRIGGER_UP);
    EPWM_TxPHS_Value_Config(EPWMx, phaseShiftValue);
    if (EPWM11_SFR == EPWMx)
    {
        EPWM_SYNC_Event_Out_Select(EPWMx, EPWM_SYNC_EVENT_OUT_EPWM_SWF);
    }
    else
    {
        EPWM_SYNC_Event_Out_Select(EPWMx, EPWM_SYNC_EVENT_OUT_DISABLE);
    }
}
