/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for EPWM update dead zone
 *                      delay time.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files
******************************************************************************/
#include "Board_GpioCfg.h"
#include "EPWM.h"

/*******************************************************************************
**                      	Private Macro Definitions
*******************************************************************************/

/*******************************************************************************
**                     			Global Functions
*******************************************************************************/
/**
 *  @brief :Initialize the different GPIO ports
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void BoardGpioInit(void)
{
    /* Configure LED1/2 output */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);

    /* Configure user key input */
    GPIOInit_Input_Config(KEY2_PORT, KEY2_PIN);
    GPIO_Pull_Down_Enable(KEY2_PORT, KEY2_PIN, TRUE);
}

/*******************************************************************************
**                     			main Functions
*******************************************************************************/

/**
 * @brief:This example shows how to configure PWM independent output and
 * complementary output of ECCP (dead-time delay)
 * @param in:None
 * @param out:None
 * @retval:None
 */
int main()
{
    volatile uint32_t TimingDelay = 0;
    volatile uint32_t deadTime    = 100U;

    /* System clock configuration */
    SystemInit(72);
    systick_delay_init(72);
    BoardGpioInit();

    /* Initialize EPWMx */
    EPWM_GpioInit();
#ifdef EPWM_EDGE_ALIGNMENT

    EPWM_PwmGeneratorConfig_ComplementaryOutpt(EPWM13_SFR, 9999U, 2499U, 100U);
#else
    /* Center alignment */
    EPWM_PwmGeneratorConfig_ComplementaryOutpt(EPWM13_SFR, 5000U, 1250U, 100U);
#endif

    while (1)
    {
        /* Blinking LED1 every 1s */
        if (!TimingDelay)
        {
            GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
        }
        TimingDelay = (++TimingDelay) % 10;

        /* Update the dead time when key is pressed */
        if (Bit_RESET == GPIO_Read_Input_Data_Bit(KEY2_PORT, KEY2_PIN))
        {
            systick_delay_ms(100U);
            if (Bit_RESET == GPIO_Read_Input_Data_Bit(KEY2_PORT, KEY2_PIN))
            {
                /* Increasing the dead time by 50 each untile 300 */
                deadTime += 50;
                if (deadTime > 300U)
                {
                    deadTime = 100;
                }
                EPWM_UpdateDeadTime(EPWM13_SFR, deadTime, EPWM_UPDATE_PERIOD);
            }
        }
        else
        {
            systick_delay_ms(50U);
        }
    }
}

/**
 *  @brief:Reports the name of the source file and the source line number
 *  where the assert_param error has occurred.
 *  @param in:*File-file pointer to the source file name
 * 	Line-line assert_param error line source number
 *  @param out:None
 *  @retval:None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
