/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for EPWM complementary output 
 *                      mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files
******************************************************************************/
#include "Board_GpioCfg.h"
#include "EPWM.h"

/*******************************************************************************
**                      	    Variables
*******************************************************************************/
volatile uint32_t deadTime = 0x10;

/*******************************************************************************
**                     			Global Functions
*******************************************************************************/
/**
 *  @brief :Initialize the different GPIO ports
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void BoardGpioInit(void)
{
    /* Configure LED1 output */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);
}

/**
 *  @brief: Delay time
 *  @param in: nms
 *  @param out : None
 *  @retval : None
 */
void delay_ms(volatile uint32_t nms)
{
    volatile uint32_t i, j;
    for (i = 0; i < nms; i++)
    {
        j = 5000;
        while (j--)
            ;
    }
}

/*******************************************************************************
**                     			Main Functions
*******************************************************************************/

int main(void)
{
    volatile uint32_t TimingDelay = 0;

    /* System clock configuration */
    SystemInit(72);
    systick_delay_init(72);
    BoardGpioInit();

    /* Initialize EPWMx */
    EPWM_GpioInit();

#ifdef EPWM_EDGE_ALIGNMENT
    EPWM_PwmGeneratorConfig_ComplementaryOutpt(EPWM13_SFR, 9999U, 2500U, 99U);
#else
    /* Center alignment */
    EPWM_PwmGeneratorConfig_ComplementaryOutpt(EPWM13_SFR, 10000U, 1250U, 99U);
#endif

    while (1)
    {
        /* Blinking LED1 every 1s */
        if (!TimingDelay)
        {
            GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
        }
        TimingDelay = (++TimingDelay) % 10U;
        systick_delay_ms(50U);
    }
}

/**
 *  @brief:Reports the name of the source file and the source line number
 *  where the assert_param error has occurred.
 *  @param in:*File-file pointer to the source file name
 * 	Line-line assert_param error line source number
 *  @param out:None
 *  @retval:None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
