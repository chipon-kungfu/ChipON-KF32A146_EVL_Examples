/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for EPWM sync phase shift.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files
******************************************************************************/
#include "Board_GpioCfg.h"
#include "EPWM.h"

/*******************************************************************************
**                      	Private Macro Definitions
*******************************************************************************/

/*******************************************************************************
**                     			Global Functions
*******************************************************************************/
/**
 *  @brief :Initialize the different GPIO ports
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void BoardGpioInit(void)
{
    /* Configure LED1 output */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);

    /* Configure user key input */
    GPIOInit_Input_Config(KEY2_PORT, KEY2_PIN);
    GPIO_Pull_Down_Enable(KEY2_PORT, KEY2_PIN, TRUE);
}

/**
 *  @brief: Delay time
 *  @param in: nms
 *  @param out : None
 *  @retval : None
 */
void delay_ms(volatile uint32_t nms)
{
    volatile uint32_t i, j;
    for (i = 0; i < nms; i++)
    {
        j = 5000;
        while (j--)
            ;
    }
}

/**
 * @brief: Initialise the EPWM pins.
 *
 * @param in: None
 * @param out: None
 * @retval: None
 */
void EPWM12_GpioInit()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_Struct_Init(&GPIO_InitStructure);
    GPIO_InitStructure.m_Pin      = EPWM12_GPIO_PIN_MASK_A;
    GPIO_InitStructure.m_Speed    = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Mode     = GPIO_MODE_RMP;
    GPIO_InitStructure.m_PullUp   = GPIO_NOPULL;
    GPIO_InitStructure.m_PullDown = GPIO_NOPULL;
    GPIO_Configuration(EPWM12_GPIO_PORT_A, &GPIO_InitStructure);

    GPIO_InitStructure.m_Pin = EPWM12_GPIO_PIN_MASK_B;
    GPIO_Configuration(EPWM12_GPIO_PORT_B, &GPIO_InitStructure);

    /* Remap pins */
    GPIO_Pin_RMP_Config(EPWM12_GPIO_PORT_A, EPWM12_GPIO_PIN_NUM_A, EPWM12_GPIO_REMAP_A);
    GPIO_Pin_RMP_Config(EPWM12_GPIO_PORT_B, EPWM12_GPIO_PIN_NUM_B, EPWM12_GPIO_REMAP_B);
}

/*******************************************************************************
**                     			main Functions
*******************************************************************************/

/**
 * @brief:This example shows how to configure PWM independent output and
 * complementary output of ECCP (dead-time delay)
 * @param in:None
 * @param out:None
 * @retval:None
 */
int main()
{
    volatile uint32_t TimingDelay     = 0;
    uint8_t           PhaseShfitFlage = 0u;

    /* System clock configuration */
    SystemInit(72);
    systick_delay_init(72);
    BoardGpioInit();

    /* Initialize EPWMx */
    EPWM_GpioInit();
    EPWM12_GpioInit();

#ifdef EPWM_EDGE_ALIGNMENT

    EPWM_PwmGeneratorConfig_IndependentOutpt(EPWM11_SFR, 9999U, 2499U, 4999U);
    EPWM_Enable(EPWM11_SFR, FALSE);
    /* Configure phase shift size */
    EPWM_SyncAndPhaseShift_Config(EPWM11_SFR, 0);
    EPWM_PwmGeneratorConfig_IndependentOutpt(EPWM12_SFR, 9999U, 2499, 4999U);
    EPWM_Enable(EPWM12_SFR, FALSE);
    /* Configure phase shift size */
    EPWM_SyncAndPhaseShift_Config(EPWM12_SFR, 1000U);
    /* Enable EPWMx */
    EPWM_Enable(EPWM11_SFR, TRUE);
    EPWM_Enable(EPWM12_SFR, TRUE);
#else
    /* Center alignment */
    EPWM_PwmGeneratorConfig_IndependentOutpt(EPWM11_SFR, 5000U, 1250U, 2500U);
    EPWM_Enable(EPWM11_SFR, FALSE);
    /* Configure phase shift size */
    EPWM_SyncAndPhaseShift_Config(EPWM11_SFR, 0U);
    EPWM_PwmGeneratorConfig_IndependentOutpt(EPWM12_SFR, 5000U, 1250U, 2500U);
    EPWM_Enable(EPWM12_SFR, FALSE);
    /* Enable EPWMx */
    EPWM_Enable(EPWM11_SFR, TRUE);
    EPWM_Enable(EPWM12_SFR, TRUE);
#endif

    while (1)
    {
        /* Blinking LED1 every 1s */
        if (!TimingDelay)
        {
            GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
        }
        TimingDelay = (++TimingDelay) % 10U;

        /* Generate a software synchronization event when key is pressed */
        if (Bit_RESET == GPIO_Read_Input_Data_Bit(KEY2_PORT, KEY2_PIN))
        {
            systick_delay_ms(80U);
            if (Bit_RESET == GPIO_Read_Input_Data_Bit(KEY2_PORT, KEY2_PIN))
            {
                if (0U == PhaseShfitFlage)
                {
                    PhaseShfitFlage = 1U;
                    EPWM_SyncAndPhaseShift_Config(EPWM12_SFR, 1000U);
                    EPWM_Software_SYNC_Event(EPWM11_SFR);
                }
                else
                {
                    PhaseShfitFlage = 0U;
                    EPWM_SyncAndPhaseShift_Config(EPWM12_SFR, 0U);
                    EPWM_Software_SYNC_Event(EPWM11_SFR);
                }
            }
        }
        else
        {
            systick_delay_ms(50U);
        }
    }
}

/**
 *  @brief:Reports the name of the source file and the source line number
 *  where the assert_param error has occurred.
 *  @param in:*File-file pointer to the source file name
 * 	Line-line assert_param error line source number
 *  @param out:None
 *  @retval:None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
