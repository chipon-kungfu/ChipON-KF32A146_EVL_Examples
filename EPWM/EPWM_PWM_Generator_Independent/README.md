#### 文件所有
本文适用于KF32A146的 EPWM输出PWM的示例例程，例程文件名：EPWM_PWM_Generator_Independent
* * *


#### 版权说明
目前的固件只是为了给使用者提供指导，目的是向客户提供有关产品的代码信息，
与使用者的产品信息和代码无关。因此，对于因此类固件内容和（或）客户使用此
处包含的与其产品相关的编码信息而引起的任何索赔，上海芯旺微电子技术有限公
司不承担任何直接、间接或后果性损害赔偿责任
* * *

#### 使用说明
* 本例展示了如何配置EPWM模块输出PWM
* 例程输出周期为0.1s，A路占空比为25%的，B路占空比为50%的两路PWM
* PWM波采用边沿对齐方式，两路PWM波采用**独立输出**
* PWM设置了自动关断模式，当TZ1上出现高电平时，EPWM自动关断，在A路上输出低电平，在B路上输出高电平，当自动关断源消失后EPWM自动恢复
* * *