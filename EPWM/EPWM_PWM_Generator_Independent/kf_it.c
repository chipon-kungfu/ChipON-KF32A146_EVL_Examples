/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions handler 
 *                      and peripherals interrupt service routine
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                   KF32A156 Processor Exceptions Handlers
*******************************************************************************/

/**
 * @brief:NMI Interrupt Course
 * @param in:None
 * @param out:None
 * @retval:None
 */
void __attribute__((interrupt)) _NMI_exception(void) {}

/**
 * @brief:HardFault Interrupt Course
 * @param in:None
 * @param out:None
 * @retval:None
 */
void __attribute__((interrupt)) _HardFault_exception(void) {}

/**
 * @brief:StackFault Interrupt Course
 * @param in:None
 * @param out:None
 * @retval:None
 */
void __attribute__((interrupt)) _StackFault_exception(void) {}

/**
 * @brief:SVC Interrupt Course
 * @param in:None
 * @param out:None
 * @retval:None
 */
void __attribute__((interrupt)) _SVC_exception(void) {}

/**
 * @brief:SoftSV Interrupt Course
 * @param in:None
 * @param out:None
 * @retval:None
 */
void __attribute__((interrupt)) _SoftSV_exception(void) {}

/**
 * @brief:SysTick Interrupt Course
 * @param in:None
 * @param out:None
 * @retval:None
 */
void __attribute__((interrupt)) _SysTick_exception(void) {}

/**
 * @brief:T11 Interrupt Course
 * @param in:None
 * @param out:None
 * @retval:None
 */
void __attribute__((interrupt)) _T11_exception(void)
{
    INT_Clear_Interrupt_Flag(INT_T11);
    if (EPWM_Get_INT_Flag(EPWM11_SFR, EPWM_COUNT_OVERFLOW))
    {
        EPWM_Clear_INT_Flag(EPWM11_SFR, EPWM_COUNT_OVERFLOW);
        // GPIO_Toggle_Output_Data_Config(LED2_PORT, LED2_PIN);
    }

    if (EPWM_Get_INT_Flag(EPWM11_SFR, EPWM_COUNT_EQUAL_EPWMRA))
    {
        EPWM_Clear_INT_Flag(EPWM11_SFR, EPWM_COUNT_EQUAL_EPWMRA);
        // GPIO_Toggle_Output_Data_Config(LED3_PORT, LED3_PIN);
    }

    if (EPWM_Get_INT_Flag(EPWM11_SFR, EPWM_COUNT_EQUAL_EPWMRB))
    {
        EPWM_Clear_INT_Flag(EPWM11_SFR, EPWM_COUNT_EQUAL_EPWMRB);
        // GPIO_Toggle_Output_Data_Config(LED2_PORT, LED2_PIN);
    }
}
