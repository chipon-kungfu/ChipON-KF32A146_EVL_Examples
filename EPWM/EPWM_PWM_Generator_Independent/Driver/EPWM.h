/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : EPWM.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file gives examples of the use of EPWM.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef __EPWM_H_
#define __EPWM_H_

/*******************************************************************************
**                               Include Files                                **
*******************************************************************************/

/*******************************************************************************
**                                 Macro Defines                              **
*******************************************************************************/

/* Waveform alignment */
#define EPWM_CENTER_ALIGNMENT (1U)
// #define EPWM_EDGE_ALIGNMENT (0)

/* Update Mode */
#define EPWM_UPDATE_NOW    (1U)
#define EPWM_UPDATE_PERIOD (0)

/*******************************************************************************
**                              Private variables                             **
*******************************************************************************/

/*******************************************************************************
**                               Public variables                             **
*******************************************************************************/

/*******************************************************************************
**                              Public Functions                              **
*******************************************************************************/
extern void EPWM_GpioInit();

extern void EPWM_PwmGeneratorConfig_IndependentOutpt(
  EPWM_SFRmap *EPWMx, uint32_t period, uint32_t dutyCycle_A, uint32_t dutyCycle_B);

extern void
EPWM_PwmGeneratorConfig_ComplementaryOutpt(EPWM_SFRmap *EPWMx, uint32_t period, uint32_t dutyCycle, uint32_t deadTime);

extern void EPWM_UpdateDeadTime(EPWM_SFRmap *EPWMx, uint32_t deadTime, uint32_t updateMode);

extern void EPWM_SyncAndPhaseShift_Config(EPWM_SFRmap *EPWMx, uint32_t phaseShiftValue);

#endif
