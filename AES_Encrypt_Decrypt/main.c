/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file is a standard template project.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "kflog.h"
#include "User_Aes.h"
#include <string.h>

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    uint32_t AesKey[8] = {0x2b7e1516, 0x28aed2a6, 0xabf71588, 0x09cf4f3c, 0x2b7e1516, 0x28aed2a6, 0xabf71588, 0x09cf4f3c};
    uint32_t AesPlain[4] = {0x6bc1bee2, 0x2e409f96, 0xe93d7e11, 0x7393172a};
    uint32_t AesCipher[4] = {0x3ad77bb4, 0x0d7a3660, 0xa89ecaf3, 0x2466ef97};
    uint32_t Output[4] = {0x0};

    /* Initialize the system clock is 120M */
    SystemInit(120);
    kfLog_Init();
    kf_printf("POWER ON.\r\n");

    Aes_Config_t AesInit = {
        .ClkSrc = AES_TRNG_SCK_HFCLK,
        .ClkDiv = AES_SCK_DIV0,
        .Algorithm = AES_STANDARD_ALGORITHM,
        .AesMode = AES_WORK_ECB_MODE,
        .EnOrDecrytion = AES_ENCRYPT,
        .KeyLen = AES_KEY_128BITS,
        .ExtensionNum = 0U,
        .SBoxInv = AES_SBOX_NOR_REPLACE,
        .Key = AesKey,
    };

    kf_printf("/* ======== Encrypt ======== */\r\n");
    AES_Configuration(&AesInit);
    AES_LoadData(AesPlain);
    AES_DataProcess(Output);
    for(uint8_t i = 0U; i < 4U; i++)
    {
    	kf_printf("Cipher[%d]: 0x%08x\r\n", i, Output[i]);
       if(Output[i] != AesCipher[i])
       {
           kf_printf("Encrypt Fail!!!\r\n");
           break;
       }
       else
       {/*Empty*/}
    }
    

    kf_printf("/* ======== Decrypt ======== */\r\n");
    AesInit.EnOrDecrytion = AES_DECRYPT;
    AES_Configuration(&AesInit);
    AES_LoadData(AesCipher);
    memset(Output, 0, 16U);
    AES_DataProcess(Output);
    for(uint8_t i = 0U; i < 4U; i++)
    {
    	kf_printf("Plain[%d]: 0x%08x\r\n", i, Output[i]);
       if(Output[i] != AesPlain[i])
       {
           kf_printf("Decrypt Fail!!!\r\n");
           break;
       }
       else
       {/*Empty*/}
    }


    while (1)
    {}
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
