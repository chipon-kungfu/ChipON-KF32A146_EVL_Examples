/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_Aes.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                      for AES
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef __USER_AES_H_
#define __USER_AES_H_
/*******************************************************************************
**                            Include Files
*******************************************************************************/

/*******************************************************************************
**                            Macro/Typedef Defines                        
*******************************************************************************/
/*Length of AES KEY*/
#define AES_KEY_128BITS (0x3U)
#define AES_KEY_256BITS (0x7U)

typedef struct
{
    /*AES clock source, 具体见宏定义 AES工作时钟源选择*/
    uint32_t ClkSrc;
    /*AES clock scaler, 具体见宏定义 AES工作时钟分频选择*/
    uint32_t ClkDiv;
    /*Algorithm, 具体见宏定义 算法自定义使能*/
    uint32_t Algorithm;
    /*AES work mode, 具体见宏定义 AES CBC模式*/
    uint32_t AesMode;
    /*Encrytion or Decrytion, 具体见宏定义 AES加密功能选择*/
    uint32_t EnOrDecrytion;
    /*Length of KEY, 具体见宏定义 密钥长度*/
    uint32_t KeyLen;
    /*Key extension round number, 取值小于0x1F*/
    uint32_t ExtensionNum;
    /*S box inv, 具体可见宏定义 S 盒子逆替换使能 */
    uint32_t SBoxInv;
    /*Pointer to KEY buffer*/
    uint32_t *Key;
} Aes_Config_t;

/*Choose end of data*/
#define SWAP_END_OF_DATA (0x1U)



/*******************************************************************************
**                            Public Functions
*******************************************************************************/

void AES_Configuration(Aes_Config_t *AesConfig);
void AES_LoadData(uint32_t *AesData);
uint32_t *AES_DataProcess(uint32_t *OutPutBuf);

#endif /* __USER_AES_H_ */
