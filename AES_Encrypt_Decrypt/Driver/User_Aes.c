/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_Aes.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                      for AES
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/*******************************************************************************
**                            Include Files
*******************************************************************************/
#include "system_init.h"
#include "User_Aes.h"
#include <stdio.h>

/*******************************************************************************
**                            Macro Defines                        
*******************************************************************************/
/*KEY register*/
#define AES_KEY_REG_ADDR (0X1FFEFC00U)
#define AES_KEY_PTR      ((volatile uint32_t *)AES_KEY_REG_ADDR)
#define AES_KEY_0        (*(volatile uint32_t *)(AES_KEY_REG_ADDR))
#define AES_KEY_1        (*(volatile uint32_t *)(AES_KEY_REG_ADDR + 0x4U))
#define AES_KEY_2        (*(volatile uint32_t *)(AES_KEY_REG_ADDR + 0x8U))
#define AES_KEY_3        (*(volatile uint32_t *)(AES_KEY_REG_ADDR + 0xCU))

/*******************************************************************************
**                            Public variables
*******************************************************************************/

/*******************************************************************************
**                            Public Functions
*******************************************************************************/

/**
 * @brief Confige AES
 * 
 * @param AesConfig Pointer to AES configuration structure.
 * @retval void
 */
void AES_Configuration(Aes_Config_t *AesConfig)
{
    if(NULL == AesConfig)
    {
        return;
    }
    else{
        AES_Reset();
        /* Disable AES high speed mode */
        AES_High_Speed_Enable(DISABLE);
        /* Set AES work clock source and scaler*/
        AES_TRNG_SCK_Select(AesConfig->ClkSrc);
        AES_SCK_DIV_Select(AesConfig->ClkDiv);
        /*Slect AES standard algorithm or customize algorithm*/
        AES_CUSTOMIZE_ALGORITHM_Enable(AesConfig->Algorithm);
        /*Set AES work mode*/
        AES_CBC_Mode_Select(AesConfig->AesMode);
        /*Slect AES function, Encryt or Decryt*/
        AES_ENCRYPT_Function_Enable(AesConfig->EnOrDecrytion);
        /*Set length of KEY*/
        AES_KEY_LEN_Config(AesConfig->KeyLen);
        /*Key Extension round number*/
        AES_RND_NUM_Config(AesConfig->ExtensionNum);
        /* Set S Box inv */
        AES_SBOX_INV_Enable(AesConfig->SBoxInv);

        /* Clear AES interrupt flag */
	    AES_Clear_INT_Flag();
        AES_Enable(TRUE);

        /*Write KEY into RAM*/
        for(uint8_t i = 0U, j = 0U; i <= AesConfig->KeyLen; i++)
        {
#if (0U != SWAP_END_OF_DATA)
            /*Reverse word order every 128bits*/
            j = (AesConfig->KeyLen >> 2U) - (i >> 2U);
            AES_KEY_PTR[i] = AesConfig->Key[AesConfig->KeyLen - (j << 2U) - i];
#else
            AES_KEY_PTR[i] = AesConfig->Key[i];
#endif
        } 
    }
}


/**
 * @brief Write data which are ready to be processed into register.
 * 
 * @param AesData Pointer to input data buffer.
 * @retval void
 */
void AES_LoadData(uint32_t *AesData)
{
    if(NULL == AesData)
    {
        return ;
    }
    else
    {
#if (0U != SWAP_END_OF_DATA)
        AES_INPUT0_DATA(AesData[3]);
        AES_INPUT1_DATA(AesData[2]);
        AES_INPUT2_DATA(AesData[1]);
        AES_INPUT3_DATA(AesData[0]);
#else
        AES_INPUT0_DATA(AesData[0]);
        AES_INPUT1_DATA(AesData[1]);
        AES_INPUT2_DATA(AesData[2]);
        AES_INPUT3_DATA(AesData[3]);
#endif
    }
}

/**
 * @brief AES data process(Encrypt or Decrypt)
 * 
 * @param OutPutBuf Pointer to output buffer
 * @return uint32_t* Pointer to output buffer. NULL when input is error.
 */
uint32_t *AES_DataProcess(uint32_t *OutPutBuf)
{
    if(NULL == OutPutBuf)
    {
        /*Empty*/
    }
    else
    {
        AES_Run_Enable(TRUE);
        while(RESET == AES_Get_INT_Flag())
        {
            ;
        }
        AES_Clear_INT_Flag();
#if (0U != SWAP_END_OF_DATA)
        OutPutBuf[0] = AES_OUTPUT3;
        OutPutBuf[1] = AES_OUTPUT2;
        OutPutBuf[2] = AES_OUTPUT1;
        OutPutBuf[3] = AES_OUTPUT0;
#else
        OutPutBuf[0] = AES_OUTPUT0;
        OutPutBuf[1] = AES_OUTPUT1;
        OutPutBuf[2] = AES_OUTPUT2;
        OutPutBuf[3] = AES_OUTPUT3;
#endif
    }

    return OutPutBuf;
}

