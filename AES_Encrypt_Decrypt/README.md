#### 文件所有
本文适用于KF32A146的 AES ECB模式加解密 的示例例程，例程文件名：AES_Encrypt_Decrypt
* * *


#### 版权说明
目前的固件只是为了给使用者提供指导，目的是向客户提供有关产品的代码信息，
与使用者的产品信息和代码无关。因此，对于因此类固件内容和（或）客户使用此
处包含的与其产品相关的编码信息而引起的任何索赔，上海芯旺微电子技术有限公
司不承担任何直接、间接或后果性损害赔偿责任
* * *

#### 使用说明
* 本例展示了如何配置AES模块进行ECB模式标准加解密运算。例程使用128bits标准AES对测试向量进行加解密，经测试256bitsAES加解密运算亦可正常计算。
* **注意**：*当前例程仅针对ECB模式加解密，CBC模式在使用上有差异，具体流程可参考用户手册*。

* 编译例程并下载到开发板，连接串口，开发板上电后通过串口打印计算得到的加密内容和解密内容；
* 当加解密数据出错时，串口打印错误提示信息，比对正常时则不打印提示信息。

* * *
