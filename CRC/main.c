/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This project demonstrates the use of CRC module to
 *                      verify a series of data
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "kflog.h"
#include "stdio.h"

/** Swap end of data*/
#define SWAP_BIG_LITTLE_END (1U)

/******************************************************************************
**                          Private variables
******************************************************************************/
/* CRC data, to be calculating */
const uint32_t DataBuffer[] = {
  0x00001021, 0x20423063, 0x408450a5, 0x60c670e7, 0x9129a14a, 0xb16bc18c, 0xd1ade1ce, 0xf1ef1231, 0x32732252,
  0x52b54294, 0x72f762d6, 0x93398318, 0xa35ad3bd, 0xc39cf3ff, 0xe3de2462, 0x34430420, 0x64e674c7, 0x44a45485,
  0xa56ab54b, 0x85289509, 0xf5cfc5ac, 0xd58d3653, 0x26721611, 0x063076d7, 0x569546b4, 0xb75ba77a, 0x97198738,
  0xf7dfe7fe, 0xc7bc48c4, 0x58e56886, 0x78a70840, 0x18612802, 0xc9ccd9ed, 0xe98ef9af, 0x89489969, 0xa90ab92b,
  0x4ad47ab7, 0x6a961a71, 0x0a503a33, 0x2a12dbfd, 0xfbbfeb9e, 0x9b798b58, 0xbb3bab1a, 0x6ca67c87, 0x5cc52c22,
  0x3c030c60, 0x1c41edae, 0xfd8fcdec, 0xad2abd0b, 0x8d689d49, 0x7e976eb6, 0x5ed54ef4, 0x2e321e51, 0x0e70ff9f,
  0xefbedfdd, 0xcffcbf1b, 0x9f598f78, 0x918881a9, 0xb1caa1eb, 0xd10cc12d, 0xe16f1080, 0x00a130c2, 0x20e35004,
  0x40257046, 0x83b99398, 0xa3fbb3da, 0xc33dd31c, 0xe37ff35e, 0x129022f3, 0x32d24235, 0x52146277, 0x7256b5ea,
  0x95a88589, 0xf56ee54f, 0xd52cc50d, 0x34e224c3, 0x04817466, 0x64475424, 0x4405a7db, 0xb7fa8799, 0xe75ff77e,
  0xc71dd73c, 0x26d336f2, 0x069116b0, 0x76764615, 0x5634d94c, 0xc96df90e, 0xe92f99c8, 0xb98aa9ab, 0x58444865,
  0x78066827, 0x18c008e1, 0x28a3cb7d, 0xdb5ceb3f, 0xfb1e8bf9, 0x9bd8abbb, 0x4a755a54, 0x6a377a16, 0x0af11ad0,
  0x2ab33a92, 0xed0fdd6c, 0xcd4dbdaa, 0xad8b9de8, 0x8dc97c26, 0x5c644c45, 0x3ca22c83, 0x1ce00cc1, 0xef1fff3e,
  0xdf7caf9b, 0xbfba8fd9, 0x9ff86e17, 0x7e364e55, 0x2e933eb2, 0x0ed11ef0};

/******************************************************************************
**                              Global Functions
******************************************************************************/

/**
 * @brief: Reset CRC module, polynomial of default CRC32 is 0x04C11DB7
 *
 * @param[in] InputDataBuffer Data to be verified
 * @param[in] InputDataLenth Data lenth
 * @retval uint32_t CRC check result
 */
uint32_t CRC32_CalcBlock(uint32_t const *InputDataBuffer, uint16_t InputDataLenth)
{
    uint32_t        i;
    int32_t         j;
    CRC_InitTypeDef CRCInitStruct;

    uint8_t *kpCurVal = (uint8_t *)InputDataBuffer;

    /* Reset and Enable CRC module */
    CRC_Reset();
    CRC_SET_PLN(0x04C11DB7);
    CRC_SET_INITVALUE(0xFFFFFFFF);
    CRC_SET_RXOR(0xFFFFFFFF);
    CRCInitStruct.m_CalUnitReset  = CRC_CAL_RSET_ENABLE;
    CRCInitStruct.m_InputSize     = CRC_INPUT_SEZE_8;
    CRCInitStruct.m_InputReverse  = CRC_INPUT_REVERSE_ENABLE;
    CRCInitStruct.m_ResultReverse = CRC_RESULT_REVERSE_ENABLE;
    CRC_Configuration(&CRCInitStruct);

#ifdef SWAP_BIG_LITTLE_END
    for (i = 0; i < InputDataLenth; i++)
    {
        for (j = 3; j >= 0; j--)
        {
            CRC_INPUT_DATA(*(kpCurVal + j));
        }
        kpCurVal += 4;
    }
#else
    for (i = 0; i < InputDataLenth * 4; i++)
    {
        CRC_INPUT_DATA(*kpCurVal);
        kpCurVal++;
    }
#endif

    return CRC_GET_RESULT();
}

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    /* Initialize the system clock is 120M */
    SystemInit(72U);
    /*Initialize the print function*/
    kfLog_Init();
    systick_delay_init(72U);

    /* Initialize the USART IOs */
    kf_printf("START CRC32 \r\n");
    /* CRC calculates and outputs observation data through USART0 */
    kf_printf("CRC Value is :0x%x\r\n", CRC32_CalcBlock(DataBuffer, sizeof(DataBuffer) / sizeof(DataBuffer[0])));
    while (1)
    {

        systick_delay_ms(200);
        systick_delay_ms(200);
        systick_delay_ms(200);
        systick_delay_ms(200);
        systick_delay_ms(200);
        kf_printf("CRC Value is :0x%x\r\n", CRC32_CalcBlock(DataBuffer, sizeof(DataBuffer) / sizeof(DataBuffer[0])));
    }
}

/**
 * @brief: Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *
 * @param[in] File file pointer to the source file name
 * @param[in] Line line assert_param error line source number
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
