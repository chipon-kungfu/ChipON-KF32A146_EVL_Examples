/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_ECCP.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                           for ECCP
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/*******************************************************************************
**                               Include Files
*******************************************************************************/
#include "system_init.h"
#include "User_ECCP.h"
#include <stdio.h>

/*******************************************************************************
**                                 Macro Defines
*******************************************************************************/

/*******************************************************************************
**                               Public variables
*******************************************************************************/
#if (CAPTURE_PWM_MEASURE)
/* variables for PWM measure */
uint8_t  Status_CalPWM;
uint32_t HighVol_Time;
uint32_t LowVol_Time;
#endif

/*******************************************************************************
**                              Public Functions
*******************************************************************************/

/**
 * @brief: Configure ECCP to capture mode.
 *
 * @param ECCPx: Pointer to ECCP or general timer register structure
 * @param Channel:  Channel of ECCP. Value can be as follow:
 *          ECCP_CHANNEL_1 - ECCP_CHANNEL_4
 * @param EdgeConfig: Capture edge.
 *          ECCP_CAP_RISING_EDGE
 *          ECCP_CAP_FALLING_EDGE
 *          ECCP_CAP_4TH_RISING_EDGE
 *          ECCP_CAP_16TH_RISING_EDGE
 * @retval: None
 */
void ECCP_Capture_Mode_Init(ECCP_SFRmap *const ECCPx, const uint32_t Channel, const uint32_t EdgeConfig)
{
    /* Set the prescaler value of the timer and the mode of the capture channel */
    /* Timer peripheral reset, enable peripheral clock */
    TIM_Reset(ECCPx);
    /* Set slave mode: reset mode */
    ATIM_X_Slave_Mode_Config(ECCPx, ATIM_SLAVE_RESET_MODE);
    /* Select the trigger source : CH1 */
    ATIM_Trigger_Select_Config(ECCPx, ATIM_TRIGGER_ECCPXCH1);
    /* Set the capture channel mode: capture every rising edge */
    ECCP_Capture_Mode_Config(ECCPx, Channel, EdgeConfig);

    /* Immediately update the output control register with Tx as the time base */
    ATIM_X_Updata_Output_Ctl(ECCPx, TRUE);
    /* Update control periodically */
    ATIM_X_Updata_Immediately_Config(ECCPx, FALSE);
    /* Configuration update enable */
    ATIM_X_Updata_Enable(ECCPx, TRUE);
    /* Timing mode selection */
    ATIM_X_Work_Mode_Config(ECCPx, ATIM_TIMER_MODE);
    /* Timer count value */
    ATIM_X_Set_Counter(ECCPx, 0U);

    /* Timer prescaler value The prescaler is 15, 1us counts once */
    ATIM_X_Set_Prescaler(ECCPx, 15U);
    /* Up, overflow generates an interrupt flag */
    ATIM_X_Counter_Mode_Config(ECCPx, ATIM_COUNT_UP_OF);
    /* Select HFCLK as the timer clock source */
    ATIM_X_Clock_Config(ECCPx, ATIM_HFCLK);
    /* Enable general timer */
    ATIM_X_Cmd(ECCPx, TRUE);
}

/**
 * @brief: Configure ECCP interrupt.
 *
 * @param ECCPx Pointer to ECCP or general timer register structure.
 * @param intConfig Pointer to ECCP interrupt config structure.
 *              ->newState: Interrupt enable status. TRUE/FALSE
 *              ->intIndex: Interrupt index number. Example: INT_T18.
 *              ->ECCPInterrupt: Interrupt type of ECCP.
 *                  ECCP_INT_CAPTURE_CH1 -- Capture/Compare interrupt of ECCP channel1
 *                  ECCP_INT_CAPTURE_CH2 -- Capture/Compare interrupt of ECCP channel2
 *                  ECCP_INT_CAPTURE_CH3 -- Capture/Compare interrupt of ECCP channel3
 *                  ECCP_INT_CAPTURE_CH4 -- Capture/Compare interrupt of ECCP channel4
 *                  ...
 *                  You can find the other interrupt type of ECCP in the enumeration of Eccp_IntConfig_t.
 *                  You can connect them with "|", if you want to enable more than one ECCP interrupt.
 *              ->preemption: preemption of interrupt.
 *              ->subPriority: Sub-Priority of interrupt.
 * @retval: void
 */
void ECCP_INT_Config(ECCP_SFRmap *const ECCPx, const Eccp_IntConfig_t *const intConfig)
{
    uint32_t tmpReg = (uint32_t)(intConfig->eccpInterrupt) << 6U;

    if (FALSE == intConfig->newState)
    {
        /* Disable ECCP interrupt */
        ECCPx->ECCPXIE &= (~intConfig->eccpInterrupt);
        INT_Interrupt_Enable(intConfig->intIndex, FALSE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(intConfig->intIndex);
    }
    else
    {
        /* Set timer interrupt priority */
        INT_Interrupt_Priority_Config(intConfig->intIndex, intConfig->preemption, intConfig->subPriority);
        /* Enable ECCP interrupt */
        ECCPx->ECCPXIE |= (intConfig->eccpInterrupt);
        INT_Interrupt_Enable(intConfig->intIndex, TRUE);
        /* Clear timer interrupt flag */
        ECCPx->ECCPXEGIF |= tmpReg;
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        ECCPx->ECCPXEGIF &= tmpReg;
        INT_Clear_Interrupt_Flag(intConfig->intIndex);
    }
}

#if (CAPTURE_PWM_MEASURE)
/**
 * @brief: Modify ECCP capture mode and update timer.
 *
 * @param ECCPx Pointer to ECCP or general timer register structure
 * @param ECCPx_Channel Channel of ECCP. Value can be as follow:
 *          ECCP_CHANNEL_1 - ECCP_CHANNEL_4
 * @param ECCPx_CaptureMode ECCP capture mode.
 *          ECCP_CAP_RISING_EDGE
 *          ECCP_CAP_FALLING_EDGE
 *          ECCP_CAP_4TH_RISING_EDGE
 *          ECCP_CAP_16TH_RISING_EDGE
 * @retval: void
 */
void ECCP_Capture_Change_Mode(ECCP_SFRmap *ECCPx, uint16_t ECCPx_Channel, uint16_t ECCPx_CaptureMode)
{
    //    fprintf(USART0_STREAM, "CNT: %d\r\n", ECCPx->TXCNT);

    /* Disable ECCP interrupt */
    ECCP_Channel_INT_Enable(ECCPx, ECCPx_Channel, FALSE);
    /* modify ECCP capture mode */
    ECCP_Capture_Mode_Config(ECCPx, ECCPx_Channel, ECCPx_CaptureMode);
    /* Clear interrupt flag */
    ECCP_Clear_Channel_INT_Flag(ECCPx, ECCPx_Channel);
    /* Enable ECCP interrupt */
    ECCP_Channel_INT_Enable(ECCPx, ECCPx_Channel, TRUE);
}
#endif

/**
 * @brief: Configure ECCPx to compare mode.
 *
 * @param ECCPx: Pointer to ECCP or general timer register structure
 * @param CompareConfig: Pointer to CCP compare configuratio structure.
 *              ->Channel: CCPx channel will be used for output.
 *                  ECCP_CHANNEL_1 -- ECCP_CHANNEL_4
 *              ->CompareValue: Comparison value.(0x0 -- 0xffff)
 *              ->EdgeConfig: ECCP compare mode output.
 *                  ECCP_MODE_RST
 *                  ECCP_CMP_TOGGLE_LEVEL
 *                  ECCP_CMP_ACTIVE_LEVEL
 *                  ECCP_CMP_INACTIVE_LEVEL
 *                  ECCP_CMP_SOFT_INT
 *                  ECCP_CMP_SPECIAL_EVENT
 *              ->Period: Period of CCP timer.
 * @retval: None
 */
void ECCP_Compare_Mode_Init(ECCP_SFRmap *ECCPx, Eccp_Config_t *CompareConfig)
{
    /* Timer peripheral reset, enable peripheral clock */
    TIM_Reset(ECCPx);

    /* Configure CCPx to compare mode, the output level is inverted during comparison match*/
    ECCP_Compare_Mode_Config(ECCPx, CompareConfig->Channel, CompareConfig->EdgeConfig);

#ifdef ECCP_COMPARE_WITH_TXCK
    /* Configure CCPx compare register */
    ECCP_Set_Compare_Result(ECCPx, CompareConfig->Channel, CompareConfig->CompareValue - 1U);
    /* Configure timer to update immediately */
    ATIM_X_Updata_Immediately_Config(ECCPx, FALSE);
    /* Enable timer update the period, duty cycle, counter etc. */
    ATIM_X_Updata_Enable(ECCPx, TRUE);
    /* Configure timer work mode to timing mode */
    ATIM_X_Work_Mode_Config(ECCPx, ATIM_COUNTER_MODE);
    /* Set timer period */
    ATIM_X_Set_Period(ECCPx, CompareConfig->Period - 1U);
    /* Set timer prescaler */
    ATIM_X_Set_Prescaler(ECCPx, 0u);
    /* Forbidden using timer slave mode */
    ATIM_X_Slave_Mode_Config(ECCPx, ATIM_SLAVE_FORBIDDEN_MODE);
    /* Triggered whether external pluse are synchronized with clock or not */
    ATIM_X_External_Pulse_Sync_Config(ECCPx, ATIM_NO_SYNC_MODE);
    /* Set timer counter to zero. Update CNT when the first pulse occurs. */
    ATIM_X_Set_Counter(ECCPx, 0U);
    /* Set timer count mode */
    ATIM_X_Counter_Mode_Config(ECCPx, ATIM_COUNT_UP_OF);
#else
    /* Configure CCPx compare register */
    ECCP_Set_Compare_Result(ECCPx, CompareConfig->Channel, CompareConfig->CompareValue);
    /* Update periodically */
    ATIM_X_Updata_Immediately_Config(ECCPx, FALSE);
    /* Configuration update enable */
    ATIM_X_Updata_Enable(ECCPx, TRUE);
    /* Timing mode selection */
    ATIM_X_Work_Mode_Config(ECCPx, ATIM_TIMER_MODE);
    /* Timer count value */
    ATIM_X_Set_Counter(ECCPx, 0U);
    /* Timer period value */
    ATIM_X_Set_Period(ECCPx, CompareConfig->Period);
    /* Timer prescaler value */
    ATIM_X_Set_Prescaler(ECCPx, 15U);
    /* Up-down counting, overflow and underflow generate interrupt flags */
    ATIM_X_Counter_Mode_Config(ECCPx, ATIM_COUNT_UP_OF);
    /* Configure working clock  */
    ATIM_X_Clock_Config(ECCPx, ATIM_HFCLK);
#endif
    /* Timer start control enable */
    ATIM_X_Cmd(ECCPx, TRUE);
}

/**
 * @brief: Configure CCPx to PWM measurement mode.
 *          CAUTION:
 *              PWM Measurement need to use CCPX CH1 and CH2 in the same time.
 *
 * @param CCPx Pointer to CCP register structure.
 * @retval: void
 */
void ECCP_PWM_Measurement_Mode_init(ECCP_SFRmap *ECCPx)
{
    /* Set the prescaler value of the timer and the mode of the capture channel */
    /* Timer peripheral reset, enable peripheral clock */
    TIM_Reset(ECCPx);

    /* PWM Input measurement mode enable */
    ECCP_PWM_Input_Enable(ECCPx, TRUE);
    /* Set slave mode: reset mode */
    ATIM_X_Slave_Mode_Config(ECCPx, ATIM_SLAVE_RESET_MODE);
    /* Select the trigger source : CH1 */
    ATIM_Trigger_Select_Config(ECCPx, ATIM_TRIGGER_ECCPXCH1);
    /* Set the capture channel mode: capture every rising edge */
    ECCP_Capture_Mode_Config(ECCPx, ECCP_CHANNEL_1, ECCP_CAP_FALLING_EDGE);
    ECCP_Capture_Mode_Config(ECCPx, ECCP_CHANNEL_2, ECCP_CAP_RISING_EDGE);
    /* Update control immediately */
    ATIM_X_Updata_Immediately_Config(ECCPx, TRUE);
    /* Configuration update enable */
    ATIM_X_Updata_Enable(ECCPx, TRUE);
    /* Timing mode selection */
    ATIM_X_Work_Mode_Config(ECCPx, ATIM_TIMER_MODE);
    /* Timer count value */
    ATIM_X_Set_Counter(ECCPx, 0U);
    /* Timer prescaler value The prescaler is 15, 1us counts once */
    ATIM_X_Set_Prescaler(ECCPx, 15U);
    /* Up, overflow generates an interrupt flag */
    ATIM_X_Counter_Mode_Config(ECCPx, ATIM_COUNT_UP_OF);
    /* Select SCLK as the timer clock source */
    ATIM_X_Clock_Config(ECCPx, ATIM_HFCLK);

    /* Enable general timer */
    ATIM_X_Cmd(ECCPx, TRUE);
}

/**
 * @brief: Configure ECCPx to PWM mode.
 *
 * @param ECCPx: Pointer to ECCP register structure.
 * @param Channel: Channel of CCP.
 *          ECCP_CHANNEL_1 - ECCP_CHANNEL_4
 * @param PwmConfig: Pointer to ECCP PWM configuration structure.
 *          ->DutyCycle: Duty cycle of PWM.
 *          ->DeadZone: Dead zone delay time of PWM.
 *          ->PwmOutputMode: ECCP output mode.
 *              ECCP_OUTPUT_INDEPENDENT
 *              ECCP_OUTPUT_COMPLEMENTARY
 *          ->AutoShutDown: Automatic shut down signal.
 *              ECCP_CHANNEL_SHUTDOWN_FORBID: Forbidden shut down
 *              ECCP_CHANNEL_CMP2CMP3_ACTIVE: Compare 2&3 output
 *              ECCP_CHANNEL_BKIN_INACTIVE: Low level of ECCP_BKIN
 *          ->AutoRestart: Restart when error disappear. TRUE/FALSE
 *          ->LoPortCloseLvl: Low port output when ECCP closed.
 *              PIN_INACTIVE: Inactive level
 *              PIN_ACTIVE: Active level
 *              PIN_TRISTATE: Hi-Z state
 *          ->HiPortCloseLvl: High port output when ECCP closed.
 *              PIN_INACTIVE: Inactive level
 *              PIN_ACTIVE: Active level
 *              PIN_TRISTATE: Hi-Z state
 *          ->LoPortOutLvl: Low port output when ECCP open.
 *              ECCP_CHANNEL_OUTPUT_PWM_ACTIVE: PWM
 *              ECCP_CHANNEL_OUTPUT_PWM_INACTIVE: PWM
 *              ECCP_CHANNEL_OUTPUT_INACTIVE
 *              ECCP_CHANNEL_OUTPUT_ACTIVE
 *          ->HiPortOutLvl: High port output when ECCP open.
 *              ECCP_CHANNEL_OUTPUT_PWM_ACTIVE: PWM
 *              ECCP_CHANNEL_OUTPUT_PWM_INACTIVE: PWM
 *              ECCP_CHANNEL_OUTPUT_INACTIVE
 *              ECCP_CHANNEL_OUTPUT_ACTIVE
 * @retval: None
 */
void ECCP_PWM_Mode_Init(ECCP_SFRmap *ECCPx, uint32_t Channel, Eccp_PwmConfig_t *PwmConfig)
{
    /* PWM free mode */
    ECCP_PWM_Mode_Config(ECCPx, ECCP_PWM_FREE);

    ECCP_Channel_Output_Mode(ECCPx, Channel, (uint32_t)PwmConfig->PwmOutputMode);
    ECCP_Channel_Shutdown_Signal(ECCPx, Channel, (uint32_t)PwmConfig->AutoShutDown);

    /* Enable automatic restart  */
    ECCP_PWM_Restart_Enable(ECCPx, (FunctionalState)PwmConfig->AutoRestart);

    /* Closed state  */
    ECCP_Channel_Pin_Ctl(ECCPx, Channel, ECCP_PORT_LOW, (uint32_t)PwmConfig->LoPortCloseLvl);
    ECCP_Channel_Pin_Ctl(ECCPx, Channel, ECCP_PORT_HIGH, (uint32_t)PwmConfig->HiPortCloseLvl);
    /* PWM output */
    ECCP_Channel_Output_Control(ECCPx, Channel, ECCP_PORT_LOW, (uint32_t)PwmConfig->LoPortOutLvl);
    ECCP_Channel_Output_Control(ECCPx, Channel, ECCP_PORT_HIGH, (uint32_t)PwmConfig->HiPortOutLvl);

    /* Channel dead zone delay time */
    ECCP_Dead_Time_Config(ECCPx, Channel, (PwmConfig->DeadZone));
    /* PWM duty cycle  */
    ECCP_Set_Compare_Result(ECCPx, Channel, (PwmConfig->DutyCycle));
}

/**
 *  @brief:Tx inialization
 *
 *  @param in:ATIM_SFRmap *ATIMx
 *  @retval:None
 */
void ECCP_Timer_Init(ATIM_SFRmap *ATIMx, uint32_t PreScaler, uint32_t Period)
{
    /* Timing mode */
    ATIM_X_Work_Mode_Config(ATIMx, ATIM_TIMER_MODE);
    /* Count value is cleared to 0  */
    ATIM_X_Set_Counter(ATIMx, 0);
    /* Period value */
    ATIM_X_Set_Period(ATIMx, Period);
    /* Prescaler 1:1  */
    ATIM_X_Set_Prescaler(ATIMx, PreScaler);
    /* The post-divider ratio is 1:1  */
    ATIM_X_Postscaler_Config(ATIMx, ATIM_POSTSCALER_DIV_1);
    /* Center-aligned PWM signal, counts up, and generates an interrupt flag
     * when it overflows */
    ATIM_X_Counter_Mode_Config(ATIMx, ATIM_COUNT_UP_OF);
    /* Choose HFCLK clock as Tx working clock  */
    ATIM_X_Clock_Config(ATIMx, ATIM_HFCLK);
    /* Enable immediate update  */
    ATIM_X_Updata_Immediately_Config(ATIMx, FALSE);

    /* Immediately update the output control register with Tx as the time base
     */
    ATIM_X_Updata_Output_Ctl(ATIMx, FALSE);
    /* Allow to change related configuration information with Tx as the time
     * base  */
    ATIM_X_Updata_Enable(ATIMx, TRUE);
    /* Tx start control enable  */
    ATIM_X_Cmd(ATIMx, TRUE);
}

/**
 * @brief: Update duty cycle
 *
 * @param ECCPx: Pointer to ECCP register structure.
 * @param Channel: Channel of CCP.
 *          ECCP_CHANNEL_1 - ECCP_CHANNEL_4
 * @param DutyCycle: New duty cycle.
 * @retval: void
 */
void ECCP_PWM_Update_DutyCycle(ECCP_SFRmap *ECCPx, uint32_t Channel, uint16_t DutyCycle)
{
    ECCP_Set_Compare_Result(ECCPx, Channel, DutyCycle);
}

/**
 * @brief: Update dead zone delay time.
 *
 * @param ECCPx: Pointer to ECCP register structure.
 * @param Channel: Channel of CCP.
 *          ECCP_CHANNEL_1 - ECCP_CHANNEL_4
 * @param DutyCycle: New dead zone delay time
 * @retval: void
 */
void ECCP_PWM_Update_DeadZone(ECCP_SFRmap *ECCPx, uint32_t Channel, uint16_t DeadDelay)
{
    ECCP_Dead_Time_Config(ECCPx, Channel, DeadDelay);
}
