/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for CCP PWM mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_ECCP.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                                 Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    /* System clock configuration */
    SystemInit(72U);
    systick_delay_init(72U);

    /* Configure PWM output IO port, PB0 */
    GPIO_Write_Mode_Bits(PF6_ECCP5_CH1H_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PF6_ECCP5_CH1H_AF);
    GPIO_Write_Mode_Bits(PB10_ECCP5_CH1L_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PB10_ECCP5_CH1L_AF);

    GPIO_Write_Mode_Bits(PA11_ECCP5_CH2H_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PA11_ECCP5_CH2H_AF);
    GPIO_Write_Mode_Bits(PG13_ECCP5_CH2L_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PG13_ECCP5_CH2L_AF);

    GPIO_Pull_Up_Enable(PF2_ECCP5_BKIN_PIN, TRUE);
    GPIO_Write_Mode_Bits(PF2_ECCP5_BKIN_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PF2_ECCP5_BKIN_AF);

    /**PD4 -- S2*/
    GPIO_Write_Mode_Bits(PD4_KEY2_PIN, GPIO_MODE_IN);
    /* LED1 */
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);

    {
        Eccp_PwmConfig_t Eccp5Ch1Config = {
          .DutyCycle      = 500U,
          .DeadZone       = 10U,
          .PwmOutputMode  = ECCP_OUTPUT_INDEPENDENT,
          .AutoShutDown   = ECCP_CHANNEL_BKIN_INACTIVE,
          .AutoRestart    = TRUE,
          .HiPortCloseLvl = PIN_INACTIVE,
          .LoPortCloseLvl = PIN_ACTIVE,
          .HiPortOutLvl   = ECCP_CHANNEL_OUTPUT_PWM_INACTIVE,
          .LoPortOutLvl   = ECCP_CHANNEL_OUTPUT_PWM_INACTIVE,
        };
        Eccp_PwmConfig_t Eccp5Ch2Config = {
          .DutyCycle      = 200U,
          .DeadZone       = 10U,
          .PwmOutputMode  = ECCP_OUTPUT_COMPLEMENTARY,
          .AutoShutDown   = ECCP_CHANNEL_BKIN_INACTIVE,
          .AutoRestart    = TRUE,
          .HiPortCloseLvl = PIN_INACTIVE,
          .LoPortCloseLvl = PIN_INACTIVE,
          .HiPortOutLvl   = ECCP_CHANNEL_OUTPUT_PWM_ACTIVE,
          .LoPortOutLvl   = ECCP_CHANNEL_OUTPUT_PWM_ACTIVE,
        };

        /* Initialise ECCP5 channel 1&2 to PWM mode, and set period and duty cycle of PWM */
        TIM_Reset(ECCP5_SFR);
        ECCP_PWM_Mode_Init(ECCP5_SFR, ECCP_CHANNEL_1, &Eccp5Ch1Config);
        ECCP_PWM_Mode_Init(ECCP5_SFR, ECCP_CHANNEL_2, &Eccp5Ch2Config);
        ECCP_Timer_Init(ECCP5_SFR, 1599U, 1000U);
    }
    while (1)
    {
        /* Get S2 status */
        if (RESET == GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN))
        {
            systick_delay_ms(100U);
            if (RESET == GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN))
            {
                ECCP_PWM_Update_DutyCycle(ECCP5_SFR, ECCP_CHANNEL_1, 700U);
                ECCP_PWM_Update_DeadZone(ECCP5_SFR, ECCP_CHANNEL_2, 20U);
            }
        }
        else
        {
            systick_delay_ms(100U);
        }
        GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
