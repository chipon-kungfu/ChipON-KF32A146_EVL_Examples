/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for ECCP capture
 *                      interrupt mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_ECCP.h"
#include "Usart.h"
#include "Board_GpioCfg.h"
#include <stdio.h>

/*******************************************************************************
**                          Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main(void)
{
    /* Initialize the system clock is 72MHz*/
    SystemInit(72U);
    systick_delay_init(72U);

    /* LED1 */
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    /**LED2*/
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);
    /* PF6 remap to capture mode */
    GPIO_Pull_Up_Enable(PF6_ECCP5_CH1H_PIN, TRUE);
    GPIO_Write_Mode_Bits(PF6_ECCP5_CH1H_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PF6_ECCP5_CH1H_AF);

    {
        Eccp_IntConfig_t eccpIntConfig = {
          .newState      = TRUE,
          .intIndex      = INT_T5,
          .eccpInterrupt = ECCP_INT_CAPTURE_CH1,
          .preemption    = 4U,
          .subPriority   = 0U};
        ECCP_Capture_Mode_Init(ECCP5_SFR, ECCP_CHANNEL_1, ECCP_CAP_RISING_EDGE);
        ECCP_INT_Config(ECCP5_SFR, &eccpIntConfig);
    }

    /* Enable all interrupt */
    INT_All_Enable(TRUE);
    while (1)
    {
        systick_delay_ms(250U);
        systick_delay_ms(250U);
        /*Toggle LED2*/
        GPIO_Toggle_Output_Data_Config(PF11_LED2_PIN);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
