/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Usart.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the serial routine related
 *                     configuration functions, including:
 *                          + Initializes the GPIO of the Usart
 *                          + Usart to send data
 *                          + Configure USART mode
 *                          + Configuration USART Initialize
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
 *                                Include Files
 ******************************************************************************/
#include "system_init.h"
#include "Usart.h"

/*******************************************************************************
**                                 private Functions
*******************************************************************************/
static uint32_t USRAT_Set_baudRate(USART_SFRmap *USARTx, uint8_t freq, uint32_t baudRate);

/**
 * @brief: calculate baudRate of USARTx.
 *
 * @param USARTx: Pointer to USART register structure.
 * @param freq: Frequency of USART work clock source.
 * @param baudRate: Baud rate of USART.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
static uint32_t USRAT_Set_baudRate(USART_SFRmap *USARTx, uint8_t freq, uint32_t baudRate)
{
    uint32_t baudRate_Integer;
    uint32_t baudRate_Numerator = 1u;
    uint32_t baudRate_Denominator;
    uint32_t coefficient;
    uint32_t freq_Tmp     = freq * 10000u;
    uint32_t baudRate_Tmp = baudRate / 100u;

    if ((baudRate < 1200u) || (baudRate > 921600u))
    {
        return 1u;
    }
    else
    {
        /* Empty */
    }

    baudRate_Integer = (uint16_t)(freq_Tmp / (16u * baudRate_Tmp));
    coefficient      = (freq_Tmp * 1000u) / (16u * baudRate_Integer * baudRate_Tmp);

    if (coefficient <= 1000u)
    {
        /* When the bit is 0, the decimal baud rate generator does not affect
         * the baud rate */
        baudRate_Numerator   = 0u;
        baudRate_Denominator = 0u;
    }
    else
    {
        baudRate_Denominator = (baudRate_Numerator * 1000u) / (coefficient - 1000u);
        /* Numerator and denominator register, maximum 4bits, otherwise set to 0
         */
        if (baudRate_Denominator > 15u)
        {
            /* When the bit is 0, the decimal baud rate generator does not
             * affect the baud rate */
            baudRate_Numerator   = 0u;
            baudRate_Denominator = 0u;
        }
        else
        {
            /* Empty */
        }
    }

    /* Configure USART baduRate */
    USART_BaudRate_Integer_Config(USARTx, baudRate_Integer);
    USART_BaudRate_Decimal1_Config(USARTx, baudRate_Numerator);
    USART_BaudRate_Decimal2_Config(USARTx, baudRate_Denominator);

    return 0U;
}

/*******************************************************************************
**                                Global Functions
*******************************************************************************/

/**
 * @brief: Initializes the Tx  GPIO of the Usart
 *
 * @param GPIOx: The pointer to GPIO register,
 *                  GPIOA_SFR ~ GPIOH_SFR
 * @param GPIO_Pin: The mask of Rx pin,
 *                  GPIO_PIN_MASK_0 ~ GPIO_PIN_MASK_15
 * @param Rmp: Remap mode,
 *                  GPIO_RMP_AF0 ~ GPIO_RMP_A15
 * @retval: void
 */
void USART_TxGpio_Config(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t Rmp)
{
    volatile uint16_t GpioPinTemp = GPIO_Pin, GPIO_PinNum = 0;
    GPIO_InitTypeDef  GPIO_InitStructure;

    GPIO_InitStructure.m_Mode      = GPIO_MODE_RMP;
    GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
    GPIO_InitStructure.m_PullDown  = GPIO_NOPULL;
    GPIO_InitStructure.m_PullUp    = GPIO_NOPULL;
    GPIO_InitStructure.m_Speed     = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Pin       = GPIO_Pin;
    GPIO_Configuration(GPIOx, &GPIO_InitStructure);
    while ((GpioPinTemp & (0x1)) != 1)
    {
        GPIO_PinNum++;
        GpioPinTemp >>= 1;
    }
    GPIO_Pin_RMP_Config(GPIOx, GPIO_PinNum, Rmp);
}

/**
 * @brief: Initializes the Rx  GPIO of the Usart
 * @param GPIOx: The pointer to GPIO register,
 *              GPIOA_SFR ~ GPIOH_SFR
 * @param GPIO_Pin: The mask of Rx pin,
 *              GPIO_PIN_MASK_0 ~ GPIO_PIN_MASK_15
 * @param Rmp: Remap mode,
 *                  GPIO_RMP_AF0 ~ GPIO_RMP_A15
 * @retval: void
 */
void USART_RxGpio_Config(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t Rmp)
{
    volatile uint16_t GpioPinTemp = GPIO_Pin, GPIO_PinNum = 0;
    /*RX pin, it should be pullup */
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_InitStructure.m_Mode      = GPIO_MODE_RMP;
    GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
    GPIO_InitStructure.m_PullUp    = GPIO_PULLUP;
    GPIO_InitStructure.m_PullUp    = GPIO_NOPULL;
    GPIO_InitStructure.m_Speed     = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Pin       = GPIO_Pin;
    GPIO_Configuration(GPIOx, &GPIO_InitStructure);
    while ((GpioPinTemp & (0x1)) != 1)
    {
        GPIO_PinNum++;
        GpioPinTemp >>= 1;
    }
    GPIO_Pin_RMP_Config(GPIOx, GPIO_PinNum, Rmp);
}

/**
 *  @brief :Usart to send data
 *
 *  @param USARTx: A pointer to the USART memory structure
 *                 with a value of USART0_SFR
 *                                 USART1_SFR
 *                                 USART2_SFR
 *                                 USART5_SFR
 *  @param Databuf: The pointer to data buffer
 *  @param length: The number of bytes to be send
 *  @retval :void
 */
void USART_Send(USART_SFRmap *USARTx, uint8_t *Databuf, uint32_t length)
{
    for (volatile uint32_t i = 0; i < length; i++)
    {
        /* Serial transmission */
        USART_SendData(USARTx, Databuf[i]);
    }
}

/**
 * @brief: Configure USART.
 *          In default, GPTIM use HFCLK as work clock source.
 *          If you want to use other clock source you NEED TO
 *              - modify member $m_BaudRateBRCKS$ of $USART_InitStructure$.
 *              - modify entrance parameter $freq$ of $USRAT_Set_baudRate()$, in MHz.
 *
 * @param USARTx: pointer to the USART register structure.
 * @param baudRate: The baud rate of USART.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
uint32_t USART_Mode_Config(USART_SFRmap *USARTx, uint32_t baudRate)
{
    /* Reset USART and enable clock */
    USART_Reset(USARTx);

    USART_InitTypeDef USART_InitStructure;
    USART_Struct_Init(&USART_InitStructure);
    /* Set Usart To Async Mode */
    USART_InitStructure.m_Mode        = USART_MODE_FULLDUPLEXASY;
    USART_InitStructure.m_TransferDir = USART_DIRECTION_FULL_DUPLEX;
    USART_InitStructure.m_WordLength  = USART_WORDLENGTH_8B;
    USART_InitStructure.m_StopBits    = USART_STOPBITS_1;

    USART_InitStructure.m_BaudRateBRCKS = USART_CLK_HFCLK;
    USART_Configuration(USARTx, &USART_InitStructure);
    /*  Set baudrate */
    if (0u != USRAT_Set_baudRate(USARTx, 16U, baudRate))
    {
        return 1U;
    }
    else
    {
        /* Empty */
    }

    /* Set Usart to send enable */
    USART_Passageway_Select_Config(USARTx, USART_U7816R_PASSAGEWAY_TX0);
    USART_RESHD_Enable(USARTx, TRUE);
    USART_Transmit_Data_Enable(USARTx, TRUE);
    USART_Cmd(USARTx, TRUE);

    return 0U;
}

/**
 *  @brief :Configuration USART Initialize
 *
 *  @param USARTx: A pointer to the USART memory structure
 *                 with a value of USART0_SFR
 *                                 USART1_SFR
 *                                 USART2_SFR
 *                                 USART5_SFR
 *    @param Peripheral: Interrupt vector number
 *                      with a value of INT_USART0
 *                                      INT_USART1
 *                                      INT_USART2
 *                                      INT_USART5
 *  @retval :void
 */
void USART_Int_Config(USART_SFRmap *USARTx, InterruptIndex Peripheral)
{
    /* Resetting USARTx sends the BUF interrupt bit */
    USART_Receive_Data_Enable(USARTx, TRUE);
    /* SET USARTx RDR interrupt enable */
    USART_RDR_INT_Enable(USARTx, TRUE);
    /* Configure USART to receive idle frame interrupt flag generation mode */
    USART_Receive_Idle_Frame_Config(USARTx, TRUE);
    /* SET USARTx IDLE interrupt enable */
    USART_IDLE_INT_Enable(USARTx, TRUE);
    /*Interrupt enables a peripheral or kernel interrupt vector */
    INT_Interrupt_Enable(Peripheral, TRUE);
}
