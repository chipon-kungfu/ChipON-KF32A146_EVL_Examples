/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Usart.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the serial routine related
 *                     configuration functions, including:
 *                          + Initializes the GPIO of the Usart
 *                          + Usart to send data
 *                          + Configure USART mode
 *                          + Configuration USART Initialize
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#ifndef _USART_H_
#define _USART_H_
/******************************************************************************
**                       	Include Files                                     *
******************************************************************************/
#include <stdint.h>

/******************************************************************************
 *                      Global Macro                                          *
 ******************************************************************************/
#define USART_DMA (0)

/******************************************************************************
 *                      Functional definition                                 *
 ******************************************************************************/

extern void     USART_TxGpio_Config(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t Rmp);
extern void     USART_RxGpio_Config(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t Rmp);
extern void     USART_Send(USART_SFRmap *USARTx, uint8_t *Databuf, uint32_t length);
extern uint32_t USART_Mode_Config(USART_SFRmap *USARTx, uint32_t baudRate);
extern void     USART_Int_Config(USART_SFRmap *USARTx, InterruptIndex Peripheral);

#if (USART_DMA)
extern void USART_TxDma_Config(USART_SFRmap *USARTx, DMA_SFRmap *DMAx);
extern void USART_RxDma_Config(USART_SFRmap *USARTx, DMA_SFRmap *DMAx);
extern void USART_RxDma_Buffer_Regiter(USART_SFRmap *USARTx, DMA_SFRmap *DMAx, uint8_t *buffer, uint16_t buf_size);
extern void USART_TxDma_Transmit(USART_SFRmap *USARTx, DMA_SFRmap *DMAx, uint8_t *pData, uint16_t nSize);
#endif

#endif
