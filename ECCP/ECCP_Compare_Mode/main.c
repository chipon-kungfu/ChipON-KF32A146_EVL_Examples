/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for CCP compare mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_ECCP.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                          Private Functions
*******************************************************************************/
static void ATIM_Gpio_Init(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t PinRemap)
{

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.m_Mode      = GPIO_MODE_RMP;
    GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
    GPIO_InitStructure.m_PullDown  = GPIO_PULLDOWN;
    GPIO_InitStructure.m_PullUp    = GPIO_NOPULL;
    GPIO_InitStructure.m_Speed     = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Pin       = ((uint32_t)1U << (uint32_t)GPIO_Pin);
    GPIO_Configuration(GPIOx, &GPIO_InitStructure);

    GPIO_Pin_RMP_Config(GPIOx, GPIO_Pin, PinRemap);
}

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    uint8_t CompareFlag = 0U;

    /* System clock configuration */
    SystemInit(72U);
    systick_delay_init(72U);

    /* LED1 */
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    /**PD4 -- S2*/
    GPIO_Write_Mode_Bits(PD4_KEY2_PIN, GPIO_MODE_IN);

    /* PF6 remap to Compare mode */
    GPIO_Write_Mode_Bits(PF6_ECCP5_CH1H_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PF6_ECCP5_CH1H_AF);

    /* ECCP5 channel 1 Compare mode initialization, Configure
       channel 1 trigger source*/
#ifdef ECCP_COMPARE_WITH_TXCK
    ATIM_Gpio_Init(PA5_T5CK_AF);
    {
        Eccp_Config_t eccpConfig = {
          .Channel      = ECCP_CHANNEL_1,
          .EdgeConfig   = ECCP_CMP_TOGGLE_LEVEL,
          .CompareValue = 500U,
          .Period       = 950U,
        };
        Eccp_IntConfig_t eccpIntConfig = {
          .newState      = TRUE,
          .intIndex      = INT_T5,
          .eccpInterrupt = ECCP_INT_CAPTURE_CH1,
          .preemption    = 4U,
          .subPriority   = 0U,
        };

        ECCP_Compare_Mode_Init(ECCP5_SFR, &eccpConfig);
        ECCP_INT_Config(ECCP5_SFR, &eccpIntConfig);
        /* Enable ATIM overflow interrupt*/
        ATIM_X_Overflow_INT_Enable(T5_SFR, TRUE);
        ATIM_X_Clear_Overflow_INT_Flag(T5_SFR);
    }
#else
    {
        Eccp_Config_t eccpConfig = {
          .Channel      = ECCP_CHANNEL_1,
          .EdgeConfig   = ECCP_CMP_TOGGLE_LEVEL,
          .CompareValue = 200U,
          .Period       = 1000U,
        };
        Eccp_IntConfig_t eccpIntConfig = {
          .newState      = TRUE,
          .intIndex      = INT_T5,
          .eccpInterrupt = ECCP_INT_CAPTURE_CH1,
          .preemption    = 4U,
          .subPriority   = 0U,
        };

        /* Configure ECCP compare mode, set period to 1000, compare to 200. */
        ECCP_Compare_Mode_Init(ECCP5_SFR, &eccpConfig);
        ECCP_INT_Config(ECCP5_SFR, &eccpIntConfig);

        /**Enable ATIM overflow interrupt*/
        ATIM_X_Overflow_INT_Enable(T5_SFR, TRUE);
        ATIM_X_Clear_Overflow_INT_Flag(T5_SFR);
    }

#endif

    INT_All_Enable(TRUE);
    while (1)
    {
#ifdef ECCP_COMPARE_WITH_TXCK
        ;
#else
        if (RESET == GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN))
        {
            systick_delay_ms(100U);
            if (RESET == GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN))
            {
                /* Update compare value. It will be valid in next period. */
                CompareFlag = (++CompareFlag) % 2U;
                /* Configure CCPx compare register */
                ECCP_Set_Compare_Result(ECCP5_SFR, CCP_CHANNEL_1, 200U + (CompareFlag * 200U));
            }
        }
#endif
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
