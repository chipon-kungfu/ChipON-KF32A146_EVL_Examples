/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_ECCP.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                           for ECCP
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_ECCP_H_
#define USER_ECCP_H_
/*******************************************************************************
**                               Include Files                                **
*******************************************************************************/

/*******************************************************************************
**                                 Macro Defines                              **
*******************************************************************************/
/* PWM measure enable status */
// #define CAPTURE_PWM_MEASURE (1U)
/** Compare with TxCK enable status */
// #define ECCP_COMPARE_WITH_TXCK (1U)

/*ECCP interrupt index*/
typedef enum
{
    /* Capture/Compare interrupt of CCP channel1 */
    ECCP_INT_CAPTURE_CH1 = 0x1U,
    /* Capture/Compare interrupt of CCP channel2 */
    ECCP_INT_CAPTURE_CH2 = 0x2U,
    /* Capture/Compare interrupt of CCP channel3 */
    ECCP_INT_CAPTURE_CH3 = 0x4U,
    /* Capture/Compare interrupt of CCP channel4 */
    ECCP_INT_CAPTURE_CH4 = 0x8U,
    /* Tx trigger event interrupt */
    ECCP_INT_TX_TRIGER_EVENT = 0x10U,
    /* Tz overflow interrupt */
    ECCP_INT_TZ_OVERFLOW = 0x20U,
    /* Tx overflow interrupt */
    ECCP_INT_TX_OVERFLOW = 0x40U,
    /* Tz update interrupt */
    ECCP_INT_TZ_UPDATE_EVENT = 0x80U,
    /* Tx update interrupt */
    ECCP_INT_TX_UPDATE_EVENT = 0x100U,
} Eccp_Int_t;

/* CCP interrupt configuration typedef */
typedef struct
{
    /* Interrupt enable status */
    FunctionalState newState;
    /* Interrupt index */
    InterruptIndex intIndex;
    /* Interrut type of CCP */
    Eccp_Int_t eccpInterrupt;
    uint8_t    preemption;
    uint8_t    subPriority;
} Eccp_IntConfig_t;

#ifdef CAPTURE_PWM_MEASURE
/* State of PWM measur */
enum CalPWM_Status
{
    Init_CalPWM = 0,
    HighVol_CalPWM,
    LowVol_CalPWM,
    End_CalPWM
};
/* variables for PWM measure */
extern uint8_t  Status_CalPWM;
extern uint32_t HighVol_Time;
extern uint32_t LowVol_Time;
#endif

/* ECCP compare mode configure structure */
typedef struct ECCP_CONFIG
{
    uint32_t Channel;
    uint32_t Period;
    uint32_t CompareValue;
    uint32_t EdgeConfig;
} Eccp_Config_t;

/* ECCP compare mode configure structure */
typedef struct ECCP_PWM_CONFIG
{
    uint16_t DutyCycle;
    uint8_t  DeadZone;
    uint8_t  PwmOutputMode;
    uint8_t  AutoShutDown;
    uint8_t  AutoRestart;
    uint8_t  HiPortCloseLvl;
    uint8_t  LoPortCloseLvl;
    uint8_t  HiPortOutLvl;
    uint8_t  LoPortOutLvl;
} Eccp_PwmConfig_t;

/*******************************************************************************
**                              Public Functions                              **
*******************************************************************************/

extern void ECCP_Capture_Mode_Init(ECCP_SFRmap *const ECCPx, const uint32_t Channel, const uint32_t EdgeConfig);
extern void ECCP_INT_Config(ECCP_SFRmap *const ECCPx, const Eccp_IntConfig_t *const intConfig);
#ifdef CAPTURE_PWM_MEASURE
extern void ECCP_Capture_Change_Mode(ECCP_SFRmap *ECCPx, uint16_t ECCPx_Channel, uint16_t ECCPx_CaptureMode);
#endif

extern void ECCP_Compare_Mode_Init(ECCP_SFRmap *ECCPx, Eccp_Config_t *CompareConfig);
extern void ECCP_PWM_Measurement_Mode_init(ECCP_SFRmap *ECCPx);
extern void ECCP_PWM_Mode_Init(ECCP_SFRmap *ECCPx, uint32_t Channel, Eccp_PwmConfig_t *PwmConfig);
extern void ECCP_Timer_Init(ATIM_SFRmap *ATIMx, uint32_t PreScaler, uint32_t Period);
extern void ECCP_PWM_Update_DutyCycle(ECCP_SFRmap *ECCPx, uint32_t Channel, uint16_t DutyCycle);
extern void ECCP_PWM_Update_DeadZone(ECCP_SFRmap *ECCPx, uint32_t Channel, uint16_t DeadDelay);

#endif /* USER_ECCP_H_ */
