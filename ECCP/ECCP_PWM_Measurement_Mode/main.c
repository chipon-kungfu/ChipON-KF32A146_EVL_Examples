/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for ECCP PWM measurement
 *                      mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_ECCP.h"
#include "Board_GpioCfg.h"

/* Capture the PWM duty cycle variable */
volatile uint16_t UP_PLUSE_WIDTH = 0;
/* Capture the PWM cycle */
volatile uint16_t DW_PLUSE_WIDTH = 0;
/*******************************************************************************
**                          Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    /* System clock configuration */
    SystemInit(72U);
    systick_delay_init(72U);

    /* LED1 */
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    /* LED2 */
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);

    /* PF6 remap to capture mode */
    GPIO_Write_Mode_Bits(PF6_ECCP5_CH1H_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PF6_ECCP5_CH1H_AF);

    /* ECCP5 channel 1 and channel 2 Capture mode initialization, Configure
       channel 1 trigger source*/
    ECCP_PWM_Measurement_Mode_init(ECCP5_SFR);

    while (1)
    {
        systick_delay_ms(250U);
        systick_delay_ms(250U);

        if (ECCP_Get_Channel_Trigger_INT_Flag(ECCP5_SFR, CCP_CHANNEL_1))
        {
            ECCP_Clear_Channel_INT_Flag(ECCP5_SFR, CCP_CHANNEL_1);
            /* Capture duty cycle */
            UP_PLUSE_WIDTH = ECCP_Get_Capture_Result(ECCP5_SFR, CCP_CHANNEL_1);
            /* 1KHZ PWM signal, duty cycle is 50% */
            if ((UP_PLUSE_WIDTH > 480) && (UP_PLUSE_WIDTH < 520))
            {
                /* PD12 control led 1 flashing */
                GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
            }
            else
            {}

            /* Capture the cycle */
            DW_PLUSE_WIDTH = ECCP_Get_Capture_Result(ECCP5_SFR, CCP_CHANNEL_2);
            /* 1KHZ PWM signal count value */
            if ((DW_PLUSE_WIDTH > 980) && (DW_PLUSE_WIDTH < 1020))
            {
                /* PH3 control led 2 flashing */
                GPIO_Toggle_Output_Data_Config(PF11_LED2_PIN);
            }
            else
            {}
        }
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
