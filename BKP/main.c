/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use BKP
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
/*******************************************************************************
**                                Global Functions
*******************************************************************************/
/**
 *  @brief:Initialize the LED GPIO ports
 *  @param[in]  None
 *  @param[out] None
 *  @retval:None
 */
void BoardGpioInit(void)
{
    /* Configure  LED2 output */
    GPIOInit_Output_Config(LED2_PORT, LED2_PIN);
}

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    /* Initialize the system clock is 72MHz */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 48MHz */
    systick_delay_init(72U);
    /* Initialize the LED IOs */
    BoardGpioInit();
    /* Set the BKP area is enable */
    BKP_Write_And_Read_Enable(TRUE);
    /* Read and write data in the backup domain 0, increment by 1 each time,
    and reset to 0 when it reaches 40 */
    if (++BKP_DATA0 >= 40)
        BKP_DATA0 = 0;
    /* Turn on LED2 */
    GPIO_Set_Output_Data_Bits(LED2_PORT, LED2_PIN, Bit_SET);
    /* Delay a little time. time = (BKP_DATA0 * 5ms) */
    for (int i = 0; i < BKP_DATA0; i++)
    {
        systick_delay_ms(5);
    }
    /* Turn off LED2 */
    GPIO_Set_Output_Data_Bits(LED2_PORT, LED2_PIN, Bit_RESET);
    /* Delay a little time. time = (BKP_DATA0 * 5ms) */
    for (int i = 0; i < BKP_DATA0; i++)
    {
        systick_delay_ms(5);
    }
    /* Disable read and write of BKP */
    BKP_Write_And_Read_Enable(FALSE);
    /* MCU reset, but bkp area will not reset */
    asm("RESET");

    while (1)
    {}
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  File: pointer to the source file name
                Line: assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
