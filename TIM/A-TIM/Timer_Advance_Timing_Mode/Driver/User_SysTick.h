/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_SysTick.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                         for User_systick
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_SYSTICK_H_
#define USER_SYSTICK_H_

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/
void TimingDelay_Decrement(void);
void Systick_Delay(uint32_t delayTime);

uint8_t SysTick_Config(uint8_t Frq, uint16_t Reload_ms);

#endif /* USER_SYSTICK_H_ */
