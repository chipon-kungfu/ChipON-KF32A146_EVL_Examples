/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_ATIM.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides template for Advanced timer.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_ADVANCE_TIMER_H_
#define USER_ADVANCE_TIMER_H_

/* Timer work mode typedef */
typedef enum TIM_MODE
{
    TIM_Timer = 0x1U,
    TIM_Count,
} Time_Mode_t;

/* Timer initialise configuration typedef */
typedef struct TIM_CONFIG
{
    /**Timer work mode*/
    Time_Mode_t mode;
    /**Timer count manner*/
    uint32_t countMode;
} Tim_Config_t;

/* Timer interrupt configuration typedef */
typedef struct TIM_INT_CONFIG
{
    /**Interrupt enable status*/
    FunctionalState newState;
    /**Interrupt index of timer*/
    InterruptIndex timerIntIndex;
    uint8_t        preemption;
    uint8_t        subPriority;
} Tim_Int_Config_t;

/*******************************************************************************
**                                 Macro Defines                              **
*******************************************************************************/

/**
 * @brief: Configure ATIM.
 *
 * @param ATIMx: Pointer to ATIM register structure.
 * @param config: Pointer to ATIM config strucre.
 *              ->mode: Mode of ATIM
 *                  TIM_Timer or TIM_Count
 *              ->countMode: Mode of count
 *                  ATIM_COUNT_DOWN_UF
 *                  ATIM_COUNT_UP_OF
 *                  ATIM_COUNT_UP_DOWN_OF
 *                  ATIM_COUNT_UP_DOWN_UF
 *                  ATIM_COUNT_UP_DOWN_OUF
 * @param period: Timer period or counter cycle.
 *          Timer period: (for TIMER MODE only)
 *          ==============================================
 *             scope          |           stepper/us
 *          ------------------+---------------------------
 *             0-65000        |           1
 *             65001-650000   |           10
 *             650001-6500000 |           100
 *          ==============================================
 *          counter cycle: (for COUNT MODE only)
 *          It should be less than 65535.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
/* Max delay time in us: 6500000 */
#define ATIM_Init_us(ATIMx, config, period) ATIM_Init((ATIMx), (config), (period))
/* Max delay time in ms: 6500 */
#define ATIM_Init_ms(ATIMx, config, period) ATIM_Init((ATIMx), (config), (period)*1000u)

/*******************************************************************************
**                      	Global Functions 		             	      	  **
*******************************************************************************/

extern uint32_t ATIM_Init(ATIM_SFRmap *ATIMx, Tim_Config_t *config, uint32_t period);
extern void     ATIM_INT_Config(ATIM_SFRmap *ATIMx, Tim_Int_Config_t *intConfig);
extern void     ATIM_Gpio_Init(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t PinRemap);

#endif /* USER_ADVANCE_TIMER_H_ */
