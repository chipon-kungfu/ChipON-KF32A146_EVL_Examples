/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by advance timer.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "User_ATIM.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                     			main Functions 		             	     	  **
*******************************************************************************/
int main()
{
    /*initialize system clock,default SCLK is 72MHz,select INTHF as Clock source*/
    SystemInit(72U);
    systick_delay_init(72U);
    /**LED1*/
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    /**LED2*/
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);

    /**
     * Initialize Timer 5 and configuration Timer 5 interrupt
     * Set T5 to timing mode, counting up, period is 100ms.
     */
    {
        Tim_Config_t T5_Config = {
          .mode      = TIM_Timer,
          .countMode = ATIM_COUNT_UP_OF,
        };
        Tim_Int_Config_t T5_Int_Config = {
          .newState      = TRUE,
          .timerIntIndex = INT_T5,
          .preemption    = 4U,
          .subPriority   = 0U,
        };

        ATIM_Init_ms(T5_SFR, &T5_Config, 100U);
        ATIM_INT_Config(T5_SFR, &T5_Int_Config);
    }

    INT_All_Enable(TRUE);
    while (1)
    {
        systick_delay_ms(250U);
        systick_delay_ms(250U);
        /*Toggle LED2*/
        GPIO_Toggle_Output_Data_Config(PF11_LED2_PIN);
    }
}
/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
