/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_ATIM.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides template for Advanced timer.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "User_ATIM.h"

/*******************************************************************************
**                      	Private Functions 		             	      	  **
*******************************************************************************/
static uint32_t ATIM_Set_Timer_Parameter(ATIM_SFRmap *ATIMx, uint32_t timerClock, uint32_t cycle);

/**
 * @brief: Calculate ATIM prescaler and period.
 *
 * @param ATIMx: Pointer to ATIM register structure.
 * @param timerClock: GPTIM work clock source(MHz).
 * @param cycle: Timer period.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
static uint32_t ATIM_Set_Timer_Parameter(ATIM_SFRmap *ATIMx, uint32_t timerClock, uint32_t cycle)
{
    uint32_t tmpPrescaler = 0U;
    uint32_t tmpPeriod    = 0U;
    if (((cycle > 65000U) && ((cycle % 10U) != 0U)) || ((cycle > 650000U) && ((cycle % 100U) != 0U)) ||
        (cycle > 6500000U) || (timerClock < 1U))
    {
        return 1U;
    }
    else
    {
        /* Empty */
    }
    if (cycle <= 65000U)
    {
        tmpPrescaler = timerClock;
        tmpPeriod    = cycle;
    }
    else if (cycle <= 650000U)
    {
        tmpPrescaler = timerClock * 10U;
        tmpPeriod    = cycle / 10U;
    }
    else
    {
        tmpPrescaler = timerClock * 100U;
        tmpPeriod    = cycle / 100U;
    }

    /* Set timer period */
    ATIM_X_Set_Period(ATIMx, tmpPeriod);
    /* Set timer prescaler */
    ATIM_X_Set_Prescaler(ATIMx, (tmpPrescaler - 1U));
    ATIM_X_Postscaler_Config(ATIMx, ATIM_POSTSCALER_DIV_1);

    return 0u;
}

/*******************************************************************************
**                      	Global Functions 		             	      	  **
*******************************************************************************/

/**
 * @brief: Configure ATIM.
 *          In defualt, ATIM use HFCLK as work clock source.
 *          If you want to use other clock source you NEED TO
 *              - modify entrance parameter $NewClock$ of
 *                $ATIM_Clock_Config()$.
 *              - modify entrance parameter $timerClock$ of
 *                $ATIM_Set_Timer_Parameter()$, in MHz.
 *
 * @param ATIMx: Pointer to ATIM register structure.
 * @param config: Pointer to ATIM config strucre.
 *              ->mode: Mode of ATIM
 *                  TIM_Timer or TIM_Count
 *              ->countMode: Mode of count
 *                  ATIM_COUNT_DOWN_UF
 *                  ATIM_COUNT_UP_OF
 *                  ATIM_COUNT_UP_DOWN_OF
 *                  ATIM_COUNT_UP_DOWN_UF
 *                  ATIM_COUNT_UP_DOWN_OUF
 * @param period: Timer period or counter cycle.
 *          Timer period: (for TIMER MODE only)
 *          ==============================================
 *             scope          |           stepper/us
 *          ------------------+---------------------------
 *             0-65000        |           1
 *             65001-650000   |           10
 *             650001-6500000 |           100
 *          ==============================================
 *          counter cycle: (for COUNT MODE only)
 *          It should be less than 65535.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
uint32_t ATIM_Init(ATIM_SFRmap *ATIMx, Tim_Config_t *config, uint32_t period)
{
    if((ATIMx == NULL) || (config == NULL) || (period == 0U))
    {
        return 1U;
    }
    else
    {
        /* Empty */
    }

    /* Reset timer and enable peripheral clock */
    TIM_Reset(ATIMx);
    /* Configure timer to update immediately */
    ATIM_X_Updata_Immediately_Config(ATIMx, TRUE);
    /* Enable timer update the period, duty cycle, counter etc. */
    ATIM_X_Updata_Enable(ATIMx, TRUE);

    if (TIM_Timer == config->mode)
    {
        /* Timer Mode */

        /*Select internal high frequency as clock source*/
        ATIM_X_Clock_Config(ATIMx, ATIM_HFCLK);
        /* Configure timer work mode to timing mode */
        ATIM_X_Work_Mode_Config(ATIMx, ATIM_TIMER_MODE);
        /* Set prescaler, period, count */
        ATIM_X_Set_Counter(ATIMx, 0u);
        if (0u != ATIM_Set_Timer_Parameter(ATIMx, 16U, period))
        {
            return 1U;
        }
        else
        {
            /* Empty */
        }
        /* Set timer count up and overflow */
        ATIM_X_Counter_Mode_Config(ATIMx, config->countMode);
    }
    else
    {
        /* Count Mode */

        /* Configure timer work mode to timing mode */
        ATIM_X_Work_Mode_Config(ATIMx, ATIM_COUNTER_MODE);
        /* Set timer counter to zero */
        ATIM_X_Set_Counter(ATIMx, 0u);
        /* Set timer period */
        ATIM_X_Set_Period(ATIMx, period - 1U);
        /* Set timer prescaler */
        ATIM_X_Set_Prescaler(ATIMx, 0u);
        ATIM_X_Postscaler_Config(ATIMx, ATIM_POSTSCALER_DIV_1);
        /* Set timer count up and overflow */
        ATIM_X_Counter_Mode_Config(ATIMx, config->countMode);
        /* Forbidden using timer slave mode */
        ATIM_X_Slave_Mode_Config(ATIMx, ATIM_SLAVE_FORBIDDEN_MODE);
        /* Triggered whether external pluse are synchronized with clock or not
         */
        ATIM_X_External_Pulse_Sync_Config(ATIMx, ATIM_NO_SYNC_MODE);
    }
    /* Enable timer */
    ATIM_X_Cmd(ATIMx, TRUE);

    return 0u;
}

/**
 * @brief: Configure interrupt priority, enable overflow interrupt.
 *
 * @param ATIMx: Pointer to ATIM register structure.
 * @param intConfig: Pointer to BTIM interrupt config strucre.
 *              ->newState: The status of interrupt.
 *              ->timerIntIndex: The index of interrupt. Such as: INT_T6.
 *              ->preemption: Preemption of interrupt.
 *              ->subPriority:Subpriority of interrupt.
 * @retval: None
 */
void ATIM_INT_Config(ATIM_SFRmap *ATIMx, Tim_Int_Config_t *intConfig)
{
    if((ATIMx == NULL) || (intConfig == NULL))
    {
        return;
    }
    else
    {
        /* Empty */
    }
    
    if (FALSE == intConfig->newState)
    {
        /* Enable timer overflow interrupt */
        ATIM_X_Overflow_INT_Enable(ATIMx, FALSE);
        /* Enable timer interrupt */
        INT_Interrupt_Enable(intConfig->timerIntIndex, FALSE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(intConfig->timerIntIndex);
    }
    else
    {
        /* Set timer interrupt priority */
        INT_Interrupt_Priority_Config(intConfig->timerIntIndex, intConfig->preemption, intConfig->subPriority);
        /* Enable timer overflow interrupt */
        ATIM_X_Overflow_INT_Enable(ATIMx, TRUE);
        /* Enable timer interrupt */
        INT_Interrupt_Enable(intConfig->timerIntIndex, TRUE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(intConfig->timerIntIndex);

        /* Confirm timer is ENABLE */
        if (0u == (ATIMx->TZCTL & ATIM_TZCTL_TZEN))
        {
            ATIM_X_Cmd(ATIMx, TRUE);
        }
        else
        {
            /* Empty */
        }
    }
}

/**
 * @brief: Initialize TxCK pin of  advance timer.
 *
 * @param GPIOx: The pointer to GPIO Port
 * @param GPIO_Pin: Pin number of TxCK
 * @param PinRemap: Pin remap mode
 * @retval: void
 */
void ATIM_Gpio_Init(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t PinRemap)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.m_Mode      = GPIO_MODE_RMP;
    GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
    GPIO_InitStructure.m_PullDown  = GPIO_PULLDOWN;
    GPIO_InitStructure.m_PullUp    = GPIO_NOPULL;
    GPIO_InitStructure.m_Speed     = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Pin       = ((uint32_t)1U << (uint32_t)GPIO_Pin);
    GPIO_Configuration(GPIOx, &GPIO_InitStructure);

    GPIO_Pin_RMP_Config(GPIOx, GPIO_Pin, PinRemap);
}
