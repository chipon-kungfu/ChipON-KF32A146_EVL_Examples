/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by advance timer.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "User_ATIM.h"
#include "Usart.h"
#include "Board_GpioCfg.h"
#include <stdio.h>

/*******************************************************************************
**                               Public variables                             **
*******************************************************************************/
uint16_t ATimerCounter  = 0;
uint8_t  USART_Buf[128] = {0};

/*******************************************************************************
**                     			main Functions 		             	     	  **
*******************************************************************************/
int main()
{
    volatile uint16_t TmpATimerCounter = 0;

    /*initialize system clock,default SCLK is 72MHz,select INTHF as Clock source*/
    SystemInit(72U);
    systick_delay_init(72U);
    /**LED1*/
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    /**LED2*/
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);

    /**
     * Initialize Timer 5 and configuration Timer 6 interrupt
     * Set T5 to count mode, counting up, MAX. count number is 500.
     */
    ATIM_Gpio_Init(PA5_T5CK_AF);
    {
        Tim_Config_t T5_Config = {
          .mode      = TIM_Count,
          .countMode = ATIM_COUNT_UP_OF,
        };
        Tim_Int_Config_t T5_Int_Config = {
          .newState      = TRUE,
          .timerIntIndex = INT_T5,
          .preemption    = 4U,
          .subPriority   = 0U,
        };

        ATIM_Init(T5_SFR, &T5_Config, 950U);
        ATIM_INT_Config(T5_SFR, &T5_Int_Config);
    }
    /**
     * The initial value of CNT of ATIM is random,
     * so need to read value of CNT as the initial value of $ATimerCounter$.
     */
    ATimerCounter = T5_SFR->TXCNT;

    USART_TxGpio_Config(PC12_USART2_TX_AF);
    USART_RxGpio_Config(PC13_USART2_RX_AF);
    USART_Mode_Config(USART2_SFR, 115200U);
    systick_delay_ms(5U);
    USART_Send(USART2_SFR, "POWER ON!\r\n", strlen("POWER ON!\r\n"));

    INT_All_Enable(TRUE);
    TmpATimerCounter = ATimerCounter;

    while (1)
    {
        systick_delay_ms(250U);
        systick_delay_ms(250U);

        GPIO_Toggle_Output_Data_Config(PF11_LED2_PIN);
        TmpATimerCounter = T5_SFR->TXCNT;
        if (ATimerCounter != TmpATimerCounter)
        {
            ATimerCounter = TmpATimerCounter;
            sprintf(USART_Buf, "CNT: 0x%x, =%d\r\n", ATimerCounter, ATimerCounter);
            USART_Send(USART2_SFR, USART_Buf, strlen(USART_Buf));
        }
    }
}
/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param file pointer to the source file name
 * 	@param line assert_param error line source number
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
