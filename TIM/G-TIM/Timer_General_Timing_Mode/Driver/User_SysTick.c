/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_SysTick.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides provides template for
 *                      system tick
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
#include "User_SysTick.h"

/*******************************************************************************
**                      	Private Variables Definitions                  	  **
*******************************************************************************/
volatile static uint32_t TimingDelay;

/*******************************************************************************
**                      	Global Functions 		             	      	  **
*******************************************************************************/
/**
 * @brief  Decrements the TimingDelay variable.
 * @retval void
 */
void TimingDelay_Decrement(void)
{
    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }
}

/**
 *  @brief :Inserts a delay time.
 *  @param delayTime: specifies the delay time length, in milliseconds.
 *  @retval :void
 */
void Systick_Delay(uint32_t delayTime)
{
    TimingDelay = delayTime;
    while (TimingDelay != 0)
        ;
}

/**
 * @brief: Initialize system tick timer.
 *
 * @param Frq: SCLK(System clock), in MHz.
 * @param Reload_ms: Tick time, in ms.
 * @retval uint32_t
 *          0 -- success
 *          1 -- failure
 */
uint8_t SysTick_Config(uint8_t Frq, uint16_t Reload_ms)
{
    uint32_t Reload;

    Reload = Frq * Reload_ms * 500U;
    if (Reload > 0xffffffU)
    {
        return 1U;
    }
    else
    {}

    /*before initializtion,disable systick timer*/
    SYSTICK_Cmd(FALSE);
    INT_Interrupt_Enable(INT_SysTick, FALSE);
    /*write ST_CV any valer,clear ST_CV*/
    SYSTICK_Counter_Updata();
    /*select SCLK as the clock source of the system tick timer */
    SYSTICK_Clock_Config(SYSTICK_SYS_CLOCK_DIV_2);
    /**Set Reload value in ms*/
    SYSTICK_Reload_Config(Reload - 1U);
    /*enable system timer IRQ&interrupt flag*/
    SYSTICK_Systick_INT_Enable(TRUE);
    /*Configuration system timer Interrupt Priority*/
    INT_Interrupt_Priority_Config(INT_SysTick, 2, 0);
    /*enable systick timer*/
    SYSTICK_Cmd(TRUE);
    /*enable system timer Interrupt*/
    INT_Interrupt_Enable(INT_SysTick, TRUE);
    INT_Clear_Interrupt_Flag(INT_SysTick);

    return 0U;
}