/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_GPTIM.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides template for T18.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_GENERAL_TIMER_H_
#define USER_GENERAL_TIMER_H_

/* Timer work mode typedef */
typedef enum TIM_MODE
{
    TIM_Timer = 0x1U,
    TIM_Count,
} Time_Mode_t;

/* Timer initialise configuration typedef */
typedef struct TIM_CONFIG
{
    /**Timer work mode*/
    Time_Mode_t mode;
    /**Timer count manner*/
    uint32_t countMode;
} Tim_Config_t;

/* Timer interrupt configuration typedef */
typedef struct TIM_INT_CONFIG
{
    /**Interrupt enable status*/
    FunctionalState newState;
    /**Interrupt index of timer*/
    InterruptIndex timerIntIndex;
    uint8_t        preemption;
    uint8_t        subPriority;
} Tim_Int_Config_t;
/*******************************************************************************
**                                 Macro Defines                              **
*******************************************************************************/

/**
 * @brief: Configure GPTIM.
 *
 * @param in: GPTIMx: Pointer to GPTIM register structure.
 * @param in: config: Pointer to GPTIM config strucre.
 *              ->mode: Mode of GPTIM
 *                  TIM_Timer or TIM_Count
 *              ->countMode: Mode of count
 *                  GPTIM_COUNT_DOWN_UF
 *                  GPTIM_COUNT_UP_OF
 *                  GPTIM_COUNT_UP_DOWN_OF
 *                  GPTIM_COUNT_UP_DOWN_UF
 *                  GPTIM_COUNT_UP_DOWN_OUF
 * @param in: period: Timer period or counter cycle.
 *          Timer period: (for TIMER MODE only)
 *          ==============================================
 *             scope          |           stepper/us
 *          ------------------+---------------------------
 *             0-65000        |           1
 *             65001-650000   |           10
 *             650001-6500000 |           100
 *          ==============================================
 *          counter cycle: (for COUNT MODE only)
 *          It should be less than 65535.
 * @param out: None
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
/* Max delay time in us: 6500000 */
#define GPTIM_Init_us(GPTIMx, config, period) GPTIM_Init((GPTIMx), (config), (period))
/* Max delay time in ms: 6500 */
#define GPTIM_Init_ms(GPTIMx, config, period) GPTIM_Init((GPTIMx), (config), (period)*1000u)

/*******************************************************************************
**                      	Global Functions 		             	      	  **
*******************************************************************************/

extern uint32_t GPTIM_Init(GPTIM_SFRmap *GPTIMx, Tim_Config_t *config, uint32_t period);

extern void GPTIM_INT_Config(GPTIM_SFRmap *GPTIMx, Tim_Int_Config_t *intConfig);

extern void GPTIM_Gpio_Init(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t PinRemap);

#endif /* USER_GENERAL_TIMER_H_ */
