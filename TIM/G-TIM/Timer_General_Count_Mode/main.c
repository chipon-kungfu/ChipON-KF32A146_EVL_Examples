/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by general timer.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "User_GPTIM.h"
#include "Usart.h"
#include "Board_GpioCfg.h"
#include <stdio.h>

/*******************************************************************************
**                              Private variables                             **
*******************************************************************************/
uint16_t        GTimerCounter  = 0;
uint8_t         USART_Buf[128] = {0};
static uint32_t DelayCount     = 0;

/*******************************************************************************
**                               Public variables                             **
*******************************************************************************/

/*******************************************************************************
**                     			main Functions 		             	     	  **
*******************************************************************************/
int main()
{
    uint16_t TmpGTimerCounter = 0;

    /*initialize system clock,defaut SCLK is 72MHz,select INTHF as Clock source*/
    SystemInit(72U);
    systick_delay_init(72U);
    /**LED1*/
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    /**LED2*/
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);

    /**
     * Initialize Timer 19 and configuration Timer 19 interrupt
     * Set T19 to count mode, counting up, MAX. count number is 950.
     */
    GPTIM_Gpio_Init(PA11_T19CK_AF);
    {
        Tim_Config_t T19_Config = {
          .mode      = TIM_Count,
          .countMode = GPTIM_COUNT_UP_OF,
        };
        Tim_Int_Config_t T19_Int_Config = {
          .newState      = TRUE,
          .timerIntIndex = INT_T19,
          .preemption    = 4U,
          .subPriority   = 0U,
        };

        GPTIM_Init(T19_SFR, &T19_Config, 950U);
        GPTIM_INT_Config(T19_SFR, &T19_Int_Config);
    }
    /**
     * The initial value of CNT of GPTIM is random,
     * so need to read value of CNT as the initial value of $GPtimerCounter$.
     */
    GTimerCounter = T19_SFR->CNT;

    USART_TxGpio_Config(PC12_USART2_TX_AF);
    USART_RxGpio_Config(PC13_USART2_RX_AF);
    USART_Mode_Config(USART2_SFR, 115200U);
    systick_delay_ms(5U);
    USART_Send(USART2_SFR, "POWER ON!\r\n", strlen("POWER ON!\r\n"));

    INT_All_Enable(TRUE);
    TmpGTimerCounter = GTimerCounter;
    while (1)
    {
        systick_delay_ms(250U);
        systick_delay_ms(250U);

        GPIO_Toggle_Output_Data_Config(PF11_LED2_PIN);
        TmpGTimerCounter = T19_SFR->CNT;
        if (GTimerCounter != TmpGTimerCounter)
        {
            GTimerCounter = TmpGTimerCounter;
            sprintf(USART_Buf, "CNT: 0x%x, =%d\r\n", GTimerCounter, GTimerCounter);
            USART_Send(USART2_SFR, USART_Buf, strlen(USART_Buf));
        }
    }
}
/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
