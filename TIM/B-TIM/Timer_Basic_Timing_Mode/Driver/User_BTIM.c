/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_BTIM.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides template for basic timer.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "User_BTIM.h"

/*******************************************************************************
**                              Private Functions                             **
*******************************************************************************/
/**
 * @brief: Calculate BTIM prescaler and period.
 *
 * @param BTIMx: Pointer to BTIM register structure.
 * @param timerClock: GPTIM work clock source(MHz).
 * @param cycle: Timer period.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
static uint32_t BTIM_Set_Timer_Parameter(BTIM_SFRmap *BTIMx, uint32_t timerClock, uint32_t cycle)
{
    uint32_t tmpPrescaler = 0U;
    uint32_t tmpPeriod    = 0U;
    if (((cycle > 65000U) && ((cycle % 10U) != 0U)) || ((cycle > 650000U) && ((cycle % 100U) != 0U)) ||
        (cycle > 6500000U) || (timerClock < 1U))
    {
        return 1U;
    }
    else
    {
        /* Empty */
    }
    if (cycle <= 65000U)
    {
        tmpPrescaler = timerClock;
        tmpPeriod    = cycle;
    }
    else if (cycle <= 650000U)
    {
        tmpPrescaler = timerClock * 10U;
        tmpPeriod    = cycle / 10U;
    }
    else
    {
        tmpPrescaler = timerClock * 100U;
        tmpPeriod    = cycle / 100U;
    }

    /* Set timer period */
    BTIM_Set_Period(BTIMx, tmpPeriod);
    /* Set timer prescaler */
    BTIM_Set_Prescaler(BTIMx, (tmpPrescaler - 1U));

    return 0U;
}

/*******************************************************************************
**                              Public Functions                              **
*******************************************************************************/

/**
 * @brief: Configure BTIM.
 *          In defualt, BTIM use HFCLK as work clock source.
 *          If you want to use other clock source you NEED TO
 *              - modify entrance parameter $NewClock$ of
 *                $BTIM_Clock_Config()$.
 *              - modify entrance parameter $timerClock$ of
 *                $BTIM_Set_Timer_Parameter()$, in MHz.
 *
 * @param BTIMx: Pointer to BTIM register structure.
 * @param config: Pointer to BTIM config strucre.
 *              ->mode: Mode of BTIM
 *                  TIM_Timer or TIM_Count
 *              ->countMode: Mode of count
 *                  BTIM_COUNT_DOWN_UF
 *                  BTIM_COUNT_UP_OF
 *                  BTIM_COUNT_UP_DOWN_OF
 *                  BTIM_COUNT_UP_DOWN_UF
 *                  BTIM_COUNT_UP_DOWN_OUF
 * @param period: Timer period or counter cycle.
 *          Timer period: (for TIMER MODE only)
 *          ==============================================
 *             scope          |           stepper/us
 *          ------------------+---------------------------
 *             0-65000        |           1
 *             65001-650000   |           10
 *             650001-6500000 |           100
 *          ==============================================
 *          counter cycle: (for COUNT MODE only)
 *          It should be less than 65535.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
uint32_t BTIM_Init(BTIM_SFRmap *BTIMx, Tim_Config_t *config, uint32_t period)
{
    if((BTIMx == NULL) || (config == NULL) || (period == 0U))
    {
        return 1U;
    }
    else
    {
        /* Empty */
    }

    /* Reset timer and enable peripheral clock */
    TIM_Reset(BTIMx);
    /* Configure timer to update immediately */
    BTIM_Updata_Immediately_Config(BTIMx, FALSE);
    /* Enable timer update the period, duty cycle, counter etc. */
    BTIM_Updata_Enable(BTIMx, TRUE);

    if (TIM_Timer == config->mode)
    {
        /* Timer Mode */

        /*Select internal high frequency as clock source*/
        BTIM_Clock_Config(BTIMx, BTIM_HFCLK);
        /* Configure timer work mode to timing mode */
        BTIM_Work_Mode_Config(BTIMx, BTIM_TIMER_MODE);
        /* Set prescaler, period */
        if (0u != BTIM_Set_Timer_Parameter(BTIMx, 16U, period))
        {
            return 1U;
        }
        else
        {
            /* Empty */
        }
    }
    else
    {
        /* Count Mode */

        /* Configure timer work mode to timing mode */
        BTIM_Work_Mode_Config(BTIMx, BTIM_COUNTER_MODE);
        /* Set timer period */
        BTIM_Set_Period(BTIMx, period - 1U);
        /* Set prescaler factor of timer */
        BTIM_Set_Prescaler(BTIMx, 0);
        /* Forbidden using timer slave mode */
        BTIM_Slave_Mode_Config(BTIMx, BTIM_SLAVE_FORBIDDEN_MODE);
        /* Triggered whether external pluse are synchronized with clock or not */
        BTIM_External_Pulse_Sync_Config(BTIMx, BTIM_NO_SYNC_MODE);
    }

    /* Set timer counter to zero */
    BTIM_Set_Counter(BTIMx, 0);
    /* Set timer count up and overflow */
    BTIM_Counter_Mode_Config(BTIMx, config->countMode);

    /* Enable timer */
    BTIM_Cmd(BTIMx, TRUE);

    return 0U;
}

/**
 * @brief: Configure interrupt priority, enable overflow interrupt.
 *
 * @param BTIMx: Pointer to BTIM register structure.
 * @param intConfig: Pointer to BTIM interrupt config strucre.
 *              ->newState: The status of interrupt.
 *              ->timerIntIndex: The index of interrupt. Such as: INT_T14.
 *              ->preemption: Preemption of interrupt.
 *              ->subPriority:Subpriority of interrupt.
 * @retval: None
 */
void BTIM_INT_Config(BTIM_SFRmap *BTIMx, Tim_Int_Config_t *intConfig)
{
    if((BTIMx == NULL) || (intConfig == NULL))
    {
        return;
    }
    else
    {
        /* Empty */
    }

    if (FALSE == intConfig->newState)
    {
        /* Enable timer overflow interrupt */
        BTIM_Overflow_INT_Enable(BTIMx, FALSE);
        /* Enable timer interrupt */
        INT_Interrupt_Enable(intConfig->timerIntIndex, FALSE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(intConfig->timerIntIndex);
    }
    else
    {
        /* Set timer interrupt priority */
        INT_Interrupt_Priority_Config(intConfig->timerIntIndex, intConfig->preemption, intConfig->subPriority);
        /* Enable timer overflow interrupt */
        BTIM_Overflow_INT_Enable(BTIMx, TRUE);
        /* Enable timer interrupt */
        INT_Interrupt_Enable(intConfig->timerIntIndex, TRUE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(intConfig->timerIntIndex);

        /* Confirm BTIM is ENABLE */
        if (0u == (BTIMx->CTL1 & BTIM_CTL1_TXEN))
        {
            BTIM_Cmd(BTIMx, TRUE);
        }
        else
        {
            /* Empty */
        }
    }
}

/**
 *  @brief : Initialize TxCK pin of  Basic timer.
 *
 *  @retval :void
 */
void Basic_T14_Gpio_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.m_Mode      = GPIO_MODE_RMP;
    GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
    GPIO_InitStructure.m_PullDown  = GPIO_NOPULL;
    GPIO_InitStructure.m_PullUp    = GPIO_NOPULL;
    GPIO_InitStructure.m_Speed     = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Pin       = GPIO_PIN_MASK_2;
    GPIO_Configuration(GPIOG_SFR, &GPIO_InitStructure);

    GPIO_Pin_RMP_Config(GPIOG_SFR, GPIO_Pin_Num_2, GPIO_RMP_AF12);
}
