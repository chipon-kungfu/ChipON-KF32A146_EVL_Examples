/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_BTIM.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides template for basic timer.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_BASIC_TIMER_H_
#define USER_BASIC_TIMER_H_

typedef enum TIM_MODE
{
    TIM_Timer = 0x1U,
    TIM_Count,
} Time_Mode_t;

/* Timer initialise configuration typedef */
typedef struct TIM_CONFIG
{
    /**Timer work mode*/
    Time_Mode_t mode;
    /**Timer count manner*/
    uint32_t countMode;
} Tim_Config_t;

/* Timer interrupt configuration typedef */
typedef struct TIM_INT_CONFIG
{
    /**Interrupt enable status*/
    FunctionalState newState;
    /**Interrupt index of timer*/
    InterruptIndex timerIntIndex;
    uint8_t        preemption;
    uint8_t        subPriority;
} Tim_Int_Config_t;

/*******************************************************************************
**                                 Macro Defines                              **
*******************************************************************************/
/**
 * @brief: Configure BTIM.
 *          In defualt, BTIM use HFCLK as work clock source.
 *          If you want to use other clock source you NEED TO
 *              - modify entrance parameter $NewClock$ of
 *                $BTIM_Clock_Config()$.
 *              - modify entrance parameter $timerClock$ of
 *                $BTIM_Set_Timer_Parameter()$, in MHz.
 *
 * @param BTIMx: Pointer to BTIM register structure.
 * @param config: Pointer to BTIM config strucre.
 *              ->mode: Mode of BTIM
 *                  TIM_Timer or TIM_Count
 *              ->countMode: Mode of count
 *                  BTIM_COUNT_DOWN_UF
 *                  BTIM_COUNT_UP_OF
 *                  BTIM_COUNT_UP_DOWN_OF
 *                  BTIM_COUNT_UP_DOWN_UF
 *                  BTIM_COUNT_UP_DOWN_OUF
 * @param period: Timer period or counter cycle.
 *          Timer period: (for TIMER MODE only)
 *          ==============================================
 *             scope          |           stepper/us
 *          ------------------+---------------------------
 *             0-65000        |           1
 *             65001-650000   |           10
 *             650001-6500000 |           100
 *          ==============================================
 *          counter cycle: (for COUNT MODE only)
 *          It should be less than 65535.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
/* Max delay time in us: 6500000 */
#define BTIM_Init_us(BTIMx, config, period) BTIM_Init((BTIMx), (config), (period))
/* Max delay time in ms: 6500 */
#define BTIM_Init_ms(BTIMx, config, period) BTIM_Init((BTIMx), (config), (period)*1000u)

/*******************************************************************************
**                      	Global Functions 		             	      	  **
*******************************************************************************/

extern uint32_t BTIM_Init(BTIM_SFRmap *BTIMx, Tim_Config_t *config, uint32_t period);
extern void     BTIM_INT_Config(BTIM_SFRmap *BTIMx, Tim_Int_Config_t *intConfig);
extern void     Basic_T14_Gpio_Init(void);

#endif /* USER_BASIC_TIMER_H_ */
