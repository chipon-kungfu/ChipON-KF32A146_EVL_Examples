/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Board_GpioCfg.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This article provides configuration functions for gpio
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef BOARD_GPIO_DEFINE_
#define BOARD_GPIO_DEFINE_

/******************************************************************************
**                             Include Files
******************************************************************************/
#include "system_init.h"

/*****************************************************************************
**                         Private Macro Definitions
*****************************************************************************/

#define PA3_LED1_PIN  GPIOA_SFR, GPIO_PIN_MASK_3
#define PF11_LED2_PIN GPIOF_SFR, GPIO_PIN_MASK_11
#define PD4_WKUP3_PIN GPIOD_SFR, GPIO_PIN_MASK_4
#define PD4_KEY2_PIN  GPIOD_SFR, GPIO_PIN_MASK_4

/**==================== USART-START ====================*/
#define PC13_USART2_RX_AF GPIOC_SFR, GPIO_PIN_MASK_13, GPIO_RMP_AF3
#define PC12_USART2_TX_AF GPIOC_SFR, GPIO_PIN_MASK_12, GPIO_RMP_AF3
/**==================== USART-END ====================*/

/**==================== LIN-START ====================*/
#define PG9_LIN1_RX_PIN GPIOG_SFR, GPIO_PIN_MASK_9
#define PG9_LIN1_RX_AF  GPIOG_SFR, GPIO_Pin_Num_9, GPIO_RMP_AF8
#define PC2_LIN1_TX_PIN GPIOC_SFR, GPIO_PIN_MASK_2
#define PC2_LIN1_TX_AF  GPIOC_SFR, GPIO_Pin_Num_2, GPIO_RMP_AF8
/**==================== LIN-END ====================*/

/**==================== ADC-START ====================*/
#define PE6_ADC_PIN      GPIOE_SFR, GPIO_PIN_MASK_6
#define PE6_ADC_CHANNAL  ADC_CHANNEL_58
#define PA13_ADC_PIN     GPIOA_SFR, GPIO_PIN_MASK_13
#define PA13_ADC_CHANNAL ADC_CHANNEL_72

#define EXAMPLE_ADC_PIN     PA13_ADC_PIN
#define EXAMPLE_ADC_CHANNAL PA13_ADC_CHANNAL
/**==================== ADC-END ====================*/

/**==================== CLKOUT-START ====================*/
#define PA1_CLKOUT_PIN GPIOA_SFR, GPIO_PIN_MASK_1
#define PA1_CLKOUT_AF  GPIOA_SFR, GPIO_Pin_Num_1, GPIO_RMP_AF0
/**==================== CLKOUT-END ====================*/

/**==================== SPI-START ====================*/
#define PE0_SPI0_SCK_PIN GPIOE_SFR, GPIO_PIN_MASK_0
#define PE0_SPI0_SCK_AF  GPIOE_SFR, GPIO_Pin_Num_0, GPIO_RMP_AF8
#define PE6_SPI0_SDI_PIN GPIOE_SFR, GPIO_PIN_MASK_6
#define PE6_SPI0_SDI_AF  GPIOE_SFR, GPIO_Pin_Num_6, GPIO_RMP_AF4
#define PB3_SPI0_SDO_PIN GPIOB_SFR, GPIO_PIN_MASK_3
#define PB3_SPI0_SDO_AF  GPIOB_SFR, GPIO_Pin_Num_3, GPIO_RMP_AF4
#define PB4_SPI0_SS0_PIN GPIOB_SFR, GPIO_PIN_MASK_4
#define PB4_SPI0_SS0_AF  GPIOB_SFR, GPIO_Pin_Num_4, GPIO_RMP_AF4
/**==================== SPI-END ====================*/

/**==================== ECCP-START ====================*/
#define PF6_ECCP5_CH1H_PIN  GPIOF_SFR, GPIO_PIN_MASK_6
#define PF6_ECCP5_CH1H_AF   GPIOF_SFR, GPIO_Pin_Num_6, GPIO_RMP_AF11
#define PA2_ECCP5_CH1L_PIN  GPIOA_SFR, GPIO_PIN_MASK_2
#define PA2_ECCP5_CH1L_AF   GPIOA_SFR, GPIO_Pin_Num_2, GPIO_RMP_AF1
#define PA11_ECCP5_CH2H_PIN GPIOA_SFR, GPIO_PIN_MASK_11
#define PA11_ECCP5_CH2H_AF  GPIOA_SFR, GPIO_Pin_Num_11, GPIO_RMP_AF1
#define PA1_ECCP5_CH2L_PIN  GPIOA_SFR, GPIO_PIN_MASK_1
#define PA1_ECCP5_CH2L_AF   GPIOA_SFR, GPIO_Pin_Num_1, GPIO_RMP_AF1
#define PF2_ECCP5_BKIN_PIN  GPIOF_SFR, GPIO_PIN_MASK_2
#define PF2_ECCP5_BKIN_AF   GPIOF_SFR, GPIO_Pin_Num_2, GPIO_RMP_AF8
/**==================== ECCP-END ====================*/

/**==================== CCP-START ====================*/
#define PE10_CCP18_CH1_PIN GPIOE_SFR, GPIO_PIN_MASK_10
#define PE10_CCP18_CH1_AF2 GPIOE_SFR, GPIO_Pin_Num_10, GPIO_RMP_AF2
#define PD6_CCP19_CH1_PIN  GPIOD_SFR, GPIO_PIN_MASK_6
#define PD6_CCP19_CH1_AF2  GPIOD_SFR, GPIO_Pin_Num_6, GPIO_RMP_AF12
/**==================== CCP-END ====================*/

/**==================== I2C-START ====================*/
#define PC0_I2C0_SCL_PIN GPIOC_SFR, GPIO_PIN_MASK_0
#define PC0_I2C0_SCL_AF  GPIOC_SFR, GPIO_Pin_Num_0, GPIO_RMP_AF5

#define PG4_I2C0_SDA_PIN GPIOG_SFR, GPIO_PIN_MASK_4
#define PG4_I2C0_SDA_AF  GPIOG_SFR, GPIO_Pin_Num_4, GPIO_RMP_AF5
/**==================== I2C-END ====================*/

/**==================== TIM-START ====================*/
/**ATIM*/
#define PA5_T5CK_PIN GPIOA_SFR, GPIO_PIN_MASK_5
#define PA5_T5CK_AF  GPIOA_SFR, GPIO_Pin_Num_5, GPIO_RMP_AF1

/**BTIM*/

/**GPTIM*/
#define PA0_T18CK_PIN GPIOA_SFR, GPIO_PIN_MASK_0
#define PA11_T19CK_AF GPIOA_SFR, GPIO_Pin_Num_11, GPIO_RMP_AF2
/**==================== TIM-END ====================*/

#define LED1_PORT (GPIOA_SFR)
#define LED1_PIN  (GPIO_PIN_MASK_3)

#define LED2_PORT (GPIOF_SFR)
#define LED2_PIN  (GPIO_PIN_MASK_11)

#define WAUP3_PORT (GPIOD_SFR)
#define WAUP3_PIN  (GPIO_PIN_MASK_4)

#define KEY2_PORT (GPIOD_SFR)
#define KEY2_PIN  (GPIO_PIN_MASK_4)

/*****************************************************************************
**                         Private Variables Definitions
*****************************************************************************/

/*****************************************************************************
**                             Private Functions
*****************************************************************************/

/*****************************************************************************
**                             Global Functions
*****************************************************************************/

/******************************************************************************
 **                      Functional defineition
 ******************************************************************************/

/**
 * @brief: GPIOx output initialization configuration
 *
 * @param GPIOx Gpio port pointer
 * @param GpioPin Pin number
 * @retval: None
 */
static inline void GPIOInit_Output_Config(GPIO_SFRmap *GPIOx, uint16_t GpioPin)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_Struct_Init(&GPIO_InitStructure);
    GPIO_InitStructure.m_Pin      = GpioPin;
    GPIO_InitStructure.m_Speed    = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Mode     = GPIO_MODE_OUT;
    GPIO_InitStructure.m_PullUp   = GPIO_NOPULL;
    GPIO_InitStructure.m_PullDown = GPIO_NOPULL;
    GPIO_Configuration(GPIOx, &GPIO_InitStructure);

    GPIO_Set_Output_Data_Bits(GPIOx, GpioPin, Bit_RESET);
}

/**
 * @brief: Gpio Input Config
 *
 * @param GPIOx Gpio port pointer
 * @param GpioPin Pin number
 * @retval: None
 */
static inline void GPIOInit_Input_Config(GPIO_SFRmap *GPIOx, uint16_t GpioPin)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_Struct_Init(&GPIO_InitStructure);
    GPIO_InitStructure.m_Pin      = GpioPin;
    GPIO_InitStructure.m_Speed    = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Mode     = GPIO_MODE_IN;
    GPIO_InitStructure.m_PullUp   = GPIO_NOPULL;
    GPIO_InitStructure.m_PullDown = GPIO_NOPULL;
    GPIO_Configuration(GPIOx, &GPIO_InitStructure);
}

#endif
