/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_CCP.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                         for CCP
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/*******************************************************************************
**                               Include Files
*******************************************************************************/
#include "system_init.h"
#include "User_CCP.h"

/*******************************************************************************
**                                 Macro Defines
*******************************************************************************/

/*******************************************************************************
**                               Public variables
*******************************************************************************/
#ifdef CAPTURE_PWM_MEASURE
/* variables for PWM measure */
uint8_t  Status_CalPWM;
uint32_t HighVol_Time;
uint32_t LowVol_Time;
#endif

/*******************************************************************************
**                              Public Functions
*******************************************************************************/

/**
 * @brief: Configure CCP to capture mode.
 *
 * @param CCPx: Pointer to CCP or general timer register structure
 * @param Channel:  Channel of CCP. Value can be as follow:
 *          CCP_CHANNEL_1 - CCP_CHANNEL_4
 * @param EdgeConfig: Capture edge.
 *          CCP_CAP_RISING_EDGE
 *          CCP_CAP_FALLING_EDGE
 *          CCP_CAP_4TH_RISING_EDGE
 *          CCP_CAP_16TH_RISING_EDGE
 * @retval: None
 */
void CCP_Capture_Mode_Init(CCP_SFRmap *const CCPx, const uint32_t Channel, const uint32_t EdgeConfig)
{
    /* Timer peripheral reset, enable peripheral clock */
    TIM_Reset(CCPx);
    /* Set the capture channel, edge */
    CCP_Capture_Mode_Config(CCPx, Channel, EdgeConfig);

    /* Update control. Set update periodically */
    GPTIM_Updata_Immediately_Config(CCPx, FALSE);
    /* Configuration update enable */
    GPTIM_Updata_Enable(CCPx, TRUE);
    /* Timing mode selection */
    GPTIM_Work_Mode_Config(CCPx, GPTIM_TIMER_MODE);
    /* Select SCLK as the timer clock source */
    GPTIM_Clock_Config(CCPx, GPTIM_HFCLK);
    /* Timer count value */
    GPTIM_Set_Counter(CCPx, 0u);
    /* Set timer prescaler, 1us counts once */
    GPTIM_Set_Prescaler(CCPx, 15u);
    /* Up, overflow generates an interrupt flag */
    GPTIM_Counter_Mode_Config(CCPx, GPTIM_COUNT_UP_OF);

    /* Enable general timer */
    GPTIM_Cmd(CCPx, TRUE);
}

/**
 * @brief: Configure CCP interrupt.
 *
 * @param CCPx Pointer to CCP or general timer register structure.
 * @param intConfig Pointer to CCP interrupt config structure.
 *              ->newState: Interrupt enable status. TRUE/FALSE
 *              ->intIndex: Interrupt index number. Example: INT_T18.
 *              ->ccpInterrupt: Interrupt type of CCP.
 *                  CCP_INT_CAPTURE_CH1 -- Capture/Compare interrupt of CCP channel1
 *                  CCP_INT_CAPTURE_CH2 -- Capture/Compare interrupt of CCP channel2
 *                  CCP_INT_CAPTURE_CH3 -- Capture/Compare interrupt of CCP channel3
 *                  CCP_INT_CAPTURE_CH4 -- Capture/Compare interrupt of CCP channel4
 *                  CCP_INT_TRIGER_EVENT -- Tx trigger event interrupt
 *                  CCP_INT_UPDATE_EVENT -- Tx update event interrupt
 *                  You can connect them with "|", if you want to enable more than one ccp interrupt.
 *              ->preemption: preemption of interrupt.
 *              ->subPriority: Sub-Priority of interrupt.
 * @retval: void
 */
void CCP_INT_Config(CCP_SFRmap *const CCPx, const Ccp_IntConfig_t *const intConfig)
{
    if (FALSE == intConfig->newState)
    {
        /* Disable CCP interrupt */
        CCPx->CCPXCTL3 &= (~intConfig->ccpInterrupt);
        INT_Interrupt_Enable(intConfig->intIndex, FALSE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(intConfig->intIndex);
    }
    else
    {
        /* Set timer interrupt priority */
        INT_Interrupt_Priority_Config(intConfig->intIndex, intConfig->preemption, intConfig->subPriority);
        /* Enable CCP interrupt */
        CCPx->CCPXCTL3 |= (intConfig->ccpInterrupt);
        INT_Interrupt_Enable(intConfig->intIndex, TRUE);
        /* Clear timer interrupt flag */
        CCPx->CCPXSRIC |= (intConfig->ccpInterrupt);
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        CCPx->CCPXSRIC &= (~intConfig->ccpInterrupt);
        INT_Clear_Interrupt_Flag(intConfig->intIndex);
    }
}

#ifdef CAPTURE_PWM_MEASURE
/**
 * @brief: Modify CCP capture mode and update timer.
 *
 * @param CCPx Pointer to CCP or general timer register structure
 * @param CCPx_Channel Channel of CCP. Value can be as follow:
 *          CCP_CHANNEL_1 - CCP_CHANNEL_4
 * @param CCPx_CaptureMode CCP capture mode.
 *          CCP_CAP_RISING_EDGE
 *          CCP_CAP_FALLING_EDGE
 *          CCP_CAP_4TH_RISING_EDGE
 *          CCP_CAP_16TH_RISING_EDGE
 * @retval: void
 */
void CCP_Capture_Change_Mode(CCP_SFRmap *CCPx, uint16_t CCPx_Channel, uint16_t CCPx_CaptureMode)
{
    /* set timer counter */
    GPTIM_Set_Counter(CCPx, 0U);
    /* update register immediately */
    GPTIM_Updata_Immediately_Config(CCPx, TRUE);
    /* Disable CCP interrupt */
    CCP_Channel_INT_Config(CCPx, CCPx_Channel, FALSE);
    /* modify CCP capture mode */
    CCP_Capture_Mode_Config(CCPx, CCPx_Channel, CCPx_CaptureMode);
    /* Clear interrupt flag */
    CCP_Clear_Channel_INT_Flag(CCPx, CCPx_Channel);
    /* Enable CCP interrupt */
    CCP_Channel_INT_Config(CCPx, CCPx_Channel, TRUE);
}
#endif

/**
 * @brief: Configure CCPx to compare mode.
 *
 * @param CCPx: Pointer to CCP or general timer register structure
 *
 * @param CompareConfig: Pointer to CCP compare configuratio structure.
 *              ->Channel: CCPx channel will be used for output.
 *                  CCP_CHANNEL_1 -- CCP_CHANNEL_4
 *              ->CompareValue: Comparison value.(0x0 -- 0xffff)
 *              ->EdgeConfig: CCP compare mode output.
 *                  CCP_MODE_RST
 *                  CCP_CMP_TOGGLE_LEVEL
 *                  CCP_CMP_ACTIVE_LEVEL
 *                  CCP_CMP_INACTIVE_LEVEL
 *                  CCP_CMP_SOFT_INT
 *                  CCP_CMP_SPECIAL_EVENT
 *              ->Period: Period of CCP timer.
 * @retval: void
 */
void CCP_Compare_Mode_Init(CCP_SFRmap *CCPx, Ccp_Config_t *CompareConfig)
{
    /* Timer peripheral reset, enable peripheral clock */
    TIM_Reset(CCPx);

    /* Configure CCPx to compare mode, the output level is inverted during comparison match*/
    CCP_Compare_Mode_Config(CCPx, CompareConfig->Channel, CompareConfig->EdgeConfig);

#ifdef CCP_COMPARE_WITH_TXCK
    /* Configure CCPx compare register */
    CCP_Set_Compare_Result(CCPx, CompareConfig->Channel, CompareConfig->CompareValue - 1U);
    /* Configure timer to update immediately */
    GPTIM_Updata_Immediately_Config(CCPx, FALSE);
    /* Enable timer update the period, duty cycle, counter etc. */
    GPTIM_Updata_Enable(CCPx, TRUE);
    /* Configure timer work mode to timing mode */
    GPTIM_Work_Mode_Config(CCPx, GPTIM_COUNTER_MODE);
    /* Set timer period */
    GPTIM_Set_Period(CCPx, CompareConfig->Period - 1U);
    /* Set timer prescaler */
    GPTIM_Set_Prescaler(CCPx, 0u);
    /* Forbidden using timer slave mode */
    GPTIM_Slave_Mode_Config(CCPx, GPTIM_SLAVE_FORBIDDEN_MODE);
    /* Triggered whether external pluse are synchronized with clock or not */
    GPTIM_External_Pulse_Sync_Config(CCPx, GPTIM_NO_SYNC_MODE);
    /* Set timer counter to zero. Update CNT when the first pulse occurs. */
    GPTIM_Set_Counter(CCPx, 0);
    /* Set timer count mode */
    GPTIM_Counter_Mode_Config(CCPx, GPTIM_COUNT_UP_OF);
#else
    /* Configure CCPx compare register */
    CCP_Set_Compare_Result(CCPx, CompareConfig->Channel, CompareConfig->CompareValue);
    /* Update periodically */
    GPTIM_Updata_Rising_Edge_Config(CCPx, FALSE);
    /* Configuration update enable */
    GPTIM_Updata_Enable(CCPx, TRUE);
    /* Timing mode selection */
    GPTIM_Work_Mode_Config(CCPx, GPTIM_TIMER_MODE);
    /* Timer count value */
    GPTIM_Set_Counter(CCPx, 0u);
    /* Timer period value */
    GPTIM_Set_Period(CCPx, CompareConfig->Period);
    /* Timer prescaler value */
    GPTIM_Set_Prescaler(CCPx, 15u);
    /* Up-down counting, overflow and underflow generate interrupt flags */
    GPTIM_Counter_Mode_Config(CCPx, GPTIM_COUNT_UP_OF);
    /* Configure working clock  */
    GPTIM_Clock_Config(CCPx, GPTIM_HFCLK);
#endif

    /* Timer start control enable */
    GPTIM_Cmd(CCPx, TRUE);
}

/**
 * @brief: Update period and duty cycle of CCPx channelx.
 *
 * @param CCPx: Pointer to CCP register structure.
 * @param channel: Channel of CCP.
 *          CCP_CHANNEL_1 - CCP_CHANNEL_4
 * @param newPeriod: The new period of PWM.
 * @param newDuty: The new Duty cycle of PWM.
 * @retval: None
 */
void CCP_PWM_Update_Parameter(CCP_SFRmap *CCPx, uint32_t channel, uint16_t newPeriod, uint16_t newDuty)
{
    /* Update periodically */
    GPTIM_Updata_Immediately_Config(CCPx, FALSE);
    GPTIM_Updata_Rising_Edge_Config(CCPx, FALSE);
    /* Configuration update enable */
    GPTIM_Updata_Enable(CCPx, TRUE);
#if CCP_PWM_WORK == PWM_Edge_Out
    /* Timer period value */
    GPTIM_Set_Period(CCPx, newPeriod - 1u);
#else
    /* Timer period value */
    GPTIM_Set_Period(CCPx, newPeriod);
#endif
    /* PWM duty cycle */
    CCP_Set_Compare_Result(CCPx, channel, newDuty);
}

/**
 * @brief: Configure CCPx to PWM mode and set period and duty cycle.
 *          CAUTION:
 *          In default, CCP generate edge-aligned PWM.
 *          If you want to set center-aligned PWM, you NEED to
 *          - modify macro $CCP_PWM_WORK$ to $PWM_Center_Out$
 *
 * @param CCPx: Pointer to CCP register structure.
 * @param Channel: Channel of CCP.
 *          CCP_CHANNEL_1 - CCP_CHANNEL_4
 * @param Period: The new period of PWM.
 * @param Duty: The new Duty cycle of PWM.
 * @retval: None
 */
void CCP_PWM_Mode_Init(CCP_SFRmap *CCPx, uint32_t Channel, uint16_t Period, uint16_t Duty)
{
    /* Timer peripheral reset, enable peripheral clock */
    TIM_Reset(CCPx);
    /* PWM function channel mode */
    CCP_PWM_Mode_Config(CCPx, Channel, CCP_PWM_MODE);
    /* Configure CCP channel output to control PWM output, high effective */
    CCP_Channel_Output_Control(CCPx, Channel, CCP_CHANNEL_OUTPUT_PWM_ACTIVE);

    /* Update periodically */
    GPTIM_Updata_Immediately_Config(CCPx, FALSE);
    GPTIM_Updata_Rising_Edge_Config(CCPx, FALSE);
    /* Configuration update enable */
    GPTIM_Updata_Enable(CCPx, TRUE);
    /* Timing mode selection */
    GPTIM_Work_Mode_Config(CCPx, GPTIM_TIMER_MODE);
    /* Timer count value */
    GPTIM_Set_Counter(CCPx, 0u);
    /* Timer prescaler value */
    GPTIM_Set_Prescaler(CCPx, 15u);
    /* PWM duty cycle */
    CCP_Set_Compare_Result(CCPx, Channel, Duty);
#if CCP_PWM_WORK == PWM_Edge_Out
    /* Timer period value */
    GPTIM_Set_Period(CCPx, Period - 1u);
    /* Up-counting mode, that is, edge-aligned PWM signal */
    GPTIM_Counter_Mode_Config(CCPx, GPTIM_COUNT_UP_OF);
#else
    /* Timer period value */
    GPTIM_Set_Period(CCPx, Period);
    /* Up and down counting mode, that is, center-aligned PWM signal */
    GPTIM_Counter_Mode_Config(CCPx, GPTIM_COUNT_UP_DOWN_OF);
#endif
    /* Configure working clock */
    GPTIM_Clock_Config(CCPx, GPTIM_HFCLK);
    /* Enable timer */
    GPTIM_Cmd(CCPx, TRUE);
}
