/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for CCP PWM mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_CCP.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                                 Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    /* System clock configuration */
    SystemInit(72U);
    systick_delay_init(72U);

    /* Configure PWM output IO port, PD6 */
    GPIO_Write_Mode_Bits(PD6_CCP19_CH1_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PD6_CCP19_CH1_AF2);
    /**PD4 -- S2*/
    GPIO_Write_Mode_Bits(PD4_KEY2_PIN, GPIO_MODE_IN);

    /* Initialise CCP18 channel 1 to PWM mode, and set period and duty cycle of PWM */
#if (CCP_PWM_WORK == PWM_Edge_Out)
    CCP_PWM_Mode_Init(CCP19_SFR, CCP_CHANNEL_1, 1000U, 500U);
#else
    CCP_PWM_Mode_Init(CCP19_SFR, CCP_CHANNEL_1, 500U, 250U);
#endif
    while (1)
    {
        /* Get S2 status */
        if (RESET == GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN))
        {
            systick_delay_ms(100U);
            if (RESET == GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN))
            {
                /* Update period and duty cycle of PWM */
#if (CCP_PWM_WORK == PWM_Edge_Out)
                CCP_PWM_Update_Parameter(CCP19_SFR, CCP_CHANNEL_1, 100U, 70U);
#else
                CCP_PWM_Update_Parameter(CCP19_SFR, CCP_CHANNEL_1, 50U, 35U);
#endif
            }
        }
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
