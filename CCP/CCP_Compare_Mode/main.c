/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for CCP compare mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_CCP.h"
#include "User_GPTIM.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                          Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    uint8_t CompareFlag = 0U;

    /* System clock configuration */
    SystemInit(72U);
    systick_delay_init(72U);

    /* LED2 */
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);
    /**PD4 -- S2*/
    GPIO_Write_Mode_Bits(PD4_KEY2_PIN, GPIO_MODE_IN);

    /* PD6 remap to Compare mode */
    GPIO_Write_Mode_Bits(PD6_CCP19_CH1_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PD6_CCP19_CH1_AF2);

    /* CCP19 channel 1 Compare mode initialization, Configure
       channel 1 trigger source*/
#ifdef CCP_COMPARE_WITH_TXCK
    GPTIM_Gpio_Init(PA11_T19CK_AF);
    {
        Ccp_Config_t ccpConfig = {
          .Channel      = CCP_CHANNEL_1,
          .EdgeConfig   = CCP_CMP_TOGGLE_LEVEL,
          .CompareValue = 400U,
          .Period       = 500U,
        };
        Ccp_IntConfig_t ccpIntConfig = {
          .newState     = TRUE,
          .intIndex     = INT_T19,
          .ccpInterrupt = CCP_INT_CAPTURE_CH1,
          .preemption   = 4U,
          .subPriority  = 0U,
        };

        CCP_Compare_Mode_Init(CCP19_SFR, &ccpConfig);
        CCP_INT_Config(CCP19_SFR, &ccpIntConfig);
        /* Enable timer overflow interrupt*/
        GPTIM_Clear_Overflow_INT_Flag(T19_SFR);
        GPTIM_Overflow_INT_Enable(T19_SFR, TRUE);
    }
#else
    {
        Ccp_Config_t ccpConfig = {
          .Channel      = CCP_CHANNEL_1,
          .EdgeConfig   = CCP_CMP_TOGGLE_LEVEL,
          .CompareValue = 500U,
          .Period       = 1000U,
        };
        Ccp_IntConfig_t ccpIntConfig = {
          .newState     = TRUE,
          .intIndex     = INT_T19,
          .ccpInterrupt = CCP_INT_CAPTURE_CH1,
          .preemption   = 4U,
          .subPriority  = 0U,
        };

        /* Configure CCP compare mode, set period to 1000, compare to 200. */
        CCP_Compare_Mode_Init(CCP19_SFR, &ccpConfig);
        CCP_INT_Config(CCP19_SFR, &ccpIntConfig);
        /* Enable timer overflow interrupt*/
        GPTIM_Clear_Overflow_INT_Flag(T19_SFR);
        GPTIM_Overflow_INT_Enable(T19_SFR, TRUE);
    }

#endif

    INT_All_Enable(TRUE);
    while (1)
    {
#ifdef CCP_COMPARE_WITH_TXCK
        ;
#else
        if (RESET == GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN))
        {
            systick_delay_ms(100U);
            if (RESET == GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN))
            {
                /* Update compare value. It will be valid in next period. */
                CompareFlag = (++CompareFlag) % 2U;
                /* Configure CCPx compare register */
                CCP_Set_Compare_Result(CCP19_SFR, CCP_CHANNEL_1, 200U + (CompareFlag * 200U));
            }
        }
#endif
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
