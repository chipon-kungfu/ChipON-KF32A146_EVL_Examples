/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_GPTIM.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides template for T18.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_GPTIM.h"
/*******************************************************************************
**                               Public variables
*******************************************************************************/

/*******************************************************************************
**                          Private Functions
*******************************************************************************/
static uint32_t GPTIM_Set_Timer_Parameter(GPTIM_SFRmap *GPTIMx, uint32_t timerClock, uint32_t cycle);

/**
 * @brief: Calculate GPTIM prescaler and period.
 *
 * @param GPTIMx: Pointer to GPTIM register structure.
 * @param timerClock: GPTIM work clock source(MHz).
 * @param cycle: Timer period.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
static uint32_t GPTIM_Set_Timer_Parameter(GPTIM_SFRmap *GPTIMx, uint32_t timerClock, uint32_t cycle)
{
    uint32_t tmpPrescaler = 0U;
    uint32_t tmpPeriod    = 0U;
    if (((cycle > 65000U) && ((cycle % 10U) != 0U)) || ((cycle > 650000U) && ((cycle % 100U) != 0U)) ||
        (cycle > 6500000U) || (timerClock < 1U))
    {
        return 1U;
    }
    else
    {
        /* Empty */
    }

    if (cycle <= 65000U)
    {
        tmpPrescaler = timerClock;
        tmpPeriod    = cycle;
    }
    else if (cycle <= 650000U)
    {
        tmpPrescaler = timerClock * 10U;
        tmpPeriod    = cycle / 10U;
    }
    else
    {
        tmpPrescaler = timerClock * 100U;
        tmpPeriod    = cycle / 100U;
    }

    /* Set timer period */
    GPTIM_Set_Period(GPTIMx, tmpPeriod);
    /* Set timer prescaler */
    GPTIM_Set_Prescaler(GPTIMx, (tmpPrescaler - 1U));

    return 0u;
}

/*******************************************************************************
**                          Global Functions
*******************************************************************************/

/**
 * @brief: Configure GPTIM.
 *          In defualt, GPTIM use HFCLK as work clock source.
 *          If you want to use other clock source you NEED TO
 *              - modify entrance parameter $NewClock$ of
 *                $GPTIM_Clock_Config()$.
 *              - modify entrance parameter $timerClock$ of
 *                $GPTIM_Set_Timer_Parameter()$, in MHz.
 *
 * @param GPTIMx: Pointer to GPTIM register structure.
 * @param config: Pointer to GPTIM config strucre.
 *              ->mode: Mode of GPTIM
 *                  TIM_Timer or TIM_Count
 *              ->countMode: Mode of count
 *                  GPTIM_COUNT_DOWN_UF
 *                  GPTIM_COUNT_UP_OF
 *                  GPTIM_COUNT_UP_DOWN_OF
 *                  GPTIM_COUNT_UP_DOWN_UF
 *                  GPTIM_COUNT_UP_DOWN_OUF
 * @param period: Timer period or counter cycle.
 *          Timer period: (for TIMER MODE only)
 *          ==============================================
 *             scope          |           stepper/us
 *          ------------------+---------------------------
 *             0-65000        |           1
 *             65001-650000   |           10
 *             650001-6500000 |           100
 *          ==============================================
 *          counter cycle: (for COUNT MODE only)
 *          It should be less than 65535.
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 */
uint32_t GPTIM_Init(GPTIM_SFRmap *GPTIMx, Tim_Config_t *config, uint32_t period)
{
    /* Reset timer and enable peripheral clock */
    TIM_Reset(GPTIMx);
    /* Configure timer to update immediately */
    GPTIM_Updata_Immediately_Config(GPTIMx, FALSE);
    /* Enable timer update the period, duty cycle, counter etc. */
    GPTIM_Updata_Enable(GPTIMx, TRUE);

    if (TIM_Timer == config->mode)
    {
        /* Timer Mode */

        /*Select internal high frequency as clock source*/
        GPTIM_Clock_Config(GPTIMx, GPTIM_HFCLK);
        /* Configure timer work mode to timing mode */
        GPTIM_Work_Mode_Config(GPTIMx, GPTIM_TIMER_MODE);
        if (0u != GPTIM_Set_Timer_Parameter(GPTIMx, 16U, period))
        {
            return 1U;
        }
        else
        {
            /* Empty */
        }
    }
    else
    {
        /* Count Mode */

        /* Configure timer work mode to timing mode */
        GPTIM_Work_Mode_Config(GPTIMx, GPTIM_COUNTER_MODE);
        /* Set timer period */
        GPTIM_Set_Period(GPTIMx, period - 1U);
        /* Set timer prescaler */
        GPTIM_Set_Prescaler(GPTIMx, 0u);
        /* Forbidden using timer slave mode */
        GPTIM_Slave_Mode_Config(GPTIMx, GPTIM_SLAVE_FORBIDDEN_MODE);
        /* Triggered whether external pluse are synchronized with clock or not */
        GPTIM_External_Pulse_Sync_Config(GPTIMx, GPTIM_NO_SYNC_MODE);
    }

    /* Set timer counter to zero. Update CNT when the first pulse occurs. */
    GPTIM_Set_Counter(GPTIMx, 0);
    /* Set timer count mode */
    GPTIM_Counter_Mode_Config(GPTIMx, config->countMode);

    /* Enable timer */
    GPTIM_Cmd(GPTIMx, TRUE);
}

/**
 * @brief: Configure interrupt priority, enable overflow interrupt.
 *
 * @param GPTIMx: Pointer to GPTIM register structure.
 * @param intConfig: Pointer to BTIM interrupt config strucre.
 *          ->newState: The status of interrupt.
 *          ->timerIntIndex: The index of interrupt. Such as: INT_T18.
 *          ->preemption: Preemption of interrupt.
 *          ->subPriority:Subpriority of interrupt.
 * @retval: void
 */
void GPTIM_INT_Config(GPTIM_SFRmap *GPTIMx, Tim_Int_Config_t *intConfig)
{
    if (FALSE == intConfig->newState)
    {
        /* Enable timer overflow interrupt */
        GPTIM_Overflow_INT_Enable(GPTIMx, FALSE);
        /* Enable timer interrupt */
        INT_Interrupt_Enable(intConfig->timerIntIndex, FALSE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(intConfig->timerIntIndex);
    }
    else
    {
        /* Set timer interrupt priority */
        INT_Interrupt_Priority_Config(intConfig->timerIntIndex, intConfig->preemption, intConfig->subPriority);
        /* Enable timer overflow interrupt */
        GPTIM_Overflow_INT_Enable(GPTIMx, TRUE);
        /* Enable timer interrupt */
        INT_Interrupt_Enable(intConfig->timerIntIndex, TRUE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(intConfig->timerIntIndex);

        /* Confirm BTIM is ENABLE */
        if (0u == (GPTIMx->CTL1 & GPTIM_CTL1_TXEN))
        {
            GPTIM_Cmd(GPTIMx, TRUE);
        }
        else
        {
            /* Empty */
        }
    }
}

/**
 * @brief: Initialize TxCK pin of  general timer.
 *
 * @param GPIOx: The pointer to GPIO Port
 * @param GPIO_Pin: Pin number of TxCK
 * @param PinRemap: Pin remap mode
 * @retval: void
 */
void GPTIM_Gpio_Init(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t PinRemap)
{

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.m_Mode      = GPIO_MODE_RMP;
    GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
    GPIO_InitStructure.m_PullDown  = GPIO_PULLDOWN;
    GPIO_InitStructure.m_PullUp    = GPIO_NOPULL;
    GPIO_InitStructure.m_Speed     = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Pin       = ((uint32_t)1U << (uint32_t)GPIO_Pin);
    GPIO_Configuration(GPIOx, &GPIO_InitStructure);

    GPIO_Pin_RMP_Config(GPIOx, GPIO_Pin, PinRemap);
}
