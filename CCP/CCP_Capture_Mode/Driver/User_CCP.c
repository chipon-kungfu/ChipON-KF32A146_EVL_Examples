/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_CCP.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                         for CCP
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/*******************************************************************************
**                               Include Files
*******************************************************************************/
#include "system_init.h"
#include "User_CCP.h"

/*******************************************************************************
**                                 Macro Defines
*******************************************************************************/

/*******************************************************************************
**                               Public variables
*******************************************************************************/
#ifdef CAPTURE_PWM_MEASURE
/* variables for PWM measure */
uint8_t  Status_CalPWM;
uint32_t HighVol_Time;
uint32_t LowVol_Time;
#endif

/*******************************************************************************
**                              Public Functions
*******************************************************************************/

/**
 * @brief: Configure CCP to capture mode.
 *
 * @param CCPx: Pointer to CCP or general timer register structure
 * @param Channel:  Channel of CCP. Value can be as follow:
 *          CCP_CHANNEL_1 - CCP_CHANNEL_4
 * @param EdgeConfig: Capture edge.
 *          CCP_CAP_RISING_EDGE
 *          CCP_CAP_FALLING_EDGE
 *          CCP_CAP_4TH_RISING_EDGE
 *          CCP_CAP_16TH_RISING_EDGE
 * @retval: None
 */
void CCP_Capture_Mode_Init(CCP_SFRmap *const CCPx, const uint32_t Channel, const uint32_t EdgeConfig)
{
    /* Timer peripheral reset, enable peripheral clock */
    TIM_Reset(CCPx);
    /* Set the capture channel, edge */
    CCP_Capture_Mode_Config(CCPx, Channel, EdgeConfig);

    /* Update control. Set update periodically */
    GPTIM_Updata_Immediately_Config(CCPx, FALSE);
    /* Configuration update enable */
    GPTIM_Updata_Enable(CCPx, TRUE);
    /* Timing mode selection */
    GPTIM_Work_Mode_Config(CCPx, GPTIM_TIMER_MODE);
    /* Select SCLK as the timer clock source */
    GPTIM_Clock_Config(CCPx, GPTIM_HFCLK);
    /* Timer count value */
    GPTIM_Set_Counter(CCPx, 0u);
    /* Set timer prescaler, 1us counts once */
    GPTIM_Set_Prescaler(CCPx, 15u);
    /* Up, overflow generates an interrupt flag */
    GPTIM_Counter_Mode_Config(CCPx, GPTIM_COUNT_UP_OF);

    /* Enable general timer */
    GPTIM_Cmd(CCPx, TRUE);
}

/**
 * @brief: Configure CCP interrupt.
 *
 * @param CCPx Pointer to CCP or general timer register structure.
 * @param intConfig Pointer to CCP interrupt config structure.
 *              ->newState: Interrupt enable status. TRUE/FALSE
 *              ->intIndex: Interrupt index number. Example: INT_T18.
 *              ->ccpInterrupt: Interrupt type of CCP.
 *                  CCP_INT_CAPTURE_CH1 -- Capture/Compare interrupt of CCP channel1
 *                  CCP_INT_CAPTURE_CH2 -- Capture/Compare interrupt of CCP channel2
 *                  CCP_INT_CAPTURE_CH3 -- Capture/Compare interrupt of CCP channel3
 *                  CCP_INT_CAPTURE_CH4 -- Capture/Compare interrupt of CCP channel4
 *                  CCP_INT_TRIGER_EVENT -- Tx trigger event interrupt
 *                  CCP_INT_UPDATE_EVENT -- Tx update event interrupt
 *                  You can connect them with "|", if you want to enable more than one ccp interrupt.
 *              ->preemption: preemption of interrupt.
 *              ->subPriority: Sub-Priority of interrupt.
 * @retval: void
 */
void CCP_INT_Config(CCP_SFRmap *const CCPx, const Ccp_IntConfig_t *const intConfig)
{
    if (FALSE == intConfig->newState)
    {
        /* Disable CCP interrupt */
        CCPx->CCPXCTL3 &= (~intConfig->ccpInterrupt);
        INT_Interrupt_Enable(intConfig->intIndex, FALSE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(intConfig->intIndex);
    }
    else
    {
        /* Set timer interrupt priority */
        INT_Interrupt_Priority_Config(intConfig->intIndex, intConfig->preemption, intConfig->subPriority);
        /* Enable CCP interrupt */
        CCPx->CCPXCTL3 |= (intConfig->ccpInterrupt);
        INT_Interrupt_Enable(intConfig->intIndex, TRUE);
        /* Clear timer interrupt flag */
        CCPx->CCPXSRIC |= (intConfig->ccpInterrupt);
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        CCPx->CCPXSRIC &= (~intConfig->ccpInterrupt);
        INT_Clear_Interrupt_Flag(intConfig->intIndex);
    }
}

#ifdef CAPTURE_PWM_MEASURE
/**
 * @brief: Modify CCP capture mode， and update timer.
 *
 * @param CCPx Pointer to CCP or general timer register structure
 * @param CCPx_Channel Channel of CCP. Value can be as follow:
 *          CCP_CHANNEL_1 - CCP_CHANNEL_4
 * @param CCPx_CaptureMode CCP capture mode.
 *          CCP_CAP_RISING_EDGE
 *          CCP_CAP_FALLING_EDGE
 *          CCP_CAP_4TH_RISING_EDGE
 *          CCP_CAP_16TH_RISING_EDGE
 * @retval: void
 */
void CCP_Capture_Change_Mode(CCP_SFRmap *CCPx, uint16_t CCPx_Channel, uint16_t CCPx_CaptureMode)
{
    /* set timer counter */
    GPTIM_Set_Counter(CCPx, 0U);
    /* update register immediately */
    GPTIM_Updata_Immediately_Config(CCPx, TRUE);
    /* Disable CCP interrupt */
    CCP_Channel_INT_Config(CCPx, CCPx_Channel, FALSE);
    /* modify CCP capture mode */
    CCP_Capture_Mode_Config(CCPx, CCPx_Channel, CCPx_CaptureMode);
    /* Clear interrupt flag */
    CCP_Clear_Channel_INT_Flag(CCPx, CCPx_Channel);
    /* Enable CCP interrupt */
    CCP_Channel_INT_Config(CCPx, CCPx_Channel, TRUE);
}
#endif
