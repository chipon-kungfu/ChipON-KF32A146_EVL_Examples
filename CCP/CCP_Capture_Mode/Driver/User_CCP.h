/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_CCP.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                         for CCP
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_CCP_H_
#define USER_CCP_H_
/*******************************************************************************
**                               Include Files                                **
*******************************************************************************/

/*******************************************************************************
**                                 Macro Defines                              **
*******************************************************************************/

/*CCP interrupt index*/
typedef enum
{
    /* Capture/Compare interrupt of CCP channel1 */
    CCP_INT_CAPTURE_CH1 = 0x1U,
    /* Capture/Compare interrupt of CCP channel2 */
    CCP_INT_CAPTURE_CH2 = 0x2U,
    /* Capture/Compare interrupt of CCP channel3 */
    CCP_INT_CAPTURE_CH3 = 0x4U,
    /* Capture/Compare interrupt of CCP channel4 */
    CCP_INT_CAPTURE_CH4 = 0x8U,
    /* Tx trigger event interrupt */
    CCP_INT_TRIGER_EVENT = 0x10U,
    /* Tx update event interrupt */
    CCP_INT_UPDATE_EVENT = 0x20U,
} Ccp_Int_t;

/* CCP interrupt configuration typedef */
typedef struct TIM_INT_CONFIG
{
    /* Interrupt enable status */
    FunctionalState newState;
    /* Interrupt index */
    InterruptIndex intIndex;
    /* Interrut type of CCP */
    Ccp_Int_t ccpInterrupt;
    uint8_t   preemption;
    uint8_t   subPriority;
} Ccp_IntConfig_t;

#ifdef CAPTURE_PWM_MEASURE
/* State of PWM measur */
enum CalPWM_Status
{
    Init_CalPWM = 0,
    HighVol_CalPWM,
    LowVol_CalPWM,
    End_CalPWM
};
/* variables for PWM measure */
extern uint8_t  Status_CalPWM;
extern uint32_t HighVol_Time;
extern uint32_t LowVol_Time;
#endif

/*******************************************************************************
**                              Public Functions                              **
*******************************************************************************/

extern void CCP_Capture_Mode_Init(CCP_SFRmap *const CCPx, const uint32_t Channel, const uint32_t EdgeConfig);
extern void CCP_INT_Config(CCP_SFRmap *const CCPx, const Ccp_IntConfig_t *const intConfig);
#ifdef CAPTURE_PWM_MEASURE
extern void CCP_Capture_Change_Mode(CCP_SFRmap *CCPx, uint16_t CCPx_Channel, uint16_t CCPx_CaptureMode);
#endif

#endif /* USER_CCP_H_ */
