/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for CCP capture interrupt
 *                        mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_CCP.h"
#include "Usart.h"
#include "Board_GpioCfg.h"
#include <stdio.h>

/*******************************************************************************
**                          Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main(void)
{
    /* Initialize the system clock is 72MHz*/
    SystemInit(72U);
    systick_delay_init(72U);

    /* LED1 */
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    /**LED2*/
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);
    /* PE10 remap to capture mode */
    GPIO_Write_Mode_Bits(PE10_CCP18_CH1_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PE10_CCP18_CH1_AF2);

    {
        Ccp_IntConfig_t ccpIntConfig = {
          .newState     = TRUE,
          .intIndex     = INT_T18,
          .ccpInterrupt = CCP_INT_CAPTURE_CH1,
          .preemption   = 4U,
          .subPriority  = 0U,
        };
        CCP_Capture_Mode_Init(CCP18_SFR, CCP_CHANNEL_1, CCP_CAP_RISING_EDGE);
        CCP_INT_Config(CCP18_SFR, &ccpIntConfig);
    }

    /* Enable all interrupt */
    INT_All_Enable(TRUE);
    while (1)
    {
        systick_delay_ms(250U);
        systick_delay_ms(250U);
        /*Toggle LED2*/
        GPIO_Toggle_Output_Data_Config(PF11_LED2_PIN);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
