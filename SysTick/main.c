/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by system tick.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "User_SysTick.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                              Private Functions                             **
*******************************************************************************/

/*******************************************************************************
**                     			main Functions 		             	     	  **
*******************************************************************************/
int main()
{
    /*initialize system clock ,default SCLK is 72M,select INTHF as Clock source */
    SystemInit(72U);
    /**LED1*/
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    GPIO_Set_Output_Data_Bits(PA3_LED1_PIN, Bit_SET);
    /**LED2*/
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);
    GPIO_Set_Output_Data_Bits(PF11_LED2_PIN, Bit_RESET);

    /* Configure SYSTICK. Input the system clock and tick time. */
    if (0U != SysTick_Config(72U, 10U))
    {
        /* If the parameter of $SysTick_Config$ is wrong, light on LED4.*/
        GPIO_Set_Output_Data_Bits(PF11_LED2_PIN, Bit_SET);
        while (1)
        {
            ;
        }
    }
    else
    {}

    INT_All_Enable(TRUE);
    while (1)
    {
        /* Toggle LED1 */
        GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
        /* Delay 10 tick times */
        Systick_Delay(10);
    }
}
/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param File pointer to the source file name
 * 	@param Line assert_param error line source number
 *  @retval :void
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
