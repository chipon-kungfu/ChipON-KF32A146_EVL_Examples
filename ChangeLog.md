﻿## Change Log
###### 2024-12-31
V2.3.0
1. **I2C模块**
   - 调整I2C模块初始化与IO初始化顺序，先初始化模块，再进行IO初始化。
2. **PM模块**
   - PM示例程序中初始化LP-TIMER T0取消降频。
3. **WDG模块**
   - IWDG示例程序中初始化函数增加关闭备份域。

###### 2024-05-17
V2.2.0
1. **ADC模块**
   - 移除未使用变量。
2. **CANFD模块**
   - 综合示例程序初始化接收邮箱失败时，增加多次重新初始化操作。
3. **Flash**
   - 移除未使用宏定义。
   - 修复超时等待循环异常。
   - 新增flash模拟EEPROM例程。
4. **I2C模块**
   - 重构I2C主机轮询示例程序。
5. **LIN模块**
   - *LinSlave* 移除自动波特率相关内容。
   - 新增自动波特率示例程序，*LinSlave_Auto_Baud_Rate*。
6. **USART模块**
   - 增加DMA1使用注释。
7. **RTC模块**
   - 使用外部低频时钟作为RTC时钟。
8. **SPI模块**
   - 修改例程中断中固定的DMA通道。
9.  **RTOS**
   - 设置优先级基级前后分别增加关总中断和开总中断的操作.

###### 2023-09-18
V2.1 
1. **AES模块**
 - 新增加解密例程。

2. **EPWM模块**
   - 新增EPWM产生PWM波形，修改参数，多EPWM同步例程。

3. **ADC模块**
   - 修复笔误内容。

4. **CANFD模块**
   - 例程中新增修改中断优先级分组和总中断配置操作。

5. **Flash**
   - 修改例程操作Flash位置，适配小容量芯片。

6. **I2C模块**
   - 修改工作时钟为HFCLK。

7. **LIN模块**
   - 修改工作时钟为SCLK。

8. **PM模块**
   - Stop0模式修改MRBGEN位状态，删除关闭备份域操作。
   - Standby模式新增关闭备份域操作，休眠前关闭总中断操作。

**注意：具体更改内容见变更说明**

* * *
##### 2023-07-10
- ★V2.1
- **LIN模块**
  1. *LinMaster*，修改LIN间隔场长度为16bits。

- **FLASH模块**
  1. *FlashReadWrite_AssemblyAPI*，**__driver_Flash_API.S**修复汇编代码中错误条件编译`#if ... #endif`为`.if ... .endif`

##### 2023-05-22
- ★V2.1
- **CANFD模块**
  1. *CANFD_Comprehensive_Example3*，在`send10data`等发送函数内，增加超时退出机制。
  2. 删除*CANFD_Comprehensive_Example4*例程。

##### 2023-05-20
- ★V2.1
- **PM模块**
  1.  *PM_PMC_Stop0_EXTI_WakeUp*、*PM_PMC_Stop0_T0_WakeUp*、*PM_Standby_SleepMode*，操作完备份域后，及时关闭备份域。

- **WDG模块**
  1. *IWDG*，操作完备份域后，及时关闭备份域。

- **BKP模块**
  1. 操作完备份域后，及时关闭备份域。

- **RTC模块**
  1. 修正打印RTC时间可能出现乱码的情况。

### 2023-05-19
- ★V2.1
- **CAN模块**
  - *CANFD_CAN*, *CANFD_MODE*, *CANFD_RECEIVE_INTERRUPT*, *CANFD_RECEIVE_INTERRUPT_MULTIMAILBOX*, *CANFD_RECEIVE_POLLING*, *CANFD_SLEEP_MODE*, *CANFD_TRANSMIT_INTERRUPT*, *CANFD_TRANSMIT_POLLING*修改 **README**描述。
  - 新增 *CANFD_Comprehensive_Example1 ~ 4*例程。


### 2023-05-16
- ★V2.1
- **ADC模块**
  - *ADC_continuous_DMA_mode*，修正例程编译警告重复定义宏。

- **CCP模块**
  - *CCP_Capture_Mode*，删除单通道捕捉模式测量PWM参数部分内容。
  - *CCP_Compare_Mode*，优化TxCK输入脉冲进行比较例程。修改原更新中断为溢出中断，选择溢出中断更为合适。

- **CMP模块**
  - *CMP_Interrupt_Mode*，修改正极、负极引脚注释。

- **ECCP模块**
  - *ECCP_Capture_Mode*，删除单通道捕捉模式测量PWM参数部分代码。
  - *ECCP_Compare_Mode*，优化TxCK输入脉冲进行比较例程。修改部分引脚注释。README添加定义宏`ECCP_COMPARE_WITH_TXCK`的位置。
  - *ECCP_PWM_Measurement_Mode*，修改引脚注释。

- **EXTI模块**
  - *GPIO_EXTI*，失能下降沿捕捉中断，例程效果更加明显。

- **I2C模块**
  - 新增例程

- **LIN模块**
  - *LinMaster*，更换向TBUFR寄存器填充0x55发送同步场的时机。修改后为 **发送间隔场 -> USART发送使能 -> 向TBUFR填充0x55**。
  - *LinSlave*，修改README错误。

- **PM模块**
  - *PM_PMC_Stop0_EXTI_WakeUp*，新增 *Swap_SCLK_To_INTLF* 切换系统时钟。修改WKUP中断打开时机，由原初始化WKUP引脚时开启变更至执行睡眠指令前才开启。修改中文注释。
  - *PM_PMC_Stop0_T0_WakeUp*，修改同 *PM_PMC_Stop0_EXTI_WakeUp* 例程。
  - *PM_Standby_SleepMode*，删除无用代码。置零`MRBGEN`寄存器。新增清除所有中断标志位操作。修正standby特性位操作错位，修改后如下：
    ```c
    PM_CAL2 |= 0x80;
    PM_CTL0 |= 0x800;
    ```
- **RTOS**
  - `vPortEnterCritical`函数，增加是否修改成功判断。

- **SPI模块**
  - *SPI_Master_DMA_Mode*，修改SPI引脚，避免与其他外设共用。删除用发送1byte数据触发DMA机制，改用`DMA_Oneshot_Enable`函数触发。
  - *SPI_Master_Interrupt_Mode*，修改SPI引脚，删除发送首byte代码。
  - *SPI_Slave_DMA_Mode*，修改SPI引脚。删除用发送1byte数据触发DMA机制，改用`DMA_Oneshot_Enable`函数触发。
  - *SPI_Slave_Interrupt_Mode*，修改SPI引脚。

- **TIM模块**
  - 增加对接口入口参数的检查。
  - 检查初始化函数无`return 0U;`情况。

- **USART模块**
  - *USART_ASYNC_DMA*，优化USART空闲中断服务函数。优化接收数据处理。README增加使用注意事项。

- **FLASH模块**
  - 使用 *FlashReadWriteSelf_AssemblyAPI* 例程替换原 *FlashReadWriteSelf* 例程。
  - *FlashReadWriteSelf_AssemblyAPI*，使用汇编接口操作flash，增加对入口参数的校验与判断。

### 2023-03-30

- ★V2.1
- 根据已经发现的问题，重构例程

### Thu Feb 16 2023
1.适配库修改，去除WORKSOURCE_HFCLK

* * * 
### Fri Feb 10 2023

1.146 canfd a02适配
* * * 
### 2021-12-270

* ★ V1.0.0

●1 基于标准外设库例程V1.0.0版本发布

●2 基于标准外设库V1.0.0版本发布