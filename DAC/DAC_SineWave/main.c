/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of DAC
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
 *                        		Include Files                                 *
 ******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"

/******************************************************************************
 *                     	Static function declaration                           *
 ******************************************************************************/

/******************************************************************************
 *                        	Initialization function                           *
 ******************************************************************************/
/*Sine wave array*/
const uint16_t Sine12bit[32] = {
  2047, 2447, 2831, 3185, 3498, 3750, 3939, 4056, 4094, 4056, 3939, 3750, 3495, 3185, 2831, 2447,
  2047, 1647, 1263, 909,  599,  344,  155,  38,   0,    38,   155,  344,  599,  909,  1263, 1647};
uint32_t DualSine12bit[32];

/**
 *  @brief : config timer0.
 *  @param in :	None
 *  @param out :None
 *  @retval :None
 */
void GENERAL_TIMER_T0_Config(void)
{
    /*reset peripherals of Timer 0 and enable peripherals clock source.*******/
    TIM_Reset(T0_SFR);
    /*Configure the T0 immediate update control bit to update immediately*****/
    GPTIM_Updata_Immediately_Config(T0_SFR, TRUE);
    /*Enable T0 update controller*********************************************/
    GPTIM_Updata_Enable(T0_SFR, TRUE);
    /*Set to timing mode as working mode of timer 18***************************/
    GPTIM_Work_Mode_Config(T0_SFR, GPTIM_TIMER_MODE);
    /*Set the master mode******************************************************/
    GPTIM_Master_Mode_Config(T0_SFR, GPTIM_MASTER_TXIF_SIGNAL);
    /*Updata counter***********************************************************/
    GPTIM_Set_Counter(T0_SFR, 0);
    /*Set the counting period**************************************************/
    GPTIM_Set_Period(T0_SFR, 239);
    /*Set prescaler************************************************************/
    GPTIM_Set_Prescaler(T0_SFR, 2);
    /*Set up and down count****************************************************/
    GPTIM_Counter_Mode_Config(T0_SFR, GPTIM_COUNT_UP_DOWN_OUF);
    /*Select SCLK as clock source of timer 0***********************************/
    GPTIM_Clock_Config(T0_SFR, GPTIM_SCLK);
    /*Enable timer 0***********************************************************/
    GPTIM_Cmd(T0_SFR, TRUE);
}

void DAC_DMA_Config()
{
    DMA_InitTypeDef dmaNewStruct;
    DMA_Struct_Init(&dmaNewStruct);

    dmaNewStruct.m_Number = 32;

    dmaNewStruct.m_Direction = DMA_MEMORY_TO_PERIPHERAL;

    dmaNewStruct.m_Priority = DMA_CHANNEL_HIGHER;

    dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_32_BITS;

    dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_32_BITS;

    dmaNewStruct.m_PeripheralInc = FALSE;

    dmaNewStruct.m_MemoryInc = TRUE;

    dmaNewStruct.m_Channel = DMA_CHANNEL_5;

    dmaNewStruct.m_BlockMode = DMA_TRANSFER_BYTE;

    dmaNewStruct.m_LoopMode = TRUE;

    dmaNewStruct.m_PeriphAddr = (uint32_t)&DAC0_DAHD;

    dmaNewStruct.m_MemoryAddr = (uint32_t)&DualSine12bit;

    DMA_Configuration(DMA0_SFR, &dmaNewStruct);

    DMA_Channel_Enable(DMA0_SFR, DMA_CHANNEL_5, TRUE);
}

/**
 *  @brief : Init DAC.
 *  @param in :	None
 *  @param out :None
 *  @retval :None
 */
void DAC_Init_Config(void)
{
    static uint32_t ReferenceVoltage = 0;
    DAC_InitTypeDef DAC_InitStructure;
    DAC_Struct_Init(&DAC_InitStructure);
    /*reset peripherals of DAC0 and enable peripherals clock source.*******/
    DAC_Reset(DAC0_SFR);
    /*enable DAC trigger***************************************************/
    DAC_InitStructure.m_TriggerEnable = TRUE;
    /*set the T0_TRGO as the trigger of DAC********************************/
    DAC_InitStructure.m_TriggerEvent = DAC_TRIGGER_T0_TRGO;
    /*Enable DAC DMA*******************************************************/
    DAC_InitStructure.m_TriggerDMAEnable = TRUE;
    /*enable DAC DAC_WAVE_NONE*********************************************/
    DAC_InitStructure.m_Wave = DAC_WAVE_NONE;
    /*set SCLK as DAC's work clock*****************************************/
    DAC_InitStructure.m_Clock = DAC_CLK_SCLK;
    /*set AVDD as DAC's reference voltage**********************************/
    DAC_InitStructure.m_ReferenceVoltage = DAC_RFS_AVDD;
    /*enable DAC output buffer*********************************************/
    DAC_InitStructure.m_OutputBuffer = TRUE;
    /*set the DAC_OUTPUT_PIN_1 port(PA4) as DAC0's output port*************/
    DAC_InitStructure.m_OutputPin = DAC_OUTPUT_PIN_1;
    /*set initial DAC output to 0******************************************/
    DAC_InitStructure.m_Output = 0;
    /*configuration DAC*/
    DAC_Configuration(DAC0_SFR, &DAC_InitStructure);

    ReferenceVoltage = DAC_InitStructure.m_ReferenceVoltage;
    /*choose DAC_RFS_2V as referenceVoltage, needed enable the backup area*/
    if (ReferenceVoltage == DAC_RFS_2V)
    {
        OSC_Backup_Write_Read_Enable(TRUE);
        PM_Reference_Voltage_Enable(TRUE);
    }
    DAC_Cmd(DAC0_SFR, TRUE);
}
int main()
{
    /* Initialize the system clock */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function */
    systick_delay_init(72U);
    /* Initialize PA4 to DAC_OUT IO */
    GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_4, GPIO_MODE_AN);
    /**Initialize LED1*/
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);

    /*Initialize sine array*/
    for (uint8_t Idx = 0; Idx < 32; Idx++)
    {
        DualSine12bit[Idx] = (Sine12bit[Idx] << 16) + (Sine12bit[Idx]);
    }
    /*reset DMA*/
    DMA_Reset(DMA0_SFR);
    /* Initialize DMA */
    DAC_DMA_Config();
    /* Initialize DAC */
    DAC_Init_Config();
    /* Initialize T0 */
    GENERAL_TIMER_T0_Config();

    while (1)
    {
        GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
        systick_delay_ms(200U);
    }
}
