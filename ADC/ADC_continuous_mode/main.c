/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by ADC continuous mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "User_ADC.h"
#include "kflog.h"

#define ADC_OPT (KF_LOG_OPT_FUNC | KF_LOG_OPT_LINE)
KF_REG_LOG(adc_continuous_mode, ADC_OPT);
/* Private variables *********************************************************/
ADC_InitTypeDef   ADC_InitStructure;
volatile uint16_t adc_value;

/**
 *  @brief : software Delay.
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void Delay(volatile uint32_t cnt)
{
    while (cnt--)
        ;
}

/******************************************************************************
**                                 main Functions
******************************************************************************/
int main()
{
    ADC_InitTypeDef ADC_InitStructure;

    /*initialize system clock ,defaut SCLK is  72MHz*/
    SystemInit(72U);
    /* Setup SysTick Timer as delay function */
    systick_delay_init(72U);
    /* LED2 */
    GPIOInit_Output_Config(LED2_PORT, LED2_PIN);
    /*Initialize the print function*/
    kfLog_Init();
    /* ADC GPIO initialise PA13--ADC_CH72 */
    ADC_GPIO_Init(EXAMPLE_ADC_PIN);

    /* ========== Configure ADC ========== */
    /*reset ADC0 module,enable ADC0 clock source */
    ADC_Reset(ADC0_SFR);
    /* Initialize the ADC configuration information structure */
    ADC_Struct_Init(&ADC_InitStructure);
    /* Select HFCLK as the ADC0 sampling clock source
     * You can also select SCLK or LFCLK as the ADC0 sampling clock */
    ADC_InitStructure.m_Clock = ADC_HFCLK;
    /* ADC sampling clock Divider configuration */
    ADC_InitStructure.m_ClockDiv = ADC_CLK_DIV_16;
    /* Disable adc scan mode */
    ADC_InitStructure.m_ScanMode = FALSE;
    /*select continuous mode as adc vonversion mode */
    ADC_InitStructure.m_ContinuousMode = ADC_CONTINUOUS_MODE;
    /*Right-aligned sampling result data */
    ADC_InitStructure.m_DataAlign = ADC_DATAALIGN_RIGHT;
    /*disable External trigger */
    ADC_InitStructure.m_ExternalTrig_EN = FALSE;
    /*select Vdd as ADC refrence voltage */
    ADC_InitStructure.m_VoltageRef = ADC_REF_AVDD;
    /* Initialization configuration for Analog-to-Digital Converter (ADC) module. */
    ADC_Configuration(ADC0_SFR, &ADC_InitStructure);
    /* Configure the ADC to enable */
    ADC_Cmd(ADC0_SFR, TRUE);
    /* Daley is NECESSARY, after enable ADC */
    Delay(0xffu);
    /* ========== Configure ADC END ========== */

    ADC_Regular_Channel_Config(ADC0_SFR, EXAMPLE_ADC_CHANNAL, 1u);
    /*software trig adc Conversion,In continuous conversion mode,only need to
    trig for the first time */
    ADC_Software_Start_Conv(ADC0_SFR);
    /* wait for the adc conversion to complete */
    while (!ADC_Get_INT_Flag(ADC0_SFR, ADC_INT_EOC))
    {
        ;
    }

    kf_printf("adc_continuous test start...\r\n");
    KFLOG_I(adc_continuous_mode, "adc_continuous test start...\r\n");
    while (1)
    {
        /*Determine whether there is adc data that has been converted*/
        if (SET == ADC_Get_INT_Flag(ADC0_SFR, ADC_INT_EOC))
        {
            ADC_Clear_INT_Flag(ADC0_SFR, ADC_INT_EOC);
            /*sampling GPIO PB3 adc value */
            adc_value = ADC_Get_Conversion_Value(ADC0_SFR);
            KFLOG_I(adc_continuous_mode, "adc_value:%d\r\n", adc_value);
            systick_delay_ms(250u);
            systick_delay_ms(250u);
            GPIO_Toggle_Output_Data_Config(LED2_PORT, LED2_PIN);
        }
        else
        {
            Delay(0x10u);
        }
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
