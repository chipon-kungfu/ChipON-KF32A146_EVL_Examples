/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_ADC.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides template for continuous conversion
                        mode of ADC0 depend on DMA0 channel 5.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_ADC.h"
/*******************************************************************************
**                          Public Variables Definitions
*******************************************************************************/
unsigned int adc_buf[32]    = {0};
uint8_t      adc_value0[32] = {0};
uint8_t      adc_value1[32] = {0};
/*******************************************************************************
**                          Global Functions
*******************************************************************************/

/**
 *  @brief : initialize DMA channel for adc0.
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void ADC0_channel_DMA_Init(void)
{
    DMA_InitTypeDef dmaNewStruct;
    /*************************************************************************/
    /*reset DMA0 moduel*******************************************************/
    DMA_Reset(DMA0_SFR);
    /* Transfer 32 bytes at a time through the DMA0 channel 5*****************/
    dmaNewStruct.m_Number = 16;
    /*DMATransmission direction peripheral to memory*************************/
    dmaNewStruct.m_Direction = DMA_PERIPHERAL_TO_MEMORY;
    /*select low Priority for DMA0 channel*************************************/
    dmaNewStruct.m_Priority = DMA_CHANNEL_LOWER;
    /* Set Peripheral data size 32bits */
    dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_32_BITS;
    /* Set Memory data size 32bits ******************************************/
    dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_32_BITS;
    /* Disable Peripheral address increment mode in oder to read only ADC0_DATA
    register  ****************************************************************/
    dmaNewStruct.m_PeripheralInc = FALSE;
    /* Enable memory address increment mode in oder to write adc_buf**********/
    dmaNewStruct.m_MemoryInc = TRUE;
    /* Enable DMA0 channel 5 because of ADC0 corresponds to DMA0 channel 5
     * *****/
    dmaNewStruct.m_Channel = DMA_CHANNEL_5;
    /*Adc trigger once to DMA transmit once***********************************/
    dmaNewStruct.m_BlockMode = DMA_TRANSFER_BYTE;
    /* Enable loop mode*******************************************************/
    dmaNewStruct.m_LoopMode = TRUE;
    /* used ADC0_DATA register as the DMA0 peripheral address*****************/
    dmaNewStruct.m_PeriphAddr = (uint32_t)&ADC0_DATA;
    /* used adc_buf as the DMA0 memory start address**************************/
    dmaNewStruct.m_MemoryAddr = (uint32_t)adc_buf;
    /* configure DMA0 through dmaNewStruct************************************/
    DMA_Configuration(DMA0_SFR, &dmaNewStruct);
    /* enable DMA0 channel 5************************************************* */
    DMA_Channel_Enable(DMA0_SFR, DMA_CHANNEL_5, TRUE);
}

/**
 *  @brief :Configure DMA0 channel 5 trig interrupt mode.
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void Config_DMA_interrupt(void)
{
    uint8_t ADC0TrigSource = 0x6;
    /*enable DMA0 channel 5 transferred half of data to interrupt************/
    DMA_Set_INT_Enable(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_HALF_TRANSFER, FALSE);
    /*clear DMA0 channel 5 transferred half of data IRQ *********************/
    DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_HALF_TRANSFER);

    /*enable DMA0 channel 5 transferred all of data to interrupt*************/
    DMA_Set_INT_Enable(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_FINISH_TRANSFER, TRUE);
    /*clear DMA0 channel 5 transferred all of data IRQ **********************/
    DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_FINISH_TRANSFER);
    /*Trigger the channel 5 selection register *****************************/
    /*Preempt priority:4;Subpriority:0**************************************/
    INT_Interrupt_Priority_Config(INT_DMA0, 4, 0);
    /*enable DMA0 interrupt*************************************************/
    INT_Interrupt_Enable(INT_DMA0, TRUE);
    /*clear DMA0 IRQ flag***************************************************/
    INT_Clear_Interrupt_Flag(INT_DMA0);
}

/**
 *  @brief :GPIO initialize.
 *  @param[in]  GPIOx : IO grop
                GpioPin:IO Pin
 *  @param[out] None
 *  @retval :None
 */
void GPIOInit_Output_Config(GPIO_SFRmap *GPIOx, uint16_t GpioPin)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_Struct_Init(&GPIO_InitStructure);
    GPIO_InitStructure.m_Pin      = GpioPin;
    GPIO_InitStructure.m_Speed    = GPIO_LOW_SPEED;
    GPIO_InitStructure.m_Mode     = GPIO_MODE_OUT;
    GPIO_InitStructure.m_PullUp   = GPIO_NOPULL;
    GPIO_InitStructure.m_PullDown = GPIO_NOPULL;
    GPIO_Configuration(GPIOx, &GPIO_InitStructure);
    GPIO_Set_Output_Data_Bits(GPIOx, GpioPin, Bit_RESET);
}
/**
 *  @brief :Initializes the ADC information structure
 *  @param[in]  adc InitStruct:A pointer to the structure to be initialized
 *  @param[out] None
 *  @retval :None
 */
void ADC_Struct_Init(ADC_InitTypeDef *adcInitStruct)
{
    /* Initialize the adc clock source selection */
    adcInitStruct->m_Clock = ADC_SCLK;

    /* Initialize the ADC clock division */
    adcInitStruct->m_ClockDiv = ADC_CLK_DIV_2;

    /* Initialize the ADC scan mode enable */
    adcInitStruct->m_ScanMode = FALSE;

    /* Initialize the ADC continuous conversion mode */
    adcInitStruct->m_ContinuousMode = ADC_SINGLE_MODE;

    /* Initialize the ADC conversion result output format */
    adcInitStruct->m_DataAlign = ADC_DATAALIGN_RIGHT;

    /* Initialize the ADC conventional channel external trigger conversion mode
     * enable */
    adcInitStruct->m_ExternalTrig_EN = FALSE;

    /* Initialize the ADC regular channel external trigger event */
    adcInitStruct->m_ExternalTrig = ADC_EXTERNALTRIG_CCP1_CH1;

    /* Initialize the ADC high priority channel external trigger transition mode
     * enable */
    adcInitStruct->m_HPExternalTrig_EN = FALSE;

    /* Initialize the high priority channel external trigger event */
    adcInitStruct->m_HPExternalTrig = ADC_HPEXTERNALTRIG_CCP1_CH1;

    /* ADCInitialize the ADC reference voltage selection */
    adcInitStruct->m_VoltageRef = ADC_REF_AVDD;

    /* Initialize the ADC conventional channel scan length */
    adcInitStruct->m_NumOfConv = 1;

    /* Initialize the ADC high priority channel scan length */
    adcInitStruct->m_NumOfHPConv = 1;
}

/**
 * @brief: Initialise DMA to read ADC.
 *          In default, Data width is 32bits.
 *
 *  @param[in]  ADCx: Pointer to ADC register structure.
 *  @param[in]  ADC_Channel: ADC channel. Value can be as follow:
 *              ADC_CHANNEL_NORMAL -- noraml channel
 *              ADC_CHANNEL_HP0 -- high priority channel 0
 *              ADC_CHANNEL_HP1 -- high priority channel 1
 *              ADC_CHANNEL_HP2 -- high priority channel 2
 *              ADC_CHANNEL_HP3 -- high priority channel 3
 *  @param[in]  ptr_Memory: Memory start address.
 *  @param[in]  numTransfer: The numbers of bytes, which will be transmitted at
 *                          one time.
 *  @param[out] None
 * @retval: None
 */
void ADC_DMA0_Init(ADC_SFRmap *ADCx, uint32_t ADC_Channel, void *ptr_Memory, uint16_t numTransfer)
{
    uint32_t        DMA_Channel;
    DMA_InitTypeDef dmaNewStruct;
    /* Configure DMA0 */
    DMA_SFRmap *DMAx = DMA0_SFR;

#if (defined KF32A_PERIPH_ADC0)
    /* Set DMA Channel */
    if (ADC0_SFR == ADCx)
    {
        DMA_Channel    = ADC_Channel;
    }
#else
    if (0)
    {
        ;
    }
#endif
#if (defined KF32A_PERIPH_ADC1)
    else if (ADC1_SFR == ADCx)
    {
        DMA_Channel    = ADC_Channel + 1u;
    }
#endif
#if (defined KF32A_PERIPH_ADC2)
    else if (ADC2_SFR == ADCx)
    {
        DMA_Channel    = ADC_Channel + 2u;
    }
#endif
    else
    {
        /* Empty */
    }

    /* Reset DMA moduel */
    DMA_Reset(DMAx);
    /* Transfer $numTransfer$ bytes at a time */
    dmaNewStruct.m_Number = numTransfer;
    /* DMATransmission direction */
    dmaNewStruct.m_Direction = DMA_PERIPHERAL_TO_MEMORY;
    /* Select high Priority for DMA channel */
    dmaNewStruct.m_Priority = DMA_CHANNEL_HIGHER;
    /* Set Peripheral data size 32bits */
    dmaNewStruct.m_PeripheralDataSize = DMA_DATA_WIDTH_32_BITS;
    /* Set Memory data size 32bits */
    dmaNewStruct.m_MemoryDataSize = DMA_DATA_WIDTH_32_BITS;
    /* Disable Peripheral address increment mode in oder to read only ADC_DATA
    register */
    dmaNewStruct.m_PeripheralInc = FALSE;
    /* Enable memory address increment mode */
    dmaNewStruct.m_MemoryInc = TRUE;
    /* Enable DMA channel */
    dmaNewStruct.m_Channel = DMA_Channel;
    /* Adc trigger once to DMA transmit once */
    dmaNewStruct.m_BlockMode = DMA_TRANSFER_BYTE;
    /* Enable loop mode */
    dmaNewStruct.m_LoopMode = TRUE;
    /* used ADC_DATA register as the DMA peripheral address */
    dmaNewStruct.m_PeriphAddr = (uint32_t)(&ADCx->DATA);
    /* Set DMA memory start address */
    dmaNewStruct.m_MemoryAddr = (uint32_t)ptr_Memory;
    /* Configure DMA through dmaNewStruct */
    DMA_Configuration(DMAx, &dmaNewStruct);

    /* Trigger the channel selection register */

    /* enable DMA */
    DMA_Channel_Enable(DMAx, DMA_Channel, TRUE);
}

/**
 * @brief: Configure DMA interrupt.
 *
 *  @param[in]  ADCx: Pointer to ADC register structure.
 *  @param[in]  ADC_Channel: ADC channel. Value can be as follow:
 *              ADC_CHANNEL_NORMAL -- noraml channel
 *              ADC_CHANNEL_HP0 -- high priority channel 0
 *              ADC_CHANNEL_HP1 -- high priority channel 1
 *              ADC_CHANNEL_HP2 -- high priority channel 2
 *              ADC_CHANNEL_HP3 -- high priority channel 3
 *  @param[in]  Preemption: The preemption of DMA interrupt.
 *  @param[in]  SubPriority: The subpriority of DMA interrupt.
 *  @param[out] None
 * @retval: None
 */
void ADC_DMA0_Int_Config(ADC_SFRmap *ADCx, uint32_t ADC_Channel, uint32_t Preemption, uint32_t SubPriority)
{
    uint32_t DMA_Channel;
    /* Configure DMA0 and interrupt index */
    DMA_SFRmap    *DMAx      = DMA0_SFR;
    InterruptIndex INT_Index = INT_DMA0;

    /* Set DMA Channel */
    DMA_Channel = DMA_CHANNEL_5;

    /* Enable DMA transferred half of data to interrupt */
    DMA_Set_INT_Enable(DMAx, DMA_Channel, DMA_INT_HALF_TRANSFER, TRUE);
    /* clear DMA transferred half of data IRQ */
    DMA_Clear_INT_Flag(DMAx, DMA_Channel, DMA_INT_HALF_TRANSFER);

    /* enable DMA transferred all of data to interrupt */
    DMA_Set_INT_Enable(DMAx, DMA_Channel, DMA_INT_FINISH_TRANSFER, TRUE);
    /* clear DMA transferred all of data IRQ */
    DMA_Clear_INT_Flag(DMAx, DMA_Channel, DMA_INT_FINISH_TRANSFER);

    /* Set interrupt priority */
    INT_Interrupt_Priority_Config(INT_Index, Preemption, SubPriority);
    /* enable DMA interrupt */
    INT_Interrupt_Enable(INT_Index, TRUE);
    /* clear DMA IRQ flag */
    INT_Clear_Interrupt_Flag(INT_Index);
}

/**
 * @brief: Configure ADC GPIO.
 *
 *  @param[in]  GPIOx: Pointer to GPIO register structure.
 *  @param[in]  GpioPin: Pin mask of GPIO.
 *  @param[out] None
 * @retval: None
 */
void ADC_GPIO_Init(GPIO_SFRmap *GPIOx, uint16_t GpioPin)
{
    GPIO_Write_Mode_Bits(GPIOx, GpioPin, GPIO_MODE_AN);
}
