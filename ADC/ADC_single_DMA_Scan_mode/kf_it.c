/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "User_ADC.h"
/*******************************************************************************
**                   KF32A1x6 Processor Exceptions Handlers
*******************************************************************************/
/**
 * @brief:NMI Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _NMI_exception(void) {}

/**
 * @brief:HardFault Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _HardFault_exception(void) {}

/**
 * @brief:StackFault Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _StackFault_exception(void) {}

/**
 * @brief:SVC Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SVC_exception(void) {}

/**
 * @brief:SoftSV Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SoftSV_exception(void) {}

/**
 * @brief:SysTick Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SysTick_exception(void) {}

/**
 * @brief:DMA0 Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _DMA0_exception(void)
{
    uint8_t i = 0;
    /*The half of adc sampling data is transferred by DMA********************************/
    if (DMA_Get_Half_Transfer_INT_Flag(DMA0_SFR, DMA_CHANNEL_5))
    {
        DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_HALF_TRANSFER);
    }

    /*The all adc sampling data is transferred by DMA************************************/
    if (DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, DMA_CHANNEL_5))
    {
        DMA_Clear_INT_Flag(DMA0_SFR, DMA_CHANNEL_5, DMA_INT_FINISH_TRANSFER);
        adc_flag = 1;
    }
}
