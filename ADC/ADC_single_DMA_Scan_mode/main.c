/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by ADC single DMA
                        Scan mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "User_ADC.h"
#include "kflog.h"

#define ADC_OPT (KF_LOG_OPT_FUNC | KF_LOG_OPT_LINE)
KF_REG_LOG(adc_single_DMA_Scan_mode, ADC_OPT);
/* Private variables *********************************************************/
ADC_InitTypeDef   ADC_InitStructure;
volatile uint16_t adc_value;
volatile uint16_t adc_PC0_value;
volatile uint16_t adc_PA8_value;
volatile uint8_t  adc_flag = 0;
/**
 *  @brief : software Delay.
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void Delay(volatile uint32_t cnt)
{
    while (cnt--)
        ;
}
/**
 * @brief:Second delay function
 *  @param[in]  second-second num
 *  @param[out] None
 * @retval:None
 */
static void delay_second(uint32_t second)
{
    uint32_t ui = 0;

    for (ui = 0; ui < second * 10; ui++)
    {
        systick_delay_ms(100);
    }
}
/******************************************************************************
**                                 main Functions
******************************************************************************/
int main()
{

    /*initialize system clock ,defaut SCLK is  72MHz */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function */
    systick_delay_init(72U);
    GPIOInit_Output_Config(LED2_PORT, LED2_PIN);
    /*Initialize the print function*/
    kfLog_Init();
    /*ADC0 DMA0 channel configuration */
    ADC_DMA0_Init(ADC0_SFR, ADC_CHANNEL_NORMAL, adc_buf, 16u);
    /*ADC0 module configuration */
    ADC_GPIO_Init(PE6_ADC_PIN);
    ADC_GPIO_Init(PA13_ADC_PIN);

    /* ========== Configure ADC ========== */
    /*reset ADC0 module,enable ADC0 clock source */
    ADC_Reset(ADC0_SFR);
    /* Initialize the ADC configuration information structure */
    ADC_Struct_Init(&ADC_InitStructure);
    /* Select HFCLK as the ADC0 sampling clock source
     * You can also select SCLK or LFCLK as the ADC0 sampling clock */
    ADC_InitStructure.m_Clock = ADC_HFCLK;
    /* ADC sampling clock Divider configuration */
    ADC_InitStructure.m_ClockDiv = ADC_CLK_DIV_16;
    /* Disable adc scan mode */
    ADC_InitStructure.m_ScanMode = TRUE;
    /*select continuous mode as adc vonversion mode */
    ADC_InitStructure.m_ContinuousMode = ADC_SINGLE_MODE;
    /*Right-aligned sampling result data */
    ADC_InitStructure.m_DataAlign = ADC_DATAALIGN_RIGHT;
    /*disable External trigger */
    ADC_InitStructure.m_ExternalTrig_EN = FALSE;
    /*select Vdd as ADC refrence voltage */
    ADC_InitStructure.m_VoltageRef = ADC_REF_AVDD;
    /*Set the scan length of the ADC regular channel*/
    ADC_InitStructure.m_NumOfConv = 16u;
    /* Initialization configuration for Analog-to-Digital Converter (ADC) module. */
    ADC_Configuration(ADC0_SFR, &ADC_InitStructure);
    /* Configure the ADC to enable */
    ADC_Cmd(ADC0_SFR, TRUE);
    /* Daley is NECESSARY, after enable ADC */
    Delay(0xffu);
    /* ========== Configure ADC END ========== */

    /*Configure the regular channel scan sequence */
    ADC_Regular_Channel_Config(ADC0_SFR, PE6_ADC_CHANNAL, 1u);
    ADC_Regular_Channel_Config(ADC0_SFR, PA13_ADC_CHANNAL, 2u);

    /*Enable normal channel DMA memory access mode. */
    ADC_Regular_Channel_DMA_Cmd(ADC0_SFR, TRUE);
    /*Configure DMA0 channel 5 trig interrupt mode */
    ADC_DMA0_Int_Config(ADC0_SFR, ADC_CHANNEL_NORMAL, 4u, 0u);
    /*enable all interrupt */
    INT_All_Enable(TRUE);
    /*software trig adc Conversion,In continuous conversion mode,only need to
    trig for the first time */
    ADC_Software_Start_Conv(ADC0_SFR);

    kf_printf("adc_single_DMA_scan test start...\r\n");
    KFLOG_I(adc_single_DMA_Scan_mode, "adc_single_DMA_scan test start...\r\n");
    while (1)
    {
        if (adc_flag)
        {
            adc_flag = 0u;
            /* Print ADC data */
            for (uint32_t i = 0; i < 2u; i++)
            {
                kf_printf("adc[%d]: %d.\r\n", i, adc_buf[i]);
            }
            ADC_Software_Start_Conv(ADC0_SFR);
        }
        systick_delay_ms(200u);
        GPIO_Toggle_Output_Data_Config(LED2_PORT, LED2_PIN);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
