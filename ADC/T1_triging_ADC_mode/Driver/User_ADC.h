/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_ADC.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                         for User_ADC
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_ADC_H_
#define USER_ADC_H_

/*******************************************************************************
**                      	       MACRO Definitions                    	  **
*******************************************************************************/
#define ADC_CHANNEL_HP0    (0u)
#define ADC_CHANNEL_HP1    (1u)
#define ADC_CHANNEL_HP2    (2u)
#define ADC_CHANNEL_HP3    (3u)
#define ADC_CHANNEL_NORMAL (4u)

/*******************************************************************************
**                      	Public Variables Definitions                  	  **
*******************************************************************************/

extern unsigned int     adc_buf[32];
extern uint8_t          adc_value0[32];
extern uint8_t          adc_value1[32];
volatile extern uint8_t adc_flag;

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/

void ADC0_channel_DMA_Init(void);
void Config_DMA_interrupt(void);
void ADC_GPIO_Init(GPIO_SFRmap *GPIOx, uint16_t GpioPin);
void ADC_Struct_Init(ADC_InitTypeDef *adcInitStruct);
void GPIOInit_Output_Config(GPIO_SFRmap *GPIOx, uint16_t GpioPin);

void ADC_DMA0_Init(ADC_SFRmap *ADCx, uint32_t ADC_Channel, void *ptr_Memory, uint16_t numTransfer);
void ADC_DMA0_Int_Config(ADC_SFRmap *ADCx, uint32_t ADC_Channel, uint32_t Preemption, uint32_t SubPriority);
#endif /* USER_ADC_H_ */
