/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_General_Timer.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides template for T18.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_GENERAL_TIMER_H_
#define USER_GENERAL_TIMER_H_

/* Timer work mode typedef */
typedef enum TIM_MODE
{
    TIM_Timer = 0x1U,
    TIM_Count,
} Time_Mode_t;

/* Timer initialise configuration typedef */
typedef struct TIM_CONFIG
{
    Time_Mode_t mode;
    uint32_t    countMode;
} Tim_Config_t;

/* Timer interrupt configuration typedef */
typedef struct TIM_INT_CONFIG
{
    FunctionalState newState;
    InterruptIndex  timerIntIndex;
    uint8_t         preemption;
    uint8_t         subPriority;
} Tim_Int_Config_t;
/*******************************************************************************
**                                 Macro Defines                              **
*******************************************************************************/
/* Max delay time in us: 6500000 */
#define GPTIM_Init_us(GPTIMx, config, period) GPTIM_Init((GPTIMx), (config), (period))

/* Max delay time in ms: 6500 */
#define GPTIM_Init_ms(GPTIMx, config, period) GPTIM_Init((GPTIMx), (config), (period)*1000u)

/*******************************************************************************
**                      	Global Functions 		             	      	  **
*******************************************************************************/

/* =================== OLD API =================== */
extern void GPTIM_Int_Init(GPTIM_SFRmap *GPTIMx, InterruptIndex INT_TX);

extern void GPTIM_Timer_Init(GPTIM_SFRmap *GPTIMx, uint16_t CNT_T, uint16_t Prescaler);

extern void GPTIM_Count_Init(GPTIM_SFRmap *GPTIMx, uint16_t CNT_T);

/* =================== NEW API =================== */
extern uint32_t GPTIM_Init(GPTIM_SFRmap *GPTIMx, Tim_Config_t *config, uint32_t period);

extern void GPTIM_INT_Config(GPTIM_SFRmap *GPTIMx, Tim_Int_Config_t *intConfig);

extern void GPTIM_Gpio_Init(GPIO_SFRmap *GPIOx, uint16_t GPIO_Pin, uint32_t PinRemap);

#endif /* USER_GENERAL_TIMER_H_ */
