/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"

volatile extern uint8_t adc_trig_flag;

/*******************************************************************************
**                   KF32A1x6 Processor Exceptions Handlers
*******************************************************************************/
/**
 * @brief:NMI Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _NMI_exception(void) {}

/**
 * @brief:HardFault Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _HardFault_exception(void) {}

/**
 * @brief:StackFault Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _StackFault_exception(void) {}

/**
 * @brief:SVC Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SVC_exception(void) {}

/**
 * @brief:SoftSV Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SoftSV_exception(void) {}

/**
 * @brief:EINT19TO17 Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _EINT19TO17_exception(void) {}

/**
 * @brief:SysTick Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SysTick_exception(void) {}

/**
 * @brief:T1 Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _T1_exception(void)
{
    if (GPTIM_Get_Overflow_INT_Flag(T1_SFR))
    {
        GPTIM_Clear_Overflow_INT_Flag(T1_SFR);
        adc_trig_flag = 1u;
    }
}
