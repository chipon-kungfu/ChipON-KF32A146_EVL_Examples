/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by adc through T1 treg
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "User_ADC.h"
#include "User_General_Timer.h"
#include "kflog.h"

#define ADC_OPT (KF_LOG_OPT_FUNC | KF_LOG_OPT_LINE)
KF_REG_LOG(adc_T1_triging_ADC_mode, ADC_OPT);
/* Private variables *********************************************************/

volatile uint16_t adc_value;
/* public variables ***********************************************************/
volatile uint8_t adc_trig_flag = 0;

/**
 *  @brief : software Delay.
 *
 *  @param[in]  cnt
 *  @retval :None
 */
void Delay(volatile uint32_t cnt)
{
    while (cnt--)
        ;
}

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main(void)
{
    ADC_InitTypeDef ADC_InitStructure;

    /*initialize system clock,defaut SCLK is 72MHz */
    SystemInit(72U);

    GPIOInit_Output_Config(LED2_PORT, LED2_PIN);

    /*Initialize Timer 1 and configuration Timer 1 interrupt */
    {
        Tim_Config_t GPTIM_Structure = {
          .mode      = TIM_Timer,
          .countMode = GPTIM_COUNT_UP_OF,
        };
        Tim_Int_Config_t GPTIM_Int_Structure = {
          .newState      = TRUE,
          .timerIntIndex = INT_T1,
          .preemption    = 4u,
          .subPriority   = 0u,
        };

        GPTIM_Init_ms(T1_SFR, &GPTIM_Structure, 500u);
        /* Configure TxIF to trigger signal */
        GPTIM_Master_Mode_Config(T1_SFR, GPTIM_MASTER_TXIF_SIGNAL);
        GPTIM_INT_Config(T1_SFR, &GPTIM_Int_Structure);
        GPTIM_Cmd(T1_SFR, TRUE);
    }

    /*Initialize the print function*/
    kfLog_Init();
    /*ADC0 module configuration */
    ADC_GPIO_Init(EXAMPLE_ADC_PIN);

    /* ========== Configure ADC ========== */
    /*reset ADC0 module,enable ADC0 clock source */
    ADC_Reset(ADC0_SFR);
    /* Initialize the ADC configuration information structure */
    ADC_Struct_Init(&ADC_InitStructure);
    /* Select HFCLK as the ADC0 sampling clock source
     * You can also select SCLK or LFCLK as the ADC0 sampling clock */
    ADC_InitStructure.m_Clock = ADC_HFCLK;
    /* ADC sampling clock Divider configuration */
    ADC_InitStructure.m_ClockDiv = ADC_CLK_DIV_16;
    /* Disable adc scan mode */
    ADC_InitStructure.m_ScanMode = FALSE;
    /*select continuous mode as adc vonversion mode */
    ADC_InitStructure.m_ContinuousMode = ADC_SINGLE_MODE;
    /*Right-aligned sampling result data */
    ADC_InitStructure.m_DataAlign = ADC_DATAALIGN_RIGHT;
    /*disable External trigger */
    ADC_InitStructure.m_ExternalTrig_EN = TRUE;
    /*t1 is used as the external trigger condition of the ADC regular channel*/
    ADC_InitStructure.m_ExternalTrig = ADC_EXTERNALTRIG_T1TRGO;
    /*select Vdd as ADC refrence voltage */
    ADC_InitStructure.m_VoltageRef = ADC_REF_AVDD;
    /* Initialization configuration for Analog-to-Digital Converter (ADC) module. */
    ADC_Configuration(ADC0_SFR, &ADC_InitStructure);
    /* Configure the ADC to enable */
    ADC_Cmd(ADC0_SFR, TRUE);
    /* Daley is NECESSARY, after enable ADC */
    Delay(0xffu);
    /* ========== Configure ADC END ========== */

    /* Configure ADC regular channel */
    ADC_Regular_Channel_Config(ADC0_SFR, EXAMPLE_ADC_CHANNAL, 0x01);
    /*enable all interrupt */
    INT_All_Enable(TRUE);

    kf_printf("adc_T1_triging test start...\r\n");
    KFLOG_I(adc_T1_triging_ADC_mode, "adc_T1_triging test start...\r\n");
    while (1)
    {
        /*When timer T1 triggers adc0 interrupt, read the sample value of
        adc0,and output through the serial port 0 */
        if (adc_trig_flag)
        {
            adc_trig_flag = 0u;
            while (SET != ADC_Get_INT_Flag(ADC0_SFR, ADC_INT_EOC))
            {
                ;
            }
            adc_value = ADC_Get_Conversion_Value(ADC0_SFR);
            kf_printf("adc__value:%0d\r\n", adc_value);
            GPIO_Toggle_Output_Data_Config(LED2_PORT, LED2_PIN);
        }
        else
        {
            /* Empty */
        }
    }
}
/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
