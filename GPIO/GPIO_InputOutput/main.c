/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Flash the LED1 every 500ms on the KF32A146 EVB,
 *                     and press the key to flip another LED2
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
/******************************************************************************
**                          Private variables
******************************************************************************/
static volatile uint32_t TimingDelay;

/*******************************************************************************
**                                Global Functions
*******************************************************************************/
/**
 *  @brief :Initialize the different GPIO ports
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void BoardGpioInit(void)
{
    /* Configure LED1/2 output */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);
    GPIOInit_Output_Config(LED2_PORT, LED2_PIN);

    /* Configure user key input */
    GPIOInit_Input_Config(KEY2_PORT, KEY2_PIN);
}

/**
 *  @brief: Delay time
 *  @param[in]  nms
 *  @param[out] None
 *  @retval : None
 */
void delay_ms(volatile uint32_t nms)
{
    volatile uint32_t i, j;
    for (i = 0; i < nms; i++)
    {
        j = 5000;
        while (j--)
            ;
    }
}
/*******************************************************************************
**                                 Main Functions
*******************************************************************************/

int main()
{
    /* Initialize the system clock is 72MHz*/
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 72MHz */
    systick_delay_init(72U);
    /* Initialize the user IOs */
    BoardGpioInit();

    while (1)
    {
        /* Toggle LED1 */
        if (++TimingDelay >= 10)
        {
            TimingDelay = 0;
            GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
        }
        /* Turn ON/OFF the dedicate LED2 by user key */
        if (GPIO_Read_Input_Data_Bit(KEY2_PORT, KEY2_PIN) == Bit_SET)
        {
            delay_ms(30);
            if (GPIO_Read_Input_Data_Bit(KEY2_PORT, KEY2_PIN) == Bit_SET)
            {
                GPIO_Set_Output_Data_Bits(LED2_PORT, LED2_PIN, Bit_SET);
            }
        }
        else
        {
            GPIO_Set_Output_Data_Bits(LED2_PORT, LED2_PIN, Bit_RESET);
        }
        systick_delay_ms(50);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
