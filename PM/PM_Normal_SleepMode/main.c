/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This example demonstrates how to wake up by external
 *                         interrupt in normal sleep mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"

/******************************************************************************
**                      	Private variables                                **
******************************************************************************/
volatile uint32_t TimingDelay = 0;
volatile uint32_t SleepCount  = 0;

/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/
/**
 *  @brief :Initialize the LED GPIO ports
 * 
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void BoardGpioInit(void)
{
    /**LED1 LED2*/
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);

    /**S2*/
    GPIO_Write_Mode_Bits(PD4_KEY2_PIN, GPIO_MODE_IN);
}

/**
 *  @brief :Configure the PC0 is interrupt mode, and working in rising and falling edges
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void BoardUserKeyInit(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    EINT_InitTypeDef EINT_InitStructure;

    /* Configure the external interrupt */
    EINT_InitStructure.m_Line   = INT_EXTERNAL_INTERRUPT_4;
    EINT_InitStructure.m_Mask   = TRUE;
    EINT_InitStructure.m_Fall   = TRUE;
    EINT_InitStructure.m_Rise   = FALSE;
    EINT_InitStructure.m_Source = INT_EXTERNAL_SOURCE_PD;
    INT_External_Configuration(&EINT_InitStructure);
}

/*******************************************************************************
**                     			Main Functions 		             	     	  **
*******************************************************************************/
/**
 *  @brief :Main program
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
int main()
{
    /* Initialize the system clock is 72MHz*/
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 72MHz */
    systick_delay_init(72U);
    /* Initialize the LED IOs */
    BoardGpioInit();
    /* Configure PD4 in interrupt mode */
    BoardUserKeyInit();
    /* Enable and set EXTI0 Interrupt to the lowest priority */
    INT_Interrupt_Enable(INT_EINT4, TRUE);
    /* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
    INT_Interrupt_Priority_Config(INT_EINT4, 7, 1);
    /* Enable the global interrupt */
    INT_All_Enable(TRUE);

    while (1)
    {
        /* Turn ON/OFF the LED1 every 500ms */
        if (++TimingDelay >= 10)
        {
            TimingDelay = 0;
            GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
            /* The LED will enter the normal sleep mode
                 every 10 flashes and wait for wake-up */
            if (++SleepCount >= 10)
            {
                SleepCount = 0;

                /*Lower the system clock*/
                OSC_SCK_Source_Config(SCLK_SOURCE_INTHF);
                OSC_PLL_Software_Enable(FALSE);
                OSC_HFCK_Enable(FALSE);
                OSC_EXTHF_Software_Enable(FALSE);
                OSC_SCK_Division_Config(SCLK_DIVISION_16);

                /*Enter the mode of sleep*/
                asm("NOP");
                asm("NOP");
                asm("SLEEP");

                /*Recover the system clock*/
                SystemInit(72U);
            }
        }
        systick_delay_ms(50);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
