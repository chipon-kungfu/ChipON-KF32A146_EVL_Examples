/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This example demonstrates how go to Standeby sleep mode,
 *                         and wakeup source
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"

/******************************************************************************
**                      	Private variables                                **
******************************************************************************/
static volatile uint32_t TimingDelay;

/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/
/**
 *  @brief :Initialize the LED GPIO ports
 *
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void BoardGpioInit(void)
{
    /**LED1 LED2 */
    GPIO_Write_Mode_Bits(PA3_LED1_PIN, GPIO_MODE_OUT);
    GPIO_Write_Mode_Bits(PF11_LED2_PIN, GPIO_MODE_OUT);

    /**Key2*/
    GPIO_Write_Mode_Bits(PD4_KEY2_PIN, GPIO_MODE_IN);
    GPIO_Pull_Up_Enable(PD4_KEY2_PIN, TRUE);
}

/**
 *  @brief :Config the BKP area is enable, BKP register and data can be write and read
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void BKP_WriteReadEnable(void)
{
    /* The backup domain register allows reading and writing */
    SFR_SET_BIT_ASM(OSC_CTL0, OSC_CTL0_PMWREN_POS);
    /* Backup domain exits reset mode */
    SFR_SET_BIT_ASM(PM_CTL0, PM_CTL0_BKPREGCLR_POS);
    /* The backup area allows reading and writing */
    SFR_SET_BIT_ASM(PM_CTL0, PM_CTL0_BKPWR_POS);
}

/**
 *  @brief : Initialize the WKUP2 Pin in PC0, WKUP1~5 in this way
 * 			'BKP_WriteReadEnable' function called must be first!!!
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void WakeupPinInit(void)
{
    /* Enable WKUP pin pull if WKUP edge is falling. */
    GPIO_Pull_Up_Enable(PD4_WKUP3_PIN, TRUE);
    /*Enable the BKP area is enable*/
    BKP_Write_And_Read_Enable(TRUE);

    /* Enable WKUP2 to wake up*/
    PM_External_Wakeup_Pin_Enable(PM_PIN_WKP3, TRUE);
    /* Set WKUP edge trigger. */
    PM_External_Wakeup_Edge_Config(PM_PIN_WKP3, PM_TRIGGER_FALL_EDGE);
    /* Clear WKUP flag. */
    PM_Clear_External_Wakeup_Pin_Flag(PM_WAKEUP_EXTERNAL_PIN_WKP3);

    /* Configuration WKUP Interrupt Priority */
    INT_Interrupt_Priority_Config(INT_WKP3, 0, 0);
    /* Enable WKUP Interrupt. */
    INT_Interrupt_Enable(INT_WKP3, FALSE);
    /* Clear WKUP interrupt flag. */
    INT_Clear_Interrupt_Flag(INT_WKP3);
}

/**
 *  @brief :Set the standby sleep mode, and go to sleep
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void SystemGotoStandbyMode(void)
{
    PM_CAL2 |= 0x80;
    PM_CTL0 |= 0x800;

    /* Set standby sleep mode */
    PM_Low_Power_Mode_Config(PM_LOW_POWER_MODE_STANDBY);

    /**Reset MRBGEN*/
    SFR_CLR_BIT_ASM(PM_CTL0, PM_CTL0_MRBGEN_POS);

    /* Disable read and write of BKP */
    BKP_Write_And_Read_Enable(FALSE);

    // Clear all of interrupt flags
	INT_EIF0 = 0;
	INT_EIF1 = 0;
	INT_EIF2 = 0;
    /* Clear WKUP interrupt flag. */    
    INT_Clear_Interrupt_Flag(INT_WKP3);
    /*Disable interrupt*/
	asm("DSI");
    /* Go to sleep */
    asm("SLEEP");
}

/**
 *  @brief :Initialize the PMC INTERRUPT
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void PMC_Error_Init()
{
    EINT_InitTypeDef EINT_31To20;

    INT_External_Struct_Init(&EINT_31To20);
    EINT_31To20.m_Mask = TRUE;                      // EXIT enable or disable.
    EINT_31To20.m_Rise = TRUE;                      // EXIT rising.
    EINT_31To20.m_Line = INT_EXTERNAL_INTERRUPT_21; // EXIT num.
    EINT_31To20.m_Fall = FALSE;                     // EXIT falling.
    INT_External_Configuration(&EINT_31To20);

    INT_External_Clear_Flag(INT_EXTERNAL_INTERRUPT_21);
    /* Enable PMC error interrupt. */
    PM_CTL2 |= (1 << PM_CTL2_PMCIE_POS);
    /* Configuration EINT Interrupt Priority */
    INT_Interrupt_Priority_Config(INT_EINT31TO20, 0, 0);
    /* Enable EINT Interrupt. */
    INT_Interrupt_Enable(INT_EINT31TO20, TRUE);
    /* Clear EINT interrupt flag. */
    INT_Clear_Interrupt_Flag(INT_EINT31TO20);
    /*Enable Global INT */
    INT_All_Enable(TRUE);
}

/*******************************************************************************
**                     			Main Functions 		             	     	  **
*******************************************************************************/
/**
 *  @brief :Main program
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
int main()
{
    /* Initialize the system clock is 72MHz */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 72MHz */
    systick_delay_init(72U);
    /* Initialize the LED IOs */
    BoardGpioInit();
    /* Set the BKP area is enable */
    BKP_Write_And_Read_Enable(TRUE);

    /* Query whether it is WKUP2 pin wakeup, clear WKUP2 flag, and light the LED2 */
    if (PM_Get_Reset_Flag(PM_WAKEUP_EXTERNAL_PIN_WKP2))
    {
        PM_Clear_External_Wakeup_Pin_Flag(PM_WAKEUP_EXTERNAL_PIN_WKP2);
        // GPIO_Set_Output_Data_Bits(PH3_LED2_PIN, Bit_SET);
    }
    PM_External_Wakeup_Pin_Enable(PM_PIN_WKP2, FALSE);
    /* Turn ON/OFF the LED1 every 200ms for 10 times, it showing power on state */
    do
    {
        GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
        systick_delay_ms(200);
    } while ((TimingDelay++ < 10));
    while (1)
    {
        /* Scan the user key */
        if (GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN) == Bit_RESET)
        {
            PMC_Error_Init();
            /* Initialize the WKUP2 Pin in PC0 */
            WakeupPinInit();
            /* Go to Sleep */
            SystemGotoStandbyMode();
        }
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
