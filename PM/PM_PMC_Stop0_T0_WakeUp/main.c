/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This example demonstrates how go to Stop0 sleep mode,
 *                         and wakeup from T0 interrupt
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "stdio.h"
#include "system_init.h"
#include "Stop0.h"
#include "Board_GpioCfg.h"

/******************************************************************************
**                      	Private variables                                **
******************************************************************************/
#define LED1_ON()                                                                                                      \
    ;                                                                                                                  \
    GPIO_Set_Output_Data_Bits(PA3_LED1_PIN, Bit_SET);
#define LED1_OFF()                                                                                                     \
    ;                                                                                                                  \
    GPIO_Set_Output_Data_Bits(PA3_LED1_PIN, Bit_RESET);
#define LED1_Toggle()                                                                                                  \
    ;                                                                                                                  \
    GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);

#define LED2_ON()                                                                                                      \
    ;                                                                                                                  \
    GPIO_Set_Output_Data_Bits(PF11_LED2_PIN, Bit_SET);
#define LED2_OFF()                                                                                                     \
    ;                                                                                                                  \
    GPIO_Set_Output_Data_Bits(PF11_LED2_PIN, Bit_RESET);
#define LED2_Toggle()                                                                                                  \
    ;                                                                                                                  \
    GPIO_Toggle_Output_Data_Config(PF11_LED2_PIN);


/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/

/**
 * @brief: LPTIMER T0 Cconfig
 *
 * @param[in] Period Timer Period value
 * @param[in] Prescaler Timer predivision value
 */
void LPTIMER_T0_Config(uint32_t Period, uint32_t Prescaler)
{

    BKP_Write_And_Read_Enable(TRUE);                         /*Enable backup read and write*/
    PM_CTL2 &= (~PM_CTL2_CCP0LPEN);                             /*Reset T0*/
    PM_CCP0CLKLPEN_Enable(TRUE);                                /*Enable LPCCP0 clock*/
    TIM_Reset(T0_SFR);
    PM_CTL2 |= PM_CTL2_CCP0LPEN;                                /*Enable LPCCP0 reset*/
    GPTIM_Updata_Immediately_Config(T0_SFR, TRUE);              /*Timer update immediately*/
    GPTIM_Updata_Enable(T0_SFR, TRUE);
    GPTIM_Work_Mode_Config(T0_SFR, GPTIM_TIMER_MODE);           /*Select Timer work mode to Timing*/
    GPTIM_Set_Counter(T0_SFR, 0);                               /*Set counter,period,scaler of timer*/
    GPTIM_Set_Period(T0_SFR, Period);
    GPTIM_Set_Prescaler(T0_SFR, Prescaler);
    GPTIM_Counter_Mode_Config(T0_SFR, GPTIM_COUNT_UP_DOWN_OUF); /*Slect count mode*/
    GPTIM_Clock_Config(T0_SFR, GPTIM_T0_INTLF);                 /*Slect work clock*/

    INT_Interrupt_Priority_Config(INT_T0, 4, 0);    /*Coonfig interrupt of timer*/
    GPTIM_Overflow_INT_Enable(T0_SFR, TRUE);
    INT_Interrupt_Enable(INT_T0, TRUE);
    INT_Clear_Interrupt_Flag(INT_T0);
    GPTIM_Cmd(T0_SFR, TRUE);                        /*Enable timer*/
    INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);
    INT_All_Enable(TRUE);

    BKP_Write_And_Read_Enable(FALSE);
}
/*******************************************************************************
**                     			Main Functions 		             	     	  **
*******************************************************************************/

int main()
{
    /* Initialize the system clock is 72MHz */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 72MHz */
    systick_delay_init(72U);
    /* Initialize GPIO */
    GPIOInit_Input_Config(PD4_KEY2_PIN);
    GPIO_Pull_Up_Enable(PD4_KEY2_PIN, TRUE);
    GPIOInit_Output_Config(PA3_LED1_PIN);
    GPIOInit_Output_Config(PF11_LED2_PIN);
    // GPIOInit_Output_Config(PA4_LED3_PIN);
    /* Timer0 configured as follow:
        - InputClock = INTLF = 32K
        - Prescal =63+1
        - Period = 1000
     */
    LPTIMER_T0_Config(1000, 63);

    while (1)
    {
        if (GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN) == Bit_SET)
        {
            systick_delay_ms(10);
            if (GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN) == Bit_SET)
            {
                LED1_OFF();
                LED2_OFF();

                Goto_Stop0_test();
                LED1_ON();
                for (int i = 0; i < 2; i++)
                {
                    LED1_Toggle();
                    LED2_Toggle();
                    systick_delay_ms(200);
                }
            }
        }
        else if (GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN) == Bit_RESET)
        {
            systick_delay_ms(10);
            if (GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN) == Bit_RESET)
            {
                LED2_OFF();
                LED1_Toggle();
            }
        }
        systick_delay_ms(500);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param file pointer to the source file name
 * 	@param line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
