/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/***************************************    ***************************************
**                          Include Files                                    **
******************************************************************************/
#include "system_init.h"

/*******************************************************************************
**                   KF32A146 Processor Exceptions Handlers  		         **
*******************************************************************************/

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception(void) {}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _HardFault_exception(void) {}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception(void) {}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception(void) {}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception(void) {}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception(void) {}

void __attribute__((interrupt)) _EINT31TO20_exception() //
{
    volatile uint32_t wait_flag = 0x0000;

    if (INT_EINTF & (1 << INT_EXTERNAL_INTERRUPT_21))
    {
        PM_STAC |= (1 << PM_STAC_PMCIC_POS);
        INT_EINTF &= ~(1 << INT_EXTERNAL_INTERRUPT_21);
        INT_EIF2 &= ~INT_EIF2_EINT31TO20IF;

        while (((PM_STA1 & (1 << PM_STA1_STOPERROR_POS)) || (PM_STA1 & (1 << PM_STA1_PMCERROR_POS))) &&
               (wait_flag != FLAG_TIMEOUT))
        {
            wait_flag++;
        }
        PM_STAC &= ~(1 << PM_STAC_PMCIC_POS);
    }
    // USER Code
}

//*****************************************************************************************
//                              WKP4TO0 Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _WKP4TO0_exception()
{
    PM_STAC |= (1 << PM_STAC_PMCIC_POS);

    PM_STAC |= (1 << PM_STAC_WKP3SC_POS);

    PM_STAC &= ~(1 << PM_STAC_PMCIC_POS);

    PM_STAC &= ~(1 << PM_STAC_WKP3SC_POS);

    INT_EIF2 &= ~(1 << INT_EIF2_WKPIF_POS);

}
