/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This example demonstrates how go to Stop0 sleep mode,
 *                         and wakeup from EXTI interrupt1
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "stdio.h"
#include "system_init.h"
#include "Stop0.h"
#include "Board_GpioCfg.h"

/******************************************************************************
**                      	Private variables                                **
******************************************************************************/

#define LED1_ON()                                                                                                      \
    ;                                                                                                                  \
    GPIO_Set_Output_Data_Bits(PA3_LED1_PIN, Bit_SET);
#define LED1_OFF()                                                                                                     \
    ;                                                                                                                  \
    GPIO_Set_Output_Data_Bits(PA3_LED1_PIN, Bit_RESET);
#define LED1_Toggle()                                                                                                  \
    ;                                                                                                                  \
    GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);

#define LED2_ON()                                                                                                      \
    ;                                                                                                                  \
    GPIO_Set_Output_Data_Bits(PF11_LED2_PIN, Bit_SET);
#define LED2_OFF()                                                                                                     \
    ;                                                                                                                  \
    GPIO_Set_Output_Data_Bits(PF11_LED2_PIN, Bit_RESET);
#define LED2_Toggle()                                                                                                  \
    ;                                                                                                                  \
    GPIO_Toggle_Output_Data_Config(PF11_LED2_PIN);

/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/

/*******************************************************************************
**                     			Main Functions 		             	     	  **
*******************************************************************************/
/**
 *  @brief :Main program
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
int main()
{
    /* Initialize the system clock is 72MHz*/
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 72MHz */
    systick_delay_init(72U);
    /* Initialize GPIO */
    GPIOInit_Input_Config(PD4_KEY2_PIN);
    GPIO_Pull_Up_Enable(PD4_KEY2_PIN, TRUE);
    GPIOInit_Output_Config(PA3_LED1_PIN);
    GPIOInit_Output_Config(PF11_LED2_PIN);

    while (1)
    {
        if (GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN) == Bit_SET)
        {
            systick_delay_ms(50);
            if (GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN) == Bit_SET)
            {
                LED1_OFF();
                LED2_OFF();

                Goto_Stop0_test();

                LED1_ON();
                for (int i = 0; i < 10; i++)
                {
                    LED1_Toggle();
                    LED2_Toggle();
                    systick_delay_ms(200);
                }
            }
        }
        else if (GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN) == Bit_RESET)
        {
            systick_delay_ms(50);
            if (GPIO_Read_Input_Data_Bit(PD4_KEY2_PIN) == Bit_RESET)
            {
                LED2_OFF();
                LED1_Toggle();
            }
        }
        systick_delay_ms(500);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
