/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Stop0_Wakeup_Vector.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of STOP0 sleep mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"

asm(
"	.global	__Stop0_Wakeup_Vector"						"\n"
"	.section .ramvector"								"\n"
"__Stop0_Wakeup_Vector:"								"\n"
"	.long	__initial_sp"				"\n"
"	.long	startup"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_T7_QEI0_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_T0_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_USART0_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_EINT19to17_exception"					"\n"
"	.long	__Stop0_Wakeup_FlexCAN6_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_EINT31TO20_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_WKP0to4_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.long	__Stop0_Wakeup_exception"					"\n"
"	.weak	startup"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_T7_QEI0_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_T0_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_USART0_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_EINT19to17_exception"					"\n"
"	.weak	__Stop0_Wakeup_FlexCAN6_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_EINT31TO20_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_WKP0to4_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
"	.weak	__Stop0_Wakeup_exception"					"\n"
);




#define pFunc void (*Func)(void)
typedef struct
{
  pFunc;
}interruptVector;

typedef struct
{
  int  			 *value;				//auto variable by tool ,value is default sp
  interruptVector Reset_Enter;			//Enter Function,design lead function and run to main

  interruptVector NMI_Enter;
  interruptVector HardFault_Enter;
  interruptVector Rev4_Enter;
  interruptVector StackFault_Enter;
  interruptVector AriFault_Enter;
  interruptVector Intended_Rev;   		// create characteristic by linker,code write 0 here

  interruptVector interrupt[120] ;		// other
}VectorEnter;

extern int  __initial_sp  ;
extern void  startup();



//*****************************************************************************************
//                              WKP4TO0 Interrupt Course in STOP0 Mode
//*****************************************************************************************
void __attribute__((interrupt)) __attribute__((section(".indata"))) __Stop0_Wakeup_WKP0to4_exception()
{
    /* Disable WKUP Interrupt. */
    INT_EIE2 &= ~(1 << INT_EIF2_WKPIF_POS);

    PM_STAC |= (1 << PM_STAC_PMCIC_POS);
    
    PM_STAC |= (1 << PM_STAC_WKP3SC_POS);

    PM_STAC &= ~(1 << PM_STAC_PMCIC_POS);

    PM_STAC &= ~(1 << PM_STAC_WKP3SC_POS);

    INT_EIF2 &= ~(1 << INT_EIF2_WKPIF_POS);
}

//*****************************************************************************************
//                              EINT31TO20 Interrupt Course in STOP0 Mode
//*****************************************************************************************
void __attribute__((interrupt)) __attribute__((section(".indata"))) __Stop0_Wakeup_EINT31TO20_exception() // EWDT:EINT19  RTC:EINT17
{
    volatile uint32_t wait_flag = 0x0000;
    
    if (INT_EINTF & (1 << INT_EXTERNAL_INTERRUPT_21))
    {
        PM_STAC |= (1 << PM_STAC_PMCIC_POS);
        INT_EINTF &= ~(1 << INT_EXTERNAL_INTERRUPT_21);
        INT_EIF2 &= ~INT_EIF2_EINT31TO20IF;
        
        while (((PM_STA1 & (1 << PM_STA1_STOPERROR_POS)) || (PM_STA1 & (1 << PM_STA1_PMCERROR_POS))) && (wait_flag != FLAG_TIMEOUT))
        {
            wait_flag++;
        }    
        PM_STAC &= ~(1 << PM_STAC_PMCIC_POS);
    }
    // USER Code
}

void __attribute__((interrupt)) __attribute__((section(".indata"))) __Stop0_Wakeup_T7_QEI0_exception()
{
}



void __attribute__((interrupt)) __attribute__((section(".indata"))) __Stop0_Wakeup_T0_exception()
{
}


void __attribute__((interrupt)) __attribute__((section(".indata"))) __Stop0_Wakeup_USART0_exception()
{
}


void __attribute__((interrupt)) __attribute__((section(".indata"))) __Stop0_Wakeup_FlexCAN6_exception()
{
}



//*****************************************************************************************
//                              Default Interrupt Course in STOP0 Mode
//*****************************************************************************************
void __attribute__((interrupt)) __attribute__((section(".indata"))) __Stop0_Wakeup_exception()
{
    /*For undefined interrupts*/
    asm("RESET");
}

