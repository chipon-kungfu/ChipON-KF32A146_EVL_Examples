/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Stop0.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of STOP0 sleep mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "Stop0.h"
#include "Reload_Kernel.h"

void __attribute__((section(".indata"))) __Stop0_Sleep_Function(void)
{
    asm("DSI");
    /*Change vector to __Stop0_Wakeup_Vector*/
    SYS_VECTOFF = (uint32_t)__Stop0_Wakeup_Vector;
    /*Enable interrupt, it is necessary*/
    asm("ENI");
    /* Enable WKUP Interrupt. */
    INT_EIE2 |= ((uint32_t)1 << INT_EIE2_WKPIE_POS);
    asm("NOP");
    asm("SLEEP");
    asm("NOP");
    /*Disable interrupt*/
    asm("DSI");
    __Reload_Kernel_Trim();
    /*Reset Vector to default*/
    SYS_VECTOFF = (uint32_t)_start;
}

void PMC_Error_Init()
{
    EINT_InitTypeDef EINT_31To20;

    INT_External_Struct_Init(&EINT_31To20);
    EINT_31To20.m_Mask = TRUE;                      // EXIT enable or disable.
    EINT_31To20.m_Rise = TRUE;                      // EXIT rising.
    EINT_31To20.m_Line = INT_EXTERNAL_INTERRUPT_21; // EXIT num.
    EINT_31To20.m_Fall = FALSE;                     // EXIT falling.
    INT_External_Configuration(&EINT_31To20);

    INT_External_Clear_Flag(INT_EXTERNAL_INTERRUPT_21);
    /* Enable PMC error interrupt. */
    PM_CTL2 |= (1 << PM_CTL2_PMCIE_POS);
    /* Configuration EINT Interrupt Priority */
    INT_Interrupt_Priority_Config(INT_EINT31TO20, 0, 0);
    /* Enable EINT Interrupt. */
    INT_Interrupt_Enable(INT_EINT31TO20, TRUE);
    /* Clear EINT interrupt flag. */
    INT_Clear_Interrupt_Flag(INT_EINT31TO20);
    /*Enable Global INT */
    INT_All_Enable(TRUE);
}

void WKUP_Init(void)
{
    /* Enable WKUP pin pull if WKUP edge is falling. */
    GPIO_Pull_Up_Enable(GPIOD_SFR, GPIO_PIN_MASK_4, TRUE);
    /*Enable the BKP area is enable*/
    BKP_Write_And_Read_Enable(TRUE);

    /* Enable WKUP to wake up*/
    PM_External_Wakeup_Pin_Enable(PM_PIN_WKP3, TRUE);
    //    /* Set WKUP edge trigger. */
    PM_External_Wakeup_Edge_Config(PM_PIN_WKP3, PM_TRIGGER_FALL_EDGE);
    /* Clear WKUP flag. */
    PM_Clear_External_Wakeup_Pin_Flag(PM_WAKEUP_EXTERNAL_PIN_WKP3);

    /* Configuration WKUP Interrupt Priority */
    INT_Interrupt_Priority_Config(INT_WKP3, 0, 0);
    /* Enable WKUP Interrupt. */
    INT_Interrupt_Enable(INT_WKP3, FALSE);
    /* Clear WKUP interrupt flag. */
    INT_Clear_Interrupt_Flag(INT_WKP3);
}

/**
 * @brief Swap SCLK to INTLF
 * 
 * @param[in] SCLK Freaqunce of SCLK, in MHz
 * @retval uint32_t 
 *          0 -- success
 *          other -- fail
 */
uint32_t Swap_SCLK_To_INTLF(uint32_t SCLK)
{
    volatile uint32_t wait_flag = 0U;

    /* Enable INTLF */
    wait_flag = 0xFFFU;
    OSC_INTLF_Software_Enable(TRUE);
    while ((OSC_Get_INTLF_INT_Flag() != SET) && (wait_flag > 0U))
    {
        wait_flag--;
    }
    if(0U == wait_flag)
    {
        return 1U;
    }

    /* Enable INTHF */
    wait_flag = 0xFFFU;
    OSC_INTHF_Software_Enable(TRUE);
    while ((OSC_Get_INTHF_INT_Flag() != SET) && (wait_flag > 0U))
    {
        wait_flag--;
    }
    if(0U == wait_flag)
    {
        return 1U;
    }
    /* Enable low power INTLF */
    PM_Internal_Low_Frequency_Enable(TRUE);

    /* Swap SCLK to PLL DIVISION 2 */ 
    OSC_SCK_Division_Config(SCLK_DIVISION_2);
    NOP_Delay_100us(1U, SCLK / 2U);
    OSC_SCK_Source_Config(SCLK_SOURCE_INTHF);
    NOP_Delay_100us(1U, 8U);
    /* Swap SCLK to INTLF DIVISION 2 */
    OSC_SCK_Source_Config(SCLK_SOURCE_INTLF);
    OSC_SCK_Division_Config(SCLK_DIVISION_1);

    /* Disable PLL */
    OSC_PLL_Software_Enable(FALSE);
    /* Disable HFCLK */
    OSC_HFCK_Enable(FALSE);
    /* Disable INTHF */
    OSC_INTHF_Software_Enable(FALSE);
    /* Disable EXTHF. */
    OSC_EXTHF_Software_Enable(FALSE);
    /* Disable EXTLF. */
    OSC_EXTLF_Software_Enable(FALSE);

    return 0U;
}

void Goto_Stop0_test(void)
{
    BKP_Write_And_Read_Enable(TRUE);

    PMC_Error_Init();

    WKUP_Init();

    /* Set stop0 sleep mode */
    PM_Low_Power_Mode_Config(PM_LOW_POWER_MODE_STOP_0);

    /* Fail to swap SCLK to INTLF */
    if(0U != Swap_SCLK_To_INTLF(120U))
    {
        return;
    }

    /**Set MRBGEN*/
    SFR_SET_BIT_ASM(PM_CTL0, PM_CTL0_MRBGEN_POS);

    INT_EIF0 = 0; // Clear interrupt flag
    INT_EIF1 = 0;
    INT_EIF2 = 0;

    __Stop0_Sleep_Function();
    asm("ENI"); // Enable interrupt

    SystemInit(72U);
}
