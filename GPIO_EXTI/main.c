/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Flash the LED0 every 500ms on the KF32A146 Mini EVB,
 *                     and press the key to flip another LED1
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"

/******************************************************************************
**                          Private variables
******************************************************************************/
static volatile uint32_t TimingDelay;

/*******************************************************************************
**                                Global Functions
*******************************************************************************/
/**
 *  @brief :Initialize the LED GPIO ports
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void BoardGpioInit(void)
{

    GPIOInit_Output_Config(PA3_LED1_PIN);
    GPIOInit_Output_Config(PF11_LED2_PIN);
    GPIOInit_Input_Config(PD4_WKUP3_PIN);
}

/**
 *  @brief :Configure the PC0 is interrupt mode, and working in rising and falling edges
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void BoardUserKeyInit(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    EINT_InitTypeDef EINT_InitStructure;

    /* Configure the external interrupt */
    EINT_InitStructure.m_Fall   = FALSE;
    EINT_InitStructure.m_Line   = INT_EXTERNAL_INTERRUPT_4;
    EINT_InitStructure.m_Mask   = TRUE;
    EINT_InitStructure.m_Rise   = TRUE;
    EINT_InitStructure.m_Source = INT_EXTERNAL_SOURCE_PD;
    INT_External_Configuration(&EINT_InitStructure);
}

/*******************************************************************************
**                                 Main Functions
*******************************************************************************/
/**
 *  @brief :Main program
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
int main()
{
    /* Initialize the system clock is 72U*/
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 120M */
    systick_delay_init(72U);
    /* Initialize the LED IOs */
    BoardGpioInit();
    /* Configure PC0 in interrupt mode */
    BoardUserKeyInit();
    /* Configure interrupt priority group, default is 3VS1 */
	INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
    /* Enable and set EXTI4 Interrupt to the lowest priority */
    INT_Interrupt_Enable(INT_EINT4, TRUE);
    INT_Interrupt_Priority_Config(INT_EINT4, 7, 1);

    /* Enable the global interrupt */
    INT_All_Enable(TRUE);

    while (1)
    {
        /* Turn ON/OFF the LED1 every 500ms */
        if (++TimingDelay >= 10)
        {
            TimingDelay = 0;
            GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
        }
        systick_delay_ms(50);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
