/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by WWDG
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_SysTick.h"
#include "User_wwdg.h"
#include "Board_GpioCfg.h"
#include "kflog.h"

/*******************************************************************************
**                          kfprint config
*******************************************************************************/
#define WWDG_LOG_OPT (KF_LOG_OPT_LINE)
KF_REG_LOG(wwdg, WWDG_LOG_OPT);

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    uint8_t press_status = 0;
    uint8_t nopress_num  = 0;

    /*initialize system clock,defaut SCLK is 72U,select INTHF as Clock source*/
    SystemInit(72U);
    /*Low frequency working frequency selection:1/1****************************/
    OSC_LFCK_Division_Config(LFCK_DIVISION_1);
    /*Select INTLF as LFCLK clock source***************************************/
    OSC_LFCK_Source_Config(LFCK_INPUT_INTLF);
    /*enable LFCLK*************************************************************/
    OSC_LFCK_Enable(TRUE);
    /* Enable INTLF software*/
    OSC_INTLF_Software_Enable(TRUE);

    /* kfprint init */
    kfLog_Init();

    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);

    /*user key input*/
    GPIOInit_Input_Config(KEY2_PORT, KEY2_PIN);

    LED1_On();

    /*T=1/72M*72000=1ms,SCLK:120M*/
    SysTick_Configuration(72000);
    Systick_Delay(SYSTEM_DELAY_500MS);
    LED1_Off();
    WWDT_Config_TH(WWDT_Threshold);

#if (WWDT_INT_STATUS == WWDT_Enable_INT)
    KFLOG_I(wwdg, "start i2c wwdg test(enable int mode)\r\n");
#else
    KFLOG_I(wwdg, "start i2c wwdg test(disable int mode)\r\n");
#endif

    while (1)
    {
#if (WWDT_INT_STATUS == WWDT_Enable_INT)
        ;
#else
        Systick_Delay(SYSTEM_DELAY_10MS);
        if (User_Key_Down() == RESET)
        {
            if (press_status == 0)
            {
                press_status = 1;
                KFLOG_I(wwdg, "Press the button, feed the dog\r\n");
            }
            else
            {}

            if ((nopress_num >= 4u) && (nopress_num < 12u))
            {
                WWDT_Clear();
                nopress_num  = 0;
                press_status = 0;
                GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
            }
            else
            {
                nopress_num++;
            }
        }
        else
        {
            press_status = 0;
            nopress_num++;
            KFLOG_I(wwdg, "no feeding the dog,remain:%d\r\n", nopress_num);
        }
#endif
    }
}
/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
