/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_wwdg.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides provides template for 
 *                                     User_wwdg
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_IWDG_H_
#define USER_IWDG_H_

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/
/*Window limit value : 0~0x1F**************************************************/
#define WWDT_Threshold 0x1F
/*Whether to enable WWDG*******************************************************/
#define WWDT_Enable_INT  (1UL)
#define WWDT_Disable_INT (0UL)

#define WWDT_INT_STATUS (WWDT_Disable_INT)
/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/

/**
  * @brief  initialize and configure WWDT.Whether to choose to open wwdt
            interrupt
  *  @param[in]   Threshold:Set  WWDT the threshold
  *  @param[out] None
  * @retval None
  */
extern void WWDT_Config_TH(uint32_t Threshold);

/**
 * @brief  Clear the count value within the allowable operating wwdt value range
 *  @param[in]   None
 *  @param[out] None
 * @retval None
 */
extern void WWDT_Clear(void);

/**
 * @brief  get user key state.
 *  @param[in]   None
 *  @param[out] FlagStatus
 * @retval None
 */
extern FlagStatus User_Key_Down(void);

#endif /* USER_IWDG_H_ */
