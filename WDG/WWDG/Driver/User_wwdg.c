/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_wwdg.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides provides template for
 *                                     User_wwdg
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "system_init.h"
#include "User_SysTick.h"
#include "User_wwdg.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                          Private Variables Definitions
*******************************************************************************/
uint32_t WWDGCNT_DATA = 0;

/*******************************************************************************
**                          Global Functions
*******************************************************************************/
/**
  * @brief  initialize and configure WWDT.Whether to choose to open wwdt
            interrupt
  *  @param[in]  Threshold:Set  WWDT the threshold
  *  @param[out] None
  * @retval None
  */
void WWDT_Config_TH(uint32_t Threshold)
{
    /*reset WWDG*/
    WWDT_Reset();
    /*Set the lower limit of the window watchdog :0x1F*/
    WWDT_Threshold_Config(Threshold & 0x3FU);
    /*Set the prescaler 1:64, 32KHZ clock source.*/
    WWDT_Prescaler_Config(WWDT_PRESCALER_64);
    /*enable WWDT*/
    WWDT_Enable(TRUE);
#if (WWDT_INT_STATUS == WWDT_Enable_INT)
    /*enable wwdt IRQ*/
    WWDT_INT_Enable(TRUE);
    /*enable wwdt interrupt*/
    INT_Interrupt_Enable(INT_WWDT, TRUE);
    /*enable all interrupt*/
    INT_All_Enable(TRUE);
#endif
}

/**
 * @brief  Clear the count value within the allowable operating wwdt value range
 *  @param[in]  None
 *  @param[out] None
 * @retval None
 */
void WWDT_Clear(void)
{
    //    WWDT_CNT=0;
    /*get the count value of wwdt*/
    WWDGCNT_DATA = WWDT_Get_Counter();
    /*Clear the count value within the allowable operating wwdt value range*/
    if ((WWDGCNT_DATA > WWDT_Threshold) && (WWDGCNT_DATA < 0x3F))
    {
        WWDT_Counter_Config(0);
    }
}

/**
 * @brief  get user key state.
 *  @param[in]  None
 *  @param[out] FlagStatus
 * @retval None
 */
FlagStatus User_Key_Down(void)
{
    if (GPIO_Read_Input_Data_Bit(KEY2_PORT, KEY2_PIN))
    {
        return SET;
    }
    else
    {
        return RESET;
    }
}
