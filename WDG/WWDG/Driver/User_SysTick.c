/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_SysTick.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides provides template for
 *                                     User_SysTicks
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "system_init.h"
#include "User_SysTick.h"

/*******************************************************************************
**                          Private Variables Definitions
*******************************************************************************/
volatile static uint32_t TimingDelay;

/*******************************************************************************
**                          Global Functions
*******************************************************************************/
/**
 * @brief  Decrements the TimingDelay variable.
 *  @param[in]  None
 *  @param[out] None
 * @retval None
 */
void TimingDelay_Decrement(void)
{
    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }
}

/**
 *  @brief :Inserts a delay time.
 *  @param[in]  delayTime: specifies the delay time length, in milliseconds.
 *  @param[out] None
 *  @retval :None
 */
void Systick_Delay(uint32_t delayTime)
{
    TimingDelay = delayTime;
    while (TimingDelay != 0)
        ;
}

/**
 *  @brief :initialize system tick timer,T=1/SCLK*Reload.
 *  @param[in]  Reload : the count value of Systick timer enter IRQ
 *  @param[out] None
 *  @retval :None
 */
void SysTick_Configuration(uint32_t Reload)
{
    /*before initializtion,disable systick timer*/
    SYSTICK_Cmd(FALSE);
    /* set systick reload */
    SYSTICK_Reload_Config(Reload);
    /*write ST_CV any valer,clear ST_CV*/
    SYSTICK_Counter_Updata();
    /*select SCLK as the clock source of the system tick timer */
    SYSTICK_Clock_Config(SYSTICK_SYS_CLOCK_DIV_1);
    /*enable system timer IRQ&interrupt flag*/
    SYSTICK_Systick_INT_Enable(TRUE);
    /*Configuration system timer Interrupt Priority*/
    INT_Interrupt_Priority_Config(INT_SysTick, 2, 0);
    /*enable systick timer*/
    SYSTICK_Cmd(TRUE);
    /*enable system timer Interrupt*/
    INT_Interrupt_Enable(INT_SysTick, TRUE);
    /*enable all interrupt*/
    INT_All_Enable(TRUE);
}
