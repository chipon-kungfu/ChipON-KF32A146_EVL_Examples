/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_SysTick.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides provides template for 
 *                                     User_SysTicks
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef USER_SYSTICK_H_
#define USER_SYSTICK_H_

#include "Board_GpioCfg.h"

/******************************************************************************
 *                      system delay time defineition   (MS)                   *
 ******************************************************************************/
#define SYSTEM_DELAY_1MS    (1UL)
#define SYSTEM_DELAY_2MS    (2UL)
#define SYSTEM_DELAY_5MS    (5UL)
#define SYSTEM_DELAY_10MS   (10UL)
#define SYSTEM_DELAY_20MS   (20UL)
#define SYSTEM_DELAY_50MS   (50UL)
#define SYSTEM_DELAY_100MS  (100UL)
#define SYSTEM_DELAY_200MS  (200UL)
#define SYSTEM_DELAY_500MS  (500UL)
#define SYSTEM_DELAY_1000MS (1000UL)

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/
/*LED1 Debug*/
#define LED1_Off() GPIO_Set_Output_Data_Bits(LED1_PORT, LED1_PIN, Bit_SET)
#define LED1_On()  GPIO_Set_Output_Data_Bits(LED1_PORT, LED1_PIN, Bit_RESET)

/**
 * @brief  Decrements the TimingDelay variable.
 *  @param[in]   None
 *  @param[out] None
 * @retval None
 */
extern void TimingDelay_Decrement(void);

/**
 *  @brief :Inserts a delay time.
 *  @param[in]  	delayTime: specifies the delay time length, in milliseconds.
 *  @param[out] None
 *  @retval :None
 */
extern void Systick_Delay(uint32_t delayTime);

/**
 *  @brief :initialize system tick timer,T=1/SCLK*Reload.
 *  @param[in]  	Reload : the count value of Systick timer enter IRQ
 *  @param[out] None
 *  @retval :None
 */
extern void SysTick_Configuration(uint32_t Reload);

#endif /* USER_SYSTICK_H_ */
