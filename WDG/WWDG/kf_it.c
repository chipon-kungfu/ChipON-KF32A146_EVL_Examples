/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by WWDG
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "User_SysTick.h"
#include "Board_GpioCfg.h"
#include "kflog.h"

/*******************************************************************************
**                   KF32A1x6 Processor Exceptions Handlers
*******************************************************************************/
/**
 * @brief:NMI Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _NMI_exception(void) {}

/**
 * @brief:HardFault Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _HardFault_exception(void) {}

/**
 * @brief:StackFault Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _StackFault_exception(void) {}

/**
 * @brief:SVC Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SVC_exception(void) {}

/**
 * @brief:SoftSV Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SoftSV_exception(void) {}

/**
 * @brief:WWDT Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _WWDT_exception(void)
{
    WWDT_Counter_Config(0);
    WWDT_Clear_INT_Flag();
    GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
    kf_printf("int feed the dog\r\n");
}

/**
 * @brief:SysTick Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SysTick_exception(void)
{
    /*write ST_CV any valer,clear ST_CV*/
    SYSTICK_Counter_Updata();
    /*system tick count-dowm counter*/
    TimingDelay_Decrement();
}
