/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by IWDG
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_iwdg.h"
#include "Board_GpioCfg.h"
#include "kflog.h"

/*******************************************************************************
**                          kfprint config
*******************************************************************************/
#define IWDG_LOG_OPT (KF_LOG_OPT_LINE)
KF_REG_LOG(iwdg, IWDG_LOG_OPT);

/*******************************************************************************
**                                 Macro Defines
*******************************************************************************/
#define LED1_Off() GPIO_Set_Output_Data_Bits(LED1_PORT, LED1_PIN, Bit_SET)
#define LED1_On()  GPIO_Set_Output_Data_Bits(LED1_PORT, LED1_PIN, Bit_RESET)

/*******************************************************************************
**                              Private Functions
*******************************************************************************/
/**
 * @brief  get user key state.
 *  @param[in]  None
 *  @param[out] FlagStatus
 * @retval None
 */
static FlagStatus User_Key_Down(void)
{
    if (GPIO_Read_Input_Data_Bit(KEY2_PORT, KEY2_PIN))
    {
        return SET;
    }
    else
    {
        return RESET;
    }
}

/*******************************************************************************
**                                 main Functions
*******************************************************************************/

int main()
{
    uint8_t press_status = 0;
    uint8_t nopress_num  = 0;

    /*initialize system clock ,defaut SCLK is  72MHz,select INTHF as Clock
     * source*/
    SystemInit(72U);
    systick_delay_init(72U);

    /* kfprint init */
    kfLog_Init();

    /* Configure LED1 output */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);
    /*user key input*/
    GPIOInit_Input_Config(KEY2_PORT, KEY2_PIN);

#if 0
    /* Use EXTLF as IWDG clock */
    /* Enale EXTLF */
    OSC_EXTLF_Software_Enable(ENABLE);
    OSC_EXTLF_Start_Delay_Config(EXT_START_DELAY_1024);
    while (OSC_Get_INT_Flag(OSC_INT_EXTLF) != SET)
        ;
    /* set IWDG */
    IWDG_Init(1000u, IWDT_SCK_EXTLF);
#else
    /* Use INTLF as IWDG clock */
    /* set IWDG */
    IWDG_Init(1000u, IWDT_SCK_INTLF);
#endif
    KFLOG_I(iwdg, "start i2c iwdg test\r\n");

    LED1_Off();
    systick_delay_ms(100u);
    LED1_On();

    while (1)
    {
        /*if user key dowm,feed IWDG,LED On every 100ms,if not,LED On every
         * 1s.*/
        if (User_Key_Down() == RESET)
        {
            if (press_status == 0u)
            {
                press_status = 1u;
                KFLOG_I(iwdg, "Press the button, feed the dog\r\n");
            }
            nopress_num = 0u;

            /**
             * Feed dog:
             * CAUTION: Set BKP to readable and writable before feeding dog and 
             *          to unreadable and non-writable after feeding dog is necessary.
             */
            BKP_Write_And_Read_Enable(TRUE);
            IWDT_Feed_The_Dog();
            BKP_Write_And_Read_Enable(FALSE);

            LED1_Off();
            systick_delay_ms(100u);
            LED1_On();
            systick_delay_ms(100u);

            /* Feed IWDG with enable/disable BKP */
            IWDG_FeedDog();
        }
        else
        {
            press_status = 0u;
            nopress_num++;

            KFLOG_I(iwdg, "no feeding the dog, times: %d\r\n", nopress_num);
            systick_delay_ms(100u);
        }
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
