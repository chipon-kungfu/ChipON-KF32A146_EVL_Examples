/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_iwdg.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides provides template for
 *                                     User_iwdg
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "system_init.h"
#include "User_iwdg.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                          Global Functions
*******************************************************************************/

/**
 * @brief  initialize IWDG.
 *  @param[in]  Overflow:IWDG interrupt overflow value
 *               Prescaler:Prescaler of IWDG clock source
 *  @param[out] None
 * @retval None
 */
void IWDG_Config(uint32_t Overflow, uint32_t Prescaler)
{
    /*enable BKP write andread*/
    BKP_Write_And_Read_Enable(TRUE);
    /*Exit reset state*/
    PM_Independent_Watchdog_Reset_Config(PERIPHERAL_OUTRST_STATUS);
    /*select INTLF as the IWDG clock source*/
    PM_Internal_Low_Frequency_Enable(TRUE);
    /*T =1/32K*Prescaler*Overflow=1s*/
    IWDT_Overflow_Config(Overflow);
    /*set Prescaler*/
    IWDT_Prescaler_Config(Prescaler);
    /*enable IWDG*/
    IWDT_Enable(TRUE);
    BKP_Write_And_Read_Enable(FALSE);
}

/* ================================ NEW API ================================ */

/**
 * @brief: Initialize IWDG.
 *
 *  @param[in]  period_ms: The period of IWDG, in ms.
 *              The MAX. period is 32768ms. If you need to set longger period:
 *              - modify parameter of $IWDT_Prescaler_Config$
 *              - modify parameter of $IWDT_Overflow_Config$
 *  @param[in]  clk_Source: The clock source of IWDG.
 *              IWDT_SCK_INTLF
 *              IWDT_SCK_EXTLF
 *  @param[out] None
 * @retval uint32_t
 * @retval: None
 */
uint32_t IWDG_Init(uint32_t period_ms, uint32_t clk_Source)
{
    uint32_t period_Factor = 0u;
    uint32_t period_Tmp    = 0u;

    if (period_ms > 32768u)
    {
        /* IWDG period is too large */
        return 1u;
    }
    else
    {}

    /* Enable BKP write and read */
    BKP_Write_And_Read_Enable(TRUE);
    /* Exit reset state */
    PM_Independent_Watchdog_Reset_Config(PERIPHERAL_OUTRST_STATUS);

    if (IWDT_SCK_INTLF == clk_Source)
    {
        /* Select INTLF as the IWDG clock source */
        PM_Internal_Low_Frequency_Enable(TRUE);
    }
    else
    {
        /* Select EXTLF as the IWDG clock source */
        PM_External_Low_Frequency_Enable(TRUE);
    }
    IWDT_SCK_Source_Select(clk_Source);

    period_Tmp    = (period_ms * 1024u) / 1000u;
    period_Factor = period_Tmp / 4096u;
    if (period_Factor < 1u)
    {
        /*set Prescaler*/
        IWDT_Prescaler_Config(IWDT_PRESCALER_32);
        IWDT_Overflow_Config(period_Tmp);
    }
    else if (period_Factor < 2u)
    {
        IWDT_Prescaler_Config(IWDT_PRESCALER_64);
        IWDT_Overflow_Config(period_Tmp / 2u);
    }
    else if (period_Factor < 4u)
    {
        IWDT_Prescaler_Config(IWDT_PRESCALER_128);
        IWDT_Overflow_Config(period_Tmp / 4u);
    }
    else if (period_Factor < 8u)
    {
        IWDT_Prescaler_Config(IWDT_PRESCALER_256);
        IWDT_Overflow_Config(period_Tmp / 8u);
    }
    else
    {}

    /* Enable IWDG */
    IWDT_Enable(TRUE);
    BKP_Write_And_Read_Enable(FALSE);

    return 0u;
}

/**
 * @brief: Feed IWDG.
 *
 * @param[in]  None
 * @param[out] None
 * @retval void
 */
void IWDG_FeedDog(void)
{
    BKP_Write_And_Read_Enable(TRUE);
    IWDT_Feed_The_Dog();
    BKP_Write_And_Read_Enable(FALSE);
}
