/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_iwdg.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides provides template for 
 *                                     User_iwdg
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#ifndef USER_IWDG_H_
#define USER_IWDG_H_

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/

/**
 * @brief  initialize IWDG.
 *  @param[in]   Overflow:IWDG interrupt overflow value
 *  			 Prescaler:Prescaler of IWDG clock source
 *  @param[out] None
 * @retval None
 */
extern void IWDG_Config(uint32_t Overflow, uint32_t Prescaler);

/* ================================ NEW API ================================ */
extern uint32_t IWDG_Init(uint32_t period_ms, uint32_t clk_Source);
extern void IWDG_FeedDog(void);

#endif /* USER_IWDG_H_ */
