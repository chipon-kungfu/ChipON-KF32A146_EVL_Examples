/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a reference for interrupt usart
 *                    asynchronous transceiver application routines
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
 *                                Include Files
 ******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "Usart.h"

/******************************************************************************
 *                           Static function declaration
 ******************************************************************************/
static void Led_Gpio_Init(void);
static void Led_Flip(void);

/******************************************************************************
 *                           Array Definition
 ******************************************************************************/
uint8_t Usart_Send_Sdu[] = {"ChipON\r\n"};
/* Usart send data array and receive data array */
uint8_t UsartTxBuffer[100];
uint8_t UsartRxBuffer[100];

extern volatile uint32_t TransmitCount;

/******************************************************************************
 *                                Public function
 ******************************************************************************/
/**
 *  @brief :Initializes the GPIO of the LED
 *  @param[in]  None
 *  @param[out] None
 *  @retval
 */
void Led_Gpio_Init(void)
{
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);
    GPIOInit_Output_Config(LED2_PORT, LED2_PIN);
}

/**
 *  @brief :Led flip
 *  @param[in]  None
 *  @param[out] None
 *  @retval :
 */
static void Led_Flip(void)
{
    GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
}

/*****************************************************************************
 *                                  main function
 *****************************************************************************/
int main()
{
    /* Initialize the system clock */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function */
    systick_delay_init(72U);

    /*Initialize led IOs*/
    Led_Gpio_Init();

    USART_TxGpio_Config(PC12_USART2_TX_AF);
    USART_RxGpio_Config(PC13_USART2_RX_AF);
    USART_Mode_Config(USART2_SFR, 115200U);
    systick_delay_ms(10);

    /* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
    /* Configure usart interrupt */
    USART_Int_Config(USART2_SFR, INT_USART2);
    /* Global shielded interrupt enable bit */
    INT_All_Enable(TRUE);

    /* Sends a string of characters and light up the led */
    USART_Send(USART2_SFR, Usart_Send_Sdu, sizeof(Usart_Send_Sdu));
    Led_Flip();

    while (1)
    {
        /* If data is received */
        if (TransmitCount)
        {
            USART_Send(USART2_SFR, Usart_Send_Sdu, sizeof(Usart_Send_Sdu));
            USART_Send(USART2_SFR, UsartRxBuffer, TransmitCount);

            /* Clear TransmitCount */
            TransmitCount = 0;
            Led_Flip();
        }
        else
        {
            systick_delay_ms(250U);
            GPIO_Toggle_Output_Data_Config(LED2_PORT, LED2_PIN);
        }
    }
}
