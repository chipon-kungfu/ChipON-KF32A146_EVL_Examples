/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
#include "Usart.h"

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception(void) {}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************

void __attribute__((interrupt)) _HardFault_exception(void) {}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception(void) {}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception(void) {}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception(void) {}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception(void) {}

//*****************************************************************************************
//                               USART2 Interrupt Course
//*****************************************************************************************
/* USART2 receive data count */
volatile uint32_t TransmitCount = 0;
extern uint8_t    UsartTxBuffer[];
extern uint8_t    UsartRxBuffer[];
void __attribute__((interrupt)) _USART2_exception(void)
{
    static uint32_t ReceiveCount = 0;
    /*    If gets the status of the USART data ready interrupt flag */
    if (USART_Get_Receive_BUFR_Ready_Flag(USART2_SFR))
    {
        UsartRxBuffer[ReceiveCount++] = USART_ReceiveData(USART2_SFR);
    }
    /* If gets the USART interrupt flag for receiving idle frames */
    if (USART_Get_Receive_Frame_Idel_Flag(USART2_SFR))
    {
        /* Clear idle interrupt flag */
        USART_Clear_Idle_INT_Flag(USART2_SFR);
        /* Gets the length of the data received */
        TransmitCount = ReceiveCount;
        ReceiveCount  = 0;
    }
}
