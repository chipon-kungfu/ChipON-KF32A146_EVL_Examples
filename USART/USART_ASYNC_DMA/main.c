/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the relevant configuration
 *                    functions for DMA usart routines
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
 *                                Include Files
 ******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "Usart.h"
#include "main.h"

/******************************************************************************
 *                         Static function declaration
 ******************************************************************************/
static void Led_Gpio_Init(void);

volatile uint8_t USART_Rx_Flag = 0;

extern uint8_t  pre_length;
extern uint8_t *pre_address;
/******************************************************************************
 *                            Structure Definition
 ******************************************************************************/
Usart_Sdu_Type Usart_Receive_Sdu = {{0}, 0};
Usart_Sdu_Type Usart_Send_Sdu    = {
  {'C', 'H', 'I', 'P', ' ', 'O', 'N', '\r', '\n'},
  10
};

/******************************************************************************
 *                            Initialization function
 ******************************************************************************/
/**
 *  @brief :Initializes the GPIO of the LED
 *  @param[in]  None
 *  @param[out] None
 *  @retval
 */
static void Led_Gpio_Init(void)
{
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);
    GPIOInit_Output_Config(LED2_PORT, LED2_PIN);
}

/**
 *  @brief :Led flip
 *  @param[in]  None
 *  @param[out] None
 *  @retval :
 */
void Led_Flip(void)
{
    GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
}

/**
 *  @brief :DMA initialization of Usart
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void Usart_Dma_Init(void)
{
    /* Reset the DMA0 peripheral to enable the peripheral clock */
    DMA_Reset(DMA0_SFR);

    USART_TxDma_Config(USART2_SFR, DMA0_SFR);
    USART_RxDma_Config(USART2_SFR, DMA0_SFR);
    USART_RxDma_Buffer_Regiter(USART2_SFR, DMA0_SFR, &(Usart_Receive_Sdu.Data[0]), 100);

    /**
     * @brief How to use DMA1 ???
     * 
     * You need to set REMAP register.
     * 
     * DMA1_REMAP |= 0x1U;
     * Usart0 set REMAP register bit 0. 
     * 
     * DMA1_REMAP |= 0x2U;
     * Usart1 set REMAP register bit 1
     * 
     * DMA1_REMAP |= 0x4U;
     * Usart2 set REMAP register bit 2
     * 
     * DMA1_REMAP |= 0x20U;
     * Usart5 set REMAP register bit 5
     */

}

/**
 *  @brief :USART receives data processing functions
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void Receiving_data_processing(void)
{
    if((pre_length + Usart_Receive_Sdu.Length) > 100U)
    {
        /* If spanned, segmented output data */
        USART_TxDma_Transmit(USART2_SFR, DMA0_SFR, pre_address, 100U - pre_length);
        systick_delay_ms(5);
        USART_TxDma_Transmit(USART2_SFR, DMA0_SFR, Usart_Receive_Sdu.Data, (pre_length + Usart_Receive_Sdu.Length) - 100U);
    }
    else
    {
        /* Direct output */
        USART_TxDma_Transmit(USART2_SFR, DMA0_SFR, pre_address, Usart_Receive_Sdu.Length);
    }

    systick_delay_ms(5);
    pre_length  += Usart_Receive_Sdu.Length;
    pre_length %= 100U;
    pre_address = Usart_Receive_Sdu.Data + pre_length;
}

/**
 * @brief:
 *  @param[in]  None
 *  @param[out] None
 * @retval: int
 */
int main(void)
{
    /* Initialize the system clock is 120M*/
    SystemInit(72);
    /* Setup SysTick Timer as delay function */
    systick_delay_init(72);
    /* Initialize led IOs*/
    Led_Gpio_Init();

    USART_TxGpio_Config(PC12_USART2_TX_AF);
    USART_RxGpio_Config(PC13_USART2_RX_AF);
    USART_Mode_Config(USART2_SFR, 115200U);
    Usart_Dma_Init();

    /* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
    /* Idle interrupt enable */
    USART_Int_Config(USART2_SFR, INT_USART2);

    /* Tag address initialization */
    pre_address = Usart_Receive_Sdu.Data;
    /* Tag length initialization */
    pre_length = 0;

    INT_All_Enable(TRUE);

    systick_delay_ms(20);

    /*The first frame of data is sent using non-dma */
    USART_Send(USART2_SFR, Usart_Send_Sdu.Data, Usart_Send_Sdu.Length);

    while (1)
    {
        /* If Rx_Flag is 1*/
        if (USART_Rx_Flag == 1)
        {
            /* Set Rx_Flag to 0 */
            USART_Rx_Flag = 0;
            /* Led2 flip */
            GPIO_Toggle_Output_Data_Config(LED2_PORT, LED2_PIN);
            Receiving_data_processing();
        }
        //        USART_TxDma_Transmit(USART2_SFR, DMA0_SFR, Usart_Send_Sdu.Data, Usart_Send_Sdu.Length);
        systick_delay_ms(250);
        systick_delay_ms(250);
        Led_Flip();
    }
}
