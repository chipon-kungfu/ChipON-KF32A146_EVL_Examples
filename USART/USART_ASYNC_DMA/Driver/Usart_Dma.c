/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Usart_Dma.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the relevant configuration
 *                    functions for DMA usart routines
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "system_init.h"
#include "Usart.h"

#define USART0_TX_DMA_CHANNAL DMA_CHANNEL_1
#define USART0_RX_DMA_CHANNAL DMA_CHANNEL_2
#define USART1_TX_DMA_CHANNAL DMA_CHANNEL_3
#define USART1_RX_DMA_CHANNAL DMA_CHANNEL_4
#define USART2_TX_DMA_CHANNAL DMA_CHANNEL_5
#define USART2_RX_DMA_CHANNAL DMA_CHANNEL_6
#define USART5_TX_DMA_CHANNAL DMA_CHANNEL_2
#define USART5_RX_DMA_CHANNAL DMA_CHANNEL_3

#define TX_DMA_CHANNAL(USARTx) USARTx##_TX_DMA_CHANNAL
#define RX_DMA_CHANNAL(USARTx) USARTx##_RX_DMA_CHANNAL

/**
 * @brief: usart channel get
 *  @param[in]  *USARTx
 *  @param[out] None
 * @retval: uint32_t
 */
static uint32_t Get_Usart_TxDmaChannel(USART_SFRmap *USARTx)
{
    if (USART0_SFR == USARTx)
    {
        return TX_DMA_CHANNAL(USART0);
    }
    else if (USART1_SFR == USARTx)
    {
        return TX_DMA_CHANNAL(USART1);
    }
    else if (USART2_SFR == USARTx)
    {
        return TX_DMA_CHANNAL(USART2);
    }
    else if (USART5_SFR == USARTx)
    {
        return TX_DMA_CHANNAL(USART5);
    }
}

/**
 * @brief: usart channel get
 *  @param[in]  *USARTx
 *  @param[out] None
 * @retval: uint32_t
 */
static uint32_t Get_Usart_RxDmaChannel(USART_SFRmap *USARTx)
{
    if (USART0_SFR == USARTx)
    {
        return RX_DMA_CHANNAL(USART0);
    }
    else if (USART1_SFR == USARTx)
    {
        return RX_DMA_CHANNAL(USART1);
    }
    else if (USART2_SFR == USARTx)
    {
        return RX_DMA_CHANNAL(USART2);
    }
    else if (USART5_SFR == USARTx)
    {
        return RX_DMA_CHANNAL(USART5);
    }
}

/**
 * @brief: tx dma config
 *  @param[in]  *USARTx
 *  @param[in]  *DMAx
 *  @param[out] None
 * @retval: None
 */
void USART_TxDma_Config(USART_SFRmap *USARTx, DMA_SFRmap *DMAx)
{
    /*
     * DMA0_TX configured as follow:
     *   - DMA channel selection channel 1
     *   - Bit width of memory = 8
     *   - Cyclic mode disable
     */
    uint32_t        channal = Get_Usart_TxDmaChannel(USARTx);
    DMA_InitTypeDef DMA_TX_INIT;
    DMA_TX_INIT.m_Channel            = channal;
    DMA_TX_INIT.m_Direction          = DMA_MEMORY_TO_PERIPHERAL;
    DMA_TX_INIT.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;
    DMA_TX_INIT.m_MemoryDataSize     = DMA_DATA_WIDTH_8_BITS;
    DMA_TX_INIT.m_Priority           = DMA_CHANNEL_LOWER;
    DMA_TX_INIT.m_Number             = 0;
    DMA_TX_INIT.m_PeripheralInc      = FALSE;
    DMA_TX_INIT.m_MemoryInc          = TRUE;
    DMA_TX_INIT.m_LoopMode           = FALSE;
    DMA_TX_INIT.m_BlockMode          = DMA_TRANSFER_BYTE;
    DMA_TX_INIT.m_MemoryAddr         = 0;
    DMA_TX_INIT.m_PeriphAddr         = (uint32_t) & (USARTx->TBUFR);
    DMA_Configuration(DMAx, &DMA_TX_INIT);

    // DMA_Channel_Enable(DMAx, channal, TRUE);
    USART_DMA_Write_Transmit_Enable(USARTx, TRUE);
}

/**
 * @brief: rx dma config
 *  @param[in]  *USARTx
 *  @param[in]  *DMAx
 *  @param[out] None
 * @retval: None
 */
void USART_RxDma_Config(USART_SFRmap *USARTx, DMA_SFRmap *DMAx)
{
    /*
     * DMA0_RX configured as follow:
     *   - DMA channel selection channel 2
     *   - Bit width of memory = 8
     *   - Number of data transmitted = 100
     *   - Cyclic mode disable
     */
    uint32_t        channal = Get_Usart_RxDmaChannel(USARTx);
    DMA_InitTypeDef DMA_RX_INIT;
    DMA_RX_INIT.m_Channel            = channal;
    DMA_RX_INIT.m_Direction          = DMA_PERIPHERAL_TO_MEMORY;
    DMA_RX_INIT.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;
    DMA_RX_INIT.m_MemoryDataSize     = DMA_DATA_WIDTH_8_BITS;
    DMA_RX_INIT.m_Priority           = DMA_CHANNEL_LOWER;
    DMA_RX_INIT.m_Number             = 0;
    DMA_RX_INIT.m_PeripheralInc      = FALSE;
    DMA_RX_INIT.m_MemoryInc          = TRUE;
    DMA_RX_INIT.m_LoopMode           = TRUE;
    DMA_RX_INIT.m_BlockMode          = DMA_TRANSFER_BYTE;
    DMA_RX_INIT.m_PeriphAddr         = (uint32_t) & (USARTx->RBUFR);
    DMA_RX_INIT.m_MemoryAddr         = 0;
    DMA_Configuration(DMAx, &DMA_RX_INIT);

    USART_DMA_Read_Receive_Enable(USARTx, TRUE);
}

/**
 * @brief: Recv Dma_Buffer Regiter
 *  @param[in]  *USARTx
 *  @param[in]  *DMAx
 *  @param[in]  *buffer
 *  @param[in]  buf_size
 *  @param[out] None
 * @retval: None
 */
void USART_RxDma_Buffer_Regiter(USART_SFRmap *USARTx, DMA_SFRmap *DMAx, uint8_t *buffer, uint16_t buf_size)
{
    uint32_t channal = Get_Usart_RxDmaChannel(USARTx);
    DMA_Channel_Enable(DMAx, channal, DISABLE);

    DMA_Memory_Start_Address_Config(DMAx, channal, (uint32_t)buffer);
    DMA_Transfer_Number_Config(DMAx, channal, buf_size);

    DMA_Channel_Enable(DMAx, channal, ENABLE);
}
/**
 * @brief: USART transfers data using DMA
 *  @param[in]  *USARTx
 *  @param[in]  *pData
 *  @param[in]  nSize
 *  @param[out] None
 * @retval: None
 */
void USART_TxDma_Transmit(USART_SFRmap *USARTx, DMA_SFRmap *DMAx, uint8_t *pData, uint16_t nSize)
{
    uint32_t channal = Get_Usart_TxDmaChannel(USARTx);

    DMA_Channel_Enable(DMAx, channal, DISABLE);
    DMA_Memory_Start_Address_Config(DMAx, channal, (uint32_t)pData);
    DMA_Transfer_Number_Config(DMAx, channal, nSize);
    USART_Transmit_Data_Enable(USARTx, ENABLE);
    DMA_Channel_Enable(DMAx, channal, ENABLE);
}
