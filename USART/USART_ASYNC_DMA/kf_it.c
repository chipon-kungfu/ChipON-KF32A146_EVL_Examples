/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                     This file provides template for all exceptions
 *                     handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "system_init.h"
#include "main.h"

extern Usart_Sdu_Type   Usart_Receive_Sdu;
extern volatile uint8_t USART_Rx_Flag;

uint8_t  pre_length  = 0;
uint8_t *pre_address = (void *)(Usart_Receive_Sdu.Data);

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception(void) {}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************

void __attribute__((interrupt)) _HardFault_exception(void) {}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception(void) {}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception(void) {}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception(void) {}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception(void) {}

//*****************************************************************************************
//                               USART2 Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _USART2_exception(void)
{
    uint8_t i = 0U;

    if (USART_Get_Receive_Frame_Idel_Flag(USART2_SFR))
    {
        USART_Rx_Flag            = 1U;
        Usart_Receive_Sdu.Length = 0U;
        USART_Clear_Idle_INT_Flag(USART2_SFR);

        for(i = 0U; i < 100U; i++)
        {
            if(Usart_Receive_Sdu.Data[(i + pre_length) % 100U] == '\n')
            {
                break;
            }
            else
            {
                Usart_Receive_Sdu.Length++;
            }
        }
        Usart_Receive_Sdu.Length++;
    }
}
