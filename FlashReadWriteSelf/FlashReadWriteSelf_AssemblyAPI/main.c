/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Provide the operation interface of data area flash,
 *                         including reading, erasing, page writing, byte
 *                         writing, comparison and other functions
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "stdio.h"
#include "kflog.h"
#include "flash.h"
#include "string.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/
#define FlashDataSize            2048
#define FlashInformationDataSize 257
/******************************************************************************
**                       Private variables  Definitions
******************************************************************************/
/* Flash Data buffer */
uint8_t FlashDataBuffer[FlashDataSize];
uint8_t  FlashDataReadBuffer[FlashDataSize] = {0};
uint32_t FlashInformationDataBuffer[FlashInformationDataSize];
uint8_t  FlashEEDataBuffer[] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0};
/*******************************************************************************
 **                                Global Functions
 *******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main(void)
{
    uint32_t i, FlashRetVal;
    /* Initialize the system clock is 120M */
    SystemInit(72);
    /*Initialize the print function*/
    kfLog_Init();
    /* Setup SysTick Timer as delay function, and input frequency is 120M */
    systick_delay_init(72);

    /* First erase and then write. The interrupt is closed inside the function, and then recover */
    /* Write 'FlashDataBuffer' to address 0x10000 about 2K data */

    for (uint32_t i = 0U; i < FlashDataSize; i++)
    {
        FlashDataBuffer[i] = i;
    }

    memset(FlashDataBuffer, 0, 1024);

    FlashRetVal = FlashWriteNKBytes(0x10000, FlashDataSize, (uint8_t *)FlashDataBuffer);
    if (FlashRetVal == CMD_SUCCESS)
    {
        kf_printf("Write 2 Kbytes success\r\n\r\n");
    }
    else
    {
        kf_printf("Write 2 Kbytes err [%d]\r\n\r\n", FlashRetVal);
    }

    memset(FlashDataReadBuffer, 0x00, FlashDataSize);
    FlashReadNByte(0x10000, FlashDataSize, (uint8_t *)FlashDataReadBuffer);

    kf_printf("Read address 0x10000\r\n");
    for (uint32_t i = 0; i < FlashDataSize; i++)
    {
        kf_printf("%d 0x%x\t", i, FlashDataReadBuffer[i]);
    }
    kf_printf("\r\n\r\n");

    FlashEraseNPage(0x10000, FlashDataSize);

    FlashReadNByte(0x10000, FlashDataSize, (uint8_t *)FlashDataReadBuffer);

    kf_printf("Read Earse address 0x10000\r\n");
    for (uint32_t i = 0; i < FlashDataSize; i++)
    {
        kf_printf("%d 0x%x\t", i, FlashDataReadBuffer[i]);
    }
    kf_printf("\r\n\r\n");

    {
        kf_printf("8 byte FlashReadNByte \r\n");
        FlashReadNByte(0x10000, 16, (uint8_t *)FlashDataReadBuffer);
        for (uint32_t i = 0; i < 16; i++)
        {
            kf_printf(" %d 0x%x\t", i, FlashDataReadBuffer[i]);
        }
        kf_printf("\r\n");
        for (uint32_t i = 0; i < 16; i++)
        {
            FlashDataBuffer[i] = i;
        }
        for (uint32_t i = 0; i < 16; i++)
        {
            kf_printf(" %d 0x%x\t", i, FlashDataBuffer[i]);
        }
        kf_printf("\r\n");

        FlashRetVal = FlashWrite8NBytesNoErase(0x10000, 16, FlashDataBuffer);
        if (FlashRetVal == CMD_SUCCESS)
        {
            kf_printf("16 byte   Write Byte success\r\n\r\n");
        }
        else
        {
            kf_printf("Write 16 byte err [%d]\r\n\r\n", FlashRetVal);
        }
        kf_printf("8 byte read end \r\n");
        FlashReadNByte(0x10000, 16, FlashDataReadBuffer);
        for (uint32_t i = 0; i < 16; i++)
        {
            kf_printf(" %d 0x%x\t", i, FlashDataReadBuffer[i]);
        }
        kf_printf("\r\n");
    }

    while (1)
    {
        ;
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
