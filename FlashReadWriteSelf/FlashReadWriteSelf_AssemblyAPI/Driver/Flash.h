/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Flash.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provide flash information area and user
 *                         area operation function
 *                      + Read any number of bytes
 *                      + Erasure and writing of pages
 *                      + Data modification of any address
 *                      + Reading and writing of information area
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef _FLASH_H_
#define _FLASH_H_

#include "__driver_Flash_API.h"

#define BUFFERS_AUTO_ALIGN_ENABLE (1u)

/******************************************************************************
*                      Functional defineition                                 *
******************************************************************************/
void FlashReadNByte(unsigned int Address, unsigned int Length, unsigned char *Buffers);
uint32_t __FlashEraseNPage(volatile unsigned key,unsigned int Address, unsigned int Length);
uint32_t __FlashWriteNKBytes(volatile unsigned key,unsigned int Address, unsigned int Length, unsigned char *Buffers);
uint32_t __FlashWrite8NBytesNoErase(volatile unsigned key,unsigned int Address, unsigned int Length, unsigned char *Buffers);

#define FlashEraseNPage(Address,Length)             __FlashEraseNPage(Function_Parameter_Validate, Address, Length)
#define FlashWriteNKBytes(Address,Length,Buffers)           __FlashWriteNKBytes(Function_Parameter_Validate, (Address), (Length), (Buffers))
#define FlashWrite8NBytesNoErase(Address,Length,Buffers)   __FlashWrite8NBytesNoErase(Function_Parameter_Validate, (Address), (Length), (Buffers))


#endif /* FLASH_H_ */
