/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Flash.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provide flash information area and user
 *                         area operation function
 *                      + Read any number of bytes
 *                      + Erasure and writing of pages
 *                      + Data modification of any address
 *                      + Reading and writing of information area
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "flash.h"

static unsigned int Flash_Address = 0x10000;
static unsigned int Flash_Length = 0;

#if (BUFFERS_AUTO_ALIGN_ENABLE == 1u)
static uint32_t TempData[(1024 / 4)] = {0};
#endif

/*******************************************************************************
**                                Global Functions
*******************************************************************************/
/**
 *  @brief :Flash continue reads n bytes
 *  @param[in]
 *              Address :Starting  address
 *                 Length :Read length
 *  @param[out] Buffers :Read data to Buffers
 *  @retval :None
 */
__attribute__((noinline, optimize("-O0"))) void FlashReadNByte(unsigned int Address, unsigned int Length, unsigned char *Buffers)
{
    for (int i = 0; i < Length; i++)
    {
        Buffers[i] = *(unsigned char *)Address++;
    }
}

/**
 *  @brief :Erase N pages. The minimum erasure unit is 1K.
 *             During this period, the whole play will be close !!!
 *             Recovery interrupt at the end !!!
 *  @param[in]
 *              Address :address aligned by 1K
 *                 Length :length like 1024 2048 3072 4096...
 *  @param[out] None
 *  @retval :    CMD_SUCCESS                                    0x00
 *                BUSY                                          0x0B
 *                SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION       0x09
 *                INVALID_SECTOR                                0x07
 */
__attribute__((noinline, optimize("-O0"))) uint32_t __FlashEraseNPage(volatile unsigned key,unsigned int Address, unsigned int Length)
{
   uint32_t FlashRetVal, IntState;

   if (((Length & 0x3FF) > 0) || (0 == Length))
   {
	   return PARAM_ERROR;
   }

   IntState = (INT_CTL0 & INT_CTL0_AIE);
   SFR_CLR_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
   for (int i = 0; i < Length; i += 0x400)
   {
	   FlashRetVal = __FLASH_Erase__(key, (Address+i), 0x400);
	   if (FlashRetVal != CMD_SUCCESS)
	   {
		   break;
	   }
   }
   if (IntState != 0)
       SFR_SET_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
   return FlashRetVal;
}

/**
 *  @brief :Write N Kbytes.
 *             Erase first and write then.
 *             During this period, the whole play will be close !!!
 *             Recovery interrupt at the end !!!
 *  @param[in]
 *              Address :address aligned by 1K
 *                 Length :length like 1024 2048 3072 4096...
 *                 Buffers:input data
 *  @param[out] None
 *  @retval :    CMD_SUCCESS                                 0x00
 *               SRC_ADDR_ERROR                              0x02
 *               DST_ADDR_ERROR                              0x03
 *               SRC_ADDR_NOT_MAPPED                         0x04
 *               DST_ADDR_NOT_MAPPED                         0x05
 *               COUNT_ERROR(not bytes 1K 2K 3K 4K)          0x06
 *               INVALID_SECTOR                              0x07
 *               SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION     0x09
 *               BUSY                                        0x0B
 */
__attribute__((noinline, optimize("-O0"))) uint32_t __FlashWriteNKBytes(volatile unsigned key,unsigned int Address, unsigned int Length, unsigned char *Buffers)
{
    uint32_t i, FlashRetVal, IntState;

    uint32_t kvBufferAlign = TRUE;

    if (((Length & 0x3FF) > 0) || (0 == Length))
    {
 	   return PARAM_ERROR;
    }

#if (BUFFERS_AUTO_ALIGN_ENABLE == 1u)
    if (((uint32_t)Buffers & 0x3U) > 0U)
    {
        // return false;
        kvBufferAlign = FALSE;
    }
    else
    {
        /* Do Nothing */
        kvBufferAlign = TRUE;
    }
#endif

    IntState = (INT_CTL0 & INT_CTL0_AIE);
    SFR_CLR_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);

    if (kvBufferAlign == TRUE)
    {
        for (int i = 0; i < Length; i += 0x400)
        {
            FlashRetVal = __FLASH_Erase__(key, (Address + i), 0x400);
            if (FlashRetVal == CMD_SUCCESS)
            {
                FlashRetVal = __FLASH_Program__(key, (Address + i), 0x400, (unsigned int *)(&Buffers[i]));
            }
            if (FlashRetVal != CMD_SUCCESS)
            {
                break;
            }
        }
    }
    else
    {
#if (BUFFERS_AUTO_ALIGN_ENABLE == 1u)
        for (int i = 0; i < Length; i += 0x400)
        {
            memcpy(&TempData[0], &Buffers[i], 0x400);
            FlashRetVal = __FLASH_Erase__(key, (Address + i), 0x400);
            if (FlashRetVal == CMD_SUCCESS)
            {
                FlashRetVal = __FLASH_Program__(key, (Address + i), 0x400, (unsigned int *)(&TempData[0]));
            }
            if (FlashRetVal != CMD_SUCCESS)
            {
                break;
            }
        }
#endif
    }

    if (IntState != 0)
        SFR_SET_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
    return FlashRetVal;
}

/**
 *  @brief :Write 8*N bytes.
 *             No Erase .
 *             During this period, the whole play will be close !!!
 *             Recovery interrupt at the end !!!
 *  @param[in]
 *              Address :
 *                 Length :must align by 8,like 8 16 24 32 ... 8*N, or aligned by 16*N
 *                 Buffers:input data
 *  @retval :    CMD_SUCCESS                                     0x00
 */
__attribute__((optimize("-O0"))) uint32_t __FlashWrite8NBytesNoErase(volatile unsigned key,unsigned int Address, unsigned int Length, unsigned char *Buffers)
{
    uint32_t FlashRetVal, IntState;

    uint32_t kvBufferAlign = TRUE;
    if (((Length & 0x7) > 0)  || (0 == Length))
    {
 	   return PARAM_ERROR;
    }

#if (BUFFERS_AUTO_ALIGN_ENABLE == 1u)
    if (((uint32_t)Buffers & 0x3U) > 0U)
    {
        // return false;
        kvBufferAlign = FALSE;
    }
    else
    {
        /* Do Nothing */
        kvBufferAlign = TRUE;
    }
#endif

    IntState = (INT_CTL0 & INT_CTL0_AIE);
    SFR_CLR_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
    if (kvBufferAlign == TRUE)
    {
        for (int i = 0; i < Length; i += 0x8)
        {
            FlashRetVal = __FLASH_Program_NBytes__(key, (Address + i), 0x8, (unsigned int *)(&Buffers[i]));
            if (FlashRetVal != CMD_SUCCESS)
            {
                break;
            }
        }
    }
    else
    {
#if (BUFFERS_AUTO_ALIGN_ENABLE == 1u)
        for (int i = 0; i < Length; i += 0x8)
        {
            memcpy(&TempData[0], &Buffers[i], 0x8);
            FlashRetVal = __FLASH_Program_NBytes__(key, (Address + i), 0x8, (unsigned int *)(&TempData[0]));
            if (FlashRetVal != CMD_SUCCESS)
            {
                break;
            }
        }
#endif
    }

    if (IntState != 0)
        SFR_SET_BIT_ASM(INT_CTL0, INT_CTL0_AIE_POS);
    return FlashRetVal;
}
