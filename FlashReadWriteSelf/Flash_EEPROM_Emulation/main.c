/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Provide the Flash Memory Emulation eeprom Application Routines
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "kflog.h"
#include "fee.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/

/******************************************************************************
**                       Private variables  Definitions
******************************************************************************/

/*******************************************************************************
 **                                Global Functions
 *******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/

/**
 * @brief Prints specified length data arrays.
 *
 * This function prints two data arrays in hexadecimal format, with each data item represented as `0x` followed by 8 hexadecimal digits.
 * If either data array is NULL, it is not printed.
 *
 * @param Data1 Pointer to the first data array.
 * @param Data2 Pointer to the second data array.
 * @param Length The length of the data arrays to be printed.
 */
static void FEE_PrintfData(uint32_t *Data1, uint32_t *Data2, uint32_t Length)
{
    uint32_t loop;

    // Print Data1 array if it is not NULL
    if (Data1 != NULL)
    {
        kf_printf("data 1:\r\n");
        for (loop = 0U; loop < Length; loop++)
        {
            kf_printf("0x%08x ", Data1[loop]);
        }
        kf_printf("\r\n");
    }

    // Print Data2 array if it is not NULL
    if (Data2 != NULL)
    {
        kf_printf("data 2:\r\n");
        for (loop = 0U; loop < Length; loop++)
        {
            kf_printf("0x%08x ", Data2[loop]);
        }
        kf_printf("\r\n");
    }
}

/**
 * @brief This function demonstrates the usage of the Flash-based EEPROM emulation functionality (Fee).
 * It begins by checking the status of the Fee; if the status is incorrect, the function immediately returns.
 * Subsequently, the function writes a set of data into the Fee, reads back the same data, and verifies whether the read data matches the written data.
 * Lastly, the function outputs relevant information based on the final status of the Fee and the outcome of the verification.
 */
static void FEE_example(void)
{
    // Initialize write and read buffers
    uint32_t write_buffer[126] = {0}; // Buffer to hold data to be written into the simulated EEPROM
    uint32_t read_buffer[126]  = {0}; // Buffer to receive data read from the simulated EEPROM

    // Initialize the state of the Fee as uninitialized
    FEE_StateEnumType FeeState = FEE_UNINIT;

    // Output the start of the function execution
    kf_printf("\r\n%s %s", __FUNCTION__, "begin >>>\r\n");

    // Retrieve the current state of the Fee
    FeeState = FEE_GetStatus();

    // If the state is not idle, output an error message and terminate the function
    if (FeeState != FEE_IDLE)
    {
        kf_printf("FeeState State Err [%d][%s:%d]\r\n", FeeState, __func__, __LINE__);
        return;
    }

    // Populate the write buffer with incrementing values as test data
    for (uint8_t i = 0; i < 126; i++)
    {
        write_buffer[i] = i;
    }
    
    // Write the buffer contents into the Fee
    FEE_Write(0, 126 * 4, write_buffer); // Assuming FEE_Write accepts starting address, byte count, and data pointer as parameters

    // Clear the read buffer in preparation for receiving read data
    memset(&read_buffer, 0, 126 * 4);

    // Read data from the Fee into the read buffer
    FEE_Read(0, 126 * 4, (uint32_t *)&read_buffer); // Assuming FEE_Read accepts starting address, byte count, and data pointer as parameters

    // Verify that the read data matches the written data exactly
    for (int i = 0; i < 126; i++)
    {
        if (read_buffer[i] != write_buffer[i])
        {
            // If a mismatch is found, output detailed data differences and an error message, then terminate the function
            FEE_PrintfData(read_buffer, write_buffer, 126);
            kf_printf("Fee Write or Read Err [%d][%s:%d]\r\n", i, __func__, __LINE__);
            return;
        }
    }

    // Retrieve and check the current state of the Fee once more
    FeeState = FEE_GetStatus();

    // If the state is not idle, output an error message and terminate the function
    if (FeeState != FEE_IDLE)
    {
        kf_printf("FeeState State Err [%d][%s:%d]\r\n", FeeState, __func__, __LINE__);
        return;
    }

    // Output the successful completion of the function
    kf_printf("\r\n%s %s", __FUNCTION__, "Pass >>>\r\n");
}

int main(void)
{
    /* Initialize the system clock is 72M */
    SystemInit(72);
    /*Initialize the print function*/
    kfLog_Init();
    /* Setup SysTick Timer as delay function, and input frequency is 72M */
    systick_delay_init(72);
    FEE_Init();
    while (1)
    {
        FEE_example();
        systick_delay_ms(1000);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
