/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : CMP.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides CMP Hw operation function
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#ifndef _CMP_H_
#define _CMP_H_
/******************************************************************************
**                             Include Files                                **
******************************************************************************/
#include "system_init.h"

/*****************************************************************************
**                         Private Macro Definitions                        **
*****************************************************************************/

/*****************************************************************************
**                         Private Variables Definitions                    **
*****************************************************************************/

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/
void CMP0_HaLLModeInit(void);
void CMP_Interrupt_Init(void);
void delay_ms(volatile uint32_t nms);
void CMP0_IO_Init(void);
void CMP_Calibration(void);
#endif
