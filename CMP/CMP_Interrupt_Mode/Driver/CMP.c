/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : CMP.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides CMP Hw operation function
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
*******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "CMP.h"
/******************************************************************************
**                          Private variables
*******************************************************************************/

/******************************************************************************
**                            Global Functions
*******************************************************************************/

/**
 * @brief:Init CMP0
 *
 */
void CMP0_HaLLModeInit(void)
{
    CMP_InitTypeDef Cmp_ConfigPtr;

    /* CMP reset */
    CMP_Reset();

    /* Cmp positive terminal output selection bit */
    Cmp_ConfigPtr.m_PositiveInput = CMP0_PositiveINPUT_PIN_PA9;
    /* Cmp negative terminal output selection bit */
    Cmp_ConfigPtr.m_NegativeInput = CMP0_NegativeINPUT_PIN_PB3;
    /* Enable comparator falling edge trigger interrupt */
    Cmp_ConfigPtr.m_FallTriggerEnable = TRUE;
    /* Enable comparator rising edge to trigger interrupt */
    Cmp_ConfigPtr.m_RiseTriggerEnable = TRUE;
    /* Cmp filter filter clock source */
    Cmp_ConfigPtr.m_Clock = CMPx_CLKSOURCE_HFCLK;
    /* Cmp filter filtering time division frequency */
    Cmp_ConfigPtr.m_FrequencyDivision = 0x00;
    /* Sampling number selection */
    Cmp_ConfigPtr.m_SampleNumber = CMPx_FLT_SampleNumber_7;
    /* Filter enable bit */
    Cmp_ConfigPtr.m_FilterEnable = TRUE;
    /* Cmp range control enable bit */
    Cmp_ConfigPtr.m_ScopecontrolEnable = TRUE;
    /* Output polarity selection */
    Cmp_ConfigPtr.m_OutputPolarity = CMPx_OUTPUT_Normal;
    /* Cmp module enable */
    Cmp_ConfigPtr.m_CmpEnable = TRUE;

    /* Configure CMP0 */
    CMP_Configuration(CMP0_ADDR, &Cmp_ConfigPtr);

    /* Enable Hall Mode */
    SFR_SET_BIT_ASM(CMP_CTL4, CMP_CTL4_HALSEL_POS);
    /* Enable CMP0 */
    SFR_SET_BIT_ASM(CMP_CTL4, CMP_CTL4_C0EN_POS);

    /* Enable IO channel */
    CMP_IO_Channel_Enable(TRUE);
}

/**
 * @brief: Init CMP Interrupt
 *
 */
void CMP_Interrupt_Init(void)
{
    /* Trigger the interrupt flag by detecting the output change edge of the comparator */
    CMP_Trigger_Select_Config(CMP_CMPOUT_Change_INT);
    /* Enable CMP0 interrupt */
    CMP_INT_Enable(CMP0_ADDR, TRUE);

    /* Preemption priority 4, sub-priority 0 */
    INT_Interrupt_Priority_Config(INT_CMP, 4, 0);
    /* Peripheral interrupt enable */
    INT_Interrupt_Enable(INT_CMP, TRUE);
    /* Clear interrupt flag */
    INT_Clear_Interrupt_Flag(INT_CMP);
    /* Interrupt automatic stack using single word alignment */
    INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);
    /* Enable global interrupt */
    INT_All_Enable(TRUE);
}

/**
 * @brief: Init CMP0 IO
 *
 */
void CMP0_IO_Init(void)
{
    /* CMP IO inialization:C0OUT */
    GPIO_Write_Mode_Bits(PH4_CMP0OUT_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PH4_CMP0OUT_AF);

    /*PA9 Positive Input*/
    GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_9, GPIO_MODE_AN);
    /*PB3 Negative Input*/
    GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_3, GPIO_MODE_AN);
}

/**
 * @brief: CMP calibration function
 *
 */
void CMP_Calibration(void)
{
    uint32_t CMP0_Trim_P, CMP0_Trim_N;

    /* CMP reset */
    CMP_Reset();

    /* Enable CMP0 */
    CMP_CTL4 |= 0x00000021;

    /* Calibration PMOS */
    /* Enable CMPTRIMLOW */
    CMP_TRIM0 |= 0x80000000;

    /* Delay time */
    delay_ms(12);

    /* If CMO0_OUT output is high */
    if (CMP_CTL4 & 0x10000000)
    {
        /* C0TRIMPOL set to 0, negative calibration */
        CMP_TRIM0 &= 0xFFFFFF7F;
        /* Delay time */
        delay_ms(12);
        for (uint16_t i = 1; i < 33; i++)
        {
            /* CMP0_OUT level is detected to be turned to 0 */
            if (!(CMP_CTL4 & 0x10000000))
            {
                break;
            }
            CMP_TRIM0 |= 0X100 * i;
            delay_ms(12);
        }
    }
    /* If CMO0_OUT output is low */
    else if (!(CMP_CTL4 & 0x10000000))
    {
        /* C0TRIMPOL set to 1, positive calibration */
        CMP_TRIM0 |= 0x00000080;
        /* Delay time */
        delay_ms(12);
        for (uint16_t j = 1; j < 33; j++)
        {
            /* CMP0_OUT level is detected to be turned to 1 */
            if (CMP_CTL4 & 0x10000000)
            {
                break;
            }
            CMP_TRIM0 |= 0x100 * j;
            delay_ms(12);
        }
    }

    CMP0_Trim_P = CMP_TRIM0;

    /* Register initialization */
    CMP_TRIM0 &= 0;
    /* Calibration NMOS */
    CMP_TRIM0 |= 0x40000000;
    delay_ms(12);
    /* If CMO0_OUT output is high */
    if (CMP_CTL4 & 0x10000000)
    {
        /* C0TRIMNPOL set to 1, positive calibration */
        CMP_TRIM0 |= 1;
        for (uint8_t i = 1; i < 33; i++)
        {
            /* C0TRIMNPOL set to 0, negative calibration */
            if (!(CMP_CTL4 & 0x10000000))
            {
                break;
            }
            CMP_TRIM0 = CMP_TRIM0 | 0x2 * i;
            delay_ms(12);
        }
    }
    /* If CMO0_OUT output is high */
    else if (!(CMP_CTL4 & 0x10000000))
    {
        /* C0TRIMNPOL set to 0, positive calibration */
        CMP_TRIM0 &= 0xFFFFFFFE;
        for (uint8_t j = 1; j < 33; j++)
        {
            /* C0TRIMNPOL set to 0, negative calibration */
            if (CMP_CTL4 & 0x10000000)
            {
                break;
            }
            CMP_TRIM0 = CMP_TRIM0 | 0x2 * j;
            delay_ms(12);
        }
    }
    CMP0_Trim_N = CMP_TRIM0;

    /* Write calibration value */
    CMP_TRIM0 = (CMP0_Trim_P & 0x00003F00) | (CMP0_Trim_N & 0x0000007E);
}

/**
 * @brief: Hw delay
 *
 * @param[in] nms
 */
void delay_ms(volatile uint32_t nms)
{
    volatile uint32_t i, j;
    for (i = 0; i < nms; i++)
    {
        j = 2000;
        while (j--)
            ;
    }
}
