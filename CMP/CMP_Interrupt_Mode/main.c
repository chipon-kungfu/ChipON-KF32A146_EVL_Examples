/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for CMP interrupt mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "main.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/

/*******************************************************************************
**                             Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/

/**
 * @brief:
 *
 * @retval int
 */
int main()
{
    /* System clock configuration */
    SystemInit(72U);

    systick_delay_init(72U);

    /* LED control */
    GPIOInit_Output_Config(PA3_LED1_PIN);
    GPIOInit_Output_Config(PF11_LED2_PIN);

    /*Calibration the CMP module*/
    CMP_Calibration();

    /* CMP IO inialization */
    CMP0_IO_Init();

    /* CMP initialization */
    CMP0_HaLLModeInit();

    /* CMP interrupt initialization */
    CMP_Interrupt_Init();

    while (1)
    {
        systick_delay_ms(100);
        GPIO_Toggle_Output_Data_Config(PF11_LED2_PIN);
    }
}

/**
 * @brief: Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *
 * @param[in] File file pointer to the source file name
 * @param[in] Line line assert_param error line source number
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
