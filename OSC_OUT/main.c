/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Show how to use external or internal clock and output
 *                      clock through special pin
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/
/**
 * @brief Select the type of clock output from the OSC_OUT pins.In the following
 * line choose the out type
 * CLKOUT_SCLK  = 0,
 * CLKOUT_EXTLF = 1,
 * CLKOUT_EXTHF = 2,
 * CLKOUT_INTHF = 4,
 * CLKOUT_PLL   = 5,
 * CLKOUT_LP4M  = 6,
 * */
#if !defined(CLK_OUT_TYPE)
#define CLK_OUT_TYPE (CLKOUT_SCLK)
#endif

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    /* Initialize the system clock is 72MHz, and enable the INTHF,INTLF,EXTHF,PLL */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 72MHz */
    systick_delay_init(72U);
    /* Enale EXTLF */
    OSC_EXTLF_Software_Enable(TRUE);
    OSC_EXTLF_Start_Delay_Config(EXT_START_DELAY_1024);
    while (OSC_Get_EXTLF_INT_Flag() != SET)
        ;
    /* Enale LP4M */
    OSC_LP4M_Software_Enable(TRUE);
    while (OSC_Get_LP4MIF_INT_Flag() != SET)
        ;
    /* Configure PA1 remap to SCLKOUT */
    GPIO_Write_Mode_Bits(PA1_CLKOUT_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PA1_CLKOUT_AF);
    /* Select the type of clock output from the OSC_OUT pins !!! */
    OSC_SCLK_Output_Select(CLK_OUT_TYPE);
    /* Enable SCLKOUT, and frequency division is <1:16> */
    OSC_SCLK_Output_Division_Config(CLKOUT_DIVISION_128);
    OSC_SCLK_Output_Enable(TRUE);
    while (1)
    {}
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
