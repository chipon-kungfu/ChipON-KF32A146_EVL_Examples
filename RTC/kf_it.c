/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "kflog.h"

/*******************************************************************************
**                   KF32A146 Processor Exceptions Handlers
*******************************************************************************/

/**
 * @brief:EINT19TO17 Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _EINT19TO17_exception(void)
{
    // Handle interrupt for external interrupt line 17.
    if (INT_Get_External_Flag(INT_EXTERNAL_INTERRUPT_17)) // The interrupt entry used for RTC interrupt is external
                                                          // interrupt 17.
    {
        INT_External_Clear_Flag(INT_EXTERNAL_INTERRUPT_17); // The flag must be cleared before exiting the interrupt.
        if (RTC_Get_Time_Tick_INT_Flag())                   // Timer Tick interrupt.
        {
            RTC_Clear_Time_Tick_INT_Flag(); // Clear the flag of the timer tick interrupt.
            GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
        }

        if (RTC_Get_Alarm_A_INT_Flag()) // Interrupt for Alarm A.
        {
            RTC_Clear_Alarm_A_INT_Flag(); // Clear the flag of the interrupt for Alarm A.
            kf_printf("Alarm A!!\r\n");
        }
    }
}
