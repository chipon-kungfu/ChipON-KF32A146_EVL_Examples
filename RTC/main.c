/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a template project used by RTC for Perpetual
 *                      calendar
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "kflog.h"
#include "User_RTC.h"

/*******************************************************************************
**                              Private Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    uint32_t ret;

    /*initialize system clock,defaut SCLK is 72MHz,select INTHF as Clock source*/
    SystemInit(72U);
    /*Initialize the print function*/
    kfLog_Init();
    /* LED configure */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);
    /* Initialise systick */
    systick_delay_init(72U);

    kf_printf("Power On!!\r\n");

    ret = Osc_InitExternalLowFrequency();
    if(0u != 0u)
    {
        kf_printf("Osc_InitExternalLowFrequency Failed!!\r\n");
    }
    else
    {
        kf_printf("Osc_InitExternalLowFrequency ok.\n");
    }
    /* Initialise RTC */
    ret = RTC_Init("2023-02-02 14:26:00", RTC_WEEKDAY_THURSDAY_DEC);
    kf_printf("ret: %d.\r\n", ret);
    ret = 0u;

    /* Set RTC Alarm */
    ret = RTC_Set_Alarm(RTC_ALARM_A, "14:26:10", RTC_WEEKDAY_FRIDAY_DEC, RTC_ALARM_EVERY_DAY);
    kf_printf("ret: %d.\r\n", ret);

    /* Configure Time tick */
    RTC_Config_Mode_Enable(TRUE);
    RTC_Time_Tick_Config(RTC_TIME_TICK_DIV_1);
    RTC_Config_Mode_Enable(FALSE);
    /* RTC interrupt initialization */
    {
        RTC_Int_Config_t RTC_Int_Structure = {
          .newState    = TRUE,
          .intType     = RTC_TTIE | RTC_ALRAIE,
          .preemption  = 4u,
          .subPriority = 0u,
        };

        RTC_INT_Config(&RTC_Int_Structure);
    }

    /* Enable all interrupt */
    INT_All_Enable(TRUE);
    while (1)
    {
        systick_delay_ms(250u);
        systick_delay_ms(250u);
        systick_delay_ms(250u);
        systick_delay_ms(250u);
        /* Print RTC time through USART2 */
        RTC_Print_Time(USART2_SFR);
    }
}

/**
 *  @brief:Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  File: pointer to the source file name
 *  @param[in]  Line: assert_param error line source number
 *  @param[out] None
 *  @retval:None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
