/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_RTC.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides provides template for user RTC
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "User_RTC.h"
#include "kflog.h"

/**
 *  @brief :Configure RTC data,and configure RTC moduel.
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void Init_RTC_Data(void)
{
    RTC_InitTypeDef RTC_INIT;
    /*reset RTC**************************************************************/
    RTC_Reset();

    /* Set the INTLF to the RTC clock source ********************************/
    RTC_INIT.m_ClockSource = RTC_WORK_CLK_INTLF;
    /* Set RTC time format:24-hour clock ************************************/
    RTC_INIT.m_HourFormat = RTC_HOUR_FORMAT_24;
    /* Set RTC hours Initial value:20 ***************************************/
    RTC_INIT.m_TimeStruct.m_Hours = 20;
    /* Set RTC minutes Initial value:0 **************************************/
    RTC_INIT.m_TimeStruct.m_Minutes = 0;
    /* Set RTC seconds Initial value:0 **************************************/
    RTC_INIT.m_TimeStruct.m_Seconds = 0;
    /* set AM/PM.but 24-hour format don't need set AM/PM.********************/
    //    RTC_INIT.m_TimeStruct.m_AMPM = RTC_TIME_AM;
    /* Set RTC week Initial value:WEDNESDAY *********************************/
    RTC_INIT.m_DateStruct.m_WeekDay = RTC_WEEKDAY_WEDNESDAY_DEC;
    /* Set RTC year Initial value :19****************************************/
    RTC_INIT.m_DateStruct.m_Year = 19;
    /* Set RTC month Initial value: OCTOBER**********************************/
    RTC_INIT.m_DateStruct.m_Month = RTC_MONTH_OCTOBER_DEC;
    /* Set RTC date Initial value :30****************************************/
    RTC_INIT.m_DateStruct.m_Day = 30;
    /*Declare that the input parameters are not in BCD encoding format, and
    write the time and date into the register********************************/
    RTC_Configuration(RTC_TIME_FORMAT_BIN, &RTC_INIT);
}

/**
 *  @brief :Output rtc data through serial port.
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void Print_time_to_uart()
{
    char            USART_Array_Tansmit[21] = {'2', '0'};
    char           *p;
    RTC_InitTypeDef buf;
    /*    get rtc data and time    */
    RTC_Get_Time_Configuration(RTC_TIME_FORMAT_BCD, &buf.m_TimeStruct);
    RTC_Get_Date_Configuration(RTC_TIME_FORMAT_BCD, &buf.m_DateStruct);
    /*    Convert the time and date into a string and write it into the sending
    buffer    ******************************************************************/
    USART_Array_Tansmit[2]  = (buf.m_DateStruct.m_Year >> 4) + '0';
    USART_Array_Tansmit[3]  = (buf.m_DateStruct.m_Year & 0x0F) + '0';
    USART_Array_Tansmit[4]  = '-';
    USART_Array_Tansmit[5]  = (buf.m_DateStruct.m_Month >> 4) + '0';
    USART_Array_Tansmit[6]  = (buf.m_DateStruct.m_Month & 0x0F) + '0';
    USART_Array_Tansmit[7]  = '-';
    USART_Array_Tansmit[8]  = (buf.m_DateStruct.m_Day >> 4) + '0';
    USART_Array_Tansmit[9]  = (buf.m_DateStruct.m_Day & 0x0F) + '0';
    USART_Array_Tansmit[10] = ' ';
    USART_Array_Tansmit[11] = (buf.m_TimeStruct.m_Hours >> 4) + '0';
    USART_Array_Tansmit[12] = (buf.m_TimeStruct.m_Hours & 0x0F) + '0';
    USART_Array_Tansmit[13] = ':';
    USART_Array_Tansmit[14] = (buf.m_TimeStruct.m_Minutes >> 4) + '0';
    USART_Array_Tansmit[15] = (buf.m_TimeStruct.m_Minutes & 0x0F) + '0';
    USART_Array_Tansmit[16] = ':';
    USART_Array_Tansmit[17] = (buf.m_TimeStruct.m_Seconds >> 4) + '0';
    USART_Array_Tansmit[18] = (buf.m_TimeStruct.m_Seconds & 0x0F) + '0';
    USART_Array_Tansmit[19] = ' ';

    kf_printf(USART_Array_Tansmit);

    switch (buf.m_DateStruct.m_WeekDay)
    {
    case 1:
        p = "Mon";
        break;
    case 2:
        p = "Tue";
        break;
    case 3:
        p = "Wed";
        break;
    case 4:
        p = "Thu";
        break;
    case 5:
        p = "Fri";
        break;
    case 6:
        p = "Sat";
        break;
    case 7:
        p = "Sun";
        break;
    default:
        p = "error";
        break;
    }
    kf_printf(p);
    p = "\r\n";
    kf_printf(p);
}

/**
 *  @brief :Set RTC alarm.
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void Set_rtc_Alarm()
{
    RTC_AlarmTypeDef RTC_AlarmA;
    /*enter RTC configuration mode *****************************************/
    RTC_Config_Mode_Enable(TRUE);
    /*Set RTC alarm clock ring time*****************************************/
    RTC_AlarmA.m_WeekDay       = RTC_WEEKDAY_MONDAY_DEC;
    RTC_AlarmA.m_Hours         = 20;
    RTC_AlarmA.m_Minutes       = 0;
    RTC_AlarmA.m_Seconds       = 10;
    RTC_AlarmA.m_AMPM          = RTC_TIME_AM;
    RTC_AlarmA.m_WeekDayEnable = FALSE;
    RTC_AlarmA.m_HoursEnable   = TRUE;
    RTC_AlarmA.m_MinutesEnable = TRUE;
    RTC_AlarmA.m_SecondsEnable = TRUE;
    RTC_Alarm_Configuration(RTC_ALARM_A_ADDR_OFFSET, RTC_TIME_FORMAT_BIN, &RTC_AlarmA);

    /*enable alarm A*/
    RTC_Alarm_A_Enable(TRUE);
    /*enable alarm A IRQ*/
    RTC_Alarm_A_INT_Enable(TRUE);
    /*quit RTC configuration mode *******************************************/
    RTC_Config_Mode_Enable(FALSE);
}

/**
 *  @brief :The interrupt source of RTC cannot directly trigger the core
            interrupt. The RTC interrupt must use the external interrupt
            Eint17-19 as the signal transmission channel to trigger the
            core interrupt. Therefore, the use of RTC interrupts must also
            be configured with Eint17-19 interrupts..
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void Config_RTC_interrupt(void)
{
    /* Enter RTC configuration mode */
    RTC_Config_Mode_Enable(TRUE);

    uint32_t m_Rise = TRUE;
    uint32_t m_Fall = FALSE;
    uint32_t m_Mask = TRUE;
    uint32_t m_Line = INT_EXTERNAL_INTERRUPT_17;
    {
        uint32_t tmpreg;

        INT_EINTRISE = SFR_Config(INT_EINTRISE, ~(INT_EINTRISE_EINTRI16 << m_Line), m_Rise << m_Line);

        INT_EINTFALL = SFR_Config(INT_EINTFALL, ~(INT_EINTRISE_EINTRI16 << m_Line), m_Fall << m_Line);

        INT_EINTMASK = SFR_Config(INT_EINTMASK, ~(INT_EINTRISE_EINTRI16 << m_Line), m_Mask << m_Line);
    }

    /*Configure the RTC clock correction value. If the calibration function
    is not used, a value of 0 must be written. */
    RTC_Clock_Calibration_Config((int8_t)0x0);
    /*set rtc tick :1s*******************************************************/
    RTC_Time_Tick_Config(RTC_TIME_TICK_DIV_1);
    /*enable rtc tick IRQ****************************************************/
    RTC_Time_Tick_INT_Enable(TRUE);
    /*enable rtc seconds interrupt*******************************************/
    // RTC_Seconds_INT_Enable(TRUE);
    /*enable rtc*************************************************************/
    RTC_Enable(TRUE);
    /*quit RTC configuration mode *******************************************/
    RTC_Config_Mode_Enable(FALSE);
    /*enable INT_EINT19TO17 *************************************************/
    INT_Interrupt_Enable(INT_EINT19TO17, TRUE);
    /*enable all interrupt **************************************************/
    INT_All_Enable(TRUE);
}

/* ================================ NEW API ================================ */

/**
 * @brief: Initialise RTC.
 *          CAUTION:
 *              In default, RTC use INTLF as clock, if you want to use EXTLF:
 *              - enable EXTLF
 *              - modify $m_ClockSource$ to $RTC_WORK_CLK_EXTLF$
 *
 *  @param[in]  config_Str: Initialise configration string of RTC.
 *              Example: "2023-02-01 19:20:00"
 *              CAUTION:
 *                  1. '-',' ',':' are necessary.
 *                  2. Time format is 24 hours.
 *                  3. Width of month, day, hour, minute, second are 2. If they
 *                     are shorter than 2, padding 0.
 *  @param[in]  week_Day: Week day. It can be as follow:
 *              RTC_WEEKDAY_MONDAY_DEC
 *              RTC_WEEKDAY_TUESDAY_DEC
 *              RTC_WEEKDAY_WEDNESDAY_DEC
 *              RTC_WEEKDAY_THURSDAY_DEC
 *              RTC_WEEKDAY_FRIDAY_DEC
 *              RTC_WEEKDAY_SATURDAY_DEC
 *              RTC_WEEKDAY_SUNDAY_DEC
 *  @param[out] None
 * @retval uint32_t:
 *              0 -- success.
 *              other -- fail.
 */
uint32_t RTC_Init(uint8_t *config_Str, uint8_t week_Day)
{
    uint8_t  config_Year     = 0u;
    uint8_t  config_Month    = 0u;
    uint8_t  config_Day      = 0u;
    uint8_t  config_Hours    = 0u;
    uint8_t  config_Minutes  = 0u;
    uint8_t  config_Seconds  = 0u;
    uint8_t *config_Time_Ptr = NULL;

    if (NULL == config_Str)
    {
        /* Input configuration format ERR */
        return 1u;
    }
    else
    {}

    if (0u != strncmp(config_Str, "20", 2u))
    {
        /* Year ERR */
        return 2u;
    }
    else
    {
        if (('-' != config_Str[4]) || ('-' != config_Str[7]) || (' ' != config_Str[10]) || (':' != config_Str[13]) ||
            (':' != config_Str[16]))
        {
            /* Input configuration format ERR */
            return 1u;
        }
        else
        {
            /* Empty */
        }

        config_Year = ((config_Str[2] - '0') * 10u) + (config_Str[3] - '0');
        if (config_Year > 99)
        {
            /* Year ERR */
            return 2u;
        }
        else
        {
            /* Empty */
        }

        config_Month = ((config_Str[5] - '0') * 10u) + (config_Str[6] - '0');
        if ((config_Month <= 0u) || (config_Month > 12u))
        {
            /* Month ERR */
            return 3u;
        }
        else
        {
            /* Empty */
        }

        config_Day = ((config_Str[8] - '0') * 10u) + (config_Str[9] - '0');
        if ((config_Day <= 0u) || (config_Day > 31u))
        {
            /* Day ERR */
            return 4u;
        }
        else
        {
            /* Empty */
        }

        config_Time_Ptr = strstr(config_Str, " ");
        if (NULL == config_Time_Ptr)
        {
            /* Input configuration format ERR */
            return 1u;
        }
        else
        {
            /* Empty */
        }
        config_Hours = ((config_Time_Ptr[1] - '0') * 10u) + (config_Time_Ptr[2] - '0');
        if (config_Hours > 24u)
        {
            /* Day Hour */
            return 5u;
        }
        else
        {
            /* Empty */
        }

        config_Minutes = ((config_Time_Ptr[4] - '0') * 10u) + (config_Time_Ptr[5] - '0');
        if (config_Minutes > 59u)
        {
            /* Day Minute */
            return 6u;
        }
        else
        {
            /* Empty */
        }

        config_Seconds = ((config_Time_Ptr[7] - '0') * 10u) + (config_Time_Ptr[8] - '0');
        if (config_Seconds > 59u)
        {
            /* Day Second */
            return 7u;
        }
        else
        {
            /* Empty */
        }
    }

    RTC_InitTypeDef RTC_INIT;
    /* Reset RTC */
    RTC_Reset();
    /* Set the INTLF to the RTC clock source */
    RTC_INIT.m_ClockSource = RTC_WORK_CLK_EXTLF;
    /* Set RTC time format:24-hour clock */
    RTC_INIT.m_HourFormat = RTC_HOUR_FORMAT_24;
    /* Set RTC hours Initial value */
    RTC_INIT.m_TimeStruct.m_Hours = config_Hours;
    /* Set RTC minutes Initial value */
    RTC_INIT.m_TimeStruct.m_Minutes = config_Minutes;
    /* Set RTC seconds Initial value */
    RTC_INIT.m_TimeStruct.m_Seconds = config_Seconds;
    /* set AM/PM.but 24-hour format don't need set AM/PM. */
    //    RTC_INIT.m_TimeStruct.m_AMPM = RTC_TIME_AM;
    /* Set RTC week Initial value, such as: RTC_WEEKDAY_WEDNESDAY_DEC */
    RTC_INIT.m_DateStruct.m_WeekDay = week_Day;
    /* Set RTC year Initial value */
    RTC_INIT.m_DateStruct.m_Year = config_Year;
    /* Set RTC month Initial value */
    RTC_INIT.m_DateStruct.m_Month = config_Month;
    /* Set RTC date Initial value */
    RTC_INIT.m_DateStruct.m_Day = config_Day;
    /*Declare that the input parameters are not in BCD encoding format, and
    write the time and date into the register */
    RTC_Configuration(RTC_TIME_FORMAT_BIN, &RTC_INIT);

    return 0u;
}

/**
 * @brief: Set alarm.
 *
 *  @param[in]  alarm: Alarm number of RTC.
 *              RTC_ALARM_A - RTC_ALARM_B
 *  @param[in]  config_Str: Initialise configration string of RTC alarm.
 *              Example: "19:20:00"
 *              CAUTION:
 *                  1. ':' are necessary.
 *                  2. Time format is 24 hours.
 *                  3. Width of hour, minute, second are 2. If they
 *                     are shorter than 2, padding 0.
 *  @param[in]  week_Day: Week day. It can be as follow:
 *              RTC_WEEKDAY_MONDAY_DEC
 *              RTC_WEEKDAY_TUESDAY_DEC
 *              RTC_WEEKDAY_WEDNESDAY_DEC
 *              RTC_WEEKDAY_THURSDAY_DEC
 *              RTC_WEEKDAY_FRIDAY_DEC
 *              RTC_WEEKDAY_SATURDAY_DEC
 *              RTC_WEEKDAY_SUNDAY_DEC
 *  @param[in]  alarm_Type: Alarm repeat mode.
 *              RTC_ALARM_EVERY_MINUTE
 *              RTC_ALARM_EVERY_HOUR
 *              RTC_ALARM_EVERY_DAY
 *              RTC_ALARM_EVERY_WEEK
 *  @param[out] None
 * @retval uint32_t:
 *              0 -- success.
 *              other -- fail.
 */
uint32_t RTC_Set_Alarm(RTC_Alarm_t alarm, uint8_t *config_Str, uint8_t week_Day, RTC_Alarm_Type_t alarm_Type)
{
    RTC_AlarmTypeDef RTC_Alarm;
    uint8_t          config_Hours   = 0u;
    uint8_t          config_Minutes = 0u;
    uint8_t          config_Seconds = 0u;

    if (NULL == config_Str)
    {
        return 1u;
    }
    else
    {}

    config_Hours = ((config_Str[0] - '0') * 10u) + (config_Str[1] - '0');
    if (config_Hours > 24u)
    {
        /* Hour ERR */
        return 2u;
    }
    else
    {}

    config_Minutes = ((config_Str[3] - '0') * 10u) + (config_Str[4] - '0');
    if (config_Minutes > 59u)
    {
        /* Minute ERR */
        return 3u;
    }
    else
    {}

    config_Seconds = ((config_Str[6] - '0') * 10u) + (config_Str[7] - '0');
    if (config_Seconds > 59u)
    {
        /* Second ERR */
        return 3u;
    }
    else
    {}

    /*Set RTC alarm clock ring time */
    RTC_Alarm.m_WeekDay = week_Day;
    RTC_Alarm.m_Hours   = config_Hours;
    RTC_Alarm.m_Minutes = config_Minutes;
    RTC_Alarm.m_Seconds = config_Seconds;
    RTC_Alarm.m_AMPM    = RTC_TIME_AM;

    if (RTC_ALARM_EVERY_MINUTE == alarm_Type)
    {
        RTC_Alarm.m_WeekDayEnable = FALSE;
        RTC_Alarm.m_HoursEnable   = FALSE;
        RTC_Alarm.m_MinutesEnable = FALSE;
        RTC_Alarm.m_SecondsEnable = TRUE;
    }
    else if (RTC_ALARM_EVERY_HOUR == alarm_Type)
    {
        RTC_Alarm.m_WeekDayEnable = FALSE;
        RTC_Alarm.m_HoursEnable   = FALSE;
        RTC_Alarm.m_MinutesEnable = TRUE;
        RTC_Alarm.m_SecondsEnable = TRUE;
    }
    else if (RTC_ALARM_EVERY_DAY == alarm_Type)
    {
        RTC_Alarm.m_WeekDayEnable = FALSE;
        RTC_Alarm.m_HoursEnable   = TRUE;
        RTC_Alarm.m_MinutesEnable = TRUE;
        RTC_Alarm.m_SecondsEnable = TRUE;
    }
    else if (RTC_ALARM_EVERY_WEEK == alarm_Type)
    {
        RTC_Alarm.m_WeekDayEnable = TRUE;
        RTC_Alarm.m_HoursEnable   = TRUE;
        RTC_Alarm.m_MinutesEnable = TRUE;
        RTC_Alarm.m_SecondsEnable = TRUE;
    }
    else
    {}

    /*enter RTC configuration mode */
    RTC_Config_Mode_Enable(TRUE);

    if (RTC_ALARM_A == alarm)
    {
        RTC_Alarm_Configuration(RTC_ALARM_A_ADDR_OFFSET, RTC_TIME_FORMAT_BIN, &RTC_Alarm);
        /* Enable alarm A */
        RTC_Alarm_A_Enable(TRUE);
        /* Enable alarm A IRQ */
        RTC_Alarm_A_INT_Enable(TRUE);
    }
    else
    {
        RTC_Alarm_Configuration(RTC_ALARM_B_ADDR_OFFSET, RTC_TIME_FORMAT_BIN, &RTC_Alarm);
        /* Enable alarm B */
        RTC_Alarm_B_Enable(TRUE);
        /* Enable alarm B IRQ */
        RTC_Alarm_B_INT_Enable(TRUE);
    }

    /* Quit RTC configuration mode */
    RTC_Config_Mode_Enable(FALSE);

    return 0u;
}

/**
 * @brief: Print RTC time through USARTx.
 *
 *  @param[in]  USARTx: Pointer to USART register.
 *  @param[out] None
 * @retval: None
 */
void RTC_Print_Time(USART_SFRmap *USARTx)
{
    char            USART_Array_Tansmit[21] = {'2', '0'};
    char           *p;
    RTC_InitTypeDef buf;
    /*    get rtc data and time    */
    RTC_Get_Time_Configuration(RTC_TIME_FORMAT_BCD, &buf.m_TimeStruct);
    RTC_Get_Date_Configuration(RTC_TIME_FORMAT_BCD, &buf.m_DateStruct);
    /*    Convert the time and date into a string and write it into the sending
    buffer */
    USART_Array_Tansmit[2]  = (buf.m_DateStruct.m_Year >> 4) + '0';
    USART_Array_Tansmit[3]  = (buf.m_DateStruct.m_Year & 0x0F) + '0';
    USART_Array_Tansmit[4]  = '-';
    USART_Array_Tansmit[5]  = (buf.m_DateStruct.m_Month >> 4) + '0';
    USART_Array_Tansmit[6]  = (buf.m_DateStruct.m_Month & 0x0F) + '0';
    USART_Array_Tansmit[7]  = '-';
    USART_Array_Tansmit[8]  = (buf.m_DateStruct.m_Day >> 4) + '0';
    USART_Array_Tansmit[9]  = (buf.m_DateStruct.m_Day & 0x0F) + '0';
    USART_Array_Tansmit[10] = ' ';
    USART_Array_Tansmit[11] = (buf.m_TimeStruct.m_Hours >> 4) + '0';
    USART_Array_Tansmit[12] = (buf.m_TimeStruct.m_Hours & 0x0F) + '0';
    USART_Array_Tansmit[13] = ':';
    USART_Array_Tansmit[14] = (buf.m_TimeStruct.m_Minutes >> 4) + '0';
    USART_Array_Tansmit[15] = (buf.m_TimeStruct.m_Minutes & 0x0F) + '0';
    USART_Array_Tansmit[16] = ':';
    USART_Array_Tansmit[17] = (buf.m_TimeStruct.m_Seconds >> 4) + '0';
    USART_Array_Tansmit[18] = (buf.m_TimeStruct.m_Seconds & 0x0F) + '0';
    USART_Array_Tansmit[19] = ' ';

    kf_printf(USART_Array_Tansmit);

    switch (buf.m_DateStruct.m_WeekDay)
    {
    case 1:
        p = "Mon";
        break;
    case 2:
        p = "Tue";
        break;
    case 3:
        p = "Wed";
        break;
    case 4:
        p = "Thu";
        break;
    case 5:
        p = "Fri";
        break;
    case 6:
        p = "Sat";
        break;
    case 7:
        p = "Sun";
        break;
    default:
        p = "error";
        break;
    }
    kf_printf(p);
    p = "\r\n";
    kf_printf(p);
}

/**
 * @brief: Configure the RTC interrupt.
 *
 *  @param[in]  int_Config: Pointer to RTC interrupt config structure.
 *              ->newState: The state of interrupt.
 *                  TRUE/FALSE
 *              ->intType: The interrupt type of SPIx. It can be:
 *                  RTC_TSIE
 *                  RTC_TSOVFIE
 *                  RTC_TTIE
 *                  RTC_ALRBIE
 *                  RTC_ALRAIE
 *                  RTC_DAYIE
 *                  RTC_HOURIE
 *                  RTC_MINIE
 *                  RTC_SECIE
 *                  If you want to enable more than one interrupt, you can use
 *                  OR("|") operator to connect them.
 *              ->preemption: The preemption of interrupt.
 *              ->subPriority: The subPriority of interrupt.
 *  @param[out] None
 * @retval: None
 */
void RTC_INT_Config(RTC_Int_Config_t *int_Config)
{
    /* Enter RTC configuration mode */
    RTC_Config_Mode_Enable(TRUE);

    uint32_t tmpreg;
    uint32_t m_Rise = TRUE;
    uint32_t m_Fall = FALSE;
    uint32_t m_Mask = TRUE;
    uint32_t m_Line = INT_EXTERNAL_INTERRUPT_17;
    {
        INT_EINTRISE = SFR_Config(INT_EINTRISE, ~(INT_EINTRISE_EINTRI16 << m_Line), m_Rise << m_Line);

        INT_EINTFALL = SFR_Config(INT_EINTFALL, ~(INT_EINTRISE_EINTRI16 << m_Line), m_Fall << m_Line);

        INT_EINTMASK = SFR_Config(INT_EINTMASK, ~(INT_EINTRISE_EINTRI16 << m_Line), m_Mask << m_Line);
    }

    /*Configure the RTC clock correction value. If the calibration function
    is not used, a value of 0 must be written. */
    RTC_Clock_Calibration_Config((int8_t)0x0);
    /*enable rtc */
    RTC_Enable(TRUE);

    if (FALSE == int_Config->newState)
    {
        tmpreg = RTC_IER;
        /* Disable RTC interrupt */
        RTC_IER = tmpreg & (~(int_Config->intType));
        /* Disable SPIx interrupt */
        INT_Interrupt_Enable(INT_EINT19TO17, FALSE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(INT_EINT19TO17);
    }
    else
    {
        tmpreg = RTC_IER;
        /* enable RTC interrupt */
        RTC_IER = tmpreg | int_Config->intType;
        /* Disable SPIx interrupt */
        INT_Interrupt_Enable(INT_EINT19TO17, TRUE);
        /* Clear timer interrupt flag */
        INT_Clear_Interrupt_Flag(INT_EINT19TO17);
        /* Set SPIx interrupt priority */
        INT_Interrupt_Priority_Config(INT_EINT19TO17, int_Config->preemption, int_Config->subPriority);
    }

    /*quit RTC configuration mode */
    RTC_Config_Mode_Enable(FALSE);
}

/**
 * @brief Delay function.
 *
 * @param DelayCount Delay time (in us) multiplied by frequency of SCLK.
 *  For example:
 *      Now, SCLK is 16MHz and you want to delay 10us, so you should input 16*10 = 160.
 * @attention The delay time is not accurate at low frequencies.
 * @retval void
 */
__attribute__((noinline, section(".indata"), optimize("-Os"))) static void __NopDelay(uint32_t DelayCount)
{
    asm volatile("MOV R0, %0" ::"r"(DelayCount));
    asm volatile("LSR R0, #3");
    asm volatile("SUB R0, R0, #1");
    asm volatile("NOP");
    asm volatile("NOP");
    asm volatile("NOP");
    asm volatile("CMP R0, #0");
    asm volatile("JNZ $-5");
}

/**
 * @brief Initialize external low frequency oscillator
 * @note Modify the SCLK_FREQ and EXTLF_INIT_TIMEOUT according to the actual situation
 * 
 * @return uint32_t 
 */
uint32_t Osc_InitExternalLowFrequency(void)
{
    /**
     * @note Modify the SCLK_FREQ according to the actual situation
     */
#define SCLK_FREQ          (72u)     /* in MHz */
#define EXTLF_INIT_TIMEOUT (100000u) /* in 10us */

    uint32_t ret = 0u;
    volatile uint32_t delayTime = EXTLF_INIT_TIMEOUT;

    BKP_Write_And_Read_Enable(TRUE);
#ifdef KF32A136
    PM_EXTLF_PIN_Selection_Config(PM_EXTLF_PIN1_IO_PORT);
#else
    PM_EXTLF_PIN_Selection_Config(PM_EXTLF_PIN2_IO_PORT);
#endif
    
    /* choose biggest driver current */
    PM_CAL1 |= (uint32_t)3u; 
    BKP_Write_And_Read_Enable(FALSE);

    OSC_EXTLF_Software_Enable(TRUE);
    OSC_EXTLF_Start_Delay_Config(EXT_START_DELAY_32768);
    __NopDelay(SCLK_FREQ *1000000u); /*Delay 1s*/
    while (OSC_Get_EXTLF_INT_Flag() != SET)
    {
        __NopDelay(SCLK_FREQ * 10u);
        delayTime--;
    }
    if(0u == delayTime)
    {
        /* Timeout */
        ret = 1u;
    }

    return ret;

#undef SCLK_FREQ
#undef EXTLF_INIT_TIMEOUT
}