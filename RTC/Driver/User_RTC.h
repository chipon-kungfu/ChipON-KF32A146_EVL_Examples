/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : User_RTC.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the Header file configuration
 *                      for User_RTC
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#ifndef USER_RTC_H_
#define USER_RTC_H_

/*******************************************************************************
**                                 Macro Defines                              **
*******************************************************************************/
/* Enum of RTC Alarm repeat mode */
typedef enum RTC_ALARM_TYPE
{
    RTC_ALARM_EVERY_MINUTE = 1u,
    RTC_ALARM_EVERY_HOUR,
    RTC_ALARM_EVERY_DAY,
    RTC_ALARM_EVERY_WEEK,
} RTC_Alarm_Type_t;
/* Enum of RTC Alarm number */
typedef enum RTC_ALARM
{
    RTC_ALARM_A = 1u,
    RTC_ALARM_B,
} RTC_Alarm_t;

/* Enum of RTC interrupt mode */
typedef enum RTC_INT
{
    RTC_TSIE    = RTC_IER_TSIE,
    RTC_TSOVFIE = RTC_IER_TSOVFIE,
    RTC_TTIE    = RTC_IER_TTIE,
    RTC_ALRBIE  = RTC_IER_ALRBIE,
    RTC_ALRAIE  = RTC_IER_ALRAIE,
    RTC_DAYIE   = RTC_IER_DAYIE,
    RTC_HOURIE  = RTC_IER_HOURIE,
    RTC_MINIE   = RTC_IER_MINIE,
    RTC_SECIE   = RTC_IER_SECIE,
} RTC_INT_t;
/* RTC interrupt configuration typedef */
typedef struct RTC_INT_CONFIG
{
    FunctionalState newState;
    RTC_INT_t       intType;
    uint8_t         preemption;
    uint8_t         subPriority;
} RTC_Int_Config_t;

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/
void Init_RTC_Data(void);
void Print_time_to_uart();
void Set_rtc_Alarm(void);
void Config_RTC_interrupt(void);

/* ================================ NEW API ================================ */
extern uint32_t RTC_Init(uint8_t *config_Str, uint8_t week_Day);
extern void     RTC_Print_Time(USART_SFRmap *USARTx);
extern uint32_t RTC_Set_Alarm(RTC_Alarm_t alarm, uint8_t *config_Str, uint8_t week_Day, RTC_Alarm_Type_t alarm_Type);
extern void     RTC_INT_Config(RTC_Int_Config_t *int_Config);
/**
 * @brief Initialize external low frequency oscillator
 * @note Modify the SCLK_FREQ and EXTLF_INIT_TIMEOUT according to the actual situation
 * 
 * @return uint32_t 
 */
extern uint32_t Osc_InitExternalLowFrequency(void);

#endif /* USER_RTC_H_ */
