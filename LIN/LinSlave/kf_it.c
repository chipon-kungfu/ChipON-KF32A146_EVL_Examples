/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "lin_slave.h"

/*******************************************************************************
**                          Global Variables Declaration
*******************************************************************************/
extern uint8_t LINRxBuffer[LIN_MSG_LEN + 1];
extern uint8_t LIN_Sta_g;
extern uint8_t LIN_Checksum_g;
extern uint8_t BufCopy_Flag_g;

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception(void) {}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _HardFault_exception(void) {}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception(void) {}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception(void) {}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception(void) {}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception(void) {}

//*****************************************************************************************
//                              USART51Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _USART1_exception(void)
{

    INT_Clear_Interrupt_Flag(INT_USART5);

    /* Break Filed */
    if (LIN_INITIAL == LIN_Sta_g && USART_Get_Blank_Flag(USART1_SFR))
    {
        USART_Clear_Blank_INT_Flag(USART1_SFR);
        /* Enable Slave auto baudrate detection and wait for completion */
        LIN_Sta_g = LIN_BREAK;
    }
    /* Received data from LIN */
    if (USART_Get_Receive_BUFR_Ready_Flag(USART1_SFR))
    {
        static uint8_t counter = 1u;

        USART_Clear_Receive_BUFR_INT_Flag(USART1_SFR);
        switch (LIN_Sta_g)
        {
        case LIN_INITIAL:
            break;
        /* Sync Filed */
        case LIN_BREAK:
            if (0x55u == USART_ReceiveData(USART1_SFR))
            {
                LIN_Sta_g = LIN_SYNC;
            }
            break;
        /* Protected ID Filed
         *  The PID saved in the 1st byte of LINRxBuffer.
         */
        case LIN_SYNC:
            LINRxBuffer[0] = USART_ReceiveData(USART1_SFR);
            LIN_Sta_g      = LIN_PID;
            break;
        /* Data Filed */
        case LIN_PID:
            LINRxBuffer[counter++] = USART_ReceiveData(USART1_SFR);
            if (LIN_MSG_LEN + 1u == counter)
            {
                counter   = 1u;
                LIN_Sta_g = LIN_CHECKSUM;
            }
            break;
        /* CheckSum Filed */
        case LIN_CHECKSUM:
            LIN_Checksum_g = USART_ReceiveData(USART1_SFR);
            LIN_Sta_g      = LIN_END;
            BufCopy_Flag_g = 0u;
            break;

        default:
            break;
        }
    }
}
