/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : lin_slave.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use LIN in Slave mode.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef _LIN_MASTER_H_
#define _LIN_MASTER_H_

/******************************************************************************
 *                      Global Macro Defineition                              *
 ******************************************************************************/
/* LIN status */
enum LIN_Status
{
    LIN_INITIAL = 0,
    LIN_BREAK,
    LIN_SYNC,
    LIN_PID,
    LIN_DATA,
    LIN_CHECKSUM,
    LIN_END
};
/* Number of LIN frame bytes */
#define LIN_MSG_LEN (8U)

/* LIN slave ID address */
#define LINSlaveID1 (0x34)
#define LINSlaveID2 (0x36)

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/
extern void USART_LIN_config(USART_SFRmap *USARTx);

extern void LIN_Slave_Send(USART_SFRmap *USARTx, uint8_t *Databuf, uint32_t Length);

#endif /* _LIN_MASTER_H_ */
