/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : TMain Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "lin_slave.h"

/*******************************************************************************
**                          Global Variables Declaration
*******************************************************************************/
extern uint8_t LINRxBuffer[LIN_MSG_LEN + 1];
extern uint8_t LIN_Sta_g;
extern uint8_t LIN_Checksum_g;
extern uint8_t BufCopy_Flag_g;

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception(void) {}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _HardFault_exception(void) {}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception(void) {}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception(void) {}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception(void) {}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception(void) {}

//*****************************************************************************************
//                              USART1 Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _USART1_exception(void)
{
/**
 * @brief When performing porting operations, only adjustment of the corresponding 
 * macro USARTx and USARTx_INTR is needed to complete the adaptation.
 */
#define USARTx      (USART1_SFR)
#define USARTx_INTR (INT_USART1)

    INT_Clear_Interrupt_Flag(USARTx_INTR);

    /* Break Filed */
    if (USART_Get_Blank_Flag(USARTx))
    {
        USART_Clear_Blank_INT_Flag(USARTx);
        USART_Clear_Frame_ERROR_INT_Flag(USARTx);

        if(1u == Auto_BaudRate_EnableFlag_g)
        {
            Auto_BaudRate_EnableFlag_g = 0u;
		    USART_Auto_BaudRate_Detection_Enable(USARTx,TRUE);
        }

        /* Enable Slave auto baudrate detection and wait for completion */
        LIN_Sta_g = LIN_BREAK;
    }
    /* Received data from LIN */
    if (USART_Get_Receive_BUFR_Ready_Flag(USARTx))
    {
        static uint8_t counter = 1u;

        switch (LIN_Sta_g)
        {
        case LIN_INITIAL:
            USART_ReceiveData(USARTx);
            break;
        /* Sync Filed */
        case LIN_BREAK:
            if (0x55u == USART_ReceiveData(USARTx))
            {
                Auto_BaudRate_Copy_g = USARTx->BRGR;
                LIN_Sta_g = LIN_SYNC;
            }
            else
            {
                /**
                 * Set the USART baud rate to the baud rate of the last successful communication.
                 */
                USARTx->BRGR = Auto_BaudRate_Copy_g;
            }
            break;
        /* Protected ID Filed
         *  The PID saved in the 1st byte of LINRxBuffer.
         */
        case LIN_SYNC:
            LINRxBuffer[0] = USART_ReceiveData(USARTx);
            LIN_Sta_g      = LIN_PID;
            break;
        /* Data Filed */
        case LIN_PID:
            LINRxBuffer[counter++] = USART_ReceiveData(USARTx);
            if (LIN_MSG_LEN + 1u == counter)
            {
                counter   = 1u;
                LIN_Sta_g = LIN_CHECKSUM;
            }
            break;
        /* CheckSum Filed */
        case LIN_CHECKSUM:
            LIN_Checksum_g = USART_ReceiveData(USARTx);
            LIN_Sta_g      = LIN_END;
            BufCopy_Flag_g = 0u;
            break;

        default:
            USART_ReceiveData(USARTx);
            break;
        }
    }

    /** 
     * Receive Frame Idle 
     * 
     * Check the baudrate of the LIN slave.
     * Enable the wakeup function of USART, prepare for the next frame.
     */
    if(USART_Get_Receive_Frame_Idel_Flag(USARTx))
    {
        uint16_t baudRate_Integer = 0u;

        baudRate_Integer = (((USARTx->BRGR) & USART_BRGR_BRGM) >> USART_BRGR_BRGM0_POS);
        if((baudRate_Integer > (LIN_BAUD_RATE_INTEGER_DEFAULT + LIN_BAUD_RATE_INTEGER_HYSTERESIS)) || (baudRate_Integer < (LIN_BAUD_RATE_INTEGER_DEFAULT - LIN_BAUD_RATE_INTEGER_HYSTERESIS)))
        {
            USARTx->BRGR = Auto_BaudRate_Copy_g;
        }

        USART_Clear_Idle_INT_Flag(USARTx);
        Auto_BaudRate_EnableFlag_g = 1u;
        LIN_Sta_g = LIN_INITIAL;
        USART_WeakUP_Enable(USARTx, TRUE);
    }

    /**
     * Auto baudrate detection time-out
     * 
     * Set the USART baud rate to the baud rate of the last successful communication.
     */
    if(USART_Get_Auto_Baudrate_TimeOver_Flag(USARTx))
    {
        USART_Clear_Auto_BaudRate_TimeOver_INT_Flag(USARTx);

        USARTx->BRGR = Auto_BaudRate_Copy_g;
    }

    /**
     * Frame error interrupt
     * 
     * Set the USART baud rate to the baud rate of the last successful communication.
     */
    if(USART_Get_Frame_ERROR_Flag(USARTx))
    {
        USART_Clear_Frame_ERROR_INT_Flag(USARTx);

        USARTx->BRGR = Auto_BaudRate_Copy_g;
    }
    
#undef USARTx
#undef USARTx_INTR
}

