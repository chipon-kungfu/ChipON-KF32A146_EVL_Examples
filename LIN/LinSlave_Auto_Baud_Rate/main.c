/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use LIN in Slave mode.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "lin_slave.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/
/* Led control toggle output */
#define BoardLed1Toggle() (GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN))

/* LIN transfer status */
#define LIN_TRANSFER_ON()
#define LIN_TRANSFER_OFF()

/*******************************************************************************
**                          Global Variables Definitions
*******************************************************************************/
/* Transmit date buffer */
volatile uint8_t LINTxBuffer[LIN_MSG_LEN]     = {1, 2, 3, 4, 5, 6, 7, 8};
volatile uint8_t LINRxBuffer[LIN_MSG_LEN + 1] = {0};

volatile uint8_t LIN_Sta_g      = LIN_INITIAL;
volatile uint8_t LIN_Checksum_g = 0;
volatile uint8_t BufCopy_Flag_g = 0;

/*******************************************************************************
**                                Global Functions
*******************************************************************************/
/**
 *  @brief: Initialize the LED GPIO ports
 *  @param[in]  None
 *  @param[out] None
 *  @retval: None
 */
void BoardGpioInit(void)
{
    /* Configure LED1 output */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);
    /* Configure LED2 output */
    GPIOInit_Output_Config(LED2_PORT, LED2_PIN);
}

/**
 *  @brief :Initialize the LIN GPIO ports
 *             PH6    -- LIN_TX_PIN
 *             PH14 -- LIN_RX_PIN
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void LINGpioIinit(void)
{
    GPIO_Write_Mode_Bits(PG9_LIN1_RX_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PC2_LIN1_TX_PIN, GPIO_MODE_RMP);

    /* Configure PG9 remap function */
    GPIO_Pin_RMP_Config(PG9_LIN1_RX_AF);
    /* Configure PC2 remap function */
    GPIO_Pin_RMP_Config(PC2_LIN1_TX_AF);
}

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{

    /* Initialize the system clock is 72MHz */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 72MHz */
    systick_delay_init(72U);

    /* Initialize the LED IOs */
    BoardGpioInit();
    /* Initialize the LIN IOs */
    LINGpioIinit();

    /* LIN2.0 configured as follow:
        - BaudRate = 19200 baud
        - Word Length = 8 Bits
        - parity bits included
        - Receive and transmit enabled
     */
    USART_LIN_config(USART1_SFR);

    /* Enable LIN transfer */
    LIN_TRANSFER_ON();


    /* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
    INT_Interrupt_Priority_Config(INT_USART1, 4u, 1u);
    /* Enable and set INT_USART0 Interrupt priority */
    INT_Clear_Interrupt_Flag(INT_USART1);
    INT_Interrupt_Enable(INT_USART1, TRUE);
    /* Enable the global interrupt */
    INT_All_Enable(TRUE);

    while (1)
    {
        /**
         * @brief: If slave received the frame, which id is LINSlaveID2, slave
         * send the data in LINTxBuffer to master. And toggle the user LED2.
         */
        if (LIN_PID == LIN_Sta_g)
        {
            if (LINSlaveID2 == (LINRxBuffer[0] & (~0xC0u)))
            {
                LIN_Sta_g = LIN_INITIAL;
                LIN_Slave_Send(USART1_SFR, (uint8_t *)LINTxBuffer, LIN_MSG_LEN);
                GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);

            }
        }

        /**
         * @brief: If slave received a new frame, which id is not LINSlaveID2,
         * slave copy the data in LINRxBuffer to LINTxBuffer and toggle the user
         * LED4.
         * Tips: Old frames will not be copied.
         */
        if ((LIN_END == LIN_Sta_g) && (0u == BufCopy_Flag_g))
        {
            LIN_Sta_g      = LIN_INITIAL;
            BufCopy_Flag_g = 1u;
            memcpy((void *)LINTxBuffer, (void *)&LINRxBuffer[1], LIN_MSG_LEN);
            GPIO_Toggle_Output_Data_Config(LED2_PORT, LED2_PIN);
        }
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t LINe)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
