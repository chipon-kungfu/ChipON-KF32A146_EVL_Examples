/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : lin_slave.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use LIN in Slave mode.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "lin_slave.h"

/*******************************************************************************
**                          Private Functions
*******************************************************************************/
uint8_t Auto_BaudRate_EnableFlag_g = 0u;
uint32_t Auto_BaudRate_Copy_g = 0u;

static uint8_t GetCheckSumValue(uint8_t PctID, uint8_t *Databuf, uint32_t length);

static uint8_t GetParityValue(uint32_t u32id);

/**
 *  @brief: Use LIN2.0 standard check, calculate the test value of LIN data
 *  @param[in]  PctID : Data of protection segment ID
 *               Databuf: Pointer of the data to be sent
 *               Length: The length of the data to be sent
 *  @param[out] None
 *  @retval: Calculate data, the LIN2.0 standard check segment
 */
static uint8_t GetCheckSumValue(uint8_t PctID, uint8_t *Databuf, uint32_t length)
{
    uint16_t check_sum = PctID;
    for (uint8_t i = 0u; i < length; i++)
    {
        check_sum += Databuf[i];
        if (check_sum > 0xFFu)
        {
            check_sum -= 255u;
        }
    }
    return (255u - check_sum);
}

/**
 *  @brief :Use LIN2.0 standard check, calculate protection section data
 *  @param[in]  u32id Input ID data to be processed
 *  @param[out] None
 *  @retval :None
 */
static uint8_t GetParityValue(uint32_t u32id)
{
    uint32_t u32Res = 0u, ID[6], p_Bit[2], mask = 0u;

    /* Store ID in ID[0:5] */
    for (mask = 0u; mask < 6u; mask++)
        ID[mask] = (u32id & (1u << mask)) >> mask;
    /* ID6 (p_Bit[0]) is the odd parity of ID0, ID1, ID2, and ID4
        ID7(p_Bit[1]) is the even parity of ID1, ID3, ID4, ID5 */
    p_Bit[0] = (ID[0] + ID[1] + ID[2] + ID[4]) % 2u;
    p_Bit[1] = (!((ID[1] + ID[3] + ID[4] + ID[5]) % 2u));
    /* Get the ID domain value u32Res
    (the first 6 bits are ID, and the upper two bits are parity bits) */
    u32Res = u32id + (p_Bit[0] << 6u) + (p_Bit[1] << 7u);
    /* Return ID filed value */
    return u32Res;
}

/*******************************************************************************
**                                Global Functions
*******************************************************************************/
/**
 *  @brief : Configure the LIN send data.
 *              - Automatic calculation of protection section ID
 *              - Automatically calculate the data of the check section
 *              - 1.Send the User data
 *              - 2.Send the checksum data
 *  @param[in]  USARTx: The pointer to USART memory structure, select
 *                  USART0_SFR~USART8_SFR
 *             Databuf:    The pointer of receive buffer
 *             Length: Data Length
 *  @param[out] None
 *  @retval: None
 */
void LIN_Slave_Send(USART_SFRmap *USARTx, uint8_t *Databuf, uint32_t Length)
{
    /* Calculate the protection data */
    uint8_t ProtectID = GetParityValue(LINSlaveID2);
    /* Calculate the checksum data */
    uint8_t CheckVaule = GetCheckSumValue(ProtectID, Databuf, Length);

    USART_RDR_INT_Enable(USARTx, FALSE);
    /* Send user data */
    for (uint8_t i = 0u; i < Length; i++)
    {
        USART_SendData(USARTx, Databuf[i]);
    }
    /* Send checksum data */
    USART_SendData(USARTx, CheckVaule);
    USART_RDR_INT_Enable(USARTx, TRUE);
}

/**
 *  @brief :LIN initial configuration
 *             - HFCLK is used by default
 *             - 19200 baud rate
 *             - enable receive interrupt default
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void USART_LIN_config(USART_SFRmap *USARTx)
{
    USART_InitTypeDef USART_InitStructure;

    /* Reset and enable USARTx */
    USART_Reset(USARTx);
    /* configure USARTx to LIN mode */
    USART_Struct_Init(&USART_InitStructure);
    USART_InitStructure.m_Mode          = USART_MODE_FULLDUPLEXASY;
    USART_InitStructure.m_TransferDir   = USART_DIRECTION_FULL_DUPLEX;
    USART_InitStructure.m_WordLength    = USART_WORDLENGTH_8B;
    USART_InitStructure.m_StopBits      = USART_STOPBITS_1;
    USART_InitStructure.m_BaudRateBRCKS = USART_CLK_HFCLK;
    USART_InitStructure.m_BRAutoDetect  = USART_ABRDEN_OFF;
    /** Use 16M clock as an example to list the following baud rates
     *     4800    z:208    x:0    y:0
     *     9600    z:104    x:0    y:0
     *     19200   z:52     x:0    y:0
     *     115200  z:8      x:1    y:13
     */
    /* Integer part z, get value range is 0 ~ 0xFFFF */
    USART_InitStructure.m_BaudRateInteger = 104u;
    /* Numerator part x, get value range is 0 ~ 0x0f */
    USART_InitStructure.m_BaudRateNumerator = 0u;
    /* Denominator part y, get value range is 0 ~ 0x0f */
    USART_InitStructure.m_BaudRateDenominator = 1u;
    USART_Configuration(USARTx, &USART_InitStructure);
    /* Enable receive interrupt */
    USART_RDR_INT_Enable(USARTx, TRUE);
    /* Enable blank interrupt */
    USART_Blank_INT_Enable(USARTx, TRUE);
    /* Enable idle interrupt */
    USART_Receive_Idle_Frame_Config(USARTx, TRUE);
    USART_IDLE_INT_Enable(USARTx, TRUE);
    /* Enable frame error interrupt */
    USART_Frame_ERROE_INT_Enable(USARTx, TRUE);
    /* Enable auto baud rate time over interrupt */
    USART_Auto_BaudRate_TimeOver_INT_Enable(USARTx, TRUE);
    /* Enable LIN moudle */
    USART_RESHD_Enable(USARTx, TRUE);
    USART_Cmd(USARTx, TRUE);

    /* Enable USART wake up */
    Auto_BaudRate_EnableFlag_g = 1u;
    USART_WeakUP_Enable(USARTx, TRUE);
}

/**
 * @brief Check baud rate periodically. If baud rate is too large or too small,
 *        set baud rate to default value.
 * 
 * @param USARTx Pointer to USART
 */
void USART_CheckBaudRate(USART_SFRmap *USARTx)
{
    uint16_t baudRate_Integer = 0u;

    baudRate_Integer = (((USARTx->BRGR) & USART_BRGR_BRGM) >> USART_BRGR_BRGM0_POS);
    if((baudRate_Integer > (LIN_BAUD_RATE_INTEGER_DEFAULT + LIN_BAUD_RATE_INTEGER_HYSTERESIS)) || (baudRate_Integer < (LIN_BAUD_RATE_INTEGER_DEFAULT - LIN_BAUD_RATE_INTEGER_HYSTERESIS)))
    {
        USART_BaudRate_Integer_Config(USARTx, LIN_BAUD_RATE_INTEGER_DEFAULT);
    }
}
