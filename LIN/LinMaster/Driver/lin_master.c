/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : lin_master.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use LIN in Msater mode.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "lin_master.h"

/*******************************************************************************
**                          Private Functions
*******************************************************************************/
static uint8_t  GetCheckSumValue(uint8_t PctID, uint8_t *Databuf, uint32_t length);
static uint32_t USRAT_Set_baudRate(USART_SFRmap *USARTx, uint8_t freq, uint32_t baudRate);

static uint8_t GetParityValue(uint32_t u32id);

/**
 *  @brief :Use LIN2.0 standard check, calculate the test value of LIN data
 *  @param[in]  PctID     Data of protection segment ID
 *               Databuf    Pointer of the data to be sent
 *               Length    The length of the data to be sent
 *  @param[out] None
 *  @retval :calculate data, the LIN2.0 standard check segment
 */
static uint8_t GetCheckSumValue(uint8_t PctID, uint8_t *Databuf, uint32_t length)
{
    uint16_t check_sum = PctID;
    for (uint8_t i = 0u; i < length; i++)
    {
        check_sum += Databuf[i];
        if (check_sum > 0xFFu)
        {
            check_sum -= 255u;
        }
    }
    return (255u - check_sum);
}

/**
 *  @brief :Use LIN2.0 standard check, calculate protection section data
 *  @param[in]  u32id Input ID data to be processed
 *  @param[out] None
 *  @retval :None
 */
static uint8_t GetParityValue(uint32_t u32id)
{
    uint32_t u32Res = 0u, ID[6], p_Bit[2], mask = 0u;

    /* Store ID in ID[0:5] */
    for (mask = 0u; mask < 6u; mask++)
    {
        ID[mask] = (u32id & (1 << mask)) >> mask;
    }
    /* ID6 (p_Bit[0]) is the odd parity of ID0, ID1, ID2, and ID4
        ID7(p_Bit[1]) is the even parity of ID1, ID3, ID4, ID5 */
    p_Bit[0] = (ID[0] + ID[1] + ID[2] + ID[4]) % 2u;
    p_Bit[1] = (!((ID[1] + ID[3] + ID[4] + ID[5]) % 2u));
    /* Get the ID domain value u32Res
    (the first 6 bits are ID, and the upper two bits are parity bits) */
    u32Res = u32id + (p_Bit[0] << 6u) + (p_Bit[1] << 7u);
    /* Return ID filed value */
    return u32Res;
}

/**
 * @brief: calculate baudRate of USARTx.
 *
 *  @param[in]  USARTx: Pointer to USART register structure.
 *  @param[in]  freq: Frequency of USART work clock source.
 *  @param[in]  baudRate: Baud rate of USART.
 *  @param[out] None
 * @retval uint32_t: 0 -- success
 *                   other -- failure
 * @retval: None
 */
static uint32_t USRAT_Set_baudRate(USART_SFRmap *USARTx, uint8_t freq, uint32_t baudRate)
{
    uint32_t baudRate_Integer;
    uint32_t baudRate_Numerator = 1u;
    uint32_t baudRate_Denominator;
    uint32_t coefficient;
    uint32_t freq_Tmp     = freq * 10000u;
    uint32_t baudRate_Tmp = baudRate / 100u;

    if ((baudRate < 1200u) || (baudRate > 921600u))
    {
        return 1u;
    }
    else
    {
        /* Empty */
    }

    baudRate_Integer = (uint16_t)(freq_Tmp / (16u * baudRate_Tmp));
    coefficient      = (freq_Tmp * 1000u) / (16u * baudRate_Integer * baudRate_Tmp);

    if (coefficient <= 1000u)
    {
        /* When the bit is 0, the decimal baud rate generator does not affect
         * the baud rate */
        baudRate_Numerator   = 0u;
        baudRate_Denominator = 0u;
    }
    else
    {
        baudRate_Denominator = (baudRate_Numerator * 1000u) / (coefficient - 1000u);
        /* Numerator and denominator register, maximum 4bits, otherwise set to 0
         */
        if (baudRate_Denominator > 15u)
        {
            /* When the bit is 0, the decimal baud rate generator does not
             * affect the baud rate */
            baudRate_Numerator   = 0u;
            baudRate_Denominator = 0u;
        }
        else
        {
            /* Empty */
        }
    }

    /* Configure USART baduRate */
    USART_BaudRate_Integer_Config(USARTx, baudRate_Integer);
    USART_BaudRate_Decimal1_Config(USARTx, baudRate_Numerator);
    USART_BaudRate_Decimal2_Config(USARTx, baudRate_Denominator);

    return 0U;
}

/*******************************************************************************
**                                Global Functions
*******************************************************************************/

/**
 *  @brief: LIN initial configuration
 *             - HFCLK is used by default
 *             - 19200 baud rate
 *             - enable receive interrupt default
 *  @param[in]  USARTx: The pointer to USART register
 *  @param[out] None
 *  @retval: None
 */
void USART_LIN_config(USART_SFRmap *USARTx, uint32_t baudRate)
{
    USART_InitTypeDef USART_InitStructure;

    /* Reset and enable USARTx */
    USART_Reset(USARTx);
    /* configure USARTx to LIN mode */
    USART_Struct_Init(&USART_InitStructure);
    USART_InitStructure.m_Mode          = USART_MODE_FULLDUPLEXASY;
    USART_InitStructure.m_TransferDir   = USART_DIRECTION_FULL_DUPLEX;
    USART_InitStructure.m_WordLength    = USART_WORDLENGTH_8B;
    USART_InitStructure.m_StopBits      = USART_STOPBITS_1;
    USART_InitStructure.m_BaudRateBRCKS = USART_CLK_SCLK;
    USART_InitStructure.m_BRAutoDetect  = USART_ABRDEN_OFF;
    /* Use 16M clock as an example to list the following baud rates */
    USART_Configuration(USARTx, &USART_InitStructure);
    if (0u != USRAT_Set_baudRate(USARTx, 72u, baudRate))
    {
        return;
    }
    /* Enable LIN moudle */
    USART_RESHD_Enable(USARTx, TRUE);
    /*Set length of blank of LIN to 16 bits*/
    USART_Send_Blank_Length_Config(USARTx, USART_BLENGTH_16BIT);
    USART_Cmd(USARTx, TRUE);
}

/**
 *  @brief: LIN master send message header
 *  @param[in]  USARTx: The pointer to USART register
 *             SlaveID: Slave ID(NOT PROTECTED ID -- PID)
 *  @param[out] None
 *  @retval: None
 */
void LIN_Send_Head(USART_SFRmap *USARTx, uint8_t SlaveID)
{
    /* Calculate the protection ID */
    uint8_t ProtectID = GetParityValue(SlaveID);

    /* Wait for the sender to be empty */
    while (SET != USART_Get_Transmitter_Empty_Flag(USARTx))
        ;
    /* Send Break Filed and Sync Filed */
    USART_Transmit_Data_Enable(USARTx, FALSE);
    USART_Send_Blank_Enable(USARTx, TRUE);
    USART_Transmit_Data_Enable(USARTx, TRUE);
    USARTx->TBUFR = 0x55u;
    while (SET != USART_Get_Transmit_BUFR_Empty_Flag(USARTx))
        ;

    /* Send Protected ID */
    USART_SendData(USARTx, ProtectID);
    while (SET != USART_Get_Transmit_BUFR_Empty_Flag(USARTx))
        ;

    /* Receve the PID and Break Filed, and discard them */
    while (SET != USART_Get_Receive_BUFR_Ready_Flag(USARTx))
        ;
    USART_ReceiveData(USARTx);
    while (SET != USART_Get_Receive_BUFR_Ready_Flag(USARTx))
        ;
    USART_ReceiveData(USARTx);
}

/**
 *  @brief : Configure the LIN send data.
 *              - Automatic calculation of protection section ID
 *              - Automatically calculate the data of the check section
 *              - 1.Send the 'SYNCH BREAK' data
 *              - 2.Send the 'ProtectID' data
 *              - 3.Send the User data
 *              - 4.Send the checksum data
 *  @param[in]  USARTx    Pointer to USART memory structure,select
 *USART0_SFR~USART8_SFR SlaveID     receiving the data ID Databuf    Pointer of
 *the data to be sent Length    The length of the data to be sent
 *  @param[out] None
 *  @retval :None
 */
void LIN_Send(USART_SFRmap *USARTx, uint8_t SlaveID, uint8_t *Databuf, uint32_t Length)
{
    /* Calculate the protection data */
    uint8_t ProtectID = GetParityValue(SlaveID);
    /* Calculate the checksum data */
    uint8_t CheckVaule = GetCheckSumValue(ProtectID, Databuf, Length);

    /* Wait for the sender to be empty */
    while (SET != USART_Get_Transmitter_Empty_Flag(USARTx))
        ;
    /* Send interval filed and sync filed */
    USART_Transmit_Data_Enable(USARTx, FALSE);
    USART_Send_Blank_Enable(USARTx, TRUE);
    USART_Transmit_Data_Enable(USARTx, TRUE);
    USARTx->TBUFR = 0x55u;
    while (SET != USART_Get_Transmit_BUFR_Empty_Flag(USARTx))
        ;
    /* Send Protected ID */
    USART_SendData(USARTx, ProtectID);
    /* Send user data */
    for (uint8_t i = 0u; i < Length; i++)
    {
        USART_SendData(USARTx, Databuf[i]);
    }
    /* Send checksum data */
    USART_SendData(USARTx, CheckVaule);
}

/**
 *  @brief: LIN master receive message from slave
 *  @param[in]  USARTx: The pointer to USART register
 *             Recv_Buf: The pointer to receive buffer
 *             Msg_Len: Message length
 *  @param[out] None
 *  @retval: None
 */
void LIN_Master_Recv(USART_SFRmap *USARTx, uint8_t *Recv_Buf, uint8_t Msg_Len)
{
    volatile uint32_t WaitTime = 0xfffu;

    while ((SET != USART_Get_Receive_BUFR_Ready_Flag(USARTx)) && (WaitTime--))
        ;
    if (0u == WaitTime)
    {
        return;
    }
    while (Msg_Len--)
    {
        WaitTime = 0xfffu;
        while ((SET != USART_Get_Receive_BUFR_Ready_Flag(USARTx)) && (WaitTime--))
            ;
        *Recv_Buf = USART_ReceiveData(USARTx);
        Recv_Buf++;
    }
}
