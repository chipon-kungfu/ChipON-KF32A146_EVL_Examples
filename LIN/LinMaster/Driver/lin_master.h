/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : lin_master.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use LIN in Msater mode.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef _LIN_MASTER_H_
#define _LIN_MASTER_H_

/*******************************************************************************
**                      	Global Macro Definitions                         **
*******************************************************************************/
/* LIN status */
enum LIN_Status
{
    LIN_INITIAL = 0,
    LIN_BREAK,
    LIN_SYNC,
    LIN_PID,
    LIN_DATA,
    LIN_CHECKSUM,
    LIN_END
};
/* Number of LIN frame bytes */
#define LIN_MSG_LEN (8U)

/* LIN slave ID address */
#define LINSlaveID1 (0x34u)
#define LINSlaveID2 (0x36u)

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/
extern void USART_LIN_config(USART_SFRmap *USARTx, uint32_t baudRate);

extern void LIN_Send(USART_SFRmap *USARTx, uint8_t SlaveID, uint8_t *Databuf, uint32_t Length);

extern void LIN_Send_Head(USART_SFRmap *USARTx, uint8_t SlaveID);

extern void LIN_Master_Recv(USART_SFRmap *USARTx, uint8_t *Recv_Buf, uint8_t Msg_Len);

#endif /* _LIN_MASTER_H_ */
