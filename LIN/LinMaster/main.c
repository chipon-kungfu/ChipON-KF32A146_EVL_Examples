/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use LIN in Msater mode.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"

#include "lin_master.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/
/* Led control toggle output */
#define BoardLed1Toggle() (GPIO_Toggle_Output_Data_Config(PD12_LED1_PIN))

/* LIN transfer status */
#define LIN_TRANSFER_ON()
#define LIN_TRANSFER_OFF()

/*******************************************************************************
**                          Global Variables Definitions
*******************************************************************************/
/* Transmit date buffer */
uint8_t LINRxBuffer[LIN_MSG_LEN + 1] = {0};

/*******************************************************************************
**                                Global Functions
*******************************************************************************/
/**
 *  @brief :Initialize the LED GPIO ports
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void BoardGpioInit(void)
{
    /* Configure LED1 output */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);
}

/**
 *  @brief :Initialize the LIN GPIO ports
 *             PH6    -- LIN_TX_PIN
 *             PH14 -- LIN_RX_PIN
 *  @param[in]  None
 *  @param[out] None
 *  @retval :None
 */
void LINGpioIinit(void)
{
    GPIO_Write_Mode_Bits(PG9_LIN1_RX_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PC2_LIN1_TX_PIN, GPIO_MODE_RMP);

    /* Configure PG9 remap function */
    GPIO_Pin_RMP_Config(PG9_LIN1_RX_AF);
    /* Configure PC2 remap function */
    GPIO_Pin_RMP_Config(PC2_LIN1_TX_AF);
}

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main()
{
    /* Initialize the system clock is 72MHz */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function, and input frequency is 72MHz */
    systick_delay_init(72U);

    /* Initialize the LED IOs */
    BoardGpioInit();
    /* Initialize the LIN IOs */
    LINGpioIinit();

    /* LIN2.0 configured as follow:
        - BaudRate = 19200 baud
        - Word Length = 8 Bits
        - parity bits included
        - Receive and transmit enabled
     */
    USART_LIN_config(USART1_SFR, 19200u);
    /* Enable LIN transfer */
    LIN_TRANSFER_ON();

    while (1)
    {

        /**
         * @brief: Read data from salve.
         *  1. Send the header of frame;
         *  2. Waiting for slave to send data.
         * Tips:
         *  1. The CheckSum is saved in the last bytes of LINRxBuffer;
         *  2. Msg_LEN needs to be 1 greater than LIN_MSG_LEN.
         */
        LIN_Send_Head(USART1_SFR, LINSlaveID2);
        LIN_Master_Recv(USART1_SFR, LINRxBuffer, LIN_MSG_LEN + 1u);
        systick_delay_ms(50u);

        /* Sending the previously received data  */
        LIN_Send(USART1_SFR, LINSlaveID1, &LINRxBuffer[0], LIN_MSG_LEN);
        systick_delay_ms(10u);
        /* A break time is required between each frame of data */
        LIN_Send(USART1_SFR, LINSlaveID1, &LINRxBuffer[LIN_MSG_LEN], 1u);
        systick_delay_ms(50u);

        /* ON/OFF the LED1 to indicates that the system is running */
        GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t LINe)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
