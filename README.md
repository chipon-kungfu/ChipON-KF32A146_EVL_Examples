﻿## ChipON-KF32A146_EVL_Examples

#### 介绍
基于KF32A146型号的MCU标准固件库例程。

#### 使用方法
* KF32A146_StdPeriph_Template文件夹是基于标准外设库的基础模板工程，可通过IDE导入
* 将例程代码文件复制到模板工程后可直接编译使用
* 每一个例程代码都有对应的 README.MD 文件，文件中将说明对应例程的使用方法

#### 文件目录
模板工程中的标准外设库源代码目录结构如下所示：
| 名称  | 描述  |
|---|---|
| _config | KF32 MCU启动文件及中断向量表  |
| inc | KF32 MCU相关系列标准固件库函数头文件 |
| src |  KF32 MCU相关系列标准固件库函数源文件 |
| system_init.c | 系统时钟配置源文件  |
| system_init.h | 系统时钟配置头文件  |
| ChangeLog | 代码发布版本记录  |
| LICENSE | 开源协议  |

#### ChipON IDE For KungFu32安装教程

1.  为KungFu32系列32位MCU设计的集成开发环境，内置C编译器，支持C语言、汇编联合开发，支持DEBUG调试
2.  [下载地址：https://www.chipon-ic.com/Product/kaifa/77091ccf-ee3e-4d68-aebb-677635ab9558?type=d2f35882-7c3c-482e-b147-02bb77e66acb#bawei]

#### 公司说明
上海芯旺微电子是一家专注基于自主IP KungFu内核架构研发高可靠、高品质8位MCU、32位MCU&DSP的高新技术企业。迄今为止已成功向专业芯片应用市场输送KF8F、KF8L、KF8A、KF8TS、KF8S 、KF32A、KF32F、KF32L、KF32LS等产品，及ChipON IDE集成开发环境、ChipON PRO编程软件、KungFu Minipro 仿真编程器，真正实现从芯片内核设计到工具开发整个生态链的全自有IP。
[公司官网：https://www.chipon-ic.com/]