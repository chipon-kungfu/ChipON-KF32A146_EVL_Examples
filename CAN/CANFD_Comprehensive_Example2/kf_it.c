/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/


#include "system_init.h"
#include "Canfd.h"
#include "LoopFifo.h"


extern LPFifo_TypeDef LPFifo;
extern volatile uint16_t time_10ms;
extern volatile uint16_t time_20ms;
extern volatile uint16_t time_50ms;
extern volatile uint16_t time_100ms;
extern volatile uint16_t time_1000ms;
extern volatile uint16_t time_5000ms;
volatile uint8_t ArbLose_Flag = 0;
volatile uint32_t Writewrong_Flag = 0;
volatile uint32_t delay_num = CLEAR_FLAG_TIME;
volatile uint8_t TASK_10ms = 0;
Canfd_MailboxHeaderType * CANfd_MessageT;
Canfd_MailboxHeaderType  CANfd_MessageT_t;
extern Canfd_MailboxHeaderType Can_MailboxHeader_copy[30];

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void);
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _HardFault_exception (void);
void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void);
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               AriFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _AriFault_exception (void);
void __attribute__((interrupt)) _AriFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void);
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void);
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void);
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}
//*****************************************************************************************
//                              ECC Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _ECC_exception (void);
void __attribute__((interrupt)) _ECC_exception (void)
{
	
}

//*****************************************************************************************
//                              T18 Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt))_T18_exception (void);
void __attribute__((interrupt))_T18_exception (void)
{
	GPTIM_Clear_Updata_INT_Flag(T18_SFR);
	GPTIM_Clear_Overflow_INT_Flag(T18_SFR);
	volatile Kf32a_Canfd_Reg* ControllerRegPtr = (Kf32a_Canfd_Reg*)Can_m_ControllersInfo[CANfd6].BaseAddress;
	if(False == Fifo_IsEmpty(&LPFifo))
	{
		if (((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0x01) && (ControllerRegPtr->CANFD_CTLR0.B.TXSTA == 0x00)) || (ArbLose_Flag == 1))
		{
			CANfd_MessageT = Fifo_Read(&LPFifo);
			(*CANfd_MessageT).TransceiveType = MAIL_TRANSMIT;
			if(CAN_OK == Can_m_FdMailBox_Write(CANfd6,Mailbox_0,CANfd_MessageT))
			{
				Can_m_FdTransmit(CANfd6);
				delay_num = 0xFF;
				while((ControllerRegPtr->CANFD_CTLR0.B.TXSTA == 0x01)&& (delay_num--));
				if(Writewrong_Flag > 0)
				{
				Writewrong_Flag--;
				}
				else
				{
					Writewrong_Flag = 0;
				}
			}
			else
			{
				Fifo_Write(&LPFifo,CANfd_MessageT);
				Can_m_FdTransmit(CANfd6);
			}
			if(ArbLose_Flag == 1)
			{
				ArbLose_Flag = 0;
			}
		}
		else
		{
			Can_m_FdTransmit(CANfd6);
			Writewrong_Flag++;
			if(Writewrong_Flag == 128)
			{
				Can_m_FdControllerDeInit(CANfd6);
				Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig,Initindex_0);
				Receive_Mailboxinit(CANfd6);
			}
		}
	}
}

//*****************************************************************************************
//                              T14 Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt))_T14_exception (void);
void __attribute__((interrupt))_T14_exception (void)
{
	BTIM_Clear_Updata_INT_Flag(T14_SFR);
	BTIM_Clear_Overflow_INT_Flag (T14_SFR);
	TASK_10ms = 1;
	if(time_10ms < 2)
	{
		time_10ms++;
	}
	else
	{
		time_10ms = 2;
		for(uint8_t i = 0; i <= 3; i++)
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&Can_MailboxHeader_copy[i]);
			}
		}
	}
	if(time_20ms < 4)
	{
		time_20ms++;
	}
	else
	{
		time_20ms = 3;
		for(uint8_t j = 4; j <= 6; j++)
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				CANfd_MessageT_t.Id = Can_MailboxHeader_copy[j].Id;
				CANfd_MessageT_t.FrameData.U8Data[0] =Can_MailboxHeader_copy[j].FrameData.U8Data[0];
				Fifo_Write(&LPFifo,&Can_MailboxHeader_copy[j]);
			}
		}
	}
	if(time_50ms < 9)
	{
		time_50ms++;
	}
	else
	{
		time_50ms = 5;
		for(uint8_t k = 7; k <= 14; k++)
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&Can_MailboxHeader_copy[k]);
			}
		}
	}

	if(time_100ms < 16)
	{
		time_100ms++;
	}
	else
	{
		time_100ms = 7;
		for(uint8_t l = 15; l<= 19 ; l++)
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&Can_MailboxHeader_copy[l]);
			}
		}
	}

	if(time_1000ms < 110)
	{
		time_1000ms++;
	}
	else
	{
		time_1000ms = 11;
		for(uint8_t m = 20;m <= 21 ; m++)
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&Can_MailboxHeader_copy[m]);
			}
		}
	}

	if(time_5000ms < 512)
	{
		time_5000ms++;
	}
	else
	{
		time_5000ms = 13;
		for(uint8_t n = 22; n <= 23 ; n++)
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&Can_MailboxHeader_copy[n]);
			}
		}
	}
}

//*****************************************************************************************
//                              CANFD6 Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _CANFD6_exception (void);
void __attribute__((interrupt)) _CANFD6_exception (void)
{
	uint8_t Rmc_count = 0;
	Can_Controller_InterruptFlagType Can_Controller_InterruptFlag;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[CANfd6].BaseAddress;
	Can_m_FdGetIntFlag(CANfd6, &Can_Controller_InterruptFlag, &Rmc_count);
	Can_m_FdClearIntFlag(CANfd6, &Can_Controller_InterruptFlag,Rmc_count);
	if (Can_Controller_InterruptFlag.ArbitrateLoseFlag == 0x01)
	{
		ArbLose_Flag = 1;
		//Can_m_FdTransmit(CANfd6);
	}
	if (Can_Controller_InterruptFlag.BusOffFlag == 0x01)
	{
		Can_m_FdControllerDeInit(CANfd6);
		Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig,Initindex_0);
		Receive_Mailboxinit(CANfd6);
	}
}
