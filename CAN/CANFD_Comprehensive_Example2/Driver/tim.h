/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : tim.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : 
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef _TIM_H
#define _TIM_H
/******************************************************************************
**                       	Include Files                                     *
******************************************************************************/
#include <stdint.h>

/******************************************************************************
*                      Functional definition                                 *
******************************************************************************/

void BASIC_TIMER14_Config(uint16_t CNT_T,uint16_t Prescaler);
void GENERAL_TIMER18_Config(uint16_t CNT_T,uint16_t Prescaler);

#endif
