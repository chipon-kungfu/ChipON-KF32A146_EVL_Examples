/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Canfd.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of CANFD
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
#include "Canfd.h"
/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/
extern uint32_t Receive_aCount;
extern uint32_t Receive_bCount;
extern uint32_t Receive_cCount;
extern uint32_t Receive_dCount;
extern uint8_t Receive_a;
extern uint8_t Receive_b;
extern uint8_t Receive_c;
extern uint8_t Receive_d;
extern Canfd_MailboxHeaderType Canfd_MailboxHeader[100];

Canfd_MailboxHeaderType Can_MailboxHeader_copy[30];

void Receive_Mailboxinit(const uint8_t Can_Controller_Index)
{
	volatile uint8_t retryTimes;
	Can_ReturnType ret;

	for(uint8_t receviemailbox = 1;receviemailbox<= 30;receviemailbox++)
	{
		Canfd_Sdu.Can_MailboxHeader[0].Can_id = CAN_DATA_STANDARD;
		Canfd_Sdu.Can_MailboxHeader[0].TransceiveType = MAIL_RECEIVE;
		retryTimes = 0u;
		do
		{
			ret = Can_m_FdMailBox_Write(Can_Controller_Index, receviemailbox,&Canfd_Sdu.Can_MailboxHeader[0]);
		} while ((CAN_OK != ret) && ((retryTimes++) < CAN_RETRY_TIMES));
	}
}

/**
 *  @brief :
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void Canfd_Gpio_Init(void)
{
	//PB3=CANFD6TX,PB4=CANFD6RX
	GPIO_Pull_Up_Enable(GPIOB_SFR, GPIO_PIN_MASK_3 | GPIO_PIN_MASK_4, TRUE);
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_3 | GPIO_PIN_MASK_4, GPIO_MODE_RMP);

	GPIO_Pin_RMP_Config(GPIOB_SFR, GPIO_Pin_Num_3, GPIO_RMP_AF6);
	GPIO_Pin_RMP_Config(GPIOB_SFR, GPIO_Pin_Num_4, GPIO_RMP_AF6);
}

#if (HARDWARE_FILTER == STD_ON)
const Can_HwFilterType Can_HwFilter = {
		/* Filter 0 */
		0x00000000U, /* Mask code */

		/* Filter 1 */

		0x00000000U, /* Mask code */

		/* Filter 2 */

		0x00000000U, /* Mask code */

		/* Filter 3 */

		0x00000000U, /* Mask code */

		/* Filter 4 */

		0x00000000U, /* Mask code */

		/* Filter 5 */

		0x00000000U, /* Mask code */

		/* Filter 6 */

		0x00000000U, /* Mask code */

		/* Filter 7 */

		0x00000000U, /* Mask code */

		/* Filter 8 */

		0x00000000U, /* Mask code */

		/* Filter 9 */

		0x00000000U, /* Mask code */

		/* Filter 10 */

		0x00000000U, /* Mask code */

		/* Filter 11 */

		0x00000000U, /* Mask code */

		/* Filter 12 */

		0x00000000U, /* Mask code */

		/* Filter 13 */

		0x00000000U, /* Mask code */

		/* Filter 14 */

		0x00000000U, /* Mask code */

		/* Filter 15 */

		0x00000000U, /* Mask code */

		/* Filter 16 */

		0x00000000U, /* Mask code */

		/* Filter 17 */

		0x00000000U, /* Mask code */

		/* Filter 18 */

		0x00000000U, /* Mask code */

		/* Filter 19 */

		0x00000000U, /* Mask code */

		/* Filter 20 */

		0x00000000U, /* Mask code */

		/* Filter 21 */

		0x00000000U, /* Mask code */

		/* Filter 22 */

		0x00000000U, /* Mask code */

		/* Filter 23 */

		0x00000000U, /* Mask code */

		/* Filter 24 */

		0x00000000U, /* Mask code */

		/* Filter 25 */

		0x00000000U, /* Mask code */

		/* Filter 26 */

		0x00000000U, /* Mask code */

		/* Filter 27 */

		0x00000000U, /* Mask code */

		/* Filter 28 */

		0x00000000U, /* Mask code */

		/* Filter 29 */

		0x00000000U, /* Mask code */

		/* Filter 30 */

		0x00000000U, /* Mask code */

		/* Filter 31 */

		0x00000000U, /* Mask code */

		/* Filter 32 */

		0x00000000U, /* Mask code */

		/* Filter 33 */

		0x00000000U, /* Mask code */

		/* Filter 34 */

		0x00000000U, /* Mask code */

		/* Filter 35 */

		0x00000000U, /* Mask code */

		/* Filter 36 */

		0x00000000U, /* Mask code */

		/* Filter 37 */

		0x00000000U, /* Mask code */

		/* Filter 38 */

		0x00000000U, /* Mask code */

		/* Filter 39 */

		0x00000000U, /* Mask code */

		/* Filter 40 */

		0x00000000U, /* Mask code */

		/* Filter 41 */

		0x00000000U, /* Mask code */

		/* Filter 42 */

		0x00000000U, /* Mask code */

		/* Filter 43 */

		0x00000000U, /* Mask code */

		/* Filter 44 */

		0x00000000U, /* Mask code */

		/* Filter 45 */

		0x00000000U, /* Mask code */

		/* Filter 46 */

		0x00000000U, /* Mask code */

		/* Filter 47 */

		0x00000000U, /* Mask code */

		/* Filter 48 */

		0x00000000U, /* Mask code */

		/* Filter 49 */

		0x00000000U, /* Mask code */

		/* Filter 50 */

		0x00000000U, /* Mask code */

		/* Filter 0 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 1 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 2 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 3 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 4 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 5 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 6 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 7 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 8 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 9 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 10 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 11 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 12 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 13 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 14 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 15 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 16 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 17 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 18 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 19 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 20 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 21 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 22 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 23 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 24 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 25 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 26 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 27 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 28 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 29 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 30 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 31 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 32 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 33 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 34 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 35 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 36 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 37 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 38 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 39 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 40 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 41 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 42 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 43 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 44 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 45 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 46 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 47 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 48 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 49 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 50 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */
	
	};
#endif

const Can_Controller_InterruptConfigType Can_Controller_InterruptConfig[1] = {
	/* Interrupt config : enable or disable interrupt */
	{
		/* Rx interrupt enable */
		CAN_INTERRUPT_DISABLE,

		/* Tx interrupt enable set */
		CAN_INTERRUPT_DISABLE,

		/* Busoff interrupt enable set */
		CAN_INTERRUPT_ENABLE,

		 /* Wakeup interrupt enable set */
		CAN_INTERRUPT_DISABLE,

		/* Error alarm interrupt set */
		CAN_INTERRUPT_DISABLE,

		/* Rx data Overflow interrupt set */
		CAN_INTERRUPT_DISABLE,

		/* Negative error interupt set */
		CAN_INTERRUPT_DISABLE,

		/* Arbitrate lose interupt set */
		CAN_INTERRUPT_ENABLE,

		/* Bus error interupt set */
		CAN_INTERRUPT_ENABLE,

	    /* CAN DMA Transmit Interrupt Set */
		CAN_INTERRUPT_DISABLE,

		/* CAN DMA Receive Interrupt Set */
		CAN_INTERRUPT_DISABLE,

		/* CAN MailBox Receive Triger Interrupt Set */
		CAN_INTERRUPT_DISABLE,

		/* Preemption Priority */
		0,

		/* Sub priority */
		0,
	},
};



const Can_BDRConfigType Canfd_Controller_AllClockAndBDRConfig[1] = {
	/* Default 500k hz  75% */
	/* Clock and bandrate config 0 : Index 0 */
	{
#if (WORKSOURCE_HFCLK == STD_ON)
		.PreScale = 1U,				/* Prescale */
		.Sjw = 1U,				/* Swj */
		.TSeg1 = 11U,			/* Tseg1 */
		.TSeg2 = 2U,				/* Tseg2 */
#else
		.PreScale = 11U,			/* Prescale */
		.Sjw = 1U,				/* Swj */
		.TSeg1 = 14U,			/* Tseg1 */
		.TSeg2 = 3U,				/* Tseg2 */
#endif
		CAN_SAMPLE_ONCE /* Sample time */
	}
};


const Can_FdBDRConfigType Canfd_Controller_ALLFdBDRConfig[1] = {
	/* Default 2M hz */
	/* High Speed Clock and bandrate config 0 : Index 0 */
	{
#if (WORKSOURCE_HFCLK == STD_ON)
		.BrsPrescale = 0U,	 		/* Prescale */
		.HtSeg1 = 5U,  		/* Tseg1 */
		.HtSeg2 = 0U,			/* Tseg2 */
#else
		.BrsPrescale = 2U,
		.HtSeg1 = 14U,
		.HtSeg2 = 3U,
#endif
	},
};



Can_ControllerConfigType Canfd_Controller_AllConfig[1] = {
	/* Can 0 Config */
	{
/* Can controller Mode set
	Value Range :
	CANFD_NORMAL_MODE
	CANFD_LOOP_INTERNAL_MODE
	CANFD_LOOP_EXTERNAL_MODE
	CANFD_SILENT_MODE */
		CANFD_NORMAL_MODE,
/* Can FD Mode set
	Value Range :
	DISABLE
	ENABLE */
		ENABLE,
/* Can controller clock source set
            Value Range :
                CAN_CLOCKSOURCE_SCLK
                CAN_CLOCKSOURCE_HFCLK
                CAN_CLOCKSOURCE_LFCLK */
#if (WORKSOURCE_HFCLK == STD_ON)
		CAN_CLOCKSOURCE_HFCLK,
#else
		CAN_CLOCKSOURCE_SCLK,
#endif
/* Can controller Arbitrate clock source set
	Value Range :
	CAN_CLOCKSOURCE_SCLK
	CAN_CLOCKSOURCE_HFCLK
	CAN_CLOCKSOURCE_LFCLK */
		CAN_CLOCKSOURCE_SCLK,
		/* Mailbox block size config
             Value Range :
                CAN_8_BYTE_DATALENGTH :
                CAN_16_BYTE_DATALENGTH :
                CAN_32_BYTE_DATALENGTH :
                CAN_64_BYTE_DATALENGTH : */
		CAN_8_BYTE_DATALENGTH,
		/* Iso mode or non-iso mode config
            Value Range :
                CAN_FD_NON_ISOMODE :
                CAN_FD_ISOMODE : */
		CAN_FD_NON_ISOMODE,
		/* Global Mask Set */
#if (HARDWARE_FILTER == STD_ON)
		0x00000000,
#else
		0xFFFFFFFF,
#endif
		/* Enable/disable mailbox full receive config
       Value Range :
            CAN_MBFULLRECEIVE_DISABLE
            CAN_MBFULLRECEIVE_ENABLE */
		CAN_MBFULLRECEIVE_DISABLE,
		/* Interrupt config  */
		&Can_Controller_InterruptConfig[0],
#if (HARDWARE_FILTER == STD_ON)
		/* Hardware Filter config  */
		&Can_HwFilter,
#endif
		/* Arbitrate Segment Baudrate Config */
		&Canfd_Controller_AllClockAndBDRConfig[0],
		/* Data Segment Baudrate Config */
		&Canfd_Controller_ALLFdBDRConfig[0],
	},
	/* end */
};


Canfd_Sdu_Type Canfd_Sdu =
{
	{
		{
			{
				{
				 0x00,
				 0x01,
				 0x02,
				 0x03,
				 0x04,
				 0x05,
				 0x06,
				 0x07,
				 0x08,
				 0x09,
				 0x0A,
				 0x0B,
				 0x0C,
				 0x0D,
				 0x0E,
				 0x0F,
				 0x10,
				 0x11,
				 0x12,
				 0x13,
				 0x14,
				 0x15,
				 0x16,
				 0x17,
				 0x18,
				 0x19,
				 0x1A,
				 0x1B,
				 0x1C,
				 0x1D,
				 0x1E,
				 0x1F,
				 0x20,
				 0x21,
				 0x22,
				 0x23,
				 0x24,
				 0x25,
				 0x26,
				 0x27,
				 0x28,
				 0x29,
				 0x2A,
				 0x2B,
				 0x2C,
				 0x2D,
				 0x2E,
				 0x2F,
				 0x30,
				 0x31,
				 0x32,
				 0x33,
				 0x34,
				 0x35,
				 0x36,
				 0x37,
				 0x38,
				 0x39,
				 0x3A,
				 0x3B,
				 0x3C,
				 0x3D,
				 0x3E,
				 0x3F,
					 },
			 Data_Length_8},
			0x100,
			0x000,
			CANFD_BRS_DISABLE,

			CAN_FRAME_CLASSICAL,
			CAN_DATA_STANDARD,

			MAIL_TRANSMIT,
		},
	},
	1,
};

void CAN_FD_Transmit_Schedular(void)
{
	Can_MailboxHeader_copy[0].Id = 0x201;
	Can_MailboxHeader_copy[0].Can_frame = CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[0].Can_id = CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[0].FrameData.Data_Length = Data_Length_8;
	Can_MailboxHeader_copy[0].FrameData.U8Data[0] = 0x0A;
	Can_MailboxHeader_copy[0].FrameData.U8Data[1] = Receive_a;
	Can_MailboxHeader_copy[0].FrameData.U8Data[2] = Receive_a;
	Can_MailboxHeader_copy[0].FrameData.U8Data[3] = Receive_a;
	Can_MailboxHeader_copy[0].FrameData.U8Data[4] = (Receive_aCount&0xFF000000)>>24;
	Can_MailboxHeader_copy[0].FrameData.U8Data[5] = (Receive_aCount&0x00FF0000)>>16;
	Can_MailboxHeader_copy[0].FrameData.U8Data[6] = (Receive_aCount&0x0000FF00)>>8;
	Can_MailboxHeader_copy[0].FrameData.U8Data[7] = Receive_aCount&0x000000FF;

	Can_MailboxHeader_copy[1].Id = 0x202;
	Can_MailboxHeader_copy[1].Can_frame = CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[1].Can_id = CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[1].TransceiveType = MAIL_TRANSMIT;
	Can_MailboxHeader_copy[1].FrameData.Data_Length = Data_Length_8;
	Can_MailboxHeader_copy[1].FrameData.U8Data[0] = 0x0A;
	Can_MailboxHeader_copy[1].FrameData.U8Data[1] = Receive_b;
	Can_MailboxHeader_copy[1].FrameData.U8Data[2] = Receive_b;
	Can_MailboxHeader_copy[1].FrameData.U8Data[3] = Receive_b;
	Can_MailboxHeader_copy[1].FrameData.U8Data[4] = (Receive_bCount&0xFF000000)>>24;
	Can_MailboxHeader_copy[1].FrameData.U8Data[5] = (Receive_bCount&0x00FF0000)>>16;
	Can_MailboxHeader_copy[1].FrameData.U8Data[6] = (Receive_bCount&0x0000FF00)>>8;
	Can_MailboxHeader_copy[1].FrameData.U8Data[7] = (Receive_bCount&0x000000FF);

	Can_MailboxHeader_copy[2].Id=0x203;
	Can_MailboxHeader_copy[2].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[2].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[2].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[2].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[2].FrameData.U8Data[0]=0x0A;
	Can_MailboxHeader_copy[2].FrameData.U8Data[1]=Receive_c;
	Can_MailboxHeader_copy[2].FrameData.U8Data[2]=Receive_c;
	Can_MailboxHeader_copy[2].FrameData.U8Data[3]=Receive_c;
	Can_MailboxHeader_copy[2].FrameData.U8Data[4]=(Receive_cCount&0xFF000000)>>24;
	Can_MailboxHeader_copy[2].FrameData.U8Data[5]=(Receive_cCount&0x00FF0000)>>16;
	Can_MailboxHeader_copy[2].FrameData.U8Data[6]=(Receive_cCount&0x0000FF00)>>8;
	Can_MailboxHeader_copy[2].FrameData.U8Data[7]=(Receive_cCount&0x000000FF);

	Can_MailboxHeader_copy[3].Id=0x204;
	Can_MailboxHeader_copy[3].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[3].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[3].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[3].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[3].FrameData.U8Data[0]=0x0A;
	Can_MailboxHeader_copy[3].FrameData.U8Data[1]=Receive_d;
	Can_MailboxHeader_copy[3].FrameData.U8Data[2]=Receive_d;
	Can_MailboxHeader_copy[3].FrameData.U8Data[3]=Receive_d;
	Can_MailboxHeader_copy[3].FrameData.U8Data[4]=(Receive_d&0xFF000000)>>24;
	Can_MailboxHeader_copy[3].FrameData.U8Data[5]=(Receive_d&0x00FF0000)>>16;
	Can_MailboxHeader_copy[3].FrameData.U8Data[6]=(Receive_d&0x0000FF00)>>8;
	Can_MailboxHeader_copy[3].FrameData.U8Data[7]=(Receive_d&0x000000FF);

	Can_MailboxHeader_copy[4].Id=0x301;
	Can_MailboxHeader_copy[4].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[4].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[4].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[4].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[4].FrameData.U8Data[0]=0x0B;
	Can_MailboxHeader_copy[4].FrameData.U8Data[1]=0x01;
	Can_MailboxHeader_copy[4].FrameData.U8Data[2]=0x01;
	Can_MailboxHeader_copy[4].FrameData.U8Data[3]=0x01;
	Can_MailboxHeader_copy[4].FrameData.U8Data[4]=0x01;
	Can_MailboxHeader_copy[4].FrameData.U8Data[5]=0x01;
	Can_MailboxHeader_copy[4].FrameData.U8Data[6]=0x01;
	Can_MailboxHeader_copy[4].FrameData.U8Data[7]=0x01;

	Can_MailboxHeader_copy[5].Id=0x302;
	Can_MailboxHeader_copy[5].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[5].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[5].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[5].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[5].FrameData.U8Data[0]=0x0B;
	Can_MailboxHeader_copy[5].FrameData.U8Data[1]=0x02;
	Can_MailboxHeader_copy[5].FrameData.U8Data[2]=0x02;
	Can_MailboxHeader_copy[5].FrameData.U8Data[3]=0x02;
	Can_MailboxHeader_copy[5].FrameData.U8Data[4]=0x02;
	Can_MailboxHeader_copy[5].FrameData.U8Data[5]=0x02;
	Can_MailboxHeader_copy[5].FrameData.U8Data[6]=0x02;
	Can_MailboxHeader_copy[5].FrameData.U8Data[7]=0x02;

	Can_MailboxHeader_copy[6].Id=0x303;
	Can_MailboxHeader_copy[6].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[6].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[6].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[6].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[6].FrameData.U8Data[0]=0x0B;
	Can_MailboxHeader_copy[6].FrameData.U8Data[1]=0x03;
	Can_MailboxHeader_copy[6].FrameData.U8Data[2]=0x03;
	Can_MailboxHeader_copy[6].FrameData.U8Data[3]=0x03;
	Can_MailboxHeader_copy[6].FrameData.U8Data[4]=0x03;
	Can_MailboxHeader_copy[6].FrameData.U8Data[5]=0x03;
	Can_MailboxHeader_copy[6].FrameData.U8Data[6]=0x03;
	Can_MailboxHeader_copy[6].FrameData.U8Data[7]=0x03;

	Can_MailboxHeader_copy[7].Id=0x401;
	Can_MailboxHeader_copy[7].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[7].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[7].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[7].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[7].FrameData.U8Data[0]=0x0C;
	Can_MailboxHeader_copy[7].FrameData.U8Data[1]=0x01;
	Can_MailboxHeader_copy[7].FrameData.U8Data[2]=0x01;
	Can_MailboxHeader_copy[7].FrameData.U8Data[3]=0x01;
	Can_MailboxHeader_copy[7].FrameData.U8Data[4]=0x01;
	Can_MailboxHeader_copy[7].FrameData.U8Data[5]=0x01;
	Can_MailboxHeader_copy[7].FrameData.U8Data[6]=0x01;
	Can_MailboxHeader_copy[7].FrameData.U8Data[7]=0x01;

	Can_MailboxHeader_copy[8].Id=0x402;
	Can_MailboxHeader_copy[8].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[8].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[8].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[8].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[8].FrameData.U8Data[0]=0x0C;
	Can_MailboxHeader_copy[8].FrameData.U8Data[1]=0x02;
	Can_MailboxHeader_copy[8].FrameData.U8Data[2]=0x02;
	Can_MailboxHeader_copy[8].FrameData.U8Data[3]=0x02;
	Can_MailboxHeader_copy[8].FrameData.U8Data[4]=0x02;
	Can_MailboxHeader_copy[8].FrameData.U8Data[5]=0x02;
	Can_MailboxHeader_copy[8].FrameData.U8Data[6]=0x02;
	Can_MailboxHeader_copy[8].FrameData.U8Data[7]=0x02;

	Can_MailboxHeader_copy[9].Id=0x403;
	Can_MailboxHeader_copy[9].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[9].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[9].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[9].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[9].FrameData.U8Data[0]=0x0C;
	Can_MailboxHeader_copy[9].FrameData.U8Data[1]=0x03;
	Can_MailboxHeader_copy[9].FrameData.U8Data[2]=0x03;
	Can_MailboxHeader_copy[9].FrameData.U8Data[3]=0x03;
	Can_MailboxHeader_copy[9].FrameData.U8Data[4]=0x03;
	Can_MailboxHeader_copy[9].FrameData.U8Data[5]=0x03;
	Can_MailboxHeader_copy[9].FrameData.U8Data[6]=0x03;
	Can_MailboxHeader_copy[9].FrameData.U8Data[7]=0x03;

	Can_MailboxHeader_copy[10].Id=0x404;
	Can_MailboxHeader_copy[10].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[10].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[10].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[10].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[10].FrameData.U8Data[0]=0x0C;
	Can_MailboxHeader_copy[10].FrameData.U8Data[1]=0x04;
	Can_MailboxHeader_copy[10].FrameData.U8Data[2]=0x04;
	Can_MailboxHeader_copy[10].FrameData.U8Data[3]=0x04;
	Can_MailboxHeader_copy[10].FrameData.U8Data[4]=0x04;
	Can_MailboxHeader_copy[10].FrameData.U8Data[5]=0x04;
	Can_MailboxHeader_copy[10].FrameData.U8Data[6]=0x04;
	Can_MailboxHeader_copy[10].FrameData.U8Data[7]=0x04;

	Can_MailboxHeader_copy[11].Id=0x405;
	Can_MailboxHeader_copy[11].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[11].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[11].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[11].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[11].FrameData.U8Data[0]=0x0C;
	Can_MailboxHeader_copy[11].FrameData.U8Data[1]=0x05;
	Can_MailboxHeader_copy[11].FrameData.U8Data[2]=0x05;
	Can_MailboxHeader_copy[11].FrameData.U8Data[3]=0x05;
	Can_MailboxHeader_copy[11].FrameData.U8Data[4]=0x05;
	Can_MailboxHeader_copy[11].FrameData.U8Data[5]=0x05;
	Can_MailboxHeader_copy[11].FrameData.U8Data[6]=0x05;
	Can_MailboxHeader_copy[11].FrameData.U8Data[7]=0x05;

	Can_MailboxHeader_copy[12].Id=0x406;
	Can_MailboxHeader_copy[12].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[12].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[12].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[12].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[12].FrameData.U8Data[0]=0x0C;
	Can_MailboxHeader_copy[12].FrameData.U8Data[1]=0x06;
	Can_MailboxHeader_copy[12].FrameData.U8Data[2]=0x06;
	Can_MailboxHeader_copy[12].FrameData.U8Data[3]=0x06;
	Can_MailboxHeader_copy[12].FrameData.U8Data[4]=0x06;
	Can_MailboxHeader_copy[12].FrameData.U8Data[5]=0x06;
	Can_MailboxHeader_copy[12].FrameData.U8Data[6]=0x06;
	Can_MailboxHeader_copy[12].FrameData.U8Data[7]=0x06;

	Can_MailboxHeader_copy[13].Id=0x407;
	Can_MailboxHeader_copy[13].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[13].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[13].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[13].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[13].FrameData.U8Data[0]=0x0C;
	Can_MailboxHeader_copy[13].FrameData.U8Data[1]=0x07;
	Can_MailboxHeader_copy[13].FrameData.U8Data[2]=0x07;
	Can_MailboxHeader_copy[13].FrameData.U8Data[3]=0x07;
	Can_MailboxHeader_copy[13].FrameData.U8Data[4]=0x07;
	Can_MailboxHeader_copy[13].FrameData.U8Data[5]=0x07;
	Can_MailboxHeader_copy[13].FrameData.U8Data[6]=0x07;
	Can_MailboxHeader_copy[13].FrameData.U8Data[7]=0x07;

	Can_MailboxHeader_copy[14].Id=0x408;
	Can_MailboxHeader_copy[14].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[14].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[14].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[14].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[14].FrameData.U8Data[0]=0x0C;
	Can_MailboxHeader_copy[14].FrameData.U8Data[1]=0x08;
	Can_MailboxHeader_copy[14].FrameData.U8Data[2]=0x08;
	Can_MailboxHeader_copy[14].FrameData.U8Data[3]=0x08;
	Can_MailboxHeader_copy[14].FrameData.U8Data[4]=0x08;
	Can_MailboxHeader_copy[14].FrameData.U8Data[5]=0x08;
	Can_MailboxHeader_copy[14].FrameData.U8Data[6]=0x08;
	Can_MailboxHeader_copy[14].FrameData.U8Data[7]=0x08;

	Can_MailboxHeader_copy[15].Id=0x501;
	Can_MailboxHeader_copy[15].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[15].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[15].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[15].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[15].FrameData.U8Data[0]=0x0D;
	Can_MailboxHeader_copy[15].FrameData.U8Data[1]=0x01;
	Can_MailboxHeader_copy[15].FrameData.U8Data[2]=0x01;
	Can_MailboxHeader_copy[15].FrameData.U8Data[3]=0x01;
	Can_MailboxHeader_copy[15].FrameData.U8Data[4]=0x01;
	Can_MailboxHeader_copy[15].FrameData.U8Data[5]=0x01;
	Can_MailboxHeader_copy[15].FrameData.U8Data[6]=0x01;
	Can_MailboxHeader_copy[15].FrameData.U8Data[7]=0x01;

	Can_MailboxHeader_copy[16].Id=0x502;
	Can_MailboxHeader_copy[16].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[16].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[16].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[16].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[16].FrameData.U8Data[0]=0x0D;
	Can_MailboxHeader_copy[16].FrameData.U8Data[1]=0x02;
	Can_MailboxHeader_copy[16].FrameData.U8Data[2]=0x02;
	Can_MailboxHeader_copy[16].FrameData.U8Data[3]=0x02;
	Can_MailboxHeader_copy[16].FrameData.U8Data[4]=0x02;
	Can_MailboxHeader_copy[16].FrameData.U8Data[5]=0x02;
	Can_MailboxHeader_copy[16].FrameData.U8Data[6]=0x02;
	Can_MailboxHeader_copy[16].FrameData.U8Data[7]=0x02;

	Can_MailboxHeader_copy[17].Id=0x503;
	Can_MailboxHeader_copy[17].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[17].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[17].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[17].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[17].FrameData.U8Data[0]=0x0D;
	Can_MailboxHeader_copy[17].FrameData.U8Data[1]=0x03;
	Can_MailboxHeader_copy[17].FrameData.U8Data[2]=0x03;
	Can_MailboxHeader_copy[17].FrameData.U8Data[3]=0x03;
	Can_MailboxHeader_copy[17].FrameData.U8Data[4]=0x03;
	Can_MailboxHeader_copy[17].FrameData.U8Data[5]=0x03;
	Can_MailboxHeader_copy[17].FrameData.U8Data[6]=0x03;
	Can_MailboxHeader_copy[17].FrameData.U8Data[7]=0x03;

	Can_MailboxHeader_copy[18].Id=0x504;
	Can_MailboxHeader_copy[18].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[18].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[18].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[18].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[18].FrameData.U8Data[0]=0x0D;
	Can_MailboxHeader_copy[18].FrameData.U8Data[1]=0x04;
	Can_MailboxHeader_copy[18].FrameData.U8Data[2]=0x04;
	Can_MailboxHeader_copy[18].FrameData.U8Data[3]=0x04;
	Can_MailboxHeader_copy[18].FrameData.U8Data[4]=0x04;
	Can_MailboxHeader_copy[18].FrameData.U8Data[5]=0x04;
	Can_MailboxHeader_copy[18].FrameData.U8Data[6]=0x04;
	Can_MailboxHeader_copy[18].FrameData.U8Data[7]=0x04;

	Can_MailboxHeader_copy[19].Id=0x505;
	Can_MailboxHeader_copy[19].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[19].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[19].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[19].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[19].FrameData.U8Data[0]=0x0D;
	Can_MailboxHeader_copy[19].FrameData.U8Data[1]=0x05;
	Can_MailboxHeader_copy[19].FrameData.U8Data[2]=0x05;
	Can_MailboxHeader_copy[19].FrameData.U8Data[3]=0x05;
	Can_MailboxHeader_copy[19].FrameData.U8Data[4]=0x05;
	Can_MailboxHeader_copy[19].FrameData.U8Data[5]=0x05;
	Can_MailboxHeader_copy[19].FrameData.U8Data[6]=0x05;
	Can_MailboxHeader_copy[19].FrameData.U8Data[7]=0x05;

	Can_MailboxHeader_copy[20].Id=0x601;
	Can_MailboxHeader_copy[20].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[20].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[20].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[20].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[20].FrameData.U8Data[0]=0x0E;
	Can_MailboxHeader_copy[20].FrameData.U8Data[1]=0x01;
	Can_MailboxHeader_copy[20].FrameData.U8Data[2]=0x01;
	Can_MailboxHeader_copy[20].FrameData.U8Data[3]=0x01;
	Can_MailboxHeader_copy[20].FrameData.U8Data[4]=0x01;
	Can_MailboxHeader_copy[20].FrameData.U8Data[5]=0x01;
	Can_MailboxHeader_copy[20].FrameData.U8Data[6]=0x01;
	Can_MailboxHeader_copy[20].FrameData.U8Data[7]=0x01;

	Can_MailboxHeader_copy[21].Id=0x602;
	Can_MailboxHeader_copy[21].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[21].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[21].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[21].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[21].FrameData.U8Data[0]=0x0E;
	Can_MailboxHeader_copy[21].FrameData.U8Data[1]=0x02;
	Can_MailboxHeader_copy[21].FrameData.U8Data[2]=0x02;
	Can_MailboxHeader_copy[21].FrameData.U8Data[3]=0x02;
	Can_MailboxHeader_copy[21].FrameData.U8Data[4]=0x02;
	Can_MailboxHeader_copy[21].FrameData.U8Data[5]=0x02;
	Can_MailboxHeader_copy[21].FrameData.U8Data[6]=0x02;
	Can_MailboxHeader_copy[21].FrameData.U8Data[7]=0x02;

	Can_MailboxHeader_copy[22].Id=0x701;
	Can_MailboxHeader_copy[22].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[22].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[22].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[22].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[22].FrameData.U8Data[0]=0x0F;
	Can_MailboxHeader_copy[22].FrameData.U8Data[1]=0x01;
	Can_MailboxHeader_copy[22].FrameData.U8Data[2]=0x01;
	Can_MailboxHeader_copy[22].FrameData.U8Data[3]=0x01;
	Can_MailboxHeader_copy[22].FrameData.U8Data[4]=0x01;
	Can_MailboxHeader_copy[22].FrameData.U8Data[5]=0x01;
	Can_MailboxHeader_copy[22].FrameData.U8Data[6]=0x01;
	Can_MailboxHeader_copy[22].FrameData.U8Data[7]=0x01;

	Can_MailboxHeader_copy[23].Id=0x702;
	Can_MailboxHeader_copy[23].Can_frame=CAN_FRAME_CLASSICAL;
	Can_MailboxHeader_copy[23].Can_id=CAN_DATA_STANDARD;
	Can_MailboxHeader_copy[23].TransceiveType=MAIL_TRANSMIT;
	Can_MailboxHeader_copy[23].FrameData.Data_Length=Data_Length_8;
	Can_MailboxHeader_copy[23].FrameData.U8Data[0]=0x0F;
	Can_MailboxHeader_copy[23].FrameData.U8Data[1]=0x02;
	Can_MailboxHeader_copy[23].FrameData.U8Data[2]=0x02;
	Can_MailboxHeader_copy[23].FrameData.U8Data[3]=0x02;
	Can_MailboxHeader_copy[23].FrameData.U8Data[4]=0x02;
	Can_MailboxHeader_copy[23].FrameData.U8Data[5]=0x02;
	Can_MailboxHeader_copy[23].FrameData.U8Data[6]=0x02;
	Can_MailboxHeader_copy[23].FrameData.U8Data[7]=0x02;
}



