/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : tim.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : 
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
#include "tim.h"

/**
 *  @brief : Initialize Timer 14 and configuration Timer 14.
 *  @param in :	CNT_T:Counting period
 				Prescaler
 *  @param out :None
 *  @retval :None
 */
void BASIC_TIMER14_Config(uint16_t CNT_T,uint16_t Prescaler)
{
	TIM_Reset(T14_SFR);
	BTIM_Updata_Immediately_Config(T14_SFR,TRUE);//immediate update control
	BTIM_Updata_Enable(T14_SFR,TRUE);			 //Enable update control
	BTIM_Work_Mode_Config(T14_SFR,BTIM_TIMER_MODE);//Set timer work mode
	BTIM_Set_Counter(T14_SFR,0);				//Set counter
	BTIM_Set_Period(T14_SFR,CNT_T);         	//Set period
	BTIM_Set_Prescaler(T14_SFR,Prescaler);		 //Set prescaler
	BTIM_Counter_Mode_Config(T14_SFR,BTIM_COUNT_UP_OF); //Set count up mode
	/*Select internal high frequency as clock source*/
	BTIM_Clock_Config(T14_SFR,BTIM_HFCLK);       //Select HFCLK as work clock
	INT_Interrupt_Priority_Config(INT_T14,2,0);  //Set interrupt priority
	BTIM_Overflow_INT_Enable(T14_SFR,TRUE);       //Enable overflow interrupt
	INT_Interrupt_Enable(INT_T14,TRUE);			  //Enable timer interrupt
	INT_Clear_Interrupt_Flag(INT_T14);            //clear interrupt flag
	BTIM_Cmd(T14_SFR,TRUE);						  //enable timer
	INT_Stack_Align_Config(INT_STACK_SINGLE_ALIGN);//Set stack align to single
}

void GENERAL_TIMER18_Config(uint16_t CNT_T,uint16_t Prescaler)
{
	/*reset peripherals of Timer 18 and enable peripherals clock source.*******/
	TIM_Reset(T18_SFR);
	/*Configure the T18 immediate update control bit to update immediately*****/
	GPTIM_Updata_Immediately_Config(T18_SFR,TRUE);
	/*Enable T18 update controller*********************************************/
	GPTIM_Updata_Enable(T18_SFR,TRUE);
	/*Set to timing mode as working mode of timer 18***************************/
	GPTIM_Work_Mode_Config(T18_SFR,GPTIM_TIMER_MODE);
	/*Updata counter***********************************************************/
	GPTIM_Set_Counter(T18_SFR,0);
	/*Set the counting period**************************************************/
	GPTIM_Set_Period(T18_SFR,CNT_T);
	/*Set prescaler************************************************************/
	GPTIM_Set_Prescaler(T18_SFR,Prescaler);
	/*Set up and down count****************************************************/
	GPTIM_Counter_Mode_Config(T18_SFR,GPTIM_COUNT_UP_DOWN_OUF);
	/*Select SCLK  as clock source of timer 18*********************************/
	GPTIM_Clock_Config(T18_SFR,GPTIM_HFCLK);
	/*Set interrupt priority***************************************************/
	INT_Interrupt_Priority_Config(INT_T18,2,0);
	/*Enable timer 18 overflow interrupt***************************************/
	GPTIM_Overflow_INT_Enable(T18_SFR,TRUE);
	/*Enable INT_T18 interrupt*************************************************/
	INT_Interrupt_Enable(INT_T18,TRUE);
	/*Clear INT_T18 interrupt flag*********************************************/
	INT_Clear_Interrupt_Flag(INT_T18);
	/*Enable timer 18**********************************************************/
	GPTIM_Cmd(T18_SFR,TRUE);

//	GPTIM_Cmd(T18_SFR,FALSE);
}
