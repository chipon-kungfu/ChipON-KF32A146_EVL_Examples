/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of CANFD
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
*                        		Include Files                               
******************************************************************************/
#include "system_init.h"
#include "LoopFifo.h"
#include "Canfd.h"
#include "tim.h"

/******************************************************************************
*                        		Variable Definitions                                 
******************************************************************************/
Canfd_MailboxHeaderType Canfd_MailboxHeader[100];
LPFifo_TypeDef LPFifo;

extern volatile uint8_t TASK_10ms;
Canfd_MailboxHeaderType CANfd_MessageR = {0};

volatile uint16_t time_10ms = 2;
volatile uint16_t time_20ms = 3;
volatile uint16_t time_50ms = 5;
volatile uint16_t time_100ms = 7;
volatile uint16_t time_1000ms = 11;
volatile uint16_t time_5000ms = 13;

uint32_t Receive_aCount = 0;
uint32_t Receive_bCount = 0;
uint32_t Receive_cCount = 0;
uint32_t Receive_dCount = 0;
uint8_t Receive_a = 0;
uint8_t Receive_b = 0;
uint8_t Receive_c = 0;
uint8_t Receive_d = 0;

void Can_m_ClearReceiveMailbox(const uint8_t Can_Controller_Index)
{
    Canfd_MailboxHeaderType Can_Receive_Mailbox_BUFFER[1] = {0};
    for(uint8_t mailbox = Mailbox_1;mailbox< Mailbox_31;mailbox++)
    {
        Can_Receive_Mailbox_BUFFER[0].TransceiveType = MAIL_RECEIVE;
        Can_m_FdMailBox_Write(Can_Controller_Index, mailbox,&Can_Receive_Mailbox_BUFFER[0]);
    }
}  



/******************************************************************************
*                        		Private Function                                 
******************************************************************************/
static void Led_Gpio_Init(void)
{
	/*LED1, LED2*/
	GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_3, GPIO_MODE_OUT);
	GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, GPIO_MODE_OUT);
}

/******************************************************************************
*                        		Main Function                                 
******************************************************************************/
int main(void)
{
	/* Initialize the system clock */
	SystemInit(72);
	/* Setup SysTick Timer as delay function */
	systick_delay_init(72);

	Canfd_Gpio_Init();
	Led_Gpio_Init();

	/* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
	Fifo_Init(&LPFifo,100);
	Can_m_FdControllerDeInit(CANfd6);
	Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig,Initindex_0);
	
	Receive_Mailboxinit(CANfd6);
	CAN_FD_Transmit_Schedular();

	GENERAL_TIMER18_Config(500,15);

	BASIC_TIMER14_Config(10000,15);
	/*Enable all interrupt*/
	INT_All_Enable(TRUE);
	while(1)
	{
		if(TASK_10ms == 1)
		{
			TASK_10ms = 0;
			if(Can_m_FdGetFlag(CANfd6,Rx_Flag))
			{
				Can_m_FdClearFlag(CANfd6,Rx_Flag);
				for(uint8_t receviemailbox = 1;receviemailbox<=30;receviemailbox++)
				{
					if(Can_m_FdGetMailBoxState(CANfd6,receviemailbox) == MAIL_RECEIVE_SUCCESS)
					{
						if(Can_m_FdMailBox_Read(CANfd6, receviemailbox,&CANfd_MessageR,MAIL_RECEIVE) == CAN_OK)
						{
							if(CANfd_MessageR.Id == 0x101 )
							{
								Receive_aCount++;
								Receive_a = CANfd_MessageR.FrameData.U8Data[1];
							}
							if(CANfd_MessageR.Id == 0x102 )
							{
							}
							if(CANfd_MessageR.Id == 0x103 )
							{
							}
							if(CANfd_MessageR.Id == 0x104 )
							{
								GPIO_Toggle_Output_Data_Config(GPIOA_SFR, GPIO_PIN_MASK_3);
								Receive_bCount++;
								Receive_b = CANfd_MessageR.FrameData.U8Data[1];
							}
							if(CANfd_MessageR.Id == 0x105 )
							{
								GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_11);
								Receive_cCount++;
								Receive_c = CANfd_MessageR.FrameData.U8Data[1];
							}
							if(CANfd_MessageR.Id == 0x106 )
							{
								Receive_dCount++;
								Receive_d = CANfd_MessageR.FrameData.U8Data[1];
							}
						}

					}
				}
			}
			CAN_FD_Transmit_Schedular();
		}

	}		
}
