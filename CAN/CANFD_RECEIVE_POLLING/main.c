/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a KF32A156 device StdPeriph Template
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "system_init.h"
#include "Canfd.h"
static void Led_Gpio_Init(void);
static void Led_Flip(void);
uint32_t    Receive_Flag = 0x00;
void Can_m_ClearReceiveMailbox(const uint8_t Can_Controller_Index)
{
    Canfd_MailboxHeaderType Can_Receive_Mailbox_BUFFER[1] = {0};
    for(uint8_t mailbox = Mailbox_0;mailbox< Mailbox_2;mailbox++)
    {
        Can_Receive_Mailbox_BUFFER[0].TransceiveType = MAIL_RECEIVE;
        Can_m_FdMailBox_Write(Can_Controller_Index, mailbox,&Can_Receive_Mailbox_BUFFER[0]);
    }
}  
static void Led_Gpio_Init(void)
{
    /*Configure the GPIO PF11 as IO port output mode */
    GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, GPIO_MODE_OUT);

    GPIO_Set_Output_Data_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, Bit_RESET);
}

/**
 *  @brief :
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
static void Led_Flip(void)
{
    GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_11);
}

/*
 * 500K/2M  BAUDRATE
 * */
int main()
{
    uint8_t                          Rmc_count = 0;
    Can_Controller_InterruptFlagType Can_Controller_InterruptFlag;
    /* Initialize the system clock */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function */
    systick_delay_init(72U);
    Canfd_Gpio_Init();
    Led_Gpio_Init();

	/* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
    Can_m_FdControllerDeInit(CANfd6);
    Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig, Initindex_0);
    Canfd_Sdu.Can_MailboxHeader[0].TransceiveType = MAIL_RECEIVE;
    Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &Canfd_Sdu.Can_MailboxHeader[0]);
    Can_m_FdMailBox_Write(CANfd6, Mailbox_1, &Canfd_Sdu.Can_MailboxHeader[0]);
	/*Enable all interrupt*/
	INT_All_Enable(TRUE);
    while (1)
    {
        Can_m_FdGetIntFlag(CANfd6, &Can_Controller_InterruptFlag, &Rmc_count);
        Can_m_FdClearIntFlag(CANfd6, &Can_Controller_InterruptFlag, Rmc_count);
        if (Can_Controller_InterruptFlag.RxIntFlag == 0x01)
        {
            if (Can_m_FdGetMailBoxState(CANfd6, Mailbox_0) == MAIL_RECEIVE_SUCCESS)
            {
                if (Can_m_FdMailBox_Read(CANfd6, Mailbox_0, &Canfd_Sdu.Can_MailboxHeader[0], MAIL_RECEIVE) == CAN_OK)
                {
                    Receive_Flag++;
                    Led_Flip();
                }
            }

            if (Can_m_FdGetMailBoxState(CANfd6, Mailbox_1) == MAIL_RECEIVE_SUCCESS)
            {
                if (Can_m_FdMailBox_Read(CANfd6, Mailbox_1, &Canfd_Sdu.Can_MailboxHeader[0], MAIL_RECEIVE) == CAN_OK)
                {
                    Receive_Flag++;
                    Led_Flip();
                }
            }
        }
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
