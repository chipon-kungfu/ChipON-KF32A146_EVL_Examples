/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of CANFD
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
*                        		Include Files                                 
******************************************************************************/
#include "system_init.h"
#include "LoopFifo.h"
#include "Canfd.h"
#include "tim.h"

/******************************************************************************
*                        		Variable & Function Definitions                                
******************************************************************************/
static void Led_Gpio_Init(void);
static void Led_Flip(void);
volatile uint16_t time_10ms = 2;
volatile uint16_t time_20ms = 3;
volatile uint16_t time_50ms = 5;
volatile uint16_t time_100ms = 7;
volatile uint16_t time_1000ms = 11;
volatile uint16_t time_5000ms = 13;
extern volatile uint8_t BUSOFF_FLAG;
Canfd_MailboxHeaderType Canfd_MailboxHeader[50];
LPFifo_TypeDef LPFifo;
Canfd_MailboxHeaderType * CANfd_MessageT;


void Can_m_ClearReceiveMailbox(const uint8_t Can_Controller_Index)
{
    Canfd_MailboxHeaderType Can_Receive_Mailbox_BUFFER[1] = {0};
    for(uint8_t mailbox = Mailbox_1;mailbox< Mailbox_11;mailbox++)
    {
        Can_Receive_Mailbox_BUFFER[0].TransceiveType = MAIL_RECEIVE;
        Can_m_FdMailBox_Write(Can_Controller_Index, mailbox,&Can_Receive_Mailbox_BUFFER[0]);
    }
}  

/******************************************************************************
*                        		Global Function                                 
******************************************************************************/
/**
 *  @brief :Led_Gpio_Init
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
static void Led_Gpio_Init(void)
{
	/*LED1, LED2*/
	GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_3, GPIO_MODE_OUT);
	GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, GPIO_MODE_OUT);
}
/**
 *  @brief :Led_Flip
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
static void Led_Flip(void)
{
	GPIO_Toggle_Output_Data_Config(GPIOA_SFR, GPIO_PIN_MASK_3);
	GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_11);
}

/******************************************************************************
*                        		Main Function                                 
******************************************************************************/
int main()
{
	/* Initialize the system clock */
	SystemInit(72);
	/* Setup SysTick Timer as delay function */
	systick_delay_init(72);
	/* CANFD GPIO initialized */
	Canfd_Gpio_Init();
	/* LED GPIO initialized */
	Led_Gpio_Init();
	/* FIFO initialized */
	Fifo_Init(&LPFifo,50);

	/* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
	/* CANFD initialized */
	Can_m_FdControllerDeInit(CANfd6);
	Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig,Initindex_0);
	/* CANFD Receive messages structure initialized */
	Receive_Mailboxinit(CANfd6);
	/* T18 transmission scheduling settings timer cycle value of 500 pre-frequency value of 16, every 500 us T18 interrupt*/
	GENERAL_TIMER18_Config(500,15);
	/* T14 transmission scheduling settings timer cycle value of 10000 pre-frequency value of 16, every 10 ms T14 interrupt*/
	BASIC_TIMER14_Config(10000,15);
	/*Enable all interrupt*/
	INT_All_Enable(TRUE);
	while (1)
	{

	}
}


/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
	/* User can add his own implementation to report the file name and line number,
		ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
		;
	}
};





