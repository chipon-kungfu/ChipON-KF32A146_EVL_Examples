/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/


#include "system_init.h"
#include "Canfd.h"
#include "LoopFifo.h"

extern LPFifo_TypeDef LPFifo;
extern volatile uint16_t time_10ms;
extern volatile uint16_t time_20ms;
extern volatile uint16_t time_50ms;
extern volatile uint16_t time_100ms;
extern volatile uint16_t time_1000ms;
extern volatile uint16_t time_5000ms;
extern volatile uint8_t flag ;
volatile uint8_t ArbLose_Flag = 0;
volatile uint32_t Writewrong_Flag = 0;
volatile uint32_t delay_num = CLEAR_FLAG_TIME;
extern Canfd_MailboxHeaderType * CANfd_MessageT;
extern Canfd_MailboxHeaderType Can_Transmit_Mailbox_BUFFER[];
//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************	

void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}

//*****************************************************************************************
//                              T18 Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt))_T18_exception (void)
{
	GPTIM_Clear_Updata_INT_Flag(T18_SFR);
	GPTIM_Clear_Overflow_INT_Flag(T18_SFR);
	volatile Kf32a_Canfd_Reg* ControllerRegPtr = (Kf32a_Canfd_Reg*)Can_m_ControllersInfo[CANfd6].BaseAddress;
	if(False == Fifo_IsEmpty(&LPFifo))
	{
		/* @brief :Canfd hardware send status is OK */
		if (CAN_OK == Can_m_FdTransmit_state(CANfd6))
		{
			CANfd_MessageT = Fifo_Read(&LPFifo);
			(*CANfd_MessageT).TransceiveType = MAIL_TRANSMIT;
			/* @brief :Write transmit mailbox is ok */
			if(CAN_OK == Can_m_FdMailBox_Write(CANfd6,Mailbox_0,CANfd_MessageT))
			{
				/* @brief :Enable to send */
				Can_m_FdTransmit(CANfd6);
				delay_num = 0xFF;
				while((ControllerRegPtr->CANFD_CTLR0.B.TXSTA == 0x01)&& (delay_num--));
				if(Writewrong_Flag > 0)
				{
				Writewrong_Flag--;
				}
				else
				{
					Writewrong_Flag = 0;
				}
			}
			else
			{
				/* @brief :Failed to write to Mailbox, rewrite FIFO */
				Fifo_Write(&LPFifo,CANfd_MessageT);
				Can_m_FdTransmit(CANfd6);
			}
			if(ArbLose_Flag == 1)
			{
				ArbLose_Flag = 0;
			}
		}
		else
		{
			Can_m_FdTransmit(CANfd6);
			Writewrong_Flag++;
			if(Writewrong_Flag >= 128)
			{
				Can_m_FdBusOFF_RECOVERY(CANfd6);
			}
		}
	}
}

//*****************************************************************************************
//                              T14 Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt))_T14_exception (void)
{
	BTIM_Clear_Updata_INT_Flag(T14_SFR);
	BTIM_Clear_Overflow_INT_Flag (T14_SFR);
	if(time_10ms < 2)
		{
		time_10ms++;
		}
		else
		{
			time_10ms = 2;
			for(uint8_t i = 0; i <= 2; i++)
			{
				/* @brief :Judge that FIFO is not full */
				if(Fifo_IsFull(&LPFifo) != True)
				{
					/* @brief :Write FIFO */
					Fifo_Write(&LPFifo,&Can_Transmit_Mailbox_BUFFER[i]);
				}
			}
		}
	if(time_20ms < 4)
		{
		time_20ms++;
		}
		else
		{
			time_20ms = 3;
			for(uint8_t i = 3; i <= 5; i++)
			{
				/* @brief :Judge that FIFO is not full */
				if(Fifo_IsFull(&LPFifo) != True)
				{
					/* @brief :Write FIFO */
					Fifo_Write(&LPFifo,&Can_Transmit_Mailbox_BUFFER[i]);
				}
			}
		}
	if(time_50ms < 9)
		{
		time_50ms++;
		}
		else
		{
			time_50ms = 5;
			for(uint8_t i = 6; i <= 8; i++)
			{
				/* @brief :Judge that FIFO is not full */
				if(Fifo_IsFull(&LPFifo) != True)
				{
					/* @brief :Write FIFO */
					Fifo_Write(&LPFifo,&Can_Transmit_Mailbox_BUFFER[i]);
				}
			}
		}

	if(time_100ms < 16)
		{
			time_100ms++;
		}
		else
		{
			time_100ms = 7;
			for(uint8_t i = 9; i<= 11 ; i++)
			{
				/* @brief :Judge that FIFO is not full */
				if(Fifo_IsFull(&LPFifo) != True)
				{
					/* @brief :Write FIFO */
					Fifo_Write(&LPFifo,&Can_Transmit_Mailbox_BUFFER[i]);
				}
			}
		}

	if(time_1000ms < 110)
		{
			time_1000ms++;
		}
		else
		{
			time_1000ms = 11;
			for(uint8_t i = 12;i <= 14 ; i++)
			{
				/* @brief :Judge that FIFO is not full */
				if(Fifo_IsFull(&LPFifo) != True)
				{
					/* @brief :Write FIFO */
					Fifo_Write(&LPFifo,&Can_Transmit_Mailbox_BUFFER[i]);
				}
			}
		}

	if(time_5000ms < 512)
		{
			time_5000ms++;
		}
		else
		{
			time_5000ms = 13;
			for(uint8_t i = 15; i <= 18 ; i++)
			{
				/* @brief :Judge that FIFO is not full */
				if(Fifo_IsFull(&LPFifo) != True)
				{
					/* @brief :Write FIFO */
					Fifo_Write(&LPFifo,&Can_Transmit_Mailbox_BUFFER[i]);
				}
			}
		}
}

/*****************************************************************************************
                              _CANFD6_exception Course
*****************************************************************************************/

void __attribute__((interrupt)) _CANFD6_exception (void)
{
	uint8_t Rmc_count = 0;
	Canfd_MailboxHeaderType Canfd_MailboxHeader_R = {0};
	Can_Controller_InterruptFlagType Can_Controller_InterruptFlag;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[CANfd6].BaseAddress;
	Can_m_FdGetIntFlag(CANfd6, &Can_Controller_InterruptFlag, &Rmc_count);
	Can_m_FdClearIntFlag(CANfd6, &Can_Controller_InterruptFlag,Rmc_count);

	if (Can_Controller_InterruptFlag.ArbitrateLoseFlag == 0x01)
	{
		ArbLose_Flag = 1;
	}

	if (Can_Controller_InterruptFlag.BusOffFlag == 0x01)
	{
		/* @brief :Busoff recovery,Reinitialize CANFD */
		Can_m_FdBusOFF_RECOVERY(CANfd6);
	}

	if (Can_Controller_InterruptFlag.RxIntFlag == 0x01)
		{
			for(uint8_t receviemailbox = 1;receviemailbox<=10;receviemailbox++)
			{
				if(Can_m_FdGetMailBoxState(CANfd6,receviemailbox) == MAIL_RECEIVE_SUCCESS)
				{
					Can_m_FdMailBox_Read(CANfd6, receviemailbox, &Canfd_MailboxHeader_R,MAIL_RECEIVE);
					if(Canfd_MailboxHeader_R.Id == 0x105 )
					{
						/*LED1*/
						GPIO_Toggle_Output_Data_Config(GPIOA_SFR, GPIO_PIN_MASK_3);
					}
					if(Canfd_MailboxHeader_R.Id == 0x106 )
					{
						/*LED2*/
						GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_11);
					}
				}
			}
		}
}



