/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Canfd.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of CANFD
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
#include "Canfd.h"

extern volatile uint32_t Writewrong_Flag;
extern volatile uint8_t ArbLose_Flag;
/**
 *  @brief :Receive message configuration
 */
Canfd_MailboxHeaderType Can_Receive_Mailbox_BUFFER[1];
/**
 *  @brief :Send message configuration
 */
Canfd_MailboxHeaderType Can_Transmit_Mailbox_BUFFER[] =
{
	{{{0x0A,0x01,0x01,0x01,0x01,0x01,0x01,0x01,},Data_Length_8},0x201,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0A,0x02,0x02,0x02,0x02,0x02,0x02,0x02,},Data_Length_8},0x202,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0A,0x03,0x03,0x03,0x03,0x03,0x03,0x03,},Data_Length_8},0x203,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0B,0x01,0x01,0x01,0x01,0x01,0x01,0x01,},Data_Length_8},0x301,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0B,0x02,0x02,0x02,0x02,0x02,0x02,0x02,},Data_Length_8},0x302,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0B,0x03,0x03,0x03,0x03,0x03,0x03,0x03,},Data_Length_8},0x303,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0C,0x01,0x01,0x01,0x01,0x01,0x01,0x01,},Data_Length_8},0x401,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0C,0x02,0x02,0x02,0x02,0x02,0x02,0x02,},Data_Length_8},0x402,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0C,0x03,0x03,0x03,0x03,0x03,0x03,0x03,},Data_Length_8},0x403,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0D,0x01,0x01,0x01,0x01,0x01,0x01,0x01,},Data_Length_8},0x501,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0D,0x02,0x02,0x02,0x02,0x02,0x02,0x02,},Data_Length_8},0x502,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0D,0x03,0x03,0x03,0x03,0x03,0x03,0x03,},Data_Length_8},0x503,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0E,0x01,0x01,0x01,0x01,0x01,0x01,0x01,},Data_Length_8},0x601,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0E,0x02,0x02,0x02,0x02,0x02,0x02,0x02,},Data_Length_8},0x602,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0E,0x03,0x03,0x03,0x03,0x03,0x03,0x03,},Data_Length_8},0x603,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_STANDARD,MAIL_TRANSMIT,},
	{{{0x0F,0x01,0x01,0x01,0x01,0x01,0x01,0x01,},Data_Length_8},0x3860111,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_EXTENDED,MAIL_TRANSMIT,},
	{{{0x0F,0x02,0x02,0x02,0x02,0x02,0x02,0x02,},Data_Length_8},0x3860222,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_EXTENDED,MAIL_TRANSMIT,},
	{{{0x0F,0x03,0x03,0x03,0x03,0x03,0x03,0x03,},Data_Length_8},0x3860333,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_EXTENDED,MAIL_TRANSMIT,},
	{{{0x0F,0x04,0x04,0x04,0x04,0x04,0x04,0x04,},Data_Length_8},0x3860444,0x000,CANFD_BRS_ENABLE,CAN_FRAME_FD,CAN_DATA_EXTENDED,MAIL_TRANSMIT,},
};

/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/
/**
 *  @brief :Receive Mailbox init
 *  @param in :Can_Controller_Index
 *  @param out :None
 *  @retval :None
 */
void Receive_Mailboxinit(const uint8_t Can_Controller_Index)
{
	volatile uint8_t retryTimes;
	Can_ReturnType ret;

	for(uint8_t receviemailbox = 1;receviemailbox<= 10;receviemailbox++)
	{
		Can_Receive_Mailbox_BUFFER[0].Can_id = CAN_DATA_STANDARD;
		Can_Receive_Mailbox_BUFFER[0].TransceiveType = MAIL_RECEIVE;
		retryTimes = 0u;
		do
		{
			ret = Can_m_FdMailBox_Write(Can_Controller_Index, receviemailbox,&Can_Receive_Mailbox_BUFFER[0]);
		} while ((CAN_OK != ret) && ((retryTimes++) < CAN_RETRY_TIMES));
	}
}

/**
 *  @brief :Canfd Hardware send status
 *  @param in :Can_Controller_Index
 *  @param out :CAN_OK
 *  			CAN_BUSY_TRANSMIT_MAILBOX
 *  @retval :None
 */
Can_ReturnType Can_m_FdTransmit_state(const uint8_t Can_Controller_Index)
{
	Can_ReturnType ret = CAN_UNINITIALIZED;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[Can_Controller_Index].BaseAddress;
	/* Controller Index Number Verification */
	CHECK_RESTRICTION(CHECK_CONTROLLER_INDEX(Can_Controller_Index));
	if ((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0x01) && (ControllerRegPtr->CANFD_CTLR0.B.TXSTA == 0x00)|| (ArbLose_Flag == 1))
	{
		ret = CAN_OK;
	}
	else
	{
		ret = CAN_BUSY_TRANSMIT_MAILBOX ;
	}
	return ret;
}

/**
 *  @brief :BusOFF RECOVERY
 *  @param in :Can_Controller_Index
 *  @param out :None
 *  @retval :None
 */
Can_ReturnType Can_m_FdBusOFF_RECOVERY(const uint8_t Can_Controller_Index)
{
	Can_m_FdControllerDeInit(Can_Controller_Index);
	Can_m_FdControllerInit(Can_Controller_Index, Canfd_Controller_AllConfig,Initindex_0);
	Receive_Mailboxinit(Can_Controller_Index);
	Writewrong_Flag = 0;

	return CAN_OK;
}

/**
 *  @brief :Canfd Gpio Init
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void Canfd_Gpio_Init(void)
{
	//PB3=CANFD6TX,PB4=CANFD6RX
	GPIO_Pull_Up_Enable(GPIOB_SFR, GPIO_PIN_MASK_3 | GPIO_PIN_MASK_4, TRUE);
	GPIO_Write_Mode_Bits(GPIOB_SFR, GPIO_PIN_MASK_3 | GPIO_PIN_MASK_4, GPIO_MODE_RMP);

	GPIO_Pin_RMP_Config(GPIOB_SFR, GPIO_Pin_Num_3, GPIO_RMP_AF6);
	GPIO_Pin_RMP_Config(GPIOB_SFR, GPIO_Pin_Num_4, GPIO_RMP_AF6);
}

#if (HARDWARE_FILTER == STD_ON)
const Can_HwFilterType Can_HwFilter = {
		/* Filter 0 */
		0x00000000U, /* Mask code */

		/* Filter 1 */

		0x00000000U, /* Mask code */

		/* Filter 2 */

		0x00000000U, /* Mask code */

		/* Filter 3 */

		0x00000000U, /* Mask code */

		/* Filter 4 */

		0x00000000U, /* Mask code */

		/* Filter 5 */

		0x00000000U, /* Mask code */

		/* Filter 6 */

		0x00000000U, /* Mask code */

		/* Filter 7 */

		0x00000000U, /* Mask code */

		/* Filter 8 */

		0x00000000U, /* Mask code */

		/* Filter 9 */

		0x00000000U, /* Mask code */

		/* Filter 10 */

		0x00000000U, /* Mask code */

		/* Filter 11 */

		0x00000000U, /* Mask code */

		/* Filter 12 */

		0x00000000U, /* Mask code */

		/* Filter 13 */

		0x00000000U, /* Mask code */

		/* Filter 14 */

		0x00000000U, /* Mask code */

		/* Filter 15 */

		0x00000000U, /* Mask code */

		/* Filter 16 */

		0x00000000U, /* Mask code */

		/* Filter 17 */

		0x00000000U, /* Mask code */

		/* Filter 18 */

		0x00000000U, /* Mask code */

		/* Filter 19 */

		0x00000000U, /* Mask code */

		/* Filter 20 */

		0x00000000U, /* Mask code */

		/* Filter 21 */

		0x00000000U, /* Mask code */

		/* Filter 22 */

		0x00000000U, /* Mask code */

		/* Filter 23 */

		0x00000000U, /* Mask code */

		/* Filter 24 */

		0x00000000U, /* Mask code */

		/* Filter 25 */

		0x00000000U, /* Mask code */

		/* Filter 26 */

		0x00000000U, /* Mask code */

		/* Filter 27 */

		0x00000000U, /* Mask code */

		/* Filter 28 */

		0x00000000U, /* Mask code */

		/* Filter 29 */

		0x00000000U, /* Mask code */

		/* Filter 30 */

		0x00000000U, /* Mask code */

		/* Filter 31 */

		0x00000000U, /* Mask code */

		/* Filter 32 */

		0x00000000U, /* Mask code */

		/* Filter 33 */

		0x00000000U, /* Mask code */

		/* Filter 34 */

		0x00000000U, /* Mask code */

		/* Filter 35 */

		0x00000000U, /* Mask code */

		/* Filter 36 */

		0x00000000U, /* Mask code */

		/* Filter 37 */

		0x00000000U, /* Mask code */

		/* Filter 38 */

		0x00000000U, /* Mask code */

		/* Filter 39 */

		0x00000000U, /* Mask code */

		/* Filter 40 */

		0x00000000U, /* Mask code */

		/* Filter 41 */

		0x00000000U, /* Mask code */

		/* Filter 42 */

		0x00000000U, /* Mask code */

		/* Filter 43 */

		0x00000000U, /* Mask code */

		/* Filter 44 */

		0x00000000U, /* Mask code */

		/* Filter 45 */

		0x00000000U, /* Mask code */

		/* Filter 46 */

		0x00000000U, /* Mask code */

		/* Filter 47 */

		0x00000000U, /* Mask code */

		/* Filter 48 */

		0x00000000U, /* Mask code */

		/* Filter 49 */

		0x00000000U, /* Mask code */

		/* Filter 50 */

		0x00000000U, /* Mask code */

		/* Filter 0 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 1 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 2 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 3 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 4 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 5 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 6 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 7 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 8 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 9 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 10 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 11 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 12 */

		CAN_FILTER_EXTENDED,	/* Acceptance code type */

		/* Filter 13 */

		CAN_FILTER_EXTENDED,	/* Acceptance code type */

		/* Filter 14 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 15 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 16 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 17 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 18 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 19 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 20 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 21 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 22 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 23 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 24 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 25 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 26 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 27 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 28 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 29 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 30 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 31 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 32 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 33 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 34 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 35 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 36 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 37 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 38 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 39 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 40 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 41 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 42 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 43 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 44 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 45 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 46 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 47 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 48 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 49 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */

		/* Filter 50 */

		CAN_FILTER_STANDARD,	/* Acceptance code type */
	
	};
#endif

const Can_Controller_InterruptConfigType Can_Controller_InterruptConfig[1] = {
	/* Interrupt config : enable or disable interrupt */
	{
		/* Rx interrupt enable */
		CAN_INTERRUPT_ENABLE,

		/* Tx interrupt enable set */
		CAN_INTERRUPT_DISABLE,

		/* Busoff interrupt enable set */
		CAN_INTERRUPT_ENABLE,

		 /* Wakeup interrupt enable set */
		CAN_INTERRUPT_DISABLE,

		/* Error alarm interrupt set */
		CAN_INTERRUPT_DISABLE,

		/* Rx data Overflow interrupt set */
		CAN_INTERRUPT_DISABLE,

		/* Negative error interupt set */
		CAN_INTERRUPT_DISABLE,

		/* Arbitrate lose interupt set */
		CAN_INTERRUPT_ENABLE,

		/* Bus error interupt set */
		CAN_INTERRUPT_ENABLE,

	    /* CAN DMA Transmit Interrupt Set */
		CAN_INTERRUPT_DISABLE,

		/* CAN DMA Receive Interrupt Set */
		CAN_INTERRUPT_DISABLE,

		/* CAN MailBox Receive Triger Interrupt Set */
		CAN_INTERRUPT_DISABLE,

		/* Preemption Priority */
		0,

		/* Sub priority */
		0,
	},
};


const Can_BDRConfigType Canfd_Controller_AllClockAndBDRConfig[1] = {
	/* Default 500k hz  75% */
	/* Clock and bandrate config 0 : Index 0 */
	{
#if (WORKSOURCE_HFCLK == STD_ON)
		.PreScale = 1U,
		.Sjw = 1U,
		.TSeg1 = 11U,
		.TSeg2 = 2U,
#else
		.PreScale = 11U,
		.Sjw = 1U,
		.TSeg1 = 14U,
		.TSeg2 = 3U,
#endif
		.SampleTimes = CAN_SAMPLE_ONCE
	}
};


const Can_FdBDRConfigType Canfd_Controller_ALLFdBDRConfig[1] = {
	/* Default 2M hz */
	/* High Speed Clock and bandrate config 0 : Index 0 */
	{
#if (WORKSOURCE_HFCLK == STD_ON)
		.BrsPrescale = 0U,
		.HtSeg1 = 5U,
		.HtSeg2 = 0U,
#else
		.BrsPrescale = 2U,
		.HtSeg1 = 14U,
		.HtSeg2 = 3U,
#endif
	},
};

Can_ControllerConfigType Canfd_Controller_AllConfig[1] = {
	/* Can 0 Config */
	{
/* Can controller Mode set
	Value Range :
	CANFD_NORMAL_MODE
	CANFD_LOOP_INTERNAL_MODE
	CNAFD_LOOP_EXTERNAL_MODE
	CANFD_SILENT_MODE */
		CANFD_NORMAL_MODE,
	/* Can controller Mode set
		Value Range :
		DISABLE
		ENABLE */
		ENABLE,
/* Can controller clock source set
	Value Range :
		CAN_CLOCKSOURCE_SCLK
		CAN_CLOCKSOURCE_HFCLK
		CAN_CLOCKSOURCE_LFCLK */
#if (WORKSOURCE_HFCLK == STD_ON)
		CAN_CLOCKSOURCE_HFCLK,
#else
		CAN_CLOCKSOURCE_SCLK,
#endif
/* Can controller Arbitrate clock source set
	Value Range :
	CAN_CLOCKSOURCE_SCLK
	CAN_CLOCKSOURCE_HFCLK
	CAN_CLOCKSOURCE_LFCLK */
		CAN_CLOCKSOURCE_SCLK,
		/* Mailbox block size config
             Value Range :
                CAN_8_BYTE_DATALENGTH :
                CAN_16_BYTE_DATALENGTH :
                CAN_32_BYTE_DATALENGTH :
                CAN_64_BYTE_DATALENGTH : */
		CAN_8_BYTE_DATALENGTH,
		/* Iso mode or non-iso mode config
            Value Range :
                CAN_FD_NON_ISOMODE :
                CAN_FD_ISOMODE : */
		CAN_FD_ISOMODE,
		/* Global Mask Set */
#if (HARDWARE_FILTER == STD_ON)
		0x00000000,
#else
		0xFFFFFFFF,
#endif
		/* Enable/disable mailbox full receive config
       Value Range :
            CAN_MBFULLRECEIVE_DISABLE
            CAN_MBFULLRECEIVE_ENABLE */
		CAN_MBFULLRECEIVE_DISABLE,
		/* Interrupt config  */
		&Can_Controller_InterruptConfig[0],
#if (HARDWARE_FILTER == STD_ON)
		/* Hardware Filter config  */
		&Can_HwFilter,
#endif
		/* Arbitrate Segment Baudrate Config */
		&Canfd_Controller_AllClockAndBDRConfig[0],
		/* Data Segment Baudrate Config */
		&Canfd_Controller_ALLFdBDRConfig[0],
	},
	/* end */
};



