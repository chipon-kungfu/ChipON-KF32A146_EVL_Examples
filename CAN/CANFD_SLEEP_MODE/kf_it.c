/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
extern FunctionalState Sleep_Command;

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception(void) {}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************

void __attribute__((interrupt)) _HardFault_exception(void) {}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception(void) {}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception(void) {}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception(void) {}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception(void) {}

/*****************************************************************************************
                              FlexCAN6_exception Course
*****************************************************************************************/

void __attribute__((interrupt)) _CANFD6_exception(void)
{
    uint8_t                          Rmc_count = 0;
    Can_Controller_InterruptFlagType Can_Controller_InterruptFlag;
    Canfd_MailboxHeaderType          CANfd_MessageR   = {0};
    volatile Kf32a_Canfd_Reg        *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[CANfd6].BaseAddress;
    Can_m_FdGetIntFlag(CANfd6, &Can_Controller_InterruptFlag, &Rmc_count);
    Can_m_FdClearIntFlag(CANfd6, &Can_Controller_InterruptFlag, Rmc_count);

    if (Can_Controller_InterruptFlag.RxIntFlag == 0x01)
    {
        if (Can_m_FdGetMailBoxState(CANfd6, Mailbox_0) == MAIL_RECEIVE_SUCCESS)
        {
            Can_m_FdMailBox_Read(CANfd6, Mailbox_0, &CANfd_MessageR, MAIL_RECEIVE);
            if (CANfd_MessageR.Id == 0x200)
            {
                Sleep_Command = TRUE;
            }
        }
    }
}

void __attribute__((interrupt)) _EINT4_exception(void)
{
    /* Get the interrupt flag on EXTI4 */
    if (INT_Get_External_Flag(INT_EXTERNAL_INTERRUPT_4) != RESET)
    {
        /* Clear the interrupt flag */
        INT_External_Clear_Flag(INT_EXTERNAL_INTERRUPT_4);
        Sleep_Command = FALSE;
    }
}
