/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a KF32A156 device StdPeriph Template
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "system_init.h"
#include "Canfd.h"
static void     Led_Gpio_Init(void);
static void     Key_Gpio_Init(void);
static void     Led_Flip(void);
FunctionalState Sleep_Command = FALSE;

void Can_m_ClearReceiveMailbox(const uint8_t Can_Controller_Index)
{
    Canfd_MailboxHeaderType Can_Receive_Mailbox_BUFFER[1] = {0};
    for(uint8_t mailbox = Mailbox_0;mailbox< Mailbox_1;mailbox++)
    {
        Can_Receive_Mailbox_BUFFER[0].TransceiveType = MAIL_RECEIVE;
        Can_m_FdMailBox_Write(Can_Controller_Index, mailbox,&Can_Receive_Mailbox_BUFFER[0]);
    }
}  

static void Led_Gpio_Init(void)
{
    /*Configure the GPIO PF11 as IO port output mode */
    GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, GPIO_MODE_OUT);

    GPIO_Set_Output_Data_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, Bit_RESET);
}

static void Led_Flip(void)
{
    GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_11);
}

static void Key_Gpio_Init(void)
{
    GPIO_Write_Mode_Bits(GPIOC_SFR, GPIO_PIN_MASK_0, GPIO_MODE_IN);
}

static uint32_t Get_Key_Value(void)
{
    uint32_t ret = 0;
    ret          = GPIO_Read_Input_Data_Bit(GPIOC_SFR, GPIO_PIN_MASK_0);
    return ret;
}

/*
 * 500K/2M  BAUDRATE
 * */
int main()
{
    /* Initialize the system clock */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function */
    systick_delay_init(72U);
    Canfd_Gpio_Init();
    Led_Gpio_Init();
    Key_Gpio_Init();

	/* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
    Can_m_FdControllerDeInit(CANfd6);
    Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig, Initindex_0);
    /*
     * MAILBOX0 RECEIVE_MAILBOX
     *
     * */
    Canfd_Sdu.Can_MailboxHeader[0].Id             = 0x200;
    Canfd_Sdu.Can_MailboxHeader[0].TransceiveType = MAIL_RECEIVE;
    Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &Canfd_Sdu.Can_MailboxHeader[0]);
	/*Enable all interrupt*/
	INT_All_Enable(TRUE);
    while (1)
    {
        if (Sleep_Command == TRUE)
        {
            Mcu_Goto_Sleep();
            Mcu_Exit_Sleep();
        }
        else
        {
            Led_Flip();
        }
        systick_delay_ms(200);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
