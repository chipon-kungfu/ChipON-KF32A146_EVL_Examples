/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Can.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of CAN
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
#include "Can.h"
#include "kf32a1x6_can.h"

static uint32_t  Filter_Group_Address[9] = {0x40002890,0x40002900,0x40002908,0x40002910,\
											0x40002918,0x40002920,0x40002928,0x40002930,\
											0x40002938};
/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/
/**
 *  @brief :
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void CAN_Gpio_Init(void)
{
	//PC10=CAN4TX,PC11=CNA4RX
	GPIO_Pull_Up_Enable(GPIOC_SFR, GPIO_PIN_MASK_10 | GPIO_PIN_MASK_11, TRUE);
	GPIO_Write_Mode_Bits(GPIOC_SFR, GPIO_PIN_MASK_10 | GPIO_PIN_MASK_11, GPIO_MODE_RMP);

	GPIO_Pin_RMP_Config(GPIOC_SFR, GPIO_Pin_Num_10, GPIO_RMP_AF6);
	GPIO_Pin_RMP_Config(GPIOC_SFR, GPIO_Pin_Num_11, GPIO_RMP_AF6);
}

void CAN_Init(CAN_SFRmap* CANx, CAN_InitTypeDef* canInitStruct)
{
	CAN_Reset(CANx);
	uint32_t tmpreg = 0;

	/* 参数校验 */
	CHECK_RESTRICTION(CHECK_CAN_ALL_PERIPH(CANx));
	CHECK_RESTRICTION(CHECK_FUNCTIONAL_STATE(canInitStruct->m_Enable));
	CHECK_RESTRICTION(CHECK_CAN_WORK_MODE(canInitStruct->m_Mode));
	CHECK_RESTRICTION(CHECK_CAN_SOURCE(canInitStruct->m_WorkSource));
	CHECK_RESTRICTION(CHECK_CAN_BAUDRATE_PRESET(canInitStruct->m_BaudRate));
	CHECK_RESTRICTION(CHECK_CAN_SYNC_JMP_WIDTH(canInitStruct->m_SyncJumpWidth));
	CHECK_RESTRICTION(CHECK_CAN_TIME_SEGMENT1(canInitStruct->m_TimeSeg1));
	CHECK_RESTRICTION(CHECK_CAN_TIME_SEGMENT2(canInitStruct->m_TimeSeg2));
	CHECK_RESTRICTION(CHECK_CAN_BUS_SAMPLE(canInitStruct->m_BusSample));

	/*---------------- 配置CANx_CTLR寄存器 ----------------*/
	/* 根据结构体成员m_Enable，设置CANEN位域 */
	/* 根据结构体成员m_Mode，设置LBACK和SILENT位域 */
	/* 根据结构体成员m_WorkSource，设置CANCKS位域 */
	/* 配置RSMOD，进入复位模式 */
	tmpreg = ((uint32_t)canInitStruct->m_Enable << CAN_CTLR_CANEN_POS) \
		   | canInitStruct->m_Mode \
		   | canInitStruct->m_WorkSource \
		   | CAN_CTLR_RSMOD;
	CANx->CTLR = SFR_Config (CANx->CTLR, ~CAN_CTLR_INIT_MASK, tmpreg);

	CANx->CTLR |= (0x01<<12);
	/*---------------- 配置CANx_BRGR寄存器 ----------------*/
	/* 根据结构体成员m_BaudRate，设置CANBRP位域 */
	/* 根据结构体成员m_SyncJumpWidth，设置SJW位域 */
	/* 根据结构体成员m_TimeSeg1，设置TSEG1位域 */
	/* 根据结构体成员m_TimeSeg2，设置TSEG2位域 */
	/* 根据结构体成员m_BusSample，设置SAM位域 */
	tmpreg = ((uint32_t)canInitStruct->m_BaudRate << CAN_BRGR_CANBRP0_POS) \
		   | ((uint32_t)canInitStruct->m_SyncJumpWidth << CAN_BRGR_SJW0_POS) \
		   | ((uint32_t)canInitStruct->m_TimeSeg1 << CAN_BRGR_TSEG1_0_POS) \
		   | ((uint32_t)canInitStruct->m_TimeSeg2 << CAN_BRGR_TSEG2_0_POS) \
		   | (canInitStruct->m_BusSample);
	CANx->BRGR = SFR_Config (CANx->BRGR, ~CAN_BRGR_INIT_MASK, tmpreg);
	/* Enable Bus Off Hardware Recovery*/
	CANx->CTLR |= (0x01<<13);
	/* Enable Specific Filter */
	CANx->CTLR |= (0x01<<4);
	for(uint8_t filter_number = 0;filter_number<9;filter_number++)
	{
		if((canInitStruct->Filter_Group_Ptr+filter_number)->Frame_Type == Standard_Frame)
		{
			*(uint32_t *)Filter_Group_Address[filter_number] = ((canInitStruct->Filter_Group_Ptr+filter_number)->Acceptance_Code)<<21;
			*(uint32_t *)(Filter_Group_Address[filter_number]+4) = ((canInitStruct->Filter_Group_Ptr+filter_number)->Mask_Code)<<21;
			*(uint32_t *)(Filter_Group_Address[filter_number]+4) |= 0x1FFFFF;
		}else if((canInitStruct->Filter_Group_Ptr+filter_number)->Frame_Type == Extended_Frame)
		{
			*(uint32_t *)Filter_Group_Address[filter_number] = ((canInitStruct->Filter_Group_Ptr+filter_number)->Acceptance_Code)<<3;
			*(uint32_t *)(Filter_Group_Address[filter_number]+4) = ((canInitStruct->Filter_Group_Ptr+filter_number)->Mask_Code)<<3;
			*(uint32_t *)(Filter_Group_Address[filter_number]+4) |= 0x07;
		}
	}
	/* 退出复位模式 */
	SFR_CLR_BIT_ASM(CANx->CTLR, CAN_CTLR_RSMOD_POS);
}

/**
 *  @brief :
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
RetStatus CAN_Transmit_Message_Once(volatile CAN_MessageTypeDef *CAN_Message)
{
		RetStatus ret = FAILURE;
		if (CAN_Message->m_DataLength > 8)
		{
			CAN_Message->m_DataLength = 8;
		}
		if ((!CAN_Get_Transmit_Status(CAN4_SFR, CAN_TX_BUFFER_STATUS)))
		{
			ret = FAILURE;
		}else
		{
			CAN4_SFR->CanTxBuffer.SFF.TXINFR.RTR = CAN_Message->m_RemoteTransmit;
			CAN4_SFR->CanTxBuffer.SFF.TXINFR.IDE = CAN_Message->m_FrameFormat;
			CAN4_SFR->CanTxBuffer.SFF.TXINFR.DLC = CAN_Message->m_DataLength;

			if(CAN_Message->m_FrameFormat == CAN_FRAME_FORMAT_SFF)
			{
				CAN4_SFR->CanTxBuffer.SFF.TXDATA0.ID =  CAN_Message->m_Can_ID;
				CAN4_SFR->CanTxBuffer.SFF.TXDATA0.DATA0 = CAN_Message->m_Data[0];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA0.DATA1 = CAN_Message->m_Data[1];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA1.DATA2 = CAN_Message->m_Data[2];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA1.DATA3 = CAN_Message->m_Data[3];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA1.DATA4 = CAN_Message->m_Data[4];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA1.DATA5 = CAN_Message->m_Data[5];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA2.DATA6 = CAN_Message->m_Data[6];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA2.DATA7 = CAN_Message->m_Data[7];
			}else
			{
				CAN4_SFR->CanTxBuffer.EFF.TXDATA0.ID =  CAN_Message->m_Can_ID;
				CAN4_SFR->CanTxBuffer.EFF.TXDATA1.DATA0 = CAN_Message->m_Data[0];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA1.DATA1 = CAN_Message->m_Data[1];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA1.DATA2 = CAN_Message->m_Data[2];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA1.DATA3 = CAN_Message->m_Data[3];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA2.DATA4 = CAN_Message->m_Data[4];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA2.DATA5 = CAN_Message->m_Data[5];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA2.DATA6 = CAN_Message->m_Data[6];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA2.DATA7 = CAN_Message->m_Data[7];
			}
			ret = CAN_Transmit_Single(CAN4_SFR);
		}
		return ret;
}



RetStatus CAN_Transmit_Message_Repeat(volatile CAN_MessageTypeDef *CAN_Message)
{
		RetStatus ret = FAILURE;
		if (CAN_Message->m_DataLength > 8)
		{
			CAN_Message->m_DataLength = 8;
		}
		if ((!CAN_Get_Transmit_Status(CAN4_SFR, CAN_TX_BUFFER_STATUS)))
		{
			ret = FAILURE;
		}else
		{
			CAN4_SFR->CanTxBuffer.SFF.TXINFR.RTR = CAN_Message->m_RemoteTransmit;
			CAN4_SFR->CanTxBuffer.SFF.TXINFR.IDE = CAN_Message->m_FrameFormat;
			CAN4_SFR->CanTxBuffer.SFF.TXINFR.DLC = CAN_Message->m_DataLength;

			if(CAN_Message->m_FrameFormat == CAN_FRAME_FORMAT_SFF)
			{
				CAN4_SFR->CanTxBuffer.SFF.TXDATA0.ID =  CAN_Message->m_Can_ID;
				CAN4_SFR->CanTxBuffer.SFF.TXDATA0.DATA0 = CAN_Message->m_Data[0];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA0.DATA1 = CAN_Message->m_Data[1];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA1.DATA2 = CAN_Message->m_Data[2];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA1.DATA3 = CAN_Message->m_Data[3];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA1.DATA4 = CAN_Message->m_Data[4];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA1.DATA5 = CAN_Message->m_Data[5];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA2.DATA6 = CAN_Message->m_Data[6];
				CAN4_SFR->CanTxBuffer.SFF.TXDATA2.DATA7 = CAN_Message->m_Data[7];
			}else
			{
				CAN4_SFR->CanTxBuffer.EFF.TXDATA0.ID =  CAN_Message->m_Can_ID;
				CAN4_SFR->CanTxBuffer.EFF.TXDATA1.DATA0 = CAN_Message->m_Data[0];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA1.DATA1 = CAN_Message->m_Data[1];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA1.DATA2 = CAN_Message->m_Data[2];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA1.DATA3 = CAN_Message->m_Data[3];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA2.DATA4 = CAN_Message->m_Data[4];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA2.DATA5 = CAN_Message->m_Data[5];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA2.DATA6 = CAN_Message->m_Data[6];
				CAN4_SFR->CanTxBuffer.EFF.TXDATA2.DATA7 = CAN_Message->m_Data[7];
			}
			ret = CAN_Transmit_Repeat(CAN4_SFR);
		}
		return ret;
}


/**
 *  @brief :
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
RetStatus CAN_Receive_Message(volatile Can_Pdu_TypeDef *Pdu)
{
	RetStatus ret;
	uint32_t tmpaddr = 0;
	uint8_t mailboxpoint =  CAN_Get_Point_Of_RAM_Mailbox(CAN4_SFR);
	uint8_t RmcCount = CAN_Get_Receive_Message_Counter(CAN4_SFR);
	int32_t ReceiveOffset = (mailboxpoint-RmcCount);
	if(ReceiveOffset>=0)
	{
		ReceiveOffset = ReceiveOffset*0x10;
	}else
	{
		ReceiveOffset = (ReceiveOffset+64)*0x10;
	}
	Pdu->Frame_length = RmcCount;
	for (uint8_t receive_count = 0; receive_count < RmcCount; receive_count++)
	{
		tmpaddr = CAN4_RECEIVE_ADDR;
		tmpaddr += ReceiveOffset;
		volatile CanRxBufferTypeDef *CanRxBufferPtr = (volatile CanRxBufferTypeDef *)tmpaddr;
		Pdu->CAN_Message[receive_count].m_FrameFormat = CanRxBufferPtr->SFF.RXDATA0.IDE;
		Pdu->CAN_Message[receive_count].m_DataLength = CanRxBufferPtr->SFF.RXDATA0.DLC;
		Pdu->CAN_Message[receive_count].m_RemoteTransmit = CanRxBufferPtr->SFF.RXDATA0.RTR;

		if(Pdu->CAN_Message[receive_count].m_RemoteTransmit == 0x01)
		{
			ReceiveOffset += 0x10;
			if (ReceiveOffset > 1008)
			{
				ReceiveOffset = 0;
			}
			continue;
		}else
		{
			Pdu->CAN_Message[receive_count].m_RemoteTransmit = CAN_DATA_FRAME;
		}
		if(Pdu->CAN_Message[receive_count].m_FrameFormat == 0x00)
		{
			/* Standard Frame Format  */
			Pdu->CAN_Message[receive_count].m_Can_ID = CanRxBufferPtr->SFF.RXDATA1.ID;
			Pdu->CAN_Message[receive_count].m_Data[0] = CanRxBufferPtr->SFF.RXDATA1.DATA0;
			Pdu->CAN_Message[receive_count].m_Data[1] = CanRxBufferPtr->SFF.RXDATA1.DATA1;
			Pdu->CAN_Message[receive_count].m_Data[2] = CanRxBufferPtr->SFF.RXDATA2.DATA2;
			Pdu->CAN_Message[receive_count].m_Data[3] = CanRxBufferPtr->SFF.RXDATA2.DATA3;
			Pdu->CAN_Message[receive_count].m_Data[4] = CanRxBufferPtr->SFF.RXDATA2.DATA4;
			Pdu->CAN_Message[receive_count].m_Data[5] = CanRxBufferPtr->SFF.RXDATA2.DATA5;
			Pdu->CAN_Message[receive_count].m_Data[6] = CanRxBufferPtr->SFF.RXDATA3.DATA6;
			Pdu->CAN_Message[receive_count].m_Data[7] = CanRxBufferPtr->SFF.RXDATA3.DATA7;
		}else
		{
			/* Extended Frame Format  */
			Pdu->CAN_Message[receive_count].m_Can_ID = CanRxBufferPtr->EFF.RXDATA1.ID;
			Pdu->CAN_Message[receive_count].m_Data[0] = CanRxBufferPtr->EFF.RXDATA2.DATA0;
			Pdu->CAN_Message[receive_count].m_Data[1] = CanRxBufferPtr->EFF.RXDATA2.DATA1;
			Pdu->CAN_Message[receive_count].m_Data[2] = CanRxBufferPtr->EFF.RXDATA2.DATA2;
			Pdu->CAN_Message[receive_count].m_Data[3] = CanRxBufferPtr->EFF.RXDATA2.DATA3;
			Pdu->CAN_Message[receive_count].m_Data[4] = CanRxBufferPtr->EFF.RXDATA3.DATA4;
			Pdu->CAN_Message[receive_count].m_Data[5] = CanRxBufferPtr->EFF.RXDATA3.DATA5;
			Pdu->CAN_Message[receive_count].m_Data[6] = CanRxBufferPtr->EFF.RXDATA3.DATA6;
			Pdu->CAN_Message[receive_count].m_Data[7] = CanRxBufferPtr->EFF.RXDATA3.DATA7;
		}
		ReceiveOffset += 0x10;
		if (ReceiveOffset > 1008)
		{
			ReceiveOffset = 0;
		}
	}
	CAN_Release_Receive_Buffer(CAN4_SFR, RmcCount);
	ret  = SUCCESS;
	return ret;
}


/**
 *  @brief :
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void CAN_Int_Config(Can_Interrupt_Type *Can_Interrupt)
{
	if(Can_Interrupt->CAN_TRANSMIT_INTERRUPT == INTERRUPT_ENABLE)
	{
		CAN_Set_INT_Enable(CAN4_SFR,CAN_INT_TRANSMIT,TRUE);
	}

	if(Can_Interrupt->CAN_RECEIVE_INTERRUPT == INTERRUPT_ENABLE)
	{
		CAN_Set_INT_Enable(CAN4_SFR,CAN_INT_RECEIVE,TRUE);
	}

	if(Can_Interrupt->CAN_OVERFLOW_INTERRUPT == INTERRUPT_ENABLE)
	{
		CAN_Set_INT_Enable(CAN4_SFR,CAN_INT_DATA_OVERFLOW,TRUE);
	}

	if(Can_Interrupt->CAN_BUSERROR_INTERRUPT == INTERRUPT_ENABLE)
	{
		CAN_Set_INT_Enable(CAN4_SFR,CAN_INT_BUS_ERROR,TRUE);
	}

	if(Can_Interrupt->CAN_BUSOFF_INTERRUPT == INTERRUPT_ENABLE)
	{
		CAN_Set_INT_Enable(CAN4_SFR,CAN_INT_BUS_OFF,TRUE);
	}

	if(Can_Interrupt->CAN_ERRORNEGATIVE_INTERRUPT == INTERRUPT_ENABLE)
	{
		CAN_Set_INT_Enable(CAN4_SFR,CAN_INT_ERROR_NEGATIVE,TRUE);
	}
	INT_Interrupt_Enable(INT_CAN4,TRUE);
	INT_All_Enable(TRUE);
}




