/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a KF32A156 device StdPeriph Template
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "system_init.h"
#include "LoopFifo.h"
#include "Canfd.h"
static void Led_Gpio_Init(void);
static void Led_Flip(void);



void Can_m_ClearReceiveMailbox(const uint8_t Can_Controller_Index)
{
    Canfd_MailboxHeaderType Can_Receive_Mailbox_BUFFER[1] = {0};
    for(uint8_t mailbox = Mailbox_10;mailbox< Mailbox_14;mailbox++)
    {
        Can_Receive_Mailbox_BUFFER[0].TransceiveType = MAIL_RECEIVE;
        Can_m_FdMailBox_Write(Can_Controller_Index, mailbox,&Can_Receive_Mailbox_BUFFER[0]);
    }
}  

Canfd_MailboxHeaderType  Canfd_MailboxHeader[10];
LPFifo_TypeDef           LPFifo;
Canfd_MailboxHeaderType *CANfd_MessageT;
static void              Led_Gpio_Init(void)
{
    /*Configure the GPIO PF11 as IO port output mode */
    GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, GPIO_MODE_OUT);

    GPIO_Set_Output_Data_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, Bit_RESET);
}

/**
 *  @brief :
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
static void Led_Flip(void)
{
    GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_11);
}

/*
 * 500K/2M  BAUDRATE
 * */
int main()
{
    /* Initialize the system clock */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function */
    systick_delay_init(72U);
    Canfd_Gpio_Init();
    Led_Gpio_Init();

	/* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
    Fifo_Init(&LPFifo, 10);
    Can_m_FdControllerDeInit(CANfd6);
    Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig, Initindex_0);
    /*
     * MAILBOX10~11 RECEIVE_MAILBOX ID:STD  0X100
     *
     * */
    Canfd_Sdu.Can_MailboxHeader[0].Id             = 0x100;
    Canfd_Sdu.Can_MailboxHeader[0].Can_id         = CAN_DATA_STANDARD;
    Canfd_Sdu.Can_MailboxHeader[0].TransceiveType = MAIL_RECEIVE;
    Can_m_FdMailBox_Write(CANfd6, Mailbox_10, &Canfd_Sdu.Can_MailboxHeader[0]);
    Can_m_FdMailBox_Write(CANfd6, Mailbox_11, &Canfd_Sdu.Can_MailboxHeader[0]);
    /*
     * MAILBOX12~13 RECEIVE_MAILBOX ID:EXD  0x3860000
     *
     * */
    Canfd_Sdu.Can_MailboxHeader[0].Id             = 0x3860000;
    Canfd_Sdu.Can_MailboxHeader[0].Can_id         = CAN_DATA_EXTENDED;
    Canfd_Sdu.Can_MailboxHeader[0].TransceiveType = MAIL_RECEIVE;
    Can_m_FdMailBox_Write(CANfd6, Mailbox_12, &Canfd_Sdu.Can_MailboxHeader[0]);
    Can_m_FdMailBox_Write(CANfd6, Mailbox_13, &Canfd_Sdu.Can_MailboxHeader[0]);
	/*Enable all interrupt*/
	INT_All_Enable(TRUE);
    while (1)
    {
        /*
         * MAILBOX14~MAILBOX50 TRANSMIT_MAILBOX
         *
         * */

        if (False == Fifo_IsEmpty(&LPFifo))
        {
            CANfd_MessageT = Fifo_Read(&LPFifo);
            (*CANfd_MessageT).Id--;
            (*CANfd_MessageT).TransceiveType = MAIL_TRANSMIT;

            for (uint8_t transmitbox = 14; transmitbox < 51; transmitbox++)
            {
                if (Can_m_FdMailBox_Write(CANfd6, transmitbox, CANfd_MessageT) == CAN_OK)
                {
                    break;
                }
            }
            (void)Can_m_FdTransmit(CANfd6);
        }
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
