/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : LoopFifo.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of FIFO
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "LoopFifo.h"
extern Canfd_MailboxHeaderType Canfd_MailboxHeader[10];
/*********************************************************************************************************
   Init FIFO
*********************************************************************************************************/
void Fifo_Init(LPFifo_TypeDef *pFifo,uint32_t VarLen)
{
	pFifo -> Ptr	= &Canfd_MailboxHeader[0];
	pFifo -> Size	= VarLen;
    pFifo -> Head	= 0;
    pFifo -> Tail	= 0;	
	pFifo -> Len	= 0;
}


/*********************************************************************************************************
	Deinit FIFO
*********************************************************************************************************/
tBool Fifo_DeInit(LPFifo_TypeDef *pFifo)
{
	if(pFifo -> Len == 0)
	{
		return True;
	}
	else 
		return False;
}



/*********************************************************************************************************
	Check FIFO  Message Number
*********************************************************************************************************/
uint32_t Check_FifoMessageNumber(LPFifo_TypeDef *pFifo)
{
	return pFifo->Len;
}


/*********************************************************************************************************
    Check FIFO State
*********************************************************************************************************/
tBool Fifo_IsFull(LPFifo_TypeDef *pFifo)
{
	tBool ret = Undifined;
    if(pFifo -> Len == pFifo ->Size)
    {
    	ret = True;
    }else
    {
    	ret = False;
    }
    return ret;
}


/*********************************************************************************************************
    Check FIFO State
*********************************************************************************************************/
tBool Fifo_IsEmpty(LPFifo_TypeDef *pFifo)
{
	tBool ret = Undifined;
    if(pFifo -> Len == 0)
    {
    	ret = True;
    }else
    {
    	ret = False;
    }
    return ret;
}


/*********************************************************************************************************
	Write Infor to FIFO
*********************************************************************************************************/
void Fifo_Write(LPFifo_TypeDef *pFifo,VarType *Var)
{
    *(pFifo -> Ptr + pFifo -> Head) = *Var;
    pFifo -> Head = (pFifo -> Head + 1) % pFifo -> Size;
    pFifo -> Len ++;
}


/*********************************************************************************************************
	Read Infor From FiFO
*********************************************************************************************************/
VarType * Fifo_Read(LPFifo_TypeDef *pFifo)
{
	static VarType * ReceiveData;
    ReceiveData = (pFifo -> Ptr + pFifo -> Tail);
	pFifo -> Tail = (pFifo -> Tail + 1) % (pFifo -> Size);
	pFifo -> Len --;
    return ReceiveData;
}



