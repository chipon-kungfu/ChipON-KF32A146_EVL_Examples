/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : LoopFifo.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of FIFO
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef __LOOP_FIFO_H__
#define __LOOP_FIFO_H__
#include "kf32a1x6_canfd.h"
#include "malloc.h"

#define VarType		Canfd_MailboxHeaderType
typedef enum
{
	Undifined,
    True,
    False,
}tBool;

typedef struct
{ 
    volatile uint32_t  	Head;         	
    volatile uint32_t  	Tail;         
	volatile uint32_t  	Len;		
	volatile uint32_t  	Size;			   
			 VarType *  Ptr;
} LPFifo_TypeDef;

void Fifo_Init(LPFifo_TypeDef *pFifo,uint32_t VarLen);
tBool Fifo_DeInit(LPFifo_TypeDef *pFifo);
uint32_t Check_FifoMessageNumber(LPFifo_TypeDef *pFifo);
tBool Fifo_IsFull(LPFifo_TypeDef *pFifo);
tBool Fifo_IsEmpty(LPFifo_TypeDef *pFifo);
void Fifo_Write(LPFifo_TypeDef *pFifo,VarType *Var);
VarType * Fifo_Read(LPFifo_TypeDef *pFifo);

#endif
