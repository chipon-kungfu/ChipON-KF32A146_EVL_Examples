/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
#include "Canfd.h"
extern uint32_t Receive_Flag;

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception(void) {}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************

void __attribute__((interrupt)) _HardFault_exception(void) {}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception(void) {}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception(void) {}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception(void) {}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception(void) {}

/*****************************************************************************************
                              CANFD6 exception Course
*****************************************************************************************/

void __attribute__((interrupt)) _CANFD6_exception(void)
{
    uint8_t                          Rmc_count             = 0;
    Canfd_MailboxHeaderType          Canfd_MailboxHeader_R = {0};
    Can_Controller_InterruptFlagType Can_Controller_InterruptFlag;
    volatile Kf32a_Canfd_Reg        *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[CANfd6].BaseAddress;
    Can_m_FdGetIntFlag(CANfd6, &Can_Controller_InterruptFlag, &Rmc_count);
    Can_m_FdClearIntFlag(CANfd6, &Can_Controller_InterruptFlag, Rmc_count);
    if (Can_Controller_InterruptFlag.RxIntFlag == 0x01)
    {
        if (Can_m_FdGetMailBoxState(CANfd6, Mailbox_1) == MAIL_RECEIVE_SUCCESS)
        {
            /* Read recevie mailbox information and reinitialize mailbox code */
            if (Can_m_FdMailBox_Read(CANfd6, Mailbox_1, &Canfd_MailboxHeader_R, MAIL_RECEIVE) == CAN_OK)
            {
                Receive_Flag++;
                GPIO_Toggle_Output_Data_Config(GPIOA_SFR, GPIO_PIN_MASK_3);
            }
        }

        if (Can_m_FdGetMailBoxState(CANfd6, Mailbox_2) == MAIL_RECEIVE_SUCCESS)
        {
            /* Read recevie mailbox information and reinitialize mailbox code */
            if (Can_m_FdMailBox_Read(CANfd6, Mailbox_2, &Canfd_MailboxHeader_R, MAIL_RECEIVE) == CAN_OK)
            {
                Receive_Flag++;
                GPIO_Toggle_Output_Data_Config(GPIOA_SFR, GPIO_PIN_MASK_3);
            }
        }
    }
}
