/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of CAN
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
*                        		Include Files                                 *
******************************************************************************/
#include "system_init.h"
#include "Can.h"

/******************************************************************************
*                     	Static function declaration                           *
******************************************************************************/
static void Led_Gpio_Init(void);
static void Led_Flip(void);

/******************************************************************************
*                        	Initialization function                           *
******************************************************************************/
/**
 *  @brief :Initializes the GPIO of the LED
 *  @param in :None
 *  @param out :None
 *  @retval :PD12--LED1
 *  		 PH3 --LED2
 *  		 PA4 --LED3
 *  		 PF7 --LED4
 */
static void Led_Gpio_Init(void)
{
	/*Configure the GPIO PF11 as IO port output mode */
	GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_11 , GPIO_MODE_OUT);

	GPIO_Set_Output_Data_Bits(GPIOF_SFR, GPIO_PIN_MASK_11 ,Bit_RESET );
}

/**
 *  @brief :Led flip
 *  @param in :None
 *  @param out :None
 *  @retval :PF11--LED2
 */
static void Led_Flip(void)
{
	GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_11);
}

/*
 * Filter group type:
 *			- Acceptance code
 *			- Mask code
 *			- Frame type £ºStandard Frame
 *			               Extended Frame
 */
Filter_Group_Typedef Filter_Group[9] = {
										{0x111,0x00000000,Standard_Frame},
										{0x222,0x00000000,Standard_Frame},
										{0x333,0x00000000,Standard_Frame},
										{0x444,0x00000000,Standard_Frame},
										{0x7FE,0x00000000,Standard_Frame},
										{0x3860111,0x00000000,Extended_Frame},
										{0x3860222,0x00000000,Extended_Frame},
										{0x3860333,0x00000000,Extended_Frame},
										{0x3860444,0x00000000,Extended_Frame},
									   };

/*
 * CAN configuration information:
 *			- Enable CAN
 *			- CAN mode configuration
 *          - CAN worksource configuration
 *			- Can baud rate default value is 0
 *			- CAN synchronous jump width is 1
 *			- CAN time interval 1 is 10
 *			- CAN time interval 2 is 3
 *			- CAN bus acquisition times is 3 times
 */
CAN_InitTypeDef CAN_Condition = {
									TRUE,
									CAN_MODE_NORMAL,
									CAN_SOURCE_HFCLK_DIV_2,
#if(BAUDRATE == BAUDRATE100K)
									4,
#elif(BAUDRATE == BAUDRATE250K)
									1,
#elif(BAUDRATE == BAUDRATE500K)
									0,
#endif
									1,
									11,
									2,
									CAN_BUS_SAMPLE_3_TIMES,
									&Filter_Group[0]
								};

/*
 *   Can interrupt type:
 *			- Enable CAN transmit interrupt type
 *			- Enable CAN receive interrupt type
 *          - Enable CAN overflow interrupt type
 *          - Enable CAN buserror interrupt type
 *          - Enable CAN busoff interrupt type
 *          - Enable CAN error negative interrupt type
 */
Can_Interrupt_Type Can_Interrupt = {
									INTERRUPT_DISABLE,
									INTERRUPT_ENABLE,
									INTERRUPT_DISABLE,
									INTERRUPT_ENABLE,
									INTERRUPT_ENABLE,
									INTERRUPT_DISABLE,
									};

/*
 *   Can interrupt type:
 *			- CAN message information structure:
 *					- Frame format selection
 *					- Send the request remotely
 *					- Data Length
 *					- ID
 *					- Data area
 *			- Frame length is 1
 */
Can_Pdu_TypeDef    Can_Pdu_Send = {
									{
										{
										CAN_FRAME_FORMAT_SFF,
										CAN_DATA_FRAME,
										8,
										0x111,
										{0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08},
										},
									},
									1,
								   };

int main()
{
	/* Initialize the system clock */
	SystemInit(72);
	/* Setup SysTick Timer as delay function */
	systick_delay_init(72);
	/* Initialize led IOs */
	Led_Gpio_Init();
	/* Initialize Can IOs
	 *     - PC10 = CAN4TX
	 *     - PC11 = CNA4RX
	 */
	CAN_Gpio_Init();
	/* Initialize Can Mode And Set Can Module To 500k baudrate*/
	CAN_Init(CAN4_SFR,&CAN_Condition);
	/* Initialize   Interrupt */
	CAN_Int_Config(&Can_Interrupt);
	while(1)
	{
		/* Led flip */
		Led_Flip();
		systick_delay_ms(250);
	}		
}



/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
	/* User can add his own implementation to report the file name and line number,
		ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
		;
	}
};


