/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
#include "Can.h"
#include "LoopFifo.h"


extern LPFifo_TypeDef LPFifo;
//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _NMI_exception (void)
{	

}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************	

void __attribute__((interrupt)) _HardFault_exception (void)
{

}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _StackFault_exception (void)
{

}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SVC_exception (void)
{

}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SoftSV_exception (void)
{

}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************	
void __attribute__((interrupt)) _SysTick_exception (void)
{
	
}

//*****************************************************************************************
//                             	 CAN4 Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _CAN4_exception (void)
{
	GPIO_Set_Output_Data_Bits(GPIOF_SFR, GPIO_PIN_MASK_11,Bit_SET);
	volatile Can_Pdu_TypeDef Can_Pdu;
	volatile uint32_t Can_Rcr = 0x00;
	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_TRANSMIT))
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_TRANSMIT);
	}

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_BUS_OFF))
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_BUS_OFF);
		CAN4_SFR->CTLR &= ~(0x01);
	}

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_BUS_ERROR))
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_BUS_ERROR);
		Can_Rcr = CAN4_SFR->RCR;
	}

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_RECEIVE))
	{
		CAN_Receive_Message(&Can_Pdu);
		for(uint8_t i = 0;i<Can_Pdu.Frame_length;i++)
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,Can_Pdu.CAN_Message[i]);
			}
		}
	}

	if(CAN_Get_INT_Flag(CAN4_SFR,CAN_INT_DATA_OVERFLOW))
	{
		CAN_Clear_INT_Flag(CAN4_SFR,CAN_INT_DATA_OVERFLOW);
	}
	GPIO_Set_Output_Data_Bits(GPIOF_SFR, GPIO_PIN_MASK_11,Bit_RESET);
}

