/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of CANFD
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
*                        		Include Files                                 *
******************************************************************************/
#include "system_init.h"
#include "LoopFifo.h"
#include "Canfd.h"
#include "Com.h"

/******************************************************************************
*                        		Variable & Function Definitions                                
******************************************************************************/
static void Led_Gpio_Init(void);
static void Led_Flip(void);

volatile uint32_t Writewrong_Flag = 0;
volatile uint32_t delay_num = CLEAR_FLAG_TIME;

/*timer count*/
uint8_t gBSWGPT1msFlg;
uint8_t gBSWGPT10msFlg;
uint8_t gBSWGPT20msFlg;
uint8_t gBSWGPT50msFlg;
uint8_t gBSWGPT100msFlg;
uint8_t taskingcount = 0;

uint32_t  countread = 0;

void Can_m_ClearReceiveMailbox(const uint8_t Can_Controller_Index)
{
    Canfd_MailboxHeaderType Can_Receive_Mailbox_BUFFER[1] = {0};
    for(uint8_t mailbox = Mailbox_1;mailbox< Mailbox_31;mailbox++)
    {
        Can_Receive_Mailbox_BUFFER[0].TransceiveType = MAIL_RECEIVE;
        Can_m_FdMailBox_Write(Can_Controller_Index, mailbox,&Can_Receive_Mailbox_BUFFER[0]);
    }
}  



/******************************************************************************
*                        		Global Function                                 
******************************************************************************/
void Gpt_MainFunction(void)
{
    if(ST_CTL&(0x01<<16))
    {
        gBSWGPT1msFlg = 1;
        taskingcount++;
        if((taskingcount%10)== 0)
		{
        	gBSWGPT10msFlg = 1;
		}
		if((taskingcount%20)== 0)
		{
			gBSWGPT20msFlg = 1;
		}
		if((taskingcount%50)== 0)
		{
			gBSWGPT50msFlg = 1;
		}
		if((taskingcount%100)== 0)
		{
			gBSWGPT100msFlg = 1;
			taskingcount = 0;
		}
    }
}

Canfd_MailboxHeaderType Canfd_MailboxHeader[50];
LPFifo_TypeDef LPFifo;
Canfd_MailboxHeaderType * CANfd_MessageT;


static void Led_Gpio_Init(void)
{
	/*LED1, LED2*/
	GPIO_Write_Mode_Bits(GPIOA_SFR, GPIO_PIN_MASK_3, GPIO_MODE_OUT);
	GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, GPIO_MODE_OUT);
}

void Led_Close(void)
{
	GPIO_Set_Output_Data_Bits(GPIOA_SFR, GPIO_PIN_MASK_3,Bit_RESET);
	GPIO_Set_Output_Data_Bits(GPIOF_SFR, GPIO_PIN_MASK_11,Bit_RESET);
}

void Led_Open(void)
{
	GPIO_Set_Output_Data_Bits(GPIOA_SFR, GPIO_PIN_MASK_3,Bit_SET);
	GPIO_Set_Output_Data_Bits(GPIOF_SFR, GPIO_PIN_MASK_11,Bit_SET);
}


/******************************************************************************
*                        		Main Function                                 
******************************************************************************/
int main()
 {
	Can_ReturnType ret = CAN_OK;
	volatile uint32_t timeout = 0xFFFFFF;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[0].BaseAddress;

	/* Initialize the system clock */
	SystemInit(72);
	/* Setup SysTick Timer as delay function */
	SYSTICK_Configuration(SYSTICK_SYS_CLOCK_DIV_1,FALSE,72000);
	SYSTICK_Cmd(TRUE);

	Canfd_Gpio_Init();

	Led_Gpio_Init();

	/* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
	Fifo_Init(&LPFifo,50);
	Can_m_FdControllerDeInit(CANfd6);
	Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig,Initindex_0);

	Receive_Mailboxinit(CANfd6);
	/*Enable all interrupt*/
	INT_All_Enable(TRUE);
	while (1)
	{
		Gpt_MainFunction();

		Com_uds();
		if((ControllerRegPtr->CANFD_IFR.B.BOFFIF == 1))
		{
			Can_m_FdControllerDeInit(CANfd6);
			Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig,Initindex_0);
			Receive_Mailboxinit(CANfd6);
		}
		if((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 1) || (ControllerRegPtr->CANFD_IFR.B.ALIF == 1))
		{
			if(ControllerRegPtr->CANFD_IFR.B.ALIF == 1)
		{
				ControllerRegPtr->CANFD_IER.B.ALIC = 0x00;
			}
			if(False == Fifo_IsEmpty(&LPFifo))
			{
				if((Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
						||(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_UNDEFINED) )
				{
					CANfd_MessageT = Fifo_Read(&LPFifo);
					countread++;
					ret = Can_m_FdMailBox_Write(CANfd6,Mailbox_0,CANfd_MessageT);
					if( ret == CAN_OK)
					{
						(void)Can_m_FdTransmit(CANfd6);
						delay_num = 0xFF;
						while((ControllerRegPtr->CANFD_CTLR0.B.TXSTA == 0x01)&& (delay_num--));
						if(Writewrong_Flag > 0)
						{
						Writewrong_Flag--;
						}
						else
						{
							Writewrong_Flag = 0;
						}
					}
					else
					{
						if(Fifo_IsFull(&LPFifo) != True)
						{
							Fifo_Write(&LPFifo,CANfd_MessageT);
						}
					}
				}
				else
				{
					(void)Can_m_FdTransmit(CANfd6);
					Writewrong_Flag++;
					if(Writewrong_Flag == 128)
					{
						Can_m_FdControllerDeInit(CANfd6);
						Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig,Initindex_0);
						Receive_Mailboxinit(CANfd6);
						Writewrong_Flag = 0;
					}
				}
			}
		}
		else
		{}

		/*Tasking*/
		if (gBSWGPT1msFlg)
		{
			gBSWGPT1msFlg = 0;
		}
		if (gBSWGPT10msFlg)
		{
			send10data();
			send10data_1();
			send10data_2();
			send10data_3();

			gBSWGPT10msFlg = 0;
		}
		if (gBSWGPT20msFlg)
		{
			send20data();
			send20data_1();
			send20data_2();
			send20data_3();

			gBSWGPT20msFlg = 0;

		}
		if (gBSWGPT50msFlg)
		{
			send50data();
			send50data_1();
			send50data_2();
			send50data_3();

			gBSWGPT50msFlg = 0;
		}
		if (gBSWGPT100msFlg)
		{
			send100data();
			send100data_1();
			send100data_2();
			send100data_3();
			send100data_4();
			send100data_6();
			send100data_7();
			send100data_8();
			send100data_9();
			send100data_A();

			gBSWGPT100msFlg = 0;
		}
	}
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
	/* User can add his own implementation to report the file name and line number,
		ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
		;
	}
};





