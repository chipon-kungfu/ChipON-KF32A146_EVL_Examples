/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Com.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : 
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#include "system_init.h"
#include "LoopFifo.h"
#include "Canfd.h"
#include "main.h"


uint8_t Close_comFlag = 0;
extern volatile uint32_t delay_num;
uint32_t count32 = 0;
extern LPFifo_TypeDef LPFifo;

Canfd_MailboxHeaderType CANfd_MessageO = {0};
Canfd_MailboxHeaderType CANfd_MessageE = {0};
Canfd_MailboxHeaderType CANfd_MessageTE = {0};
Canfd_MailboxHeaderType CANfd_MessageS = {0};
Canfd_MailboxHeaderType CANfd_MessageUDS = {0};


void Com_uds(void)
{
	Can_ReturnType ret = CAN_OK;
	int i = 0;
    int j = 0;
	if(Can_m_FdGetFlag(CANfd6, Rx_Flag))
	{
		Can_m_FdClearFlag(CANfd6, Rx_Flag);
		for(j = 1;j <= 30 ; j++)
		{
			if(Can_m_FdGetMailBoxState(CANfd6,j) == MAIL_RECEIVE_SUCCESS)
			{
				if(Can_m_FdMailBox_Read(CANfd6, j,&Canfd_Sdu.Can_MailboxHeader[0],MAIL_RECEIVE) == CAN_OK)
				{
					if(Canfd_Sdu.Can_MailboxHeader[0].Id == 0x7DF)
					{
						if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x04)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x31)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0x01)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0xAA)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[4] == 0x01))
						{
							Led_Open();
							CANfd_MessageUDS.FrameData.U8Data[0] = 0x05;
							CANfd_MessageUDS.FrameData.U8Data[1] = 0x31;
							CANfd_MessageUDS.FrameData.U8Data[2] = 0x01;
							CANfd_MessageUDS.FrameData.U8Data[3] = 0xAA;
							CANfd_MessageUDS.FrameData.U8Data[4] = 0x01;
							CANfd_MessageUDS.FrameData.U8Data[5] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[6] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[7] = 0x00;
							CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
							CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
							CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
							CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
							CANfd_MessageUDS.Id = 0x72F;
							CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;
							if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
							{
								ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageUDS);

								if( ret == CAN_OK)
								{
									(void)Can_m_FdTransmit(CANfd6);
								}
								else
								{
									if(Fifo_IsFull(&LPFifo) != True)
									{
										Fifo_Write(&LPFifo,&CANfd_MessageUDS);
									}
								}
							}
							else
							{
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x04)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x31)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0x01)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0xAA)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[4] == 0x02))
						{
							Led_Close();
							CANfd_MessageUDS.FrameData.U8Data[0] = 0x05;
							CANfd_MessageUDS.FrameData.U8Data[1] = 0x31;
							CANfd_MessageUDS.FrameData.U8Data[2] = 0x01;
							CANfd_MessageUDS.FrameData.U8Data[3] = 0xAA;
							CANfd_MessageUDS.FrameData.U8Data[4] = 0x02;
							CANfd_MessageUDS.FrameData.U8Data[5] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[6] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[7] = 0x00;
							CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
							CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
							CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
							CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
							CANfd_MessageUDS.Id = 0x72F;
							CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;
							if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
							{
								ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageUDS);

								if( ret == CAN_OK)
								{
									(void)Can_m_FdTransmit(CANfd6);
								}
								else
								{
									if(Fifo_IsFull(&LPFifo) != True)
									{
										Fifo_Write(&LPFifo,&CANfd_MessageUDS);
									}
								}
							}
							else
							{
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}


						}
						else if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x03)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x28)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0x01)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0x01))
						{
							Close_comFlag = 1;
							CANfd_MessageUDS.FrameData.U8Data[0] = 0x03;
							CANfd_MessageUDS.FrameData.U8Data[1] = 0x68;
							CANfd_MessageUDS.FrameData.U8Data[2] = 0x01;
							CANfd_MessageUDS.FrameData.U8Data[3] = 0x01;
							CANfd_MessageUDS.FrameData.U8Data[4] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[5] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[6] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[7] = 0x00;
							CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
							CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
							CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
							CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
							CANfd_MessageUDS.Id = 0x72F;
							CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;

							if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
							{
								ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageUDS);

								if( ret == CAN_OK)
								{

									(void)Can_m_FdTransmit(CANfd6);
								}
								else
								{
									if(Fifo_IsFull(&LPFifo) != True)
									{
										Fifo_Write(&LPFifo,&CANfd_MessageUDS);
									}
								}
							}
							else
							{
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x03)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x28)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0x03)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0x01))
						{
							Close_comFlag = 0;
							CANfd_MessageUDS.FrameData.U8Data[0] = 0x03;
							CANfd_MessageUDS.FrameData.U8Data[1] = 0x68;
							CANfd_MessageUDS.FrameData.U8Data[2] = 0x03;
							CANfd_MessageUDS.FrameData.U8Data[3] = 0x01;
							CANfd_MessageUDS.FrameData.U8Data[4] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[5] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[6] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[7] = 0x00;
							CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
							CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
							CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
							CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
							CANfd_MessageUDS.Id = 0x72F;
							CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;
							if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
							{
								ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageUDS);

								if( ret == CAN_OK)
								{
									(void)Can_m_FdTransmit(CANfd6);
								}
								else
								{
									if(Fifo_IsFull(&LPFifo) != True)
									{
										Fifo_Write(&LPFifo,&CANfd_MessageUDS);
									}
								}
							}
							else
							{
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else
						{}

					}
					if(Canfd_Sdu.Can_MailboxHeader[0].Id == 0x726)
					{
						if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x04)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x31)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0x01)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0xAA)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[4] == 0x01))
						{
							Led_Open();
							CANfd_MessageUDS.FrameData.U8Data[0] = 0x05;
							CANfd_MessageUDS.FrameData.U8Data[1] = 0x71;
							CANfd_MessageUDS.FrameData.U8Data[2] = 0x01;
							CANfd_MessageUDS.FrameData.U8Data[3] = 0xAA;
							CANfd_MessageUDS.FrameData.U8Data[4] = 0x01;
							CANfd_MessageUDS.FrameData.U8Data[5] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[6] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[7] = 0x00;
							CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
							CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
							CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
							CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
							CANfd_MessageUDS.Id = 0x72F;
							CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;
							if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
							{
								ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageUDS);

								if( ret == CAN_OK)
								{
									(void)Can_m_FdTransmit(CANfd6);
								}
								else
								{
									if(Fifo_IsFull(&LPFifo) != True)
									{
										Fifo_Write(&LPFifo,&CANfd_MessageUDS);
									}
								}
							}
							else
							{
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x04)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x31)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0x01)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0xAA)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[4] == 0x02))
						{
							Led_Close();
							CANfd_MessageUDS.FrameData.U8Data[0] = 0x05;
							CANfd_MessageUDS.FrameData.U8Data[1] = 0x71;
							CANfd_MessageUDS.FrameData.U8Data[2] = 0x01;
							CANfd_MessageUDS.FrameData.U8Data[3] = 0xAA;
							CANfd_MessageUDS.FrameData.U8Data[4] = 0x02;
							CANfd_MessageUDS.FrameData.U8Data[5] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[6] = 0x00;
							CANfd_MessageUDS.FrameData.U8Data[7] = 0x00;
							CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
							CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
							CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
							CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
							CANfd_MessageUDS.Id = 0x72F;
							CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;

							if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
							{
								ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageUDS);

								if( ret == CAN_OK)
								{
									(void)Can_m_FdTransmit(CANfd6);
								}
								else
								{
									if(Fifo_IsFull(&LPFifo) != True)
									{
										Fifo_Write(&LPFifo,&CANfd_MessageUDS);
									}
								}
							}
							else
							{
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x03)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x22)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0xf1)
								&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0x83))
						{
							count32 ++;
							for(i = 0; i< 1;i ++)
							{
								CANfd_MessageUDS.FrameData.U8Data[0] = i + 0x20;
								CANfd_MessageUDS.FrameData.U8Data[1] = 0x55;
								CANfd_MessageUDS.FrameData.U8Data[2] = 0x55;
								CANfd_MessageUDS.FrameData.U8Data[3] = 0x55;
								CANfd_MessageUDS.FrameData.U8Data[4] = (count32 & 0xFF000000)>>24;
								CANfd_MessageUDS.FrameData.U8Data[5] = (count32 & 0x00FF0000)>>16;
								CANfd_MessageUDS.FrameData.U8Data[6] = (count32 & 0x0000FF00)>>8;
								CANfd_MessageUDS.FrameData.U8Data[7] = (count32 & 0x000000FF);
								CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
								CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
								CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
								CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
								CANfd_MessageUDS.Id = 0x72F;

								CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x03)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x22)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0xf1)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0x84))
						{
							for(i = 0; i< 10;i ++)
							{
								CANfd_MessageUDS.FrameData.U8Data[0] = i + 0x30;
								CANfd_MessageUDS.FrameData.U8Data[1] = 0x66;
								CANfd_MessageUDS.FrameData.U8Data[2] = 0x66;
								CANfd_MessageUDS.FrameData.U8Data[3] = 0x66;
								CANfd_MessageUDS.FrameData.U8Data[4] = 0x66;
								CANfd_MessageUDS.FrameData.U8Data[5] = 0x66;
								CANfd_MessageUDS.FrameData.U8Data[6] = 0x66;
								CANfd_MessageUDS.FrameData.U8Data[7] = 0x66;
								CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
								CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
								CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
								CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
								CANfd_MessageUDS.Id = 0x72F;

								CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x03)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x22)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0xf1)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0x85))
						{
							for(i = 0; i< 10;i ++)
							{
								CANfd_MessageUDS.FrameData.U8Data[0] = i + 0x40;
								CANfd_MessageUDS.FrameData.U8Data[1] = 0x77;
								CANfd_MessageUDS.FrameData.U8Data[2] = 0x77;
								CANfd_MessageUDS.FrameData.U8Data[3] = 0x77;
								CANfd_MessageUDS.FrameData.U8Data[4] = 0x77;
								CANfd_MessageUDS.FrameData.U8Data[5] = 0x77;
								CANfd_MessageUDS.FrameData.U8Data[6] = 0x77;
								CANfd_MessageUDS.FrameData.U8Data[7] = 0x77;
								CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
								CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
								CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
								CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
								CANfd_MessageUDS.Id = 0x72F;

								CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x03)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x22)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0xf1)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0x86))
						{
							for(i = 0; i< 10;i ++)
							{
								CANfd_MessageUDS.FrameData.U8Data[0] = i + 0x50;
								CANfd_MessageUDS.FrameData.U8Data[1] = 0x88;
								CANfd_MessageUDS.FrameData.U8Data[2] = 0x88;
								CANfd_MessageUDS.FrameData.U8Data[3] = 0x88;
								CANfd_MessageUDS.FrameData.U8Data[4] = 0x88;
								CANfd_MessageUDS.FrameData.U8Data[5] = 0x88;
								CANfd_MessageUDS.FrameData.U8Data[6] = 0x88;
								CANfd_MessageUDS.FrameData.U8Data[7] = 0x88;
								CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
								CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
								CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
								CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
								CANfd_MessageUDS.Id = 0x72F;

								CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else if((Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[0] == 0x03)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[1] == 0x22)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[2] == 0xf1)
							&&(Canfd_Sdu.Can_MailboxHeader[0].FrameData.U8Data[3] == 0x87))
						{
							for(i = 0; i< 10;i ++)
							{
								CANfd_MessageUDS.FrameData.U8Data[0] = i + 0x60;
								CANfd_MessageUDS.FrameData.U8Data[1] = 0x99;
								CANfd_MessageUDS.FrameData.U8Data[2] = 0x99;
								CANfd_MessageUDS.FrameData.U8Data[3] = 0x99;
								CANfd_MessageUDS.FrameData.U8Data[4] = 0x99;
								CANfd_MessageUDS.FrameData.U8Data[5] = 0x99;
								CANfd_MessageUDS.FrameData.U8Data[6] = 0x99;
								CANfd_MessageUDS.FrameData.U8Data[7] = 0x99;
								CANfd_MessageUDS.FrameData.Data_Length = Data_Length_8;
								CANfd_MessageUDS.Can_frame = CAN_FRAME_FD;
								CANfd_MessageUDS.Can_id = CAN_DATA_STANDARD;
								CANfd_MessageUDS.BRS = CANFD_BRS_ENABLE;
								CANfd_MessageUDS.Id = 0x72F;

								CANfd_MessageUDS.TransceiveType = MAIL_TRANSMIT;
								if(Fifo_IsFull(&LPFifo) != True)
								{
									Fifo_Write(&LPFifo,&CANfd_MessageUDS);
								}
							}

						}
						else
						{}
					}
					else
					{}
				}
			}
		}
	}
}

uint32_t aa = 0;

void send10data(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		aa++;
		CANfd_MessageO.FrameData.U8Data[0] = 0x11;
		CANfd_MessageO.FrameData.U8Data[1] = 0x11;
		CANfd_MessageO.FrameData.U8Data[2] = 0x11;
		CANfd_MessageO.FrameData.U8Data[3] = 0x11;
		CANfd_MessageO.FrameData.U8Data[4] = (aa&0xFF000000) >> 24;
		CANfd_MessageO.FrameData.U8Data[5] = (aa&0x0000FF00) >> 16;
		CANfd_MessageO.FrameData.U8Data[6] = (aa&0x0000FF00) >> 8;
		CANfd_MessageO.FrameData.U8Data[7] = (aa&0x000000FF) ;
		CANfd_MessageO.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageO.Can_frame = CAN_FRAME_FD;
		CANfd_MessageO.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageO.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageO.Id = 0x511;
		CANfd_MessageO.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageO);
			if( ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageO);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageO);
			}
		}

	}
}

void send10data_1(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageO.FrameData.U8Data[0] = 0x12;
		CANfd_MessageO.FrameData.U8Data[1] = 0x12;
		CANfd_MessageO.FrameData.U8Data[2] = 0x12;
		CANfd_MessageO.FrameData.U8Data[3] = 0x12;
		CANfd_MessageO.FrameData.U8Data[4] = 0x12;
		CANfd_MessageO.FrameData.U8Data[5] = 0x12;
		CANfd_MessageO.FrameData.U8Data[6] = 0x12;
		CANfd_MessageO.FrameData.U8Data[7] = 0x12;
		CANfd_MessageO.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageO.Can_frame = CAN_FRAME_FD;
		CANfd_MessageO.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageO.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageO.Id = 0x512;
		CANfd_MessageO.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageO);

			if( ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageO);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageO);
			}
		}
	}
}

void send10data_2(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageO.FrameData.U8Data[0] = 0x13;
		CANfd_MessageO.FrameData.U8Data[1] = 0x13;
		CANfd_MessageO.FrameData.U8Data[2] = 0x13;
		CANfd_MessageO.FrameData.U8Data[3] = 0x13;
		CANfd_MessageO.FrameData.U8Data[4] = 0x13;
		CANfd_MessageO.FrameData.U8Data[5] = 0x13;
		CANfd_MessageO.FrameData.U8Data[6] = 0x13;
		CANfd_MessageO.FrameData.U8Data[7] = 0x13;
		CANfd_MessageO.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageO.Can_frame = CAN_FRAME_FD;
		CANfd_MessageO.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageO.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageO.Id = 0x513;
		CANfd_MessageO.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageO);

			if( ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageO);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageO);
			}
		}
	}
}

void send10data_3(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageO.FrameData.U8Data[0] = 0x14;
		CANfd_MessageO.FrameData.U8Data[1] = 0x14;
		CANfd_MessageO.FrameData.U8Data[2] = 0x14;
		CANfd_MessageO.FrameData.U8Data[3] = 0x14;
		CANfd_MessageO.FrameData.U8Data[4] = 0x14;
		CANfd_MessageO.FrameData.U8Data[5] = 0x14;
		CANfd_MessageO.FrameData.U8Data[6] = 0x14;
		CANfd_MessageO.FrameData.U8Data[7] = 0x14;
		CANfd_MessageO.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageO.Can_frame = CAN_FRAME_FD;
		CANfd_MessageO.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageO.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageO.Id = 0x514;
		CANfd_MessageO.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageO);

			if( ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageO);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageO);
			}
		}
	}
}

void send20data(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageE.FrameData.U8Data[0] = 0x20;
		CANfd_MessageE.FrameData.U8Data[1] = 0x20;
		CANfd_MessageE.FrameData.U8Data[2] = 0x20;
		CANfd_MessageE.FrameData.U8Data[3] = 0x20;
		CANfd_MessageE.FrameData.U8Data[4] = 0x20;
		CANfd_MessageE.FrameData.U8Data[5] = 0x20;
		CANfd_MessageE.FrameData.U8Data[6] = 0x20;
		CANfd_MessageE.FrameData.U8Data[7] = 0x20;
		CANfd_MessageE.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageE.Can_frame = CAN_FRAME_FD;
		CANfd_MessageE.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageE.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageE.Id = 0x520;
		CANfd_MessageE.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageE);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageE);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageE);
			}
		}

	}
}

void send20data_1(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageE.FrameData.U8Data[0] = 0x21;
		CANfd_MessageE.FrameData.U8Data[1] = 0x21;
		CANfd_MessageE.FrameData.U8Data[2] = 0x21;
		CANfd_MessageE.FrameData.U8Data[3] = 0x21;
		CANfd_MessageE.FrameData.U8Data[4] = 0x21;
		CANfd_MessageE.FrameData.U8Data[5] = 0x21;
		CANfd_MessageE.FrameData.U8Data[6] = 0x21;
		CANfd_MessageE.FrameData.U8Data[7] = 0x21;
		CANfd_MessageE.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageE.Can_frame = CAN_FRAME_FD;
		CANfd_MessageE.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageE.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageE.Id = 0x521;
		CANfd_MessageE.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageE);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageE);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageE);
			}
		}
	}
}

void send20data_2(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageE.FrameData.U8Data[0] = 0x22;
		CANfd_MessageE.FrameData.U8Data[1] = 0x22;
		CANfd_MessageE.FrameData.U8Data[2] = 0x22;
		CANfd_MessageE.FrameData.U8Data[3] = 0x22;
		CANfd_MessageE.FrameData.U8Data[4] = 0x22;
		CANfd_MessageE.FrameData.U8Data[5] = 0x22;
		CANfd_MessageE.FrameData.U8Data[6] = 0x22;
		CANfd_MessageE.FrameData.U8Data[7] = 0x22;
		CANfd_MessageE.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageE.Can_frame = CAN_FRAME_FD;
		CANfd_MessageE.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageE.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageE.Id = 0x522;
		CANfd_MessageE.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageE);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageE);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageE);
			}
		}

	}
}

void send20data_3(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageE.FrameData.U8Data[0] = 0x23;
		CANfd_MessageE.FrameData.U8Data[1] = 0x23;
		CANfd_MessageE.FrameData.U8Data[2] = 0x23;
		CANfd_MessageE.FrameData.U8Data[3] = 0x23;
		CANfd_MessageE.FrameData.U8Data[4] = 0x23;
		CANfd_MessageE.FrameData.U8Data[5] = 0x23;
		CANfd_MessageE.FrameData.U8Data[6] = 0x23;
		CANfd_MessageE.FrameData.U8Data[7] = 0x23;
		CANfd_MessageE.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageE.Can_frame = CAN_FRAME_FD;
		CANfd_MessageE.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageE.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageE.Id = 0x523;
		CANfd_MessageE.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageE);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageE);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageE);
			}
		}

	}
}

void send50data(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageTE.FrameData.U8Data[0] = 0x50;
		CANfd_MessageTE.FrameData.U8Data[1] = 0x50;
		CANfd_MessageTE.FrameData.U8Data[2] = 0x50;
		CANfd_MessageTE.FrameData.U8Data[3] = 0x50;
		CANfd_MessageTE.FrameData.U8Data[4] = 0x50;
		CANfd_MessageTE.FrameData.U8Data[5] = 0x50;
		CANfd_MessageTE.FrameData.U8Data[6] = 0x50;
		CANfd_MessageTE.FrameData.U8Data[7] = 0x50;
		CANfd_MessageTE.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageTE.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageTE.Can_frame = CAN_FRAME_FD;
		CANfd_MessageTE.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageTE.Id = 0x550;
		CANfd_MessageTE.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageTE);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageTE);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageTE);
			}
		}
	}
}

void send50data_1(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageTE.FrameData.U8Data[0] = 0x51;
		CANfd_MessageTE.FrameData.U8Data[1] = 0x51;
		CANfd_MessageTE.FrameData.U8Data[2] = 0x51;
		CANfd_MessageTE.FrameData.U8Data[3] = 0x51;
		CANfd_MessageTE.FrameData.U8Data[4] = 0x51;
		CANfd_MessageTE.FrameData.U8Data[5] = 0x51;
		CANfd_MessageTE.FrameData.U8Data[6] = 0x51;
		CANfd_MessageTE.FrameData.U8Data[7] = 0x51;
		CANfd_MessageTE.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageTE.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageTE.Can_frame = CAN_FRAME_FD;
		CANfd_MessageTE.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageTE.Id = 0x551;
		CANfd_MessageTE.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageTE);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageTE);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageTE);
			}
		}

	}
}

void send50data_2(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageTE.FrameData.U8Data[0] = 0x52;
		CANfd_MessageTE.FrameData.U8Data[1] = 0x52;
		CANfd_MessageTE.FrameData.U8Data[2] = 0x52;
		CANfd_MessageTE.FrameData.U8Data[3] = 0x52;
		CANfd_MessageTE.FrameData.U8Data[4] = 0x52;
		CANfd_MessageTE.FrameData.U8Data[5] = 0x52;
		CANfd_MessageTE.FrameData.U8Data[6] = 0x52;
		CANfd_MessageTE.FrameData.U8Data[7] = 0x52;
		CANfd_MessageTE.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageTE.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageTE.Can_frame = CAN_FRAME_FD;
		CANfd_MessageTE.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageTE.Id = 0x552;
		CANfd_MessageTE.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageTE);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageTE);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageTE);
			}
		}

	}
}

void send50data_3(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageTE.FrameData.U8Data[0] = 0x53;
		CANfd_MessageTE.FrameData.U8Data[1] = 0x53;
		CANfd_MessageTE.FrameData.U8Data[2] = 0x53;
		CANfd_MessageTE.FrameData.U8Data[3] = 0x53;
		CANfd_MessageTE.FrameData.U8Data[4] = 0x53;
		CANfd_MessageTE.FrameData.U8Data[5] = 0x53;
		CANfd_MessageTE.FrameData.U8Data[6] = 0x53;
		CANfd_MessageTE.FrameData.U8Data[7] = 0x53;
		CANfd_MessageTE.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageTE.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageTE.Can_frame = CAN_FRAME_FD;
		CANfd_MessageTE.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageTE.Id = 0x553;
		CANfd_MessageTE.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageTE);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageTE);
				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageTE);
			}
		}

	}
}

void send100data(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageS.FrameData.U8Data[0] = 0xA0;
		CANfd_MessageS.FrameData.U8Data[1] = 0xA0;
		CANfd_MessageS.FrameData.U8Data[2] = 0xA0;
		CANfd_MessageS.FrameData.U8Data[3] = 0xA0;
		CANfd_MessageS.FrameData.U8Data[4] = 0xA0;
		CANfd_MessageS.FrameData.U8Data[5] = 0xA0;
		CANfd_MessageS.FrameData.U8Data[6] = 0xA0;
		CANfd_MessageS.FrameData.U8Data[7] = 0xA0;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5A0;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}

	}
}

void send100data_1(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageS.FrameData.U8Data[0] = 0xA1;
		CANfd_MessageS.FrameData.U8Data[1] = 0xA1;
		CANfd_MessageS.FrameData.U8Data[2] = 0xA1;
		CANfd_MessageS.FrameData.U8Data[3] = 0xA1;
		CANfd_MessageS.FrameData.U8Data[4] = 0xA1;
		CANfd_MessageS.FrameData.U8Data[5] = 0xA1;
		CANfd_MessageS.FrameData.U8Data[6] = 0xA1;
		CANfd_MessageS.FrameData.U8Data[7] = 0xA1;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5A1;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}

	}
}

void send100data_2(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageS.FrameData.U8Data[0] = 0xA2;
		CANfd_MessageS.FrameData.U8Data[1] = 0xA2;
		CANfd_MessageS.FrameData.U8Data[2] = 0xA2;
		CANfd_MessageS.FrameData.U8Data[3] = 0xA2;
		CANfd_MessageS.FrameData.U8Data[4] = 0xA2;
		CANfd_MessageS.FrameData.U8Data[5] = 0xA2;
		CANfd_MessageS.FrameData.U8Data[6] = 0xA2;
		CANfd_MessageS.FrameData.U8Data[7] = 0xA2;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5A2;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}

	}
}

void send100data_3(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageS.FrameData.U8Data[0] = 0xA3;
		CANfd_MessageS.FrameData.U8Data[1] = 0xA3;
		CANfd_MessageS.FrameData.U8Data[2] = 0xA3;
		CANfd_MessageS.FrameData.U8Data[3] = 0xA3;
		CANfd_MessageS.FrameData.U8Data[4] = 0xA3;
		CANfd_MessageS.FrameData.U8Data[5] = 0xA3;
		CANfd_MessageS.FrameData.U8Data[6] = 0xA3;
		CANfd_MessageS.FrameData.U8Data[7] = 0xA3;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5A3;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}

	}
}

void send100data_4(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;
	if(Close_comFlag == 0)
	{
		CANfd_MessageS.FrameData.U8Data[0] = 0xA4;
		CANfd_MessageS.FrameData.U8Data[1] = 0xA4;
		CANfd_MessageS.FrameData.U8Data[2] = 0xA4;
		CANfd_MessageS.FrameData.U8Data[3] = 0xA4;
		CANfd_MessageS.FrameData.U8Data[4] = 0xA4;
		CANfd_MessageS.FrameData.U8Data[5] = 0xA4;
		CANfd_MessageS.FrameData.U8Data[6] = 0xA4;
		CANfd_MessageS.FrameData.U8Data[7] = 0xA4;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5A4;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}

	}
}

void send100data_6(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;

		CANfd_MessageS.FrameData.U8Data[0] = 0xA6;
		CANfd_MessageS.FrameData.U8Data[1] = 0xA6;
		CANfd_MessageS.FrameData.U8Data[2] = 0xA6;
		CANfd_MessageS.FrameData.U8Data[3] = 0xA6;
		CANfd_MessageS.FrameData.U8Data[4] = 0xA6;
		CANfd_MessageS.FrameData.U8Data[5] = 0xA6;
		CANfd_MessageS.FrameData.U8Data[6] = 0xA6;
		CANfd_MessageS.FrameData.U8Data[7] = 0xA6;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5A6;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}
}

void send100data_7(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;

		CANfd_MessageS.FrameData.U8Data[0] = 0xA7;
		CANfd_MessageS.FrameData.U8Data[1] = 0xA7;
		CANfd_MessageS.FrameData.U8Data[2] = 0xA7;
		CANfd_MessageS.FrameData.U8Data[3] = 0xA7;
		CANfd_MessageS.FrameData.U8Data[4] = 0xA7;
		CANfd_MessageS.FrameData.U8Data[5] = 0xA7;
		CANfd_MessageS.FrameData.U8Data[6] = 0xA7;
		CANfd_MessageS.FrameData.U8Data[7] = 0xA7;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5A7;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}
}

void send100data_8(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;

		CANfd_MessageS.FrameData.U8Data[0] = 0xA8;
		CANfd_MessageS.FrameData.U8Data[1] = 0xA8;
		CANfd_MessageS.FrameData.U8Data[2] = 0xA8;
		CANfd_MessageS.FrameData.U8Data[3] = 0xA8;
		CANfd_MessageS.FrameData.U8Data[4] = 0xA8;
		CANfd_MessageS.FrameData.U8Data[5] = 0xA8;
		CANfd_MessageS.FrameData.U8Data[6] = 0xA8;
		CANfd_MessageS.FrameData.U8Data[7] = 0xA8;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5A8;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}
}

void send100data_9(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;


		CANfd_MessageS.FrameData.U8Data[0] = 0xA9;
		CANfd_MessageS.FrameData.U8Data[1] = 0xA9;
		CANfd_MessageS.FrameData.U8Data[2] = 0xA9;
		CANfd_MessageS.FrameData.U8Data[3] = 0xA9;
		CANfd_MessageS.FrameData.U8Data[4] = 0xA9;
		CANfd_MessageS.FrameData.U8Data[5] = 0xA9;
		CANfd_MessageS.FrameData.U8Data[6] = 0xA9;
		CANfd_MessageS.FrameData.U8Data[7] = 0xA9;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5A9;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}
}

void send100data_A(void)
{
	Can_ReturnType ret = CAN_OK;
	volatile Kf32a_Canfd_Reg *ControllerRegPtr = (Kf32a_Canfd_Reg *)Can_m_ControllersInfo[1].BaseAddress;

		CANfd_MessageS.FrameData.U8Data[0] = 0xAA;
		CANfd_MessageS.FrameData.U8Data[1] = 0xAA;
		CANfd_MessageS.FrameData.U8Data[2] = 0xAA;
		CANfd_MessageS.FrameData.U8Data[3] = 0xAA;
		CANfd_MessageS.FrameData.U8Data[4] = 0xAA;
		CANfd_MessageS.FrameData.U8Data[5] = 0xAA;
		CANfd_MessageS.FrameData.U8Data[6] = 0xAA;
		CANfd_MessageS.FrameData.U8Data[7] = 0xAA;
		CANfd_MessageS.BRS = CANFD_BRS_ENABLE;
		CANfd_MessageS.FrameData.Data_Length = Data_Length_8;
		CANfd_MessageS.Can_frame = CAN_FRAME_FD;
		CANfd_MessageS.Can_id = CAN_DATA_STANDARD;
		CANfd_MessageS.Id = 0x5AA;
		CANfd_MessageS.TransceiveType = MAIL_TRANSMIT;
		if(Can_m_FdGetMailBoxState(CANfd6,Mailbox_0) == MAIL_TRANSMIT_SUCCESS)
		{
			delay_num = 0xFF;
			while((ControllerRegPtr->CANFD_CTLR0.B.TCSTA == 0) && (delay_num--));
			ret = Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &CANfd_MessageS);
			if(ret == CAN_OK)
			{
				(void)Can_m_FdTransmit(CANfd6);
			}
			else
			{
				if(Fifo_IsFull(&LPFifo) != True)
				{
					Fifo_Write(&LPFifo,&CANfd_MessageS);

				}
			}
		}
		else
		{
			if(Fifo_IsFull(&LPFifo) != True)
			{
				Fifo_Write(&LPFifo,&CANfd_MessageS);

			}
		}
}
