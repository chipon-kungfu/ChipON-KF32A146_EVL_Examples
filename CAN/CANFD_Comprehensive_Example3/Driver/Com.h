/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Com.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : 
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef COM_H_
#define COM_H_

extern void Com_uds(void);

extern uint8_t Close_comFlag;

extern void send10data(void);
extern void send10data_1(void);
extern void send10data_2(void);
extern void send10data_3(void);

extern void send20data(void);
extern void send20data_1(void);
extern void send20data_2(void);
extern void send20data_3(void);

extern void send50data(void);
extern void send50data_1(void);
extern void send50data_2(void);
extern void send50data_3(void);

extern void send100data(void);
extern void send100data_1(void);
extern void send100data_2(void);
extern void send100data_3(void);
extern void send100data_4(void);
extern void send100data_6(void);
extern void send100data_7(void);
extern void send100data_8(void);
extern void send100data_9(void);
extern void send100data_A(void);


#endif /* COM_H_ */
