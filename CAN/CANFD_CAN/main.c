/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a KF32A156 device StdPeriph Template
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#include "system_init.h"
#include "Canfd.h"
static void             Led_Gpio_Init(void);
static void             Led_Flip(void);
uint32_t                Receive_Flag                = 0x00;
Canfd_MailboxHeaderType Receive_Canfd_MailboxHeader = {0};

static void Led_Gpio_Init(void)
{
    /*Configure the GPIO PF11 as IO port output mode */
    GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, GPIO_MODE_OUT);

    GPIO_Set_Output_Data_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, Bit_RESET);
}

/**
 *  @brief :
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
static void Led_Flip(void)
{
    GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_11);
}

/*
 * 500K  BAUDRATE
 * */
int main()
{
    /* Initialize the system clock */
    SystemInit(72U);
    /* Setup SysTick Timer as delay function */
    systick_delay_init(72U);
    Canfd_Gpio_Init();
    Led_Gpio_Init();

	/* Configure interrupt priority group, default is 3VS1 */
    INT_Priority_Group_Config(INT_PRIORITY_GROUP_3VS1);
    Can_m_FdControllerDeInit(CANfd6);
    Can_m_FdControllerInit(CANfd6, Canfd_Controller_AllConfig, Initindex_0);

    /* Initialize Receveive Mailbox */
    Receive_Canfd_MailboxHeader.TransceiveType = MAIL_RECEIVE;
    Can_m_FdMailBox_Write(CANfd6, Mailbox_0, &Receive_Canfd_MailboxHeader);
	/*Enable all interrupt*/
	INT_All_Enable(TRUE);
    while (1)
    {
        /* Initialize Transmit Mailbox */
        if (Can_m_FdMailBox_Write(CANfd6, Mailbox_1, &Canfd_Sdu.Can_MailboxHeader[0]) == CAN_OK)
        {
            /* Enable Transmit */
            (void)Can_m_FdTransmit(CANfd6);
        }

        if (Receive_Flag > 0x00)
        {
            Receive_Canfd_MailboxHeader.Id             = 0x110;
            Receive_Canfd_MailboxHeader.TransceiveType = MAIL_TRANSMIT;
            if (Can_m_FdMailBox_Write(CANfd6, Mailbox_2, &Receive_Canfd_MailboxHeader) == CAN_OK)
            {
                /* Enable Transmit */
                (void)Can_m_FdTransmit(CANfd6);
                Receive_Flag--;
            }
        }
        systick_delay_ms(250);
        Led_Flip();
    }
}
