/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Can.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of CAN
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef _CAN_H_
#define _CAN_H_
/******************************************************************************
**                       	Include Files                                     *
******************************************************************************/
#include <stdint.h>
#include "kf32a1x6_can.h"

/******************************************************************************
*                        Configuration definition                             *
******************************************************************************/
#define BAUDRATE100K    (0U)
#define BAUDRATE250K    (1U)
#define BAUDRATE500K    (2U)

#define BAUDRATE    BAUDRATE500K

/******************************************************************************
*                        Type  definitions                                    *
******************************************************************************/
#define INTERRUPT_ENABLE   1
#define INTERRUPT_DISABLE  0


typedef struct
{
	CAN_MessageTypeDef CAN_Message[64];
	uint8_t Frame_length;
}Can_Pdu_TypeDef;

typedef struct
{
	uint32_t CAN_TRANSMIT_INTERRUPT;
	uint32_t CAN_RECEIVE_INTERRUPT;
	uint32_t CAN_OVERFLOW_INTERRUPT;
	uint32_t CAN_BUSERROR_INTERRUPT;
	uint32_t CAN_BUSOFF_INTERRUPT;
	uint32_t CAN_ERRORNEGATIVE_INTERRUPT;
}Can_Interrupt_Type;

/******************************************************************************
*                      Functional definition                                 *
******************************************************************************/
void CAN_Gpio_Init(void);
void CAN_Init(CAN_SFRmap* CANx, CAN_InitTypeDef* canInitStruct);
RetStatus CAN_Transmit_Message_Once(volatile CAN_MessageTypeDef *CAN_Message);
RetStatus CAN_Transmit_Message_Repeat(volatile CAN_MessageTypeDef *CAN_Message);
RetStatus CAN_Receive_Message(volatile Can_Pdu_TypeDef* Pdu);
void CAN_Int_Config(Can_Interrupt_Type *Can_Interrupt);
#endif

