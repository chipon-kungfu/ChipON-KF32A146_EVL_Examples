/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Canfd.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the reference code of CANFD
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef _CANFD_H
#define _CANFD_H
/******************************************************************************
**                       	Include Files                                     *
******************************************************************************/
#include <stdint.h>
#include "kf32a1x6_canfd.h"
/******************************************************************************
 *                        Type  definitions                                    *
 ******************************************************************************/
extern Can_ControllerConfigType Canfd_Controller_AllConfig[1];
extern Canfd_Sdu_Type           Canfd_Sdu;
/******************************************************************************
 *                      Functional definition                                 *
 ******************************************************************************/
void Canfd_Gpio_Init(void);
#endif
