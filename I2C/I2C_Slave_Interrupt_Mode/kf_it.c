/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "I2C.h"

extern void             I2C_receive_input(uint8_t Rev_Temp);
extern volatile uint8_t I2C_RecNum;
/* Variables that receive I2C data */
volatile uint32_t i2c_data_temp;
/*******************************************************************************
**                   KF32A156 Processor Exceptions Handlers
*******************************************************************************/

//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception(void) {}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _HardFault_exception(void) {}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception(void) {}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception(void) {}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception(void) {}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception(void) {}
//*****************************************************************************************
//                                 SPI1中断函数
//*****************************************************************************************
void __attribute__((interrupt)) _I2C0_exception(void)
{

    volatile uint32_t I2C0_DATA   = 0;
    volatile uint8_t  I2C0_REDATA = 0;
    volatile uint32_t I2C0_ADD    = 0;
    i2c_data_temp                 = 0;

    /* Clear the I2C0 interrupt flag bit */
    I2C_Clear_INTERRUPT_Flag(I2C0_SFR);
    /* Reply ACK */
    I2C_Ack_DATA_Config(I2C0_SFR, I2C_ACKDATA_ACK);

#if (I2C_ADDRESS_WIDTH == I2C_10BIT)
    if (I2C_Get_Write_Read_Flag(I2C0_SFR))
    {
        I2C_SendData(I2C0_SFR, I2C0_DATA);
    }
    else
    {
        if (I2C_Get_HighAddress_Flag(I2C0_SFR))
        { /* High address, skip */
            ;
        }
        else
        { /* Not high address, perform other actions */
            i2c_data_temp = I2C_ReceiveData(I2C0_SFR);
            if (I2C_Get_Data_Flag(I2C0_SFR))
            {
                /* Read data */
                I2C_receive_input(i2c_data_temp);
                I2C0_DATA = i2c_data_temp;
            }
            else
            {
                I2C0_ADD   = i2c_data_temp;
                I2C_RecNum = 0;
            }
        }
    }
#else
    if (I2C_Get_Write_Read_Flag(I2C0_SFR))
    { /* The host reads the status bit, and sends from the slave */
        I2C_SendData(I2C0_SFR, I2C0_DATA);
    }
    else
    { /* Host write status bit, slave receive */
        i2c_data_temp = I2C_ReceiveData(I2C0_SFR);

        if (I2C_Get_Data_Flag(I2C0_SFR))
        {
            I2C0_REDATA = i2c_data_temp & 0xff;
            I2C_receive_input(I2C0_REDATA);
        }
        else
        {
            I2C0_ADD   = i2c_data_temp;
            I2C_RecNum = 0;
        }
    }
#endif
}
