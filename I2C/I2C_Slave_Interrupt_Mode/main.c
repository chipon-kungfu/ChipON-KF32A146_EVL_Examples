/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use I2C as Slave in
 *                      interrupt mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "I2C.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/
/* I2C slave 7-bit address to be read and written by the host */
#define I2C_SLAVE_ADDR 0xA0
/* I2C slave 10-bit address to be read and written by the host */
#define I2C_SLAVE_ADDR10B 0x3A0
/*******************************************************************************
**                          Private Variables Definitions
*******************************************************************************/
volatile uint8_t I2C_RecNum          = 0;
uint8_t          i2c_buffer_read[64] = {0};

uint8_t i2c_buffer_write[] = {
  0X5A, 0X22, 0X33, 0X44, 0X66, 0X77, 0X88, 0X99, 0XAA, 0XBB, 0X01, 0X02, 0X03, 0X04, 0X05, 0X06,
  0X07, 0X08, 0X09, 0X10, 0X11, 0X12, 0X13, 0X14, 0X15, 0X16, 0X17, 0X18, 0X19, 0X20, 0X21, 0X22,
  0X23, 0X24, 0X25, 0X26, 0X27, 0X28, 0X29, 0X30, 0X31, 0X32, 0X33, 0X34, 0X35, 0X36, 0X37, 0X38,
  0X39, 0X40, 0X41, 0X42, 0X43, 0X44, 0X45, 0X46, 0X47, 0X48, 0X49, 0X50, 0X51, 0X52, 0X53, 0XA5};
uint8_t i2c_buffer_read_test[] = {
  0X5A, 0X22, 0X33, 0X44, 0X66, 0X77, 0X88, 0X99, 0XAA, 0XBB, 0X01, 0X02, 0X03, 0X04, 0X05, 0X06,
  0X07, 0X08, 0X09, 0X10, 0X11, 0X12, 0X13, 0X14, 0X15, 0X16, 0X17, 0X18, 0X19, 0X20, 0X21, 0X22,
  0X23, 0X24, 0X25, 0X26, 0X27, 0X28, 0X29, 0X30, 0X31, 0X32, 0X33, 0X34, 0X35, 0X36, 0X37, 0X38,
  0X39, 0X40, 0X41, 0X42, 0X43, 0X44, 0X45, 0X46, 0X47, 0X48, 0X49, 0X50, 0X51, 0X52, 0X53, 0XA5};
/*******************************************************************************
**                                 Global Functions
*******************************************************************************/
/**
 *  @brief: I2C receive data
 *  @param[in] Rev_Temp
 *  @param[out] None
 *  @retval : None
 */
void I2C_receive_input(uint8_t Rev_Temp)
{
    if (I2C_RecNum < sizeof(i2c_buffer_read))
    {
        i2c_buffer_read[I2C_RecNum] = Rev_Temp;
        I2C_RecNum++;
        if (I2C_RecNum == sizeof(i2c_buffer_read))
        {
            I2C_RecNum = 0;
        }
    }
}

/**
 *  @brief: I2C receive data test
 *  @param[in] None
 *  @param[out] None
 *  @retval : None
 */
uint8_t I2C_Data_test(void)
{

    volatile uint16_t i;
    for (i = 0; i < 64; i++)
    {
        if (i2c_buffer_read[i] != i2c_buffer_read_test[i])
        {
            return RESET;
        }
    }

    /* Empty array */
    memset(i2c_buffer_read, 0, sizeof(i2c_buffer_read));
    return SET;
}
/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main(void)
{
    /* System clock configuration */
    SystemInit(72U);
    systick_delay_init(72U);

    /* LED control */
    GPIOInit_Output_Config(PA3_LED1_PIN);

#if (I2C_ADDRESS_WIDTH == I2C_10BIT)
    /* I2C initialization */
    I2c_Init(I2C0_SFR, I2C_SLAVE_ADDR10B);
#else
    /* I2C initialization */
    I2c_Init(I2C0_SFR, I2C_SLAVE_ADDR);
#endif
    /* Configure IO */
    I2c_IO_Init();
    /* Interrupt enable */
    I2c_Interrupt_Init();

    while (1)
    {
        /* Determine whether the read I2C array matches the preset data */
        if (1U == I2C_Data_test())
        {
            /* LED1 flashing */
            GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
            systick_delay_ms(200);
        }
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
