/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : I2C.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the I2C configuration for KF32A156
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                             Include Files
******************************************************************************/
#include "I2C.h"
#include "Board_GpioCfg.h"

/*****************************************************************************
**                         Private Macro Definitions
*****************************************************************************/

/*****************************************************************************
**                         Private Variables Definitions
*****************************************************************************/
I2C_SFRmap *I2C_Choose;
/*****************************************************************************
**                             Private Functions
*****************************************************************************/

/*****************************************************************************
**                             Global Functions
*****************************************************************************/
/**
 *  @brief: Initialization I2C IO
 *  @param in: None
 *  @param[out] None
 *  @retval : None
 */
void I2c_IO_Init()
{
    GPIO_Write_Mode_Bits(PC0_I2C0_SCL_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PC0_I2C0_SCL_AF);
    GPIO_Open_Drain_Enable(PC0_I2C0_SCL_PIN, GPIO_POD_OD);
    GPIO_Pull_Up_Enable(PC0_I2C0_SCL_PIN, TRUE);

    GPIO_Write_Mode_Bits(PG4_I2C0_SDA_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PG4_I2C0_SDA_AF);
    GPIO_Open_Drain_Enable(PG4_I2C0_SDA_PIN, GPIO_POD_OD);
    GPIO_Pull_Up_Enable(PG4_I2C0_SDA_PIN, TRUE);
}

/**
 *  @brief: Initialization I2C HW module
 *  @param in: I2Cx
 *             I2c_Address
 *  @param[out] None
 *  @retval : None
 */
void I2c_Init(I2C_SFRmap *I2cx, uint16_t I2c_Address)
{
    I2C_InitTypeDef I2cConfigPtr;

    /* I2c mode */
    I2cConfigPtr.m_Mode = I2C_MODE_I2C;
    /* I2c clock */
    I2cConfigPtr.m_ClockSource = I2C_CLK_HFCLK;
/* Configure slave address width */
#if (I2C_ADDRESS_WIDTH == I2C_7BIT)
    I2cConfigPtr.m_BADR10 = I2C_BUFRADDRESS_7BIT;
#else
    I2cConfigPtr.m_BADR10 = I2C_BUFRADDRESS_10BIT;
#endif
    /* SMBus type */
    I2cConfigPtr.m_MasterSlave = I2C_MODE_SMBUSDEVICE;
    /* I2c baud rate low level time */
    I2cConfigPtr.m_BaudRateL = 15;
    /* I2c baud rate high level time */
    I2cConfigPtr.m_BaudRateH = 15;
    /* Enable ACK */
    I2cConfigPtr.m_AckEn = TRUE;
    /* Select the response signal as ACK */
    I2cConfigPtr.m_AckData = I2C_ACKDATA_ACK;

    /* I2C reset */
    I2C_Reset(I2cx);
    /* Configue I2C */
    I2C_Configuration(I2cx, &I2cConfigPtr);

    /* Set slave address */
    I2C_ADDR_Config(I2cx, 0, I2c_Address);
    I2C_ADDR_Config(I2cx, 1, I2c_Address);
    I2C_ADDR_Config(I2cx, 2, I2c_Address);
    I2C_ADDR_Config(I2cx, 3, I2c_Address);
    /* Configure I2C address mask bit */
    I2C_MSK_Config(I2cx, 0, 0x0000);

    /* Clear Interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);

    /* Enable I2C address match */
    I2C_MATCH_ADDRESS_Config(I2cx, TRUE);

    /* Enable I2C HW module */
    I2C_Cmd(I2cx, TRUE);
}

/**
 *  @brief: nitialization I2C Interrupt
 *  @param in: None
 *  @param[out] None
 *  @retval : None
 */
void I2c_Interrupt_Init()
{
    /* Enable I2c receive interrupt */
    INT_Interrupt_Enable(INT_I2C0, TRUE);
    /* Enable I2c interrupt */
    I2C_ISIE_INT_Enable(I2C0_SFR, TRUE);
    INT_All_Enable(TRUE);
}
