/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : I2C.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A156-MINI-EVB_V1.2
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the I2Cx configuration for KF32A156
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#ifndef _I2C_H_
#define _I2C_H_
/******************************************************************************
**                             Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"

/*****************************************************************************
**                         Private Macro Definitions                        **
*****************************************************************************/
#define I2C_7BIT  (0U)
#define I2C_10BIT (1U)

#define I2C_ADDRESS_WIDTH (I2C_7BIT)

/* I2C slave 7-bit address to be read and written by the host */
#define I2C_SLAVE_ADDR 0xA0
/* I2C slave 10-bit address to be read and written by the host */
#define I2C_SLAVE_ADDR10B 0xF6A0

/* I2C wait time for time out */
#define I2C_WAIT_TIME (0xFFFFU)

/*****************************************************************************
**                         Private Variables Definitions                    **
*****************************************************************************/

/*****************************************************************************
**                             Private Functions                            **
*****************************************************************************/

/*****************************************************************************
**                             Global Functions                            **
*****************************************************************************/

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/
void I2c_Init(I2C_SFRmap *I2cx);
void I2c_IO_Init();
uint32_t I2C_Buffer_write(I2C_SFRmap* I2C_Choose,uint16_t Write_i2c_Addr, uint8_t *p_buffer, uint16_t number_of_byte);
uint32_t I2C_Buffer_read(I2C_SFRmap* I2C_Choose, uint16_t Read_I2C_Addr, uint8_t *p_buffer, uint16_t number_of_byte);
uint32_t I2C_Buffer_write_read(I2C_SFRmap* I2cx, uint16_t I2c_Addr, uint8_t *write_buffer, uint16_t write_len, uint8_t *read_buffer, uint16_t read_len);

#endif
