/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : I2C.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A156-MINI-EVB_V1.2
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the I2C configuration for KF32A156
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                             Include Files
******************************************************************************/
#include "I2C.h"

#include "kflog.h"

/*****************************************************************************
**                         Private Macro Definitions
*****************************************************************************/

/*****************************************************************************
**                         Private Variables Definitions
*****************************************************************************/

/*****************************************************************************
**                         Global Variables Definitions
*****************************************************************************/

/*****************************************************************************
**                             Private Functions
*****************************************************************************/

/*****************************************************************************
**                             Global Functions
*****************************************************************************/
/**
 *  @brief: Initialization I2C IO
 *  @param in: None
 *  @param[out] None
 *  @retval : None
 */
void I2c_IO_Init()
{
    GPIO_Write_Mode_Bits(PC0_I2C0_SCL_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PC0_I2C0_SCL_AF);
    GPIO_Open_Drain_Enable(PC0_I2C0_SCL_PIN, GPIO_POD_OD);
    GPIO_Pull_Up_Enable(PC0_I2C0_SCL_PIN, TRUE);

    GPIO_Write_Mode_Bits(PG4_I2C0_SDA_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PG4_I2C0_SDA_AF);
    GPIO_Open_Drain_Enable(PG4_I2C0_SDA_PIN, GPIO_POD_OD);
    GPIO_Pull_Up_Enable(PG4_I2C0_SDA_PIN, TRUE);
}

/**
 *  @brief: Initialization I2C HW module
 *  @param in: I2Cx
 *             I2c_Address
 *  @param[out] None
 *  @retval : None
 */
void I2c_Init(I2C_SFRmap *I2cx)
{
    I2C_InitTypeDef I2cConfigPtr;

    /* I2c mode */
    I2cConfigPtr.m_Mode = I2C_MODE_I2C;
    /* I2c clock */
    I2cConfigPtr.m_ClockSource = I2C_CLK_HFCLK;
/* Configure slave address width */
#if (I2C_ADDRESS_WIDTH == I2C_7BIT)
    I2cConfigPtr.m_BADR10 = I2C_BUFRADDRESS_7BIT;
#else
    I2cConfigPtr.m_BADR10 = I2C_BUFRADDRESS_10BIT;
#endif
    /* SMBus type */
    I2cConfigPtr.m_MasterSlave = I2C_MODE_SMBUSHOST;
    /* I2c baud rate low level time */
    I2cConfigPtr.m_BaudRateL = 15;
    /* I2c baud rate high level time */
    I2cConfigPtr.m_BaudRateH = 15;
    /* Enable ACK */
    I2cConfigPtr.m_AckEn = TRUE;
    /* Select the response signal as ACK */
    I2cConfigPtr.m_AckData = I2C_ACKDATA_ACK;

    I2C_Reset(I2cx);
    /* Configue I2C */
    I2C_Configuration(I2cx, &I2cConfigPtr);

    //    /* Enable I2C HW module */
    I2C_Cmd(I2cx, TRUE);
    /* Clear Interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);

    /* Enable I2C address match */
    I2C_MATCH_ADDRESS_Config(I2cx, TRUE);

    /* Enable I2C HW module */
    I2C_Cmd(I2cx, TRUE);
}

/**
 * @brief 
 * 
 * @param I2cx 
 * @param Addr 
 * @return uint32_t 
 */
static uint32_t Send_10bit_Slave_Address(I2C_SFRmap *I2cx, uint16_t Addr)
{
    uint32_t retVal = 0U;
    volatile uint32_t WaitTime = 0U;

    uint8_t addrByte[2U] = {0U};

    addrByte[0U] = (uint8_t)(((Addr | 0xF000U) & 0xFF00U) >> 8U);
    addrByte[1U] = (uint8_t)(Addr & 0x00FFU);

    for(uint32_t i = 0U; i < 2U; i++)
    {
        /* Send 7-bit slave address */
        I2C_SendData8(I2cx, addrByte[i]);
        /* Clear the ISIF bit of the I2C interrupt flag */
        I2C_Clear_INTERRUPT_Flag(I2cx);
        /* Wait for sending to complete */
        WaitTime = I2C_WAIT_TIME;
        while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
            ;

        if(0U == WaitTime)
        {
            retVal = 1U;
            break;
        }
        else if(I2C_Get_Ack_Fail_Flag(I2cx))
        {
            /* Determine whether there is an ACK response. if not, return error, if there is, continue to send */
            retVal = 1U;
            I2C_Clear_Ack_Fail_Flag(I2cx);
            break;
        }
    }

    return retVal;
}

/**
 * @brief 
 * 
 * @param I2cx 
 * @param Addr 
 * @param TimeOut 
 * @return uint32_t 
 */
static uint32_t Send_Slave_Address(I2C_SFRmap *I2cx, uint16_t Addr, uint8_t ReadWriteState)
{
    uint32_t retVal = 0U;
    volatile uint32_t WaitTime = 0U;

    uint8_t addrByte = 0U;

#if (I2C_ADDRESS_WIDTH == I2C_10BIT)
    addrByte = (uint8_t)(((Addr | 0xF000U) & 0xFF00U) >> 8U) | ReadWriteState;
#else
    addrByte = (uint8_t)(Addr & 0x00FFU) | ReadWriteState;
#endif

    /* Send 7-bit slave address */
    I2C_SendData8(I2cx, addrByte);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Wait for sending to complete */
    WaitTime = I2C_WAIT_TIME;
    while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
        ;

    if(0U == WaitTime)
    {
        retVal = 1U;
    }
    else if(I2C_Get_Ack_Fail_Flag(I2cx))
    {
        /* Determine whether there is an ACK response. if not, return error, if there is, continue to send */
        retVal = 1U;
        I2C_Clear_Ack_Fail_Flag(I2cx);
    }

    return retVal;
}

/**
 * @brief 
 * 
 * @param I2cx 
 * @param Data 
 * @param Len 
 * @return uint32_t 
 */
static uint32_t Write_Data(I2C_SFRmap *I2cx, const uint8_t *Data, uint8_t Len)
{
    uint32_t retVal = 0U;
    volatile uint32_t WaitTime = 0U;

    for (uint32_t i = 0U; i < Len; i++)
    {
        /* Send data */
        I2C_SendData8(I2cx, Data[i]);
        I2C_Clear_INTERRUPT_Flag(I2cx);

        WaitTime = I2C_WAIT_TIME;
        while ((!(I2C_Get_INTERRUPT_Flag(I2cx))) && (--WaitTime != 0U))
        {
            ;
        }
        if (0U == WaitTime)
        {
            retVal = 1U;
            break;
        }
        /*Confirm ACK data is ACK*/
        else if (I2C_Get_Ack_Fail_Flag(I2cx))
        {
            I2C_Clear_Ack_Fail_Flag(I2cx);

            if (i != Len - 1U)
            {
                retVal = 2U;
                break;
            }
        }
    }

    return retVal;
}

/**
 * @brief 
 * 
 * @param I2cx 
 * @param Data 
 * @param Len 
 * @return uint32_t 
 */
static uint32_t Read_Data(I2C_SFRmap *I2cx, uint8_t *Data, uint8_t Len)
{
    uint32_t retVal = 0U;
    volatile uint32_t WaitTime = 0U;

    for (uint32_t i = 0U; i < Len;)
    {
        /* Read data */
        WaitTime = I2C_WAIT_TIME;
        while ((!(I2C_Get_INTERRUPT_Flag(I2cx))) && (--WaitTime != 0U))
        {
            ;
        }
        if (0U == WaitTime)
        {
            retVal = 1U;
            break;
        }
        else if (I2C_Get_Receive_Buff_Flag(I2cx))
        {
            Data[i] = (uint8_t)(I2C_ReceiveData(I2cx) & 0xFFU);
            i++;
            if (i == Len - 1)
            {
                I2C_Ack_Config(I2cx, FALSE);
                I2C_Ack_DATA_Config(I2cx, I2C_ACKDATA_NO_ACK);
            }
        }

        if (Len != i)
        {
            I2C_Clear_INTERRUPT_Flag(I2cx);
        }
    }

    return retVal;
}

/**
 * @brief I2C write multiple bytes of data
 * 
 * @param I2cx 
 * @param Write_i2c_Addr 
 * @param p_buffer 
 * @param number_of_byte 
 * @return uint32_t 
 */
uint32_t I2C_Buffer_write(I2C_SFRmap* I2cx, uint16_t Write_i2c_Addr, uint8_t *p_buffer, uint16_t number_of_byte)
{
    volatile uint32_t WaitTime = 0xff;
    uint32_t retVal = 0U;

    /* Clear interrupt flag ISIF */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Enable I2C HW module*/
    I2C_Cmd(I2cx, TRUE);

    /* Start bit */
    I2C_Generate_START(I2cx, TRUE);
    /* Wait for the start signal to stabilize */
    WaitTime = I2C_WAIT_TIME;
    while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
        ;

    if(0U == WaitTime)
    {
        kf_printf("start fail\r\n");
        retVal = 1U;
    }
    /* Send address byte of slave */
#if (I2C_ADDRESS_WIDTH == I2C_10BIT)
    else if(0U == Send_10bit_Slave_Address(I2cx, Write_i2c_Addr))
#else
    else if(0U == Send_Slave_Address(I2cx, Write_i2c_Addr, 0x0U))
#endif
    {
        /* Send data bytes */
        retVal = Write_Data(I2cx, p_buffer, number_of_byte); 
    }
    else
    {
        kf_printf("addr fail\r\n");
        retVal = 1U;
    }
    
    WaitTime = 0xff;
    while (WaitTime--)
        ;

    /* Stop bit */
    I2C_Generate_STOP(I2cx, TRUE);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Wait for the stop to complete */
    WaitTime = I2C_WAIT_TIME;
    while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
        ;

    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Clear the I2C stop flag PIF bit */
    I2C_Clear_Stop_Flag(I2cx);
    /* Stop I2C module */
    I2C_Cmd(I2cx, FALSE);

    return retVal;
}

/**
 * @brief I2C Read multiple bytes of data
 * 
 * @param I2cx 
 * @param Read_I2C_Addr 
 * @param p_buffer 
 * @param number_of_byte 
 * @return uint32_t 
 */
uint32_t I2C_Buffer_read(I2C_SFRmap* I2cx, uint16_t Read_I2C_Addr, uint8_t *p_buffer, uint16_t number_of_byte)
{
    volatile uint32_t WaitTime = 0U;
    uint32_t retVal = 0U;


    /* Clear interrupt flag ISIF */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Enable I2C HW module*/
    I2C_Cmd(I2cx, TRUE);

    /* Start bit */
    I2C_Generate_START(I2cx, TRUE);
    /* Wait for the start signal to stabilize */
    WaitTime = I2C_WAIT_TIME;
    while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
        ;

    if(0U == WaitTime)
    {
        retVal = 1U;
    }
    /* Send address byte of slave */
    else
    {
#if (I2C_ADDRESS_WIDTH == I2C_10BIT)
        if(0U == Send_10bit_Slave_Address(I2cx, Read_I2C_Addr))
        {
            /* Start bit */
            I2C_Generate_START(I2cx, TRUE);
            /* Clear interrupt flag ISIF */
            I2C_Clear_INTERRUPT_Flag(I2cx);
            /* Wait for the start signal to stabilize */
            WaitTime = I2C_WAIT_TIME;
            while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
                ;

            if(0U == WaitTime)
            {
                retVal = 1U;
            }
            else
#endif
            if(0U == Send_Slave_Address(I2cx, Read_I2C_Addr, 0x1U))
            {
                if(number_of_byte > 1U)
                {
                    I2C_Ack_DATA_Config(I2cx, I2C_ACKDATA_ACK);
                    I2C_Ack_Config(I2cx, TRUE);
                }
                else
                {
                    I2C_Ack_Config(I2cx, FALSE);
                    I2C_Ack_DATA_Config(I2cx, I2C_ACKDATA_NO_ACK);
                }

                retVal = Read_Data(I2cx, p_buffer, number_of_byte);
            }
            else
            {
                retVal = 1U;
            }
#if (I2C_ADDRESS_WIDTH == I2C_10BIT)
        }
        else
        {
            retVal = 1U;
        }
#endif 
    }

    WaitTime = 0xff;
    while (WaitTime--)
        ;

    /* Stop bit */
    I2C_Generate_STOP(I2cx, TRUE);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Wait for the stop to complete */
    WaitTime = I2C_WAIT_TIME;
    while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
        ;

    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Clear the I2C stop flag PIF bit */
    I2C_Clear_Stop_Flag(I2cx);
    /* Turn off the I2C module */
    I2C_Cmd(I2cx, FALSE);

    return retVal;
}

/**
 * @brief I2C write&read multiple bytes of data with RESTART signal
 * 
 * @param I2cx 
 * @param I2c_Addr 
 * @param write_buffer 
 * @param write_len 
 * @param read_buffer 
 * @param read_len 
 * @return uint32_t 
 */
uint32_t I2C_Buffer_write_read(I2C_SFRmap* I2cx, uint16_t I2c_Addr, uint8_t *write_buffer, uint16_t write_len, uint8_t *read_buffer, uint16_t read_len)
{
    volatile uint32_t WaitTime = 0xff;
    uint32_t retVal = 0U;

    /* Clear interrupt flag ISIF */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Enable I2C HW module*/
    I2C_Cmd(I2cx, TRUE);

    /**
     * @brief Start signal
     * 
     */
    I2C_Generate_START(I2cx, TRUE);
    /* Wait for the start signal to stabilize */
    WaitTime = I2C_WAIT_TIME;
    while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
        ;

    /**
     * @brief Write phase
     * 
     */
    if(0U == WaitTime)
    {
        retVal = 1U;
    }
    /* Send address byte of slave */
#if (I2C_ADDRESS_WIDTH == I2C_10BIT)
    else if(0U == Send_10bit_Slave_Address(I2cx, I2c_Addr))
#else
    else if(0U == Send_Slave_Address(I2cx, I2c_Addr, 0x0U))
#endif
    {
        /* Send data bytes */
        retVal = Write_Data(I2cx, write_buffer, write_len);
    }
    else
    {
        retVal = 1U;
    }

    /**
     * @brief Read phase
     * 
     */
    if(0U == retVal)
    {
        /* Start bit */
        I2C_Generate_START(I2cx, TRUE);
        /* Clear interrupt flag ISIF */
        I2C_Clear_INTERRUPT_Flag(I2cx);
        /* Wait for the start signal to stabilize */
        WaitTime = I2C_WAIT_TIME;
        while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
            ;

        if(0U == WaitTime)
        {
            retVal = 1U;
        }
        /* Send address byte of slave */
        else if(0U == Send_Slave_Address(I2cx, I2c_Addr, 0x1U))
        {
            if(read_len > 1U)
            {
                I2C_Ack_DATA_Config(I2cx, I2C_ACKDATA_ACK);
                I2C_Ack_Config(I2cx, TRUE);
            }
            else
            {
                I2C_Ack_Config(I2cx, FALSE);
                I2C_Ack_DATA_Config(I2cx, I2C_ACKDATA_NO_ACK);
            }

            retVal = Read_Data(I2cx, read_buffer, read_len);
        }
        else
        {
            retVal = 1U;
        }
    }


    WaitTime = 0xff;
    while (WaitTime--)
        ;

    /**
     * @brief Stop signal
     * 
     */
    I2C_Generate_STOP(I2cx, TRUE);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Wait for the stop to complete */
    WaitTime = I2C_WAIT_TIME;
    while (!I2C_Get_INTERRUPT_Flag(I2cx) && (WaitTime--))
        ;

    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);
    /* Clear the I2C stop flag PIF bit */
    I2C_Clear_Stop_Flag(I2cx);
    /* Stop I2C module */
    I2C_Cmd(I2cx, FALSE);

    return retVal;
}
