/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use I2C as Master
 *                        polling mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "I2C.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/

/*******************************************************************************
**                          Private Variables Definitions
*******************************************************************************/
uint8_t i2c_buffer_write[] = {0x50, 0x00 ,0x55, 0x55, 0x5A, 0x5A, 0x5A, 0x5A, 0xAA, 0xAA};
uint8_t i2c_Read_buffer[64];
/* Wait Ack time out flag */
extern volatile uint8_t AckTimeoutFlag;
/*******************************************************************************
**                                 Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main(void)
{
    /* System clock configuration */
    SystemInit(72U);
    systick_delay_init(72U);

    /* LED1,LED2 control */
    GPIOInit_Output_Config(PA3_LED1_PIN);
    GPIOInit_Output_Config(PF11_LED2_PIN);
    GPIO_Set_Output_Data_Bits(PF11_LED2_PIN, Bit_RESET);

    /* I2C initialization */
    I2c_Init(I2C0_SFR);
    /* Configure IO */
    I2c_IO_Init();

    while (1)
    {
        /* Delay time */
        systick_delay_ms(200);

#if (I2C_ADDRESS_WIDTH == I2C_10BIT)
        /* Ten-digit address sending */
        I2C_Buffer_write(I2C0_SFR, I2C_SLAVE_ADDR10B, i2c_buffer_write, sizeof(i2c_buffer_write));
#else
        /* Seven-digit address transmission  */
        /**
         * @brief Read & write EEPROM BL24C256, page address 2bytes
         */
        I2C_Buffer_write(I2C0_SFR, I2C_SLAVE_ADDR, i2c_buffer_write, sizeof(i2c_buffer_write));
        /* Delay time */
        systick_delay_ms(200);
        // I2C_Buffer_read(I2C0_SFR, I2C_SLAVE_ADDR, i2c_Read_buffer, 8U);
        I2C_Buffer_write_read(I2C0_SFR, I2C_SLAVE_ADDR, i2c_buffer_write, 2U, i2c_Read_buffer, 8U);
#endif
        /* Check data */
        GPIO_Set_Output_Data_Bits(LED2_PORT,LED2_PIN,Bit_RESET);
        for(uint32_t i = 0U; i < 8U; i++)
        {
            if(i2c_Read_buffer[i] != i2c_buffer_write[i + 2U])
            {
                GPIO_Set_Output_Data_Bits(LED2_PORT,LED2_PIN,Bit_SET);
                break;
            }
            i2c_Read_buffer[i] = 0U;
        }
        GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
