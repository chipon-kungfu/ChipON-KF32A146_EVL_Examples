#### 文件所有
本文适用于KF32A156的 I2C主机轮询模式配置的示例例程，例程文件名：I2C_Master_Polling_Mode
* * *


#### 版权说明
目前的固件只是为了给使用者提供指导，目的是向客户提供有关产品的代码信息，
与使用者的产品信息和代码无关。因此，对于因此类固件内容和（或）客户使用此
处包含的与其产品相关的编码信息而引起的任何索赔，上海芯旺微电子技术有限公
司不承担任何直接、间接或后果性损害赔偿责任
* * *

#### 使用说明
* 本例展示了如何配置I2C主机轮询模式的相关信息
* 主循环内先向EEPROM发送8字节的数据; 再从EEPROM读取8字节数据
* 当发送数据与读取到的数据不一致时，LED3 点亮
* 正常运行时 LED1 闪烁

* I2C_Buffer_write 用于发送数据
* I2C_Buffer_read 仅实现标准I2C读取时序，无法实现读取EEPROM
* I2C_Buffer_write_read 实现先写后读的组合时序，`write_buffer` 为待发送数据buffer地址，`write_len`为发送数据量，`read_buffer`为接收buffer首地址，`read_len`为接收数据量。用于读取EEPROM时，在`write_buffer` 中存放EEPROM页地址，在`write_len`存放页地址长度。

* * *