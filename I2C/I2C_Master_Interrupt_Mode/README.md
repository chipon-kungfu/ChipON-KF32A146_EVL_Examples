#### 文件所有
本文适用于KF32A146的 I2C主机中断模式配置的示例例程，例程文件名：I2C_Master_Interrupt_Mode
* * *


#### 版权说明
目前的固件只是为了给使用者提供指导，目的是向客户提供有关产品的代码信息，
与使用者的产品信息和代码无关。因此，对于因此类固件内容和（或）客户使用此
处包含的与其产品相关的编码信息而引起的任何索赔，上海芯旺微电子技术有限公
司不承担任何直接、间接或后果性损害赔偿责任
* * *

#### 使用说明
* 本例展示了如何配置I2C主机中断模式的相关信息
* 主机发送完成LED1闪烁
* * *