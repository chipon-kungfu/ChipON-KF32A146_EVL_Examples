/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines.
 *                      This file provides template for all exceptions
 *                      handler and peripherals interrupt service routine.
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "I2C.h"
/*******************************************************************************
**                   KF32A156 Processor Exceptions Handlers
*******************************************************************************/
uint8_t i2c_buffer_write[] = {
  0X5A, 0X22, 0X33, 0X44, 0X66, 0X77, 0X88, 0X99, 0XAA, 0XBB, 0X01, 0X02, 0X03, 0X04, 0X05, 0X06,
  0X07, 0X08, 0X09, 0X10, 0X11, 0X12, 0X13, 0X14, 0X15, 0X16, 0X17, 0X18, 0X19, 0X20, 0X21, 0X22,
  0X23, 0X24, 0X25, 0X26, 0X27, 0X28, 0X29, 0X30, 0X31, 0X32, 0X33, 0X34, 0X35, 0X36, 0X37, 0X38,
  0X39, 0X40, 0X41, 0X42, 0X43, 0X44, 0X45, 0X46, 0X47, 0X48, 0X49, 0X50, 0X51, 0X52, 0X53, 0XA5};
uint16_t number_of_byre       = sizeof(i2c_buffer_write);
uint8_t  I2C_AddressSend_Flag = 0;
//*****************************************************************************************
//                                 NMI Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _NMI_exception(void) {}

//*****************************************************************************************
//                               HardFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _HardFault_exception(void) {}

//*****************************************************************************************
//                               StackFault Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _StackFault_exception(void) {}

//*****************************************************************************************
//                               SVC Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SVC_exception(void) {}

//*****************************************************************************************
//                              SoftSV Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SoftSV_exception(void) {}

//*****************************************************************************************
//                              SysTick Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _SysTick_exception(void) {}

//*****************************************************************************************
//                              I2C0 Interrupt Course
//*****************************************************************************************
void __attribute__((interrupt)) _I2C0_exception(void)
{
    static uint8_t    I2C_SLAVE_ADDRESS10BH = 0;
    static uint8_t    I2C_SLAVE_ADDRESS10BL = 0;
    volatile uint32_t WaitTime;

#if (I2C_ADDRESS_WIDTH == I2C_10BIT)
    /* Low 8 bit register address */
    I2C_SLAVE_ADDRESS10BL = I2C_SLAVE_ADDR10B & 0xff;
    /* High bit address */
    I2C_SLAVE_ADDRESS10BH = (I2C_SLAVE_ADDR10B >> 8) & 0xff;
    if (I2C_Get_Start_Flag(I2C0_SFR) == 1)
    { /* Wait for the start signal to stabilize */
        if (I2C_AddressSend_Flag == 0)
        {
            number_of_byre = 64;
            /* Send slave 10 bits high address */
            I2C_SendData8(I2C0_SFR, I2C_SLAVE_ADDRESS10BH);
            /* Clear Interrupt flag */
            I2C_Clear_INTERRUPT_Flag(I2C0_SFR);
            I2C_AddressSend_Flag = 1;
        }
        else
        {
            /* Send slave 10 bits high address */
            I2C_SendData8(I2C0_SFR, I2C_SLAVE_ADDRESS10BL);
            /* Clear Interrupt flag */
            I2C_Clear_INTERRUPT_Flag(I2C0_SFR);
            I2C_Clear_Start_Flag(I2C0_SFR);
            I2C_AddressSend_Flag = 0;
        }
    }
#else
    if (I2C_Get_Start_Flag(I2C0_SFR) == 1)
    { /* Wait for the start signal to stabilize */
        /* Send slave address */
        number_of_byre = 64;
        I2C_SendData8(I2C0_SFR, I2C_SLAVE_ADDR);
        /* Clear Interrupt flag */
        I2C_Clear_INTERRUPT_Flag(I2C0_SFR);
        I2C_Clear_Start_Flag(I2C0_SFR);
    }
#endif
    else if (I2C_Get_Ack_Fail_Flag(I2C0_SFR))
    { /* Determine whether there is an ACK response, if not, resend, if there is, continue to send */
        I2C_Clear_Ack_Fail_Flag(I2C0_SFR);
        /* Send stop bit */
        I2C_Generate_STOP(I2C0_SFR, TRUE);
        /* Clear Interrupt flag */
        I2C_Clear_INTERRUPT_Flag(I2C0_SFR);
    }
    else
    {
        if (number_of_byre--)
        {
            /* Send data */
            I2C_SendData8(I2C0_SFR, i2c_buffer_write[63 - number_of_byre]);
            /* Clear Interrupt flag */
            I2C_Clear_INTERRUPT_Flag(I2C0_SFR);
        }
        else
        {
            WaitTime = 0xff;
            while (WaitTime--)
                ;
            /* Send stop bit */
            I2C_Generate_STOP(I2C0_SFR, TRUE);
            /* Clear Interrupt flag */
            I2C_Clear_INTERRUPT_Flag(I2C0_SFR);
        }
    }
    /* Get the I2C stop flag PIF bit */
    if (I2C_Get_Stop_Flag(I2C0_SFR))
    {
        /* Clear Interrupt flag */
        I2C_Clear_INTERRUPT_Flag(I2C0_SFR);
    }
}
