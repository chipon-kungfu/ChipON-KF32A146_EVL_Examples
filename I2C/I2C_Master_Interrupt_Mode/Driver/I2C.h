/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : I2C.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the I2Cx configuration for KF32A156
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef _I2C_H_
#define _I2C_H_
/******************************************************************************
**                             Include Files                                **
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"

/*****************************************************************************
**                         Private Macro Definitions                        **
*****************************************************************************/
#define I2C_7BIT  (0U)
#define I2C_10BIT (1U)

#define I2C_ADDRESS_WIDTH (I2C_7BIT)

/* I2C slave 7-bit address to be read and written by the host */
#define I2C_SLAVE_ADDR 0xA0
/* I2C slave 10-bit address to be read and written by the host */
#define I2C_SLAVE_ADDR10B 0xF6A0
/*****************************************************************************
**                         Private Variables Definitions                    **
*****************************************************************************/
/*****************************************************************************
**                             Private Functions                            **
*****************************************************************************/

/*****************************************************************************
**                             Global Functions                            **
*****************************************************************************/

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/
void I2c_Init(I2C_SFRmap *I2cx);
void I2c_IO_Init();
void I2c_Interrupt_Init();
void I2C_Byte_Write(uint16_t Write_i2c_Addr, uint32_t I2C_data);
void I2C_Buffer_write(uint16_t Write_i2c_Addr, uint8_t *p_buffer, uint16_t number_of_byte);

#endif
