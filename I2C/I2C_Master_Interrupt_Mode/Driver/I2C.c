/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : I2C.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the I2C configuration for KF32A156
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                             Include Files
******************************************************************************/
#include "I2C.h"

/*****************************************************************************
**                         Private Macro Definitions
*****************************************************************************/

/*****************************************************************************
**                         Private Variables Definitions
*****************************************************************************/
I2C_SFRmap *I2C_Choose;

/*****************************************************************************
**                         Global Variables Definitions
*****************************************************************************/
volatile uint8_t AckTimeoutFlag = 0;

/*****************************************************************************
**                             Private Functions
*****************************************************************************/
static void Wait_Ack_Flag(void);
/*****************************************************************************
**                             Global Functions
*****************************************************************************/
/**
 *  @brief: Initialization I2C IO
 *  @param in: None
 *  @param[out] None
 *  @retval : None
 */
void I2c_IO_Init()
{
    GPIO_Write_Mode_Bits(PC0_I2C0_SCL_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PC0_I2C0_SCL_AF);
    GPIO_Open_Drain_Enable(PC0_I2C0_SCL_PIN, GPIO_POD_OD);
    GPIO_Pull_Up_Enable(PC0_I2C0_SCL_PIN, TRUE);

    GPIO_Write_Mode_Bits(PG4_I2C0_SDA_PIN, GPIO_MODE_RMP);
    GPIO_Pin_RMP_Config(PG4_I2C0_SDA_AF);
    GPIO_Open_Drain_Enable(PG4_I2C0_SDA_PIN, GPIO_POD_OD);
    GPIO_Pull_Up_Enable(PG4_I2C0_SDA_PIN, TRUE);
}

/**
 *  @brief: Initialization I2C HW module
 *  @param in: I2Cx
 *             I2c_Address
 *  @param[out] None
 *  @retval : None
 */
void I2c_Init(I2C_SFRmap *I2cx)
{
    I2C_InitTypeDef I2cConfigPtr;

    /* I2c mode */
    I2cConfigPtr.m_Mode = I2C_MODE_I2C;
    /* I2c clock */
    I2cConfigPtr.m_ClockSource = I2C_CLK_HFCLK;
/* Configure slave address width */
#if (I2C_ADDRESS_WIDTH == I2C_7BIT)
    I2cConfigPtr.m_BADR10 = I2C_BUFRADDRESS_7BIT;
#else
    I2cConfigPtr.m_BADR10 = I2C_BUFRADDRESS_10BIT;
#endif
    /* SMBus type */
    I2cConfigPtr.m_MasterSlave = I2C_MODE_SMBUSHOST;
    /* I2c baud rate low level time */
    I2cConfigPtr.m_BaudRateL = 15;
    /* I2c baud rate high level time */
    I2cConfigPtr.m_BaudRateH = 15;
    /* Enable ACK */
    I2cConfigPtr.m_AckEn = TRUE;
    /* Select the response signal as ACK */
    I2cConfigPtr.m_AckData = I2C_ACKDATA_ACK;

    I2C_Reset(I2cx);
    /* Configue I2C */
    I2C_Configuration(I2cx, &I2cConfigPtr);

    //    /* Enable I2C HW module */
    I2C_Cmd(I2cx, TRUE);
    /* Clear Interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2cx);

    /* Enable I2C address match */
    I2C_MATCH_ADDRESS_Config(I2cx, TRUE);

    /* Enable I2C HW module */
    I2C_Cmd(I2cx, TRUE);
}

/**
 *  @brief: Waiting for ACK signal
 *  @param in: None
 *  @param[out] None
 *  @retval : None
 */
static void Wait_Ack_Flag(void)
{
    I2C_Choose = I2C0_SFR;
    AckTimeoutFlag       = 0;
    uint16_t AckWaitTime = 0xffff;
    while (I2C_Get_Ack_Fail_Flag(I2C_Choose) && (--AckWaitTime))
        ;
    if (AckWaitTime == 0)
    {
        /* Stop bit */
        I2C_Generate_STOP(I2C_Choose, TRUE);
        /* Clear the ISIF bit of the I2C interrupt flag */
        I2C_Clear_INTERRUPT_Flag(I2C_Choose);
        /* Wait for the stop to complete */
        while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
            ;
        /* Clear the ISIF bit of the I2C interrupt flag */
        I2C_Clear_INTERRUPT_Flag(I2C_Choose);
        /* Clear the I2C ack fail flag AFIF bit */
        I2C_Clear_Ack_Fail_Flag(I2C_Choose);
        /* Clear the I2C stop flag PIF bit */
        I2C_Clear_Stop_Flag(I2C_Choose);
        /* Stop I2C module */
        I2C_Cmd(I2C_Choose, FALSE);
        AckTimeoutFlag = 1;
    }
}

/**
 *  @brief: I2c Write a byte of data
 *  @param in: Write_i2c_Addr
 *             I2C_data
 *  @param[out] None
 *  @retval : None
 */
void I2C_Byte_Write(uint16_t Write_i2c_Addr, uint32_t I2C_data)
{
    I2C_Choose = I2C0_SFR;

    static uint8_t I2C_SLAVE_ADDRESS10BH = 0;
    static uint8_t I2C_SLAVE_ADDRESS10BL = 0;

    /* Send data */
    /* Enable I2C HW module*/
    I2C_Cmd(I2C_Choose, TRUE);
    /* Clear interrupt flag ISIF */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);

    /* Start bit */
    I2C_Generate_START(I2C_Choose, TRUE);
    /* Wait for the start signal to stabilize */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;

#if (I2C_ADDRESS_WIDTH == I2C_10BIT)

    /* Low 8 bit register address */
    I2C_SLAVE_ADDRESS10BL = Write_i2c_Addr & 0xff;
    /* High bit address */
    I2C_SLAVE_ADDRESS10BH = (Write_i2c_Addr >> 8) & 0xff;
    /* Send slave address */
    I2C_SendData(I2C_Choose, I2C_SLAVE_ADDRESS10BH);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Wait for sending to complete */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;

    /* Send slave 10-bit low address */
    I2C_SendData(I2C_Choose, I2C_SLAVE_ADDRESS10BL);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Wait for sending to complete */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;
    /* Determine whether there is an ACK response, if not, resend, if there is, continue to send */
    Wait_Ack_Flag();
    if (AckTimeoutFlag == 1)
    {
        return (void)0;
    }

#else
    /* Send 7-bit slave address */
    I2C_SendData8(I2C_Choose, Write_i2c_Addr);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Wait for sending to complete */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;
    /* Determine whether there is an ACK response, if not, resend, if there is, continue to send */
    Wait_Ack_Flag();
    if (AckTimeoutFlag == 1)
    {
        return (void)0;
    }
#endif

    /* Send data*/
    I2C_SendData(I2C_Choose, I2C_data);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Wait for sending to complete */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;

    /* Stop bit */
    I2C_Generate_STOP(I2C_Choose, TRUE);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Wait for the stop to complete */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Clear the I2C stop flag PIF bit */
    I2C_Clear_Stop_Flag(I2C_Choose);
    /* Stop I2C module */
    I2C_Cmd(I2C_Choose, FALSE);
}

/**
 *  @brief: I2C write multiple bytes of data
 *  @param in: Write_i2c_Addr
 *             p_buffer
 *             number_of_byte
 *  @param[out] None
 *  @retval : None
 */
void I2C_Buffer_write(uint16_t Write_i2c_Addr, uint8_t *p_buffer, uint16_t number_of_byte)
{
    I2C_Choose = I2C0_SFR;

    /* Send data */

    /* Clear interrupt flag ISIF */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);

    /* Enable I2C HW module*/
    I2C_Cmd(I2C_Choose, TRUE);

    /* Start bit */
    I2C_Generate_START(I2C_Choose, TRUE);
    /* Wait for the start signal to stabilize */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;

#if (I2C_ADDRESS_WIDTH == I2C_10BIT)

    static uint8_t I2C_SLAVE_ADDRESS10BH = 0;
    static uint8_t I2C_SLAVE_ADDRESS10BL = 0;

    /* Low 8 bit register address */
    I2C_SLAVE_ADDRESS10BL = Write_i2c_Addr & 0xff;
    /* High bit address */
    I2C_SLAVE_ADDRESS10BH = (Write_i2c_Addr >> 8) & 0xff;
    /* Send slave address */
    I2C_SendData(I2C_Choose, I2C_SLAVE_ADDRESS10BH);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Wait for sending to complete */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;

    /* Send slave 10-bit low address */
    I2C_SendData(I2C_Choose, I2C_SLAVE_ADDRESS10BL);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Wait for sending to complete */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;
    /* Determine whether there is an ACK response, if not, resend, if there is, continue to send */
    Wait_Ack_Flag();
    if (AckTimeoutFlag == 1)
    {
        return (void)0;
    }

#else
    /* Send 7-bit slave address */
    I2C_SendData8(I2C_Choose, Write_i2c_Addr);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Wait for sending to complete */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;
    /* Determine whether there is an ACK response, if not, resend, if there is, continue to send */
    Wait_Ack_Flag();
    if (AckTimeoutFlag == 1)
    {
        return (void)0;
    }
#endif

    /* Write data cyclically */
    while (number_of_byte--)
    {
        /* Send data */
        I2C_SendData8(I2C_Choose, *p_buffer);
        /* Clear the ISIF bit of the I2C interrupt flag */
        I2C_Clear_INTERRUPT_Flag(I2C_Choose);
        /* Point to the next byte to be written */
        p_buffer++;
        /* ISIF will be set to 1 when the Buff is read */
        while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
            ;
    }

    /* Stop bit */
    I2C_Generate_STOP(I2C_Choose, TRUE);
    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Wait for the stop to complete */
    while (!I2C_Get_INTERRUPT_Flag(I2C_Choose))
        ;

    /* Clear the ISIF bit of the I2C interrupt flag */
    I2C_Clear_INTERRUPT_Flag(I2C_Choose);
    /* Clear the I2C stop flag PIF bit */
    I2C_Clear_Stop_Flag(I2C_Choose);
    /* Stop I2C module */
    I2C_Cmd(I2C_Choose, FALSE);
}

/**
 *  @brief: I2C Interrupt enable
 *  @param in: None
 *  @param[out] None
 *  @retval : None
 */
void I2c_Interrupt_Init()
{
    INT_Interrupt_Enable(INT_I2C0, TRUE);
    I2C_ISIE_INT_Enable(I2C0_SFR, TRUE);
    INT_All_Enable(TRUE);
}
