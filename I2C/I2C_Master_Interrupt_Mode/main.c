/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for use I2C as Master in
 *                        interrupt mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "I2C.h"
#include "Board_GpioCfg.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/

/*******************************************************************************
**                                 Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/
int main(void)
{
    /* System clock configuration */
    SystemInit(72U);
    systick_delay_init(72U);

    /* LED control */
    GPIOInit_Output_Config(PA3_LED1_PIN);
    /* I2C initialization */
    I2c_Init(I2C0_SFR);
    /* Configure IO */
    I2c_IO_Init();
    /* I2C interrupt initialization */
    I2c_Interrupt_Init();

    while (1)
    {
        /* Send data */
        /* Enable I2C HW module*/
        I2C_Cmd(I2C0_SFR, TRUE);
        /* Clear interrupt flag ISIF */
        I2C_Clear_INTERRUPT_Flag(I2C0_SFR);

        /* Start bit */
        I2C_Generate_START(I2C0_SFR, TRUE);

        /* Wait stop */
        while (!I2C_Get_Stop_Flag(I2C0_SFR))
            ;
        /* Clear interrupt flag */
        I2C_Clear_INTERRUPT_Flag(I2C0_SFR);
        I2C_Clear_Stop_Flag(I2C0_SFR);

        /* Stop I2C module */
        I2C_Cmd(I2C0_SFR, FALSE);
        systick_delay_ms(250);
        GPIO_Toggle_Output_Data_Config(PA3_LED1_PIN);
    }
}

/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  file pointer to the source file name
 *  @param[in]  line assert_param error line source number
 *  @param[out] None
 *  @retval :None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line number,
        ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {};
};
