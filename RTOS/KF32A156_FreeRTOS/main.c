/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This is a KF32A146 FreeRTOS Template
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                **
******************************************************************************/
/* system includes */
#include "system_init.h"
/* RTOS includes */
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
/* drivers inlcudes */
#include <stdio.h>
#include "usart.h"

/*******************************************************************************
**                     		   Global Variables Definitions            	      **
*******************************************************************************/
TaskHandle_t LEDTask_Handler;
TaskHandle_t UARTTask_Handler;

/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/
/**
 *  @brief :Configure PA3 remap to USART TX
 *  		Configure PE7 remap to USART RX
 *  @param in :None
 *  @param out :None
 *  @retval :None
 */
void UsartGpioInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure PA3 remap mode */
	GPIO_InitStructure.m_Mode = GPIO_MODE_RMP;
	GPIO_InitStructure.m_OpenDrain = GPIO_POD_PP;
	GPIO_InitStructure.m_PullDown = GPIO_NOPULL;
	GPIO_InitStructure.m_PullUp = GPIO_NOPULL;
	GPIO_InitStructure.m_Speed = GPIO_LOW_SPEED;
	GPIO_InitStructure.m_Pin = GPIO_PIN_MASK_3;
	GPIO_Configuration(GPIOA_SFR , &GPIO_InitStructure);
	/* Configure PE7 remap mode, as RX pin, it should be pullup */
	GPIO_InitStructure.m_PullUp = GPIO_PULLUP;
	GPIO_InitStructure.m_Pin = GPIO_PIN_MASK_7;
	GPIO_Configuration(GPIOE_SFR , &GPIO_InitStructure);
	/* Configure PA3 remap function to AF3 */
	GPIO_Pin_RMP_Config(GPIOA_SFR, GPIO_Pin_Num_3, GPIO_RMP_AF3);
	/* Configure PE17 remap function to AF3 */
	GPIO_Pin_RMP_Config(GPIOE_SFR, GPIO_Pin_Num_7, GPIO_RMP_AF3);
}

/**
 *  @brief :Initialize LED task 
 * 			turn on/off the led per 500ms
 *  @param in :
 *          pvParameters : invalid param
 *  @param out :None
 *  @retval :None
 */
static void LED_task(void *pvParameters)
{
	GPIO_Write_Mode_Bits(GPIOF_SFR, GPIO_PIN_MASK_11, GPIO_MODE_OUT);
	while (1)
	{
		vTaskDelay(500);
		GPIO_Toggle_Output_Data_Config(GPIOF_SFR, GPIO_PIN_MASK_11);
	}
}

/**
 *  @brief :Initialize uart task 
 * 			send strings per 1S
 *  @param in :
 *          pvParameters : invalid param
 *  @param out :None
 *  @retval :None
 */
static void UART_task(void *pvParameters)
{
	while (1)
	{
		vTaskDelay(1000);
		printf("KungFu32 FreeRTOS is running \r\n");
	}
}

/**
  * 描述  RTOS任务初始化
  * 输入  无。
  * 返回  无。
*/
void TaskInit(void)
{
		//创建任务
	xTaskCreate((TaskFunction_t )LED_task,
				(const char*    )"LED_task",
				(uint16_t       )configMINIMAL_STACK_SIZE,
				(void*          )NULL,
				(UBaseType_t    )2,
				(TaskHandle_t*  )&LEDTask_Handler);
	xTaskCreate((TaskFunction_t )UART_task,
				(const char*    )"UART_task",
				(uint16_t       )configMINIMAL_STACK_SIZE,
				(void*          )NULL,
				(UBaseType_t    )3,
				(TaskHandle_t*  )&UARTTask_Handler);
}

/*******************************************************************************
**                     			main Functions 		             	     	  **
*******************************************************************************/
int main()
{
	/* Initialize the system clock is 72M */
	SystemInit(72);
	/* Initialize the USART IOs */
	UsartGpioInit();
	/* USARTx configured as follow:
		- BaudRate = 115200 baud
		- Word Length = 8 Bits
		- One Stop Bit
		- No parity
		- Hardware flow control disabled (RTS and CTS signals)
		- Receive and transmit enabled
	 */
	USART_Async_config(USART0_SFR);
	printf("KungFu32 FreeRTOS Start\r\n");
	TaskInit();
	vTaskStartScheduler();
	while(1)
	{
	
	}		
}

//****************************************************************************
//						OS Hook
//****************************************************************************
void vApplicationStackOverflowHook(TaskHandle_t xTask, char *pcTaskName)
{
	RTOS_DEBUG_MSG("vApplicationStackOverflowHook: %s over: %d \r\n", pcTaskName, uxTaskGetStackHighWaterMark(xTask));
	while (1)
		;
}

void vApplicationMallocFailedHook(void)
{
	RTOS_DEBUG_MSG("vApplicationMallocFailedHook\r\n");
	while (1)
		;
}


/**
 *  @brief : Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param in :	file pointer to the source file name
 * 	@param in :	line assert_param error line source number
 *  @param out :None
 *  @retval :None
 */
void check_failed(uint8_t* File, uint32_t Line)
{
	/* User can add his own implementation to report the file name and line number,
		ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while(1)
	{
		;
	}
};
