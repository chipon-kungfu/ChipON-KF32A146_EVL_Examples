/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Usart.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the USART header file
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

#ifndef _USART_H_
#define _USART_H_

/******************************************************************************
*                      Functional defineition                                 *
******************************************************************************/
void USART_Send(USART_SFRmap *USARTx, uint8_t *Databuf, uint32_t length);
void USART_Async_config(USART_SFRmap *USARTx);
void USART_ReceiveInt_config(USART_SFRmap *USARTx,InterruptIndex Peripheral);
#endif /* USART_H_ */
