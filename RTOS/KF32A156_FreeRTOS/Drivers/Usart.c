/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : Usart.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides the serial routine related
 *                     configuration functions, including                     
 *                      + serial port send                                    
 *                      + serial port asynchronous configuration              
 *                      + serial port synchronization configuration           
 *                      + serial receiving interrupt enable                   
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                      		Include Files                                 **
******************************************************************************/
#include "system_init.h"

/*******************************************************************************
**                     		   Global Functions 		             	      **
*******************************************************************************/
/**
 *  @brief :USART sends data and waits for completion
 *  @param in :
 *              USARTx :USARTx : A pointer to the USART memory structure
 *                  	 with a value of USART0_SFR/USART1_SFR/USART2_SFR/USART5_SFR
 *  			      Databuf: A pointer to the sending data
 *  			      Length : Length of transmission
 *  @param out :None
 *  @retval :None
 */
void USART_Send(USART_SFRmap *USARTx, uint8_t *Databuf, uint32_t length)
{
	for (uint32_t i = 0; i < length; i++)
	{
		/* Transmit data and waite for completion */
		USART_SendData(USARTx, Databuf[i]);
	}
}

/**
 *  @brief :Serial asynchronous full duplex configuration(Default 8 bit transceiver enabled  Full duplex 115200)
 *  @param in :	
 * 				USARTx : A pointer to the USART memory structure
 *                  	 with a value of USART0_SFR/USART1_SFR/USART2_SFR/USART5_SFR
 *  @param out :None
 *  @retval :None
 */
void USART_Async_config(USART_SFRmap *USARTx)
{
	USART_InitTypeDef USART_InitStructure;

	/* Set Usart To Async Mode */
	USART_Struct_Init(&USART_InitStructure);
	USART_InitStructure.m_Mode = USART_MODE_FULLDUPLEXASY;
	USART_InitStructure.m_TransferDir = USART_DIRECTION_FULL_DUPLEX;
	USART_InitStructure.m_WordLength = USART_WORDLENGTH_8B;
	USART_InitStructure.m_StopBits = USART_STOPBITS_1;
	USART_InitStructure.m_BaudRateBRCKS = USART_CLK_HFCLK;
	/** Use 16M clock as an example to list the following baud rates 
	 * 	4800    z:208    x:0    y:0
	 * 	9600    z:104    x:0    y:0
	 * 	19200   z:52     x:0    y:0
	 * 	115200  z:8      x:1    y:13
	*/
	/* Integer part z, get value range is 0 ~ 0xFFFF */
	USART_InitStructure.m_BaudRateInteger = 8;
	/* Numerator part x, get value range is 0 ~ 0x0f */
	USART_InitStructure.m_BaudRateNumerator = 1;
	/* Denominator part y, get value range is 0 ~ 0x0f */
	USART_InitStructure.m_BaudRateDenominator = 13;
	/* Reset and enable USARTx */
	USART_Reset(USARTx);                                      
	USART_Configuration(USARTx,&USART_InitStructure);
	/* Enable UART moudle */
	USART_RESHD_Enable (USARTx, TRUE);			
	USART_Cmd(USARTx,TRUE);  
}

/**
 *  @brief :USART receive enable, and interrupt enable
 *  @param in :
 * 				USARTx : A pointer to the USART memory structure
 *                  	 with a value of USART0_SFR/USART1_SFR/USART2_SFR/USART5_SFR
 * 				Peripheral :Interrupt vector number
 *  @param out :None
 *  @retval :None
 */
void USART_ReceiveInt_config(USART_SFRmap *USARTx,InterruptIndex Peripheral)
{
	USART_RDR_INT_Enable(USARTx,TRUE);
	INT_Interrupt_Enable(Peripheral,TRUE);
	USART_ReceiveData(USARTx);
}
