/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : SPI.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the spi configuration for KF32A146
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                             Include Files
******************************************************************************/
#include "SPI.h"
#include "Board_GpioCfg.h"

/*****************************************************************************
**                         Private Macro Definitions
*****************************************************************************/
/*****************************************************************************
**                         Private Variables Definitions
*****************************************************************************/
uint8_t Tx_Slave[SPI_TRANSMISSION_DATA_SIZE] = "KF32A146-KungFu32 SPI Example slaves";
uint8_t Spi_ReceiveData[SPI_TRANSMISSION_DATA_SIZE];
/*****************************************************************************
**                             Private Functions
*****************************************************************************/

/*****************************************************************************
**                             Global Functions
*****************************************************************************/
/**
 *  @brief:Initialize the spi module
 *  @param[in]  SPIx:A pointer to the SPI memory structure with a
 *  value of SPI0_SFR/SPI1_SFR/SPI2_SFR/SPI3_SFR
 *  @param[out] None
 *  @retval:None
 */
void Spi_Init(SPI_SFRmap *SPIx)
{
    SPI_InitTypeDef Spi_ConfigPtr;

    /* SPI mode */
    Spi_ConfigPtr.m_Mode = SPI_MODE_SLAVE;
    /* SPI clock */
    Spi_ConfigPtr.m_Clock = SPI_CLK_SCLK;
    /* Data transfer start control */
    Spi_ConfigPtr.m_FirstBit = SPI_FIRSTBIT_MSB;
    /* Spi idle state */
    Spi_ConfigPtr.m_CKP = SPI_CKP_LOW;
    /* Spi clock phase(Data shift edge) */
    Spi_ConfigPtr.m_CKE = SPI_CKE_1EDGE;
    /* Data width */
    Spi_ConfigPtr.m_DataSize = SPI_DATASIZE_8BITS;
    /* Baud rate :Fck_spi=Fck/2(m_BaudRate+1)*/
    Spi_ConfigPtr.m_BaudRate = 0x59;

    /* Spi reset */
    SPI_Reset(SPIx);
    /* Configure SPI module */
    SPI_Configuration(SPIx, &Spi_ConfigPtr);
}

/**
 *  @brief: Initialize the SPI IO
 *  @param[in]  None
 *  @param[out] None
 *  @retval : None
 */
void Spi_IO_Init()
{
    /* Configure SPI IO */
    GPIO_Write_Mode_Bits(PE0_SPI0_SCK_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PH1_SPI0_SDI_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PA2_SPI0_SDO_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PC0_SPI0_SS0_PIN, GPIO_MODE_RMP);

    GPIO_Pin_RMP_Config(PE0_SPI0_SCK_AF);
    GPIO_Pin_RMP_Config(PH1_SPI0_SDI_AF);
    GPIO_Pin_RMP_Config(PA2_SPI0_SDO_AF);
    GPIO_Pin_RMP_Config(PC0_SPI0_SS0_AF);
}

/**
 *  @brief: Initialize DMA channel
 *  @param[in]  SPIx:A pointer to the SPI memory structure with a
 *  value of SPI0_SFR/SPI1_SFR/SPI2_SFR/SPI3_SFR
 *  @param[out] None
 *  @retval : None
 */
void Spi_DMA_Init(SPI_SFRmap *SPIx)
{
    DMA_InitTypeDef Dma_ConfigPtr;
    DMA_Reset(DMA0_SFR);
    /* Receive channel */
    /* Transmit data size */
    Dma_ConfigPtr.m_Number = SPI_TRANSMISSION_DATA_SIZE;
    /* DMA transmit direction */
    Dma_ConfigPtr.m_Direction = DMA_PERIPHERAL_TO_MEMORY;
    /* DMA channel priority */
    Dma_ConfigPtr.m_Priority = DMA_CHANNEL_TOP;
    /* Peripheral data width */
    Dma_ConfigPtr.m_PeripheralDataSize = DMA_DATA_WIDTH_8_BITS;
    /* Memory data width */
    Dma_ConfigPtr.m_MemoryDataSize = DMA_DATA_WIDTH_8_BITS;
    /* Peripheral address incremental mode enable */
    Dma_ConfigPtr.m_PeripheralInc = FALSE;
    /* Memory address incremental mode enable */
    Dma_ConfigPtr.m_MemoryInc = TRUE;
    /* DMA channel select */
    Dma_ConfigPtr.m_Channel = SPI0_RX_DMA_CHANNAL;
    /* DMA data block transfer mode */
    Dma_ConfigPtr.m_BlockMode = DMA_TRANSFER_BYTE;
    /* Cycle mode enable */
    Dma_ConfigPtr.m_LoopMode = TRUE;
    /* Peripheral start address */
    Dma_ConfigPtr.m_PeriphAddr = (uint32_t) & (SPIx->BUFR);
    /* Memory start address */
    Dma_ConfigPtr.m_MemoryAddr = (uint32_t)&Spi_ReceiveData;

    /* Configure DMA */
    DMA_Configuration(DMA0_SFR, &Dma_ConfigPtr);

    /* Receive DMA */
    SPI_Receive_DMA_INT_Enable(SPIx, TRUE);
    /* DMA channel enable */
    DMA_Channel_Enable(DMA0_SFR, SPI0_RX_DMA_CHANNAL, TRUE);
    /* DMA receive finish interrupt */
    DMA_Finish_Transfer_INT_Enable(DMA0_SFR, SPI0_RX_DMA_CHANNAL, TRUE);

    /* Send channal */
    /* Transmit data size */
    Dma_ConfigPtr.m_Number = SPI_TRANSMISSION_DATA_SIZE;
    /* DMA transmit direction */
    Dma_ConfigPtr.m_Direction = DMA_MEMORY_TO_PERIPHERAL;
    /* DMA channel select */
    Dma_ConfigPtr.m_Channel = SPI0_TX_DMA_CHANNAL;
    /* Cycle mode enable */
    Dma_ConfigPtr.m_LoopMode = FALSE;
    /* Peripheral start address */
    Dma_ConfigPtr.m_PeriphAddr = (uint32_t) & (SPIx->BUFR);
    /* Memory start address */
    Dma_ConfigPtr.m_MemoryAddr = (uint32_t)&Tx_Slave;

    /* Configure DMA */
    DMA_Configuration(DMA0_SFR, &Dma_ConfigPtr);

    /* Send DMA */
    SPI_Transmit_DMA_INT_Enable(SPIx, TRUE);
    /* DMA channel enable */
    DMA_Channel_Enable(DMA0_SFR, SPI0_TX_DMA_CHANNAL, TRUE);
    /* DMA finsh interrupt enable */
    DMA_Finish_Transfer_INT_Enable(DMA0_SFR, SPI0_TX_DMA_CHANNAL, TRUE);

    /* Enable SPI module */
    SPI_Cmd(SPIx, TRUE);
}
