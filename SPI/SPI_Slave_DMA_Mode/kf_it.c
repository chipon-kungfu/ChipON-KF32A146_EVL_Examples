/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "kflog.h"
#include <stdio.h>
#include "SPI.h"
/*******************************************************************************
**                   KF32A146 Processor Exceptions Handlers
*******************************************************************************/
extern void    delay_ms(volatile uint32_t nms);
extern uint8_t Spi_Finish_Flag;
extern uint8_t Spi_Length;
uint8_t        Spi_Change_Flag = 0;

/**
 * @brief:DMA0 Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _DMA0_exception(void)
{
    /* Waiting for DMA to receive full  */
    if (DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, SPI0_RX_DMA_CHANNAL))
    {
    	/* LED1 flashing */
		GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
        /* Use the serial port to print out the received data */
        kf_printf("%s\r\n", Spi_ReceiveData);
        /* Clear interrupt flag */
        DMA_Clear_INT_Flag(DMA0_SFR, SPI0_RX_DMA_CHANNAL, DMA_INT_FINISH_TRANSFER);

    }
    /* Waiting for DMA to send complete */
    if (DMA_Get_Finish_Transfer_INT_Flag(DMA0_SFR, SPI0_TX_DMA_CHANNAL))
    {
        /* Waiting for spi to send no data */
        while (SPI_Get_BUSY_Flag(SPI0_SFR) == SET)
            ;
        /* Clear interrupt flag */
        DMA_Clear_INT_Flag(DMA0_SFR, SPI0_TX_DMA_CHANNAL, DMA_INT_FINISH_TRANSFER);
        /* Enable next transmit */
        DMA_Channel_Enable(DMA0_SFR, SPI0_TX_DMA_CHANNAL, TRUE);
    }
}
