/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "SPI.h"
#include <stdio.h>
/*******************************************************************************
**                   KF32A146 Processor Exceptions Handlers
*******************************************************************************/
extern uint8_t   Spi_DataLength;
extern uint8_t   print_flag;
volatile uint8_t SpiEndIndex = 0;

/**
 * @brief:SPI2 Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SPI0_exception(void)
{
    if (SpiEndIndex < SPI_TRANSMISSION_DATA_SIZE)
    {
        /* Wait send buffer to empty */
        if (SPI_Get_Transmit_Buf_Flag(SPI0_SFR) == RESET)
        {
            if (Spi_DataLength < SPI_TRANSMISSION_DATA_SIZE)
            {
                /* Send data */
                SPI_I2S_SendData8(SPI0_SFR, Rx_Slave[Spi_DataLength]);
                Spi_DataLength++;
                if (Spi_DataLength == SPI_TRANSMISSION_DATA_SIZE)
                {
                    Spi_DataLength = 0;
                }
            }
        }
        /* Wait receive buffer not empty */
        if (SPI_Get_Receive_Buf_Flag(SPI0_SFR) == SET)
        {
            /* Receive data */
            Spi_ReceiveData[SpiEndIndex] = SPI0_SFR->BUFR;
            SpiEndIndex++;
            if (SpiEndIndex >= SPI_TRANSMISSION_DATA_SIZE)
            {
                SpiEndIndex = 0;
                print_flag  = 1;
                INT_Interrupt_Enable(INT_SPI0, FALSE);
            }
        }
    }
}
