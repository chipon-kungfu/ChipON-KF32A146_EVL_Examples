/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for SPI slave interrupt
 *                      mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "main.h"
#include "Board_GpioCfg.h"
#include "kflog.h"

/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/
uint8_t          Spi_DataLength = 0;
volatile uint8_t print_flag     = 0;
/*******************************************************************************
**                                 Global Functions
*******************************************************************************/

/*******************************************************************************
**                                 main Functions
*******************************************************************************/

int main()
{
    SpiEndIndex = 0;

    /* System clock configuration */
    SystemInit(72U);

    /*Initialize the print function*/
    kfLog_Init();

    /* LEDQ control */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);

    /* Configure IO */
    Spi_IO_Init();

    /* SPI initialization */
    Spi_Init(SPI0_SFR);

    /* SPI interrupt initialization */
    Spi_Interrupt_Init();

    /* Enable all interrupt */
    INT_All_Enable(TRUE);
    kf_printf("spi start\r\n");
    while (1)
    {
        if (print_flag == 1)
        {
        	GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
            print_flag = 0;
            kf_printf("%s\r\n", Spi_ReceiveData);
            INT_Interrupt_Enable(INT_SPI0, TRUE);
        }
    }
}

/**
 *  @brief:Reports the name of the source file and the source line number
 *           where the assert_param error has occburred.
 *  @param[in]  File: pointer to the source file name
 *  @param[in]  Line: assert_param error line source number
 *  @param[out] None
 *  @retval:None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
