/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : SPI.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the spi configuration for KF32A146
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                             Include Files
******************************************************************************/
#include "Board_GpioCfg.h"
#include "SPI.h"

/*****************************************************************************
**                         Private Macro Definitions
*****************************************************************************/
/*****************************************************************************
**                         Private Variables Definitions
*****************************************************************************/
uint8_t Rx_Slave[SPI_TRANSMISSION_DATA_SIZE] = "KF32A152-KungFu32 SPI Example slaves";
uint8_t Spi_ReceiveData[SPI_TRANSMISSION_DATA_SIZE];
/*****************************************************************************
**                             Private Functions
*****************************************************************************/

/*****************************************************************************
**                             Global Functions
*****************************************************************************/
/**
 *  @brief:Initialize the spi module
 *  @param[in]  SPIx:A pointer to the SPI memory structure with a
 *  value of SPI0_SFR/SPI1_SFR/SPI2_SFR/SPI3_SFR
 *  @param[out] None
 *  @retval:None
 */
void Spi_Init(SPI_SFRmap *SPIx)
{
    SPI_InitTypeDef Spi_ConfigPtr;

    /* SPI mode */
    Spi_ConfigPtr.m_Mode = SPI_MODE_SLAVE;
    /* SPI clock */
    Spi_ConfigPtr.m_Clock = SPI_CLK_SCLK;
    /* Data transfer start control */
    Spi_ConfigPtr.m_FirstBit = SPI_FIRSTBIT_MSB;
    /* Spi idle state */
    Spi_ConfigPtr.m_CKP = SPI_CKP_LOW;
    /* Spi clock phase(Data shift edge) */
    Spi_ConfigPtr.m_CKE = SPI_CKE_1EDGE;
    /* Data width */
    Spi_ConfigPtr.m_DataSize = SPI_DATASIZE_8BITS;
    /* Baud rate :Fck_spi=Fck/2(m_BaudRate+1)*/
    Spi_ConfigPtr.m_BaudRate = 0x3B;

    /* Spi reset */
    SPI_Reset(SPIx);
    /* Configure SPI module */
    SPI_Configuration(SPIx, &Spi_ConfigPtr);

    /* Enable SPI module */
    SPI_Cmd(SPIx, TRUE);
}

/**
 *  @brief: Initialize the SPI IO
 *  @param[in]  None
 *  @param[out] None
 *  @retval : None
 */
void Spi_IO_Init()
{
    /* Configure SPI IO */
    GPIO_Write_Mode_Bits(PE0_SPI0_SCK_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PH1_SPI0_SDI_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PA2_SPI0_SDO_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PC0_SPI0_SS0_PIN, GPIO_MODE_RMP);

    GPIO_Pin_RMP_Config(PE0_SPI0_SCK_AF);
    GPIO_Pin_RMP_Config(PH1_SPI0_SDI_AF);
    GPIO_Pin_RMP_Config(PA2_SPI0_SDO_AF);
    GPIO_Pin_RMP_Config(PC0_SPI0_SS0_AF);
}

/**
 *  @brief: Initialize the SPI interrupt
 *  @param[in]  SPIx:A pointer to the SPI memory structure with a
 *  value of SPI0_SFR/SPI1_SFR/SPI2_SFR/SPI3_SFR
 *  @param[out] None
 *  @retval : None
 */
void Spi_Interrupt_Init()
{
    /* Send empty interrupt enable */
    SPI_TNEIE_INT_Enable(SPI0_SFR, TRUE);
    /* Receive is not empty interrupt enable */
    SPI_RNEIE_INT_Enable(SPI0_SFR, TRUE);
    /* Total interrupt enable */
    INT_Interrupt_Enable(INT_SPI0, TRUE);
}

/**
 *  @brief: Delay time
 *  @param[in]  nms:millisecond
 *  @param[out] None
 *  @retval:None
 */
void delay_ms(volatile uint32_t nms)
{
    volatile uint32_t i, j;
    for (i = 0; i < nms; i++)
    {
        j = 2000;
        while (j--)
            ;
    }
}
