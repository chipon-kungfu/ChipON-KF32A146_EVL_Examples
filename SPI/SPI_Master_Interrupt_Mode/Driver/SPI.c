/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : SPI.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the spi configuration for KF32A146
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                             Include Files
******************************************************************************/
#include "SPI.h"
#include "Board_GpioCfg.h"

/*****************************************************************************
**                         Private Macro Definitions
*****************************************************************************/

/*****************************************************************************
**                         Private Variables Definitions
*****************************************************************************/
uint8_t Tx_Master[SPI_TRANSMISSION_DATA_SIZE] = "KF32A146-KungFu32 SPI Example master";
uint8_t Spi_ReadData[SPI_TRANSMISSION_DATA_SIZE];
/*****************************************************************************
**                             Private Functions
*****************************************************************************/

/*****************************************************************************
**                             Global Functions
*****************************************************************************/
/**
 *  @brief:Initialize the spi module
 *  @param[in]  SPIx:A pointer to the SPI memory structure with a
 *  value of SPI0_SFR/SPI1_SFR/SPI2_SFR/SPI3_SFR
 *  @param[out] None
 *  @retval:None
 */
void Spi_Init(SPI_SFRmap *SPIx)
{
    SPI_InitTypeDef Spi_ConfigPtr;

    /* SPI mode */
    Spi_ConfigPtr.m_Mode = SPI_MODE_MASTER_CLKDIV4;
    /* SPI clock */
    Spi_ConfigPtr.m_Clock = SPI_CLK_SCLK;
    /* Data transfer start control */
    Spi_ConfigPtr.m_FirstBit = SPI_FIRSTBIT_MSB;
    /* Spi idle state */
    Spi_ConfigPtr.m_CKP = SPI_CKP_LOW;
    /* Spi clock phase(Data shift edge) */
    Spi_ConfigPtr.m_CKE = SPI_CKE_1EDGE;
    /* Data width */
    Spi_ConfigPtr.m_DataSize = SPI_DATASIZE_8BITS;
    /* Baud rate :Fck_spi=Fck/2(m_BaudRate+1)*/
    Spi_ConfigPtr.m_BaudRate = 0x00;
    /* Spi reset */
    SPI_Reset(SPIx);
    /* Configure SPI module */
    SPI_Configuration(SPIx, &Spi_ConfigPtr);
}

/**
 *  @brief: Initialize the SPI IO
 *  @param[in]  None
 *  @param[out] None
 *  @retval : None
 */
void Spi_IO_Init()
{
    /* Configure SPI IO */
    GPIO_Write_Mode_Bits(PE0_SPI0_SCK_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PH1_SPI0_SDI_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PA2_SPI0_SDO_PIN, GPIO_MODE_RMP);
    GPIO_Write_Mode_Bits(PC0_SPI0_SS0_PIN, GPIO_MODE_RMP);

    GPIO_Pin_RMP_Config(PE0_SPI0_SCK_AF);
    GPIO_Pin_RMP_Config(PH1_SPI0_SDI_AF);
    GPIO_Pin_RMP_Config(PA2_SPI0_SDO_AF);
    GPIO_Pin_RMP_Config(PC0_SPI0_SS0_AF);
}

/**
 *  @brief: Initialize the SPI interrupt
 *  @param[in]  SPIx:A pointer to the SPI memory structure with a
 *  value of SPI0_SFR/SPI1_SFR/SPI2_SFR/SPI3_SFR
 *  @param[out] None
 *  @retval : None
 */
void Spi_Interrupt_Init()
{
    /* Send empty interrupt enable */
    SPI_TNEIE_INT_Enable(SPI0_SFR, TRUE);
    /* Total interrupt enable */
    INT_Interrupt_Enable(INT_SPI0, TRUE);
}

/**
 * @brief:
 *
 * @param[in] Sourceclock
 * @param[in] Mode
 * @param[in] OutClock
 * @retval uint32_t
 */
uint32_t Spi_SetBaudRate(SPI_SFRmap *SPIx, uint32_t Sourceclock, uint32_t Mode, uint32_t OutClock)
{

    uint32_t kvSpiSourceClock = Sourceclock;
    uint32_t kvRetBaudRate    = 0u;

    if (Mode == SPI_MODE_MASTER_CLKDIV4)
    {
        kvSpiSourceClock = kvSpiSourceClock / 4;
    }
    else if (Mode == SPI_MODE_MASTER_CLKDIV16)
    {
        kvSpiSourceClock = kvSpiSourceClock / 16;
    }
    else if (Mode == SPI_MODE_MASTER_CLKDIV64)
    {
        kvSpiSourceClock = kvSpiSourceClock / 64;
    }

    kvRetBaudRate = ((kvSpiSourceClock / 2) / OutClock) - 1;

    asm("nop");
    asm("nop");
    asm("nop");
    asm("nop");
    asm("nop");

    SPI_BaudRate_Config(SPIx, kvRetBaudRate);
}
