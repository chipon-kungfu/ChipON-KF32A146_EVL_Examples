/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : SPI.h
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file contains the SPIx configuration for KF32A146
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/
#ifndef _SPI_H_
#define _SPI_H_
/******************************************************************************
**                             Include Files                                **
******************************************************************************/
#include "system_init.h"

/*****************************************************************************
**                         Private Macro Definitions                        **
*****************************************************************************/
#define SPI_TRANSMISSION_DATA_SIZE (36U)
/*****************************************************************************
**                         Private Variables Definitions                    **
*****************************************************************************/
extern uint8_t Tx_Master[SPI_TRANSMISSION_DATA_SIZE];
extern uint8_t Tx_slave[SPI_TRANSMISSION_DATA_SIZE];
extern uint8_t Spi_ReadData[SPI_TRANSMISSION_DATA_SIZE];
/*****************************************************************************
**                             Private Functions                            **
*****************************************************************************/

/*****************************************************************************
**                             Global Functions                            **
*****************************************************************************/

/******************************************************************************
 *                      Functional defineition                                 *
 ******************************************************************************/
void     Spi_Init(SPI_SFRmap *SPIx);
void     Spi_IO_Init();
void     Spi_Interrupt_Init();
uint32_t Spi_SetBaudRate(SPI_SFRmap *SPIx, uint32_t Sourceclock, uint32_t Mode, uint32_t OutClock);

#endif
