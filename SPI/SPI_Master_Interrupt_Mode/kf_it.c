/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : kf_it.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : Main Interrupt Service Routines
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                          Include Files
******************************************************************************/
#include "system_init.h"
#include "Board_GpioCfg.h"
#include "kflog.h"
#include "SPI.h"
#include <stdio.h>
/*******************************************************************************
**                   KF32A146 Processor Exceptions Handlers
*******************************************************************************/
extern uint8_t Spi_Length;

/**
 * @brief:SPI2 Interrupt Course
 * @param[in]  None
 * @param[out] None
 * @retval:None
 */
void __attribute__((interrupt)) _SPI0_exception(void)
{
    /* Waiting for the sending buffer to be empty */
    if (SPI_Get_Transmit_Buf_Flag(SPI0_SFR) == RESET)
    {
        /* Polling to send string  */
        if (Spi_Length < SPI_TRANSMISSION_DATA_SIZE)
        {
            /* Send data */
            SPI_I2S_SendData8(SPI0_SFR, Tx_Master[Spi_Length]);
            /* Waiting to accept that the buffer is not empty */
            while (!SPI_Get_Receive_Buf_Flag(SPI0_SFR))
                ;

            /* Read the received data */
            Spi_ReadData[Spi_Length] = SPI_I2S_ReceiveData(SPI0_SFR);
            Spi_Length++;
            /* If it is full, use the serial port to print out the received data
             */
            if (Spi_Length == SPI_TRANSMISSION_DATA_SIZE)
            {
                /* Disable the send buffer is empty interrupt */
                SPI_TNEIE_INT_Enable(SPI0_SFR, FALSE);
                /* Waiting for spi to send no data */
                while (SPI_Get_BUSY_Flag(SPI0_SFR) == SET)
                    ;
                Spi_Length = 0;
                kf_printf("%s\r\n", Spi_ReadData);
                /* LED1 flashing */
                GPIO_Toggle_Output_Data_Config(LED1_PORT, LED1_PIN);
                /* Enable the send buffer is empty interrupt */
                SPI_TNEIE_INT_Enable(SPI0_SFR, TRUE);
            }
        }
    }
}
