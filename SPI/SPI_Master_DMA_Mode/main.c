/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  
 ******************************************************************************
 *  @File Name        : main.c
 *  @Author           : ChipON AE/FAE Group                                   
 *  @Date             : 2024-12-31
 *  @Chip Version     : A02                                                   
 *  @HW Version       : KF32A146-MINI-EVB_V1.0
 *  @Example Version  : V2.3.0.241231_release
 *  @Description      : This file provides a routine for SPI master DMA mode
 *****************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 
 *  All rights reserved.                                                      
 *                                                                            
 *  This software is copyright protected and proprietary to                    
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 
 ******************************************************************************/

/******************************************************************************
**                              Include Files
******************************************************************************/
#include "main.h"
#include "Board_GpioCfg.h"
#include "kflog.h"
/*******************************************************************************
**                          Private Macro Definitions
*******************************************************************************/
uint8_t Spi_Finish_Flag = 0;
/*******************************************************************************
**                                 Global Functions
*******************************************************************************/
/**
 *  @brief: Delay time
 *  @param[in]  nms:millisecond
 *  @param[out] None
 *  @retval:None
 */
void delay_ms(volatile uint32_t nms)
{
    volatile uint32_t i, j;
    for (i = 0; i < nms; i++)
    {
        j = 2000;
        while (j--)
            ;
    }
}

int main()
{
    /* System clock configuration */
    SystemInit(72U);

    /*Initialize the print function*/
    kfLog_Init();

    /* Led1 control */
    GPIOInit_Output_Config(LED1_PORT, LED1_PIN);

    /* Configure IO */
    Spi_IO_Init();

    /* SPI initialization */
    Spi_Init(SPI0_SFR);

    /* set BaudRate*/
    Spi_SetBaudRate(SPI0_SFR, 72 * 1000000, SPI_MODE_MASTER_CLKDIV4, 200 * 1000); /* spi clk 200k */

    /* SPI DMA initialization */
    Spi_DMA_Init(SPI0_SFR);

    delay_ms(1000);

    /* Configure Interrupt */
    INT_Interrupt_Enable(INT_DMA0, TRUE);
    INT_All_Enable(TRUE);

    /* Enable SPI module */
    SPI_Cmd(SPI0_SFR, TRUE);

    DMA_Oneshot_Enable(DMA0_SFR, SPI0_TX_DMA_CHANNAL, TRUE);

    while (1)
    {
        delay_ms(1000);
        // SPI_I2S_SendData8(SPI0_SFR, 0x4b);
    }
}

/**
 *  @brief:Reports the name of the source file and the source line number
 *           where the assert_param error has occurred.
 *  @param[in]  File: pointer to the source file name
 *  @param[in]  Line: assert_param error line source number
 *  @param[out] None
 *  @retval:None
 */
void check_failed(uint8_t *File, uint32_t Line)
{
    /* User can add his own implementation to report the file name and line
       number, ex: printf("Wrong parameters value: file %s on line %d\r\n",
       file, line) */

    /* Infinite loop */
    while (1)
    {
        ;
    }
};
